#include "BSS.h"
#include "Logger.h"
#include <GameDefinitions.h>

sf::Uint32 CBSS::m_nClientIdCounter = 0;

CBSS::CBSS(const std::string& rsAddress, unsigned short nPort)
	: m_bStop(false)
	, m_sAddress(rsAddress)
	, m_nPort(nPort)
{
	m_Listener.setBlocking(false);
	if (m_Listener.listen(nPort, rsAddress) != sf::Socket::Done)
	{
		throw std::exception("Failed to call m_Listener.listen().");
	}
	m_pClientToAccept = new Client(++m_nClientIdCounter);
}

CBSS::~CBSS()
{
	Stop();
}

void CBSS::Run()
{
	log_info("Server running. Address=\"%s\", Port=%u", m_sAddress.c_str(), (DWORD)m_nPort);

	while (!m_bStop)
	{
		bool bSomethingHappened = false;

		sf::TcpListener::Status status = m_Listener.accept(m_pClientToAccept->Socket);
		if (status != sf::Socket::Done)
		{
			if (status == sf::TcpSocket::Error)
			{
				log_error("Failed to accept new connection.");
				Stop();
				return;
			}
		}
		else
		{
			bSomethingHappened = true;
			m_pClientToAccept->Socket.setBlocking(false);
			m_mapClients[m_pClientToAccept->nId].reset(m_pClientToAccept);
			log_info("Client %u connected.", m_pClientToAccept->nId);
			m_pClientToAccept = new Client(++m_nClientIdCounter);
		}

		std::map<sf::Uint32, std::unique_ptr<Client> >::iterator itClient = m_mapClients.begin();
		while (itClient != m_mapClients.end())
		{
			sf::TcpSocket::Status status = itClient->second->Socket.receive(itClient->second->InPacket);
			if (status != sf::TcpSocket::Done)
			{
				if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
				{
					if (status == sf::TcpSocket::Error)
						log_error("Client %u disconnected with error.", itClient->second->nId);
					else if (itClient->second->bRejected)
						log_debug("Client %u disconnected.", itClient->second->nId);
					else
						log_warning("Client %u disconnected.", itClient->second->nId);

					bSomethingHappened = true;
					itClient->second->Socket.disconnect();
					itClient = m_mapClients.erase(itClient);
					continue;
				}
			}
			else
			{
				bSomethingHappened = true;
				if (!ProcessProtocol(*itClient->second))
				{
					itClient->second->Socket.disconnect();
					itClient = m_mapClients.erase(itClient);
					continue;
				}
				else
				{
					itClient->second->InPacket.clear();
				}
			}

			if (!itClient->second->lstOutPackets.empty())
			{
				sf::TcpSocket::Status status = itClient->second->Socket.send(*itClient->second->lstOutPackets.front());
				if (status != sf::TcpSocket::Done)
				{
					if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
					{
						if (status == sf::TcpSocket::Error)
							log_error("Client %u disconnected with error.", itClient->second->nId);
						else if (itClient->second->bRejected)
							log_debug("Client %u disconnected.", itClient->second->nId);
						else
							log_warning("Client %u disconnected.", itClient->second->nId);

						bSomethingHappened = true;
						itClient->second->Socket.disconnect();
						itClient = m_mapClients.erase(itClient);
						continue;
					}
				}
				else
				{
					bSomethingHappened = true;
					itClient->second->lstOutPackets.pop_front();
				}
			}

			++itClient;
		}

		if (!bSomethingHappened)
			Sleep(1); // To avoid CPU load
	}

	m_cvWaitStopping.notify_all();
	log_info("Server stopped.");
}

void CBSS::Stop()
{
	std::unique_lock<std::mutex> lock(m_mtxWaitStopping);

	if (!m_bStop)
	{
		m_bStop = true;
		m_cvWaitStopping.wait(lock);
	}

	m_Listener.close();
	for (auto& client : m_mapClients)
		client.second->Socket.disconnect();
	m_mapClients.clear();
	delete m_pClientToAccept;
	m_pClientToAccept = NULL;
}

bool CBSS::ProcessProtocol(Client& rClient)
{
	if (rClient.bRejected)
		return false; // Disconnect client

	if (!rClient.bHandshake)
	{
		sf::Uint32 nData = 0;
		rClient.InPacket >> nData;
		if(nData != eBSSPacketType_Handshake)
		{
			log_error("Unexpected packet type. ClientId=%u", rClient.nId);
			return false;
		}
		rClient.InPacket >> nData;
		if (nData != BSS_CURRENT_PROTOCOL_VERSION_BCD)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)eBSSPacketType_Handshake << (sf::Uint32)eResult_Fail;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			rClient.bHandshake = true;
			rClient.bRejected = true;
			log_error("Incompatible protocol version on client. ClientId=%u", rClient.nId);
		}
		else
		{
			std::wstring sAddress;
			sf::Uint16 nPort;
			sf::Uint32 nMinimumPlayersCount;
			rClient.InPacket >> sAddress >> nPort >> nMinimumPlayersCount;
			bool bFound = false;
			for (auto& client : m_mapClients)
			{
				if ((client.second->nId != rClient.nId) &&
					(client.second->Info.GetAddress() == sAddress) &&
					(client.second->Info.GetPort() == nPort))
				{
					bFound = true;
					break;
				}
			}
			if (bFound)
			{
				sf::Packet* pPacket = new sf::Packet();
				(*pPacket) << (sf::Uint32)eBSSPacketType_Handshake << (sf::Uint32)eResult_AlreadyExists;
				rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
				rClient.bHandshake = true;
				rClient.bRejected = true;
				log_error("Client address and port already exists. ClientId=%u", rClient.nId);
			}
			else
			{
				rClient.Info.SetPlayerId(rClient.nId);
				rClient.Info.SetAddress(sAddress);
				rClient.Info.SetPort(nPort);
				rClient.dwMinimumPlayersCount = nMinimumPlayersCount;

				sf::Packet* pPacket = new sf::Packet();
				(*pPacket) << (sf::Uint32)eBSSPacketType_Handshake << (sf::Uint32)eResult_Success << (sf::Uint32)rClient.Info.GetPlayerId();
				rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
				rClient.bHandshake = true;

				CheckPlayersCount();
			}
		}
		return true;
	}

	//sf::Uint32 nPacketType = 0;
	//rClient.InPacket >> nPacketType;
	//switch (nPacketType)
	//{
	////case ePacketType_Some:
	////{
	////}
	////break;

	//default:
	//{
	//	log_error("Unknown packet type");
	//	return false;
	//}
	//break;
	//}

	return true;
}

void CBSS::CheckPlayersCount()
{
	std::list<DWORD> lstConnectionIds;
	for (DWORD i = PLAYERS_COUNT_MAX; i >= PLAYERS_COUNT_MIN; --i)
	{
		for (auto& client : m_mapClients)
		{
			if (client.second->bRejected)
				continue;
			if (i < client.second->dwMinimumPlayersCount)
				continue;
			lstConnectionIds.push_back(client.second->nId);
			if (lstConnectionIds.size() == PLAYERS_COUNT_MAX)
				break;
		}
		if (lstConnectionIds.size() >= i)
			break;
		else
			lstConnectionIds.clear();
	}

	for (auto& id1 : lstConnectionIds)
	{
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		(*pPacket) << (sf::Uint32)eBSSPacketType_StartGame << (sf::Uint32)(lstConnectionIds.size() - 1);
		for (auto& id2 : lstConnectionIds)
		{
			if (id2 == id1)
				continue;
			if (!m_mapClients.at(id2)->Info.Serialize(*pPacket))
			{
				log_error("Failed to serialize client info");
			}
		}
		m_mapClients.at(id1)->lstOutPackets.push_back(std::move(pPacket));
		m_mapClients.at(id1)->bRejected = true;
	}

	if (lstConnectionIds.size())
	{
		std::string sIds = "{ ";
		for (auto& id : lstConnectionIds)
		{
			if (sIds.size() > 2)
				sIds += ", ";
			char szBuff[10] = {};
			_snprintf_s(szBuff, _TRUNCATE, "%u", id);
			sIds += szBuff;
		}
		sIds += " }";
		log_info("Game started. PlayersCount=%u %s", (DWORD)lstConnectionIds.size(), sIds.c_str());
	}
}
