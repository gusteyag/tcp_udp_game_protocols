#pragma once
#include <Windows.h>
#include <cstdarg>
#include <cstdio>

inline void __log(const char* message, ...)
{
	va_list args;
	va_start(args, message);
	vprintf(message, args);
	va_end(args);
}

#define log_error(message, ...) __log("ERROR: " __FUNCTION__ ": " message "\r\n", __VA_ARGS__)
#define log_warning(message, ...) __log("WARNING: " __FUNCTION__ ": " message "\r\n", __VA_ARGS__)
#define log_info(message, ...) __log("INFO: " __FUNCTION__ ": " message "\r\n", __VA_ARGS__)
#define log_debug(message, ...) __log("DEBUG: " __FUNCTION__ ": " message "\r\n", __VA_ARGS__)
