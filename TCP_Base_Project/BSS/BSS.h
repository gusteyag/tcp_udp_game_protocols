#pragma once
#include <windows.h>
#include <memory>
#include <list>
#include <condition_variable>
#include <SFML\Network.hpp>
#include <Game.h>
#include "BSSProtocol.h"

class CBSS
{
public:
	CBSS(const std::string& rsAddress, unsigned short nPort);
	virtual ~CBSS();

	void Run();
	void Stop();

private:
	bool m_bStop;
	std::condition_variable m_cvWaitStopping;
	std::mutex m_mtxWaitStopping;
	static sf::Uint32 m_nClientIdCounter;
	sf::TcpListener m_Listener;
	std::string m_sAddress;
	unsigned short m_nPort;

	struct Client
	{
		sf::Uint32 nId;
		sf::TcpSocket Socket;
		sf::Packet InPacket;
		std::list<std::unique_ptr<sf::Packet> > lstOutPackets;
		bool bHandshake;
		bool bRejected;
		
		DWORD dwMinimumPlayersCount;
		CP2PClientInfo Info;

		Client(sf::Uint32 nId)
			: nId(nId)
			, bHandshake(false)
			, bRejected(false)
			, dwMinimumPlayersCount(0)
		{}
	};

	Client* m_pClientToAccept;
	std::map<sf::Uint32, std::unique_ptr<Client> > m_mapClients;

private:
	bool ProcessProtocol(Client& rClient);
	void CheckPlayersCount();
};
