#pragma once
#include <Windows.h>
#include <string>
#include <SFML\Network.hpp>


#define BSS_CURRENT_PROTOCOL_VERSION_BCD			((sf::Uint32)0x00010000) // 1.0

class CP2PClientInfo
{
public:
	CP2PClientInfo()
		: m_dwPlayerId(0)
		, m_nPort(0)
	{}
	virtual ~CP2PClientInfo() {}

	BOOL Serialize(__out sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint32)m_dwPlayerId;
		rPacket << m_sAddress;
		rPacket << (sf::Uint16)m_nPort;
		return TRUE;
	}

	BOOL Deserialize(__in sf::Packet& rPacket)
	{
		sf::Uint32 u32Data = 0;
		rPacket >> u32Data; m_dwPlayerId = (USHORT)u32Data;
		rPacket >> m_sAddress;
		sf::Uint16 u16Data = 0;
		rPacket >> u16Data; m_nPort = (USHORT)u16Data;
		return TRUE;
	}

	void SetPlayerId(DWORD dwPlayerId) { m_dwPlayerId = dwPlayerId; }
	DWORD GetPlayerId() const { return m_dwPlayerId; }

	void SetAddress(const std::wstring& rsAddress) { m_sAddress = rsAddress; }
	const std::wstring& GetAddress() const { return m_sAddress; }

	void SetPort(USHORT nPort) { m_nPort = nPort; }
	USHORT GetPort() const { return m_nPort; }

private:
	DWORD m_dwPlayerId;
	std::wstring m_sAddress;
	USHORT m_nPort;
};


enum eBSSPacketType
{
	eBSSPacketType_Handshake = 1,
	// Request from client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- BCD protocol version
	// 4 bytes	- std::wstring	- Address
	// 2 bytes	- sf::Uint16	- Port
	// 4 bytes	- sf::Uint32	- MinimumPlayersCount
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult (eResult_Succes, eResult_AlreadyExists or eResult_Fail)
	//   if eResult_Success then server sends:
	// 4 bytes	- sf::Uint32	- PlayerId

	eBSSPacketType_StartGame,
	// Sends from server to client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- client list size
	// ???		------------	- client list data
};
