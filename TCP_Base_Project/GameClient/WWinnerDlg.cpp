#include "WWinnerDlg.h"
#include "WText.h"
#include "WButton.h"


CWWinnerDlg::CWWinnerDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pTextLarge(new CWText(rScene))
	, m_pTextSmall(new CWText(rScene))
	, m_pButtonQuit(new CWButton(rScene))
	, m_eMurderCharacter(eCharacters_None)
	, m_eMurderWeapon(eWeapons_None)
	, m_eMurderRoom(eRooms_None)
	, m_bNoWinner(false)
{
	AddChild(m_pTextLarge);
	AddChild(m_pTextSmall);
	AddChild(m_pButtonQuit);

	SetVisible(false);
	SetHeaderHeight(0);
	SetFillColor(sf::Color(0, 0, 0, 200));
	//SetBorderColor(sf::Color::Yellow);

	m_pTextLarge->SetTextHAlignment(eHAlignment_Center);
	m_pTextLarge->SetTextVAlignment(eVAlignment_Center);
	m_pTextSmall->SetTextHAlignment(eHAlignment_Center);
	m_pTextSmall->SetTextVAlignment(eVAlignment_Center);

	m_pTextLarge->SetFontSize(40);
	m_pTextLarge->SetTextColor(sf::Color::Red);
	m_pTextSmall->SetFontSize(20);
	m_pTextSmall->SetTextColor(sf::Color(200, 100, 100));

	m_pTextLarge->SetPadding(30);
	m_pTextLarge->SetPadding(20);

	m_pButtonQuit->SetText(L"Quit");
	m_pButtonQuit->SetOnClickedHandler([this]() {
		SetVisible(false);
		if (m_OnQuitHandler)
			m_OnQuitHandler();
	});
}

CWWinnerDlg::~CWWinnerDlg()
{
}

void CWWinnerDlg::NormalizeSize()
{
	float fWidth = 750;
	float fHeight = 350;
	float fTextLargeHeight = 150;
	float fButtonsWidth = 100;
	float fButtonsHeight = 30;

	SetSize(sf::Vector2f(fWidth, fHeight));
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*this),
		sf::FloatRect(0, 0, GetScene().GetRenderWindow()->getDefaultView().getSize().x, GetScene().GetRenderWindow()->getDefaultView().getSize().y));

	m_pTextLarge->SetFont(GetFont());
	m_pTextSmall->SetFont(GetFont());
	m_pButtonQuit->SetFont(GetFont());

	if (m_sWinner.empty())
	{
		if (m_bNoWinner)
		{
			m_pTextLarge->SetText(L"GAME IS OVER!\nNo Winner.");
		}
		else
		{
			m_pTextLarge->SetText(L"Congratulations!\nYou are WINNER.");
		}

		m_pTextLarge->SetSize(sf::Vector2f(GetWorkingSize().x, GetWorkingSize().y - GetPadding() - fButtonsHeight));
		m_pTextLarge->SetPosition(sf::Vector2f(0, 0));

		m_pButtonQuit->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
		m_pButtonQuit->SetPosition(sf::Vector2f(
			GetWorkingSize().x / 2 - fButtonsWidth / 2,
			m_pTextLarge->GetSize().y + GetPadding()
		));

		m_pTextSmall->SetVisible(false);
	}
	else
	{
		m_pTextLarge->SetText(L"GAME IS OVER!");

		m_pTextLarge->SetSize(sf::Vector2f(GetWorkingSize().x, fTextLargeHeight));
		m_pTextLarge->SetPosition(sf::Vector2f(0, 0));

		wchar_t szBuff[256] = {};
		_snwprintf_s(szBuff, _TRUNCATE, L"Winner is \"%s\".\nCorrect answer:\n\"%s killed with the %s in the %s room\"",
			m_sWinner.c_str(), enum_desc(m_eMurderCharacter), enum_desc(m_eMurderWeapon), enum_desc(m_eMurderRoom));
		m_pTextSmall->SetText(szBuff);

		m_pTextSmall->SetVisible(true);
		m_pTextSmall->SetSize(sf::Vector2f(GetWorkingSize().x, GetWorkingSize().y - fTextLargeHeight - GetPadding() - fButtonsHeight));
		m_pTextSmall->SetPosition(sf::Vector2f(0, fTextLargeHeight));

		m_pButtonQuit->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
		m_pButtonQuit->SetPosition(sf::Vector2f(
			GetWorkingSize().x / 2 - fButtonsWidth / 2,
			fTextLargeHeight + m_pTextSmall->GetSize().y + GetPadding()
		));
	}
}

void CWWinnerDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
