#pragma once
#include <Windows.h>
#include <map>
#include <functional>
#include <memory>
#include <SFML\Network.hpp>
#include "../BSS/BSSProtocol.h"
#include "P2PProtocol.h"
#include "Game.h"
#include "INetClient.h"


class CP2PClient : public INetClient
{
public:
	CP2PClient(const std::wstring& rsP2PLocalAddress, USHORT nP2PLocalPort, const std::wstring& rsBSSAddress, USHORT nBSSPort, DWORD dwMinimumPlayersCount);
	virtual ~CP2PClient();

	virtual eNetworkType GetNetworkType() { return eNetworkType_P2P; }

	virtual bool Connect(
		const net_message_cb_t& rOnDisconnectedCB,
		const net_game_info_list_cb_t& rOnGameInfoListChangedCB,
		const net_game_state_cb_t& rOnGameStateChangedCB,
		const net_chat_cb_t& rOnChatChangedCB
		);
	virtual void Disconnect();

	virtual bool Process();

	virtual void Request_CreateNewGameAndJoin(
		const net_join_answer_cb_t& rAnswerCB,
		const std::wstring& rsGameName,
		DWORD dwSpecifiedPlayersCount,
		const std::wstring& rsPlayerName,
		eCharacters eCharacter
	)
	{	rAnswerCB(eResult_NotImplemented, 0, 0); }
	virtual void Request_Join(const net_join_answer_cb_t& rAnswerCB, DWORD dwGameId, const std::wstring& rsPlayerName, eCharacters eCharacter);
	virtual void Request_LeftGame(const net_answer_cb_t& rAnswerCB);
	
	virtual void Request_RollDices(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ClueCard(const net_answer_cb_t& rAnswerCB, ULONG64 nAssumedCard);
	virtual void Request_ClueCardAnswer(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ReceiveClueCardAnswer(const net_answer_cb_t& rAnswerCB);
	virtual void Request_Move(const net_answer_cb_t& rAnswerCB, const std::list<POINT>& rlstTrack);
	virtual void Request_MakeDeduction(const net_answer_cb_t& rAnswerCB, eWeapons eWeapon, eCharacters eCharacter);
	virtual void Request_AnswerToDeduction(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ReceiveDeductionAnswers(const net_answer_cb_t& rAnswerCB);
	virtual void Request_MakeAccusation(const net_answer_cb_t& rAnswerCB, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter);
	virtual void Request_SkipAccusation(const net_answer_cb_t& rAnswerCB);

	virtual void Request_ChatMessage(const std::wstring& rsMessage);

private:
	std::wstring m_sP2PLocalAddress;
	USHORT m_nP2PLocalPort;
	std::wstring m_sBSSAddress;
	USHORT m_nBSSPort;
	DWORD m_dwMinimumPlayersCount;

	net_message_cb_t m_OnDisconnectedCB;
	net_game_info_list_cb_t m_OnGameInfoListChangedCB;
	net_game_state_cb_t m_OnGameStateChangedCB;
	net_chat_cb_t m_OnChatChangedCB;

	sf::TcpSocket m_BSSClientSocket;
	sf::Clock m_BSSClientConnectionClock;
	bool m_bBSSClientOnceConnected;
	bool m_bBSSClientDisconnected;
	bool m_bBSSClientHandshakeSended;
	bool m_bBSSClientHandshake;
	sf::Packet m_BSSClientInPacket;
	std::list<std::unique_ptr<sf::Packet> > m_lstBSSClientOutPackets;


	enum eP2PConnectionType
	{
		eP2PConnectionType_None, // placeholder
		eP2PConnectionType_In,
		eP2PConnectionType_Out,
	};
	struct P2PConnection
	{
		sf::Uint32 nPlayerId;
		bool bRejected;
		bool bReadyToStart;
		eP2PConnectionType eType;
		std::unique_ptr<sf::TcpSocket> pSocket;
		sf::Packet InPacket;
		std::list<std::unique_ptr<sf::Packet> > lstOutPackets;

		P2PConnection()
			: nPlayerId(0)
			, bRejected(false)
			, bReadyToStart(false)
			, eType(eP2PConnectionType_None)
		{}
	};

	std::unique_ptr<sf::TcpListener> m_pP2PListener;
	std::unique_ptr<sf::TcpSocket> m_pP2PSocketToAccept;
	DWORD m_dwP2PPlayerId;

	std::map<DWORD, CP2PClientInfo> m_mapP2PClientInfo;
	std::list<std::unique_ptr<P2PConnection> > m_lstP2PHandshake;
	std::map<DWORD, std::unique_ptr<P2PConnection> > m_mapP2PConnections;

	net_join_answer_cb_t m_Join_AnswerCB;
	bool m_bGameState_NeedToSendOneMoreTime;
	bool m_bGameState_InProgress;
	bool m_bGameState_Processed;
	std::set<DWORD> m_setGameState_ConfirmedPlayersId;
	net_answer_cb_t m_GameState_AnswerCB;

	std::set<DWORD> m_setLeftGamePlayerIds;

	class CGameWrapper : public CGame
	{
		CP2PClient& m_rP2PClient;
		virtual void OnStateChanged(const CGameState& rState) {}
		virtual void OnInfoChanged(const CGameInfo& rState) {}
		virtual void OnChatChanged(const CChat& rChat, const CGameState& rState) { if (m_rP2PClient.m_OnChatChangedCB) m_rP2PClient.m_OnChatChangedCB(rChat); }
		virtual void OnGameRemoved(const CGameState& rState) {}

	public:
		CGameWrapper(DWORD dwId, const std::wstring& rsName, DWORD dwSpecifiedPlayersCount, CP2PClient& rP2PClient)
			: CGame(dwId, rsName, dwSpecifiedPlayersCount)
			, m_rP2PClient(rP2PClient)
		{}
		virtual ~CGameWrapper() {}
	};
	std::unique_ptr<CGameWrapper> m_pGame;

private:
	bool ConnectBSSClient();
	bool _ConnectBSSClient();
	void DisconnectBSSClient();
	void CheckP2PConnections();
	bool ProcessBSSClientProtocol();
	bool ProcessP2PConnectionProtocol(P2PConnection& rConnection);

	bool SendGameStateToOthers(const net_answer_cb_t& rAnswerCB);
	bool ReceiveGameState(DWORD dwPlayerId, sf::Packet& rPacket);
	bool ReceiveGameStateConfirmation(DWORD dwPlayerId);
	bool CheckConfirmation();

	bool OnPeerDisconnected(DWORD dwPlayerIds);
};
