#pragma once
#include <Windows.h>
#include <memory>
#include "INetClient.h"
#include "Scene.h"


class CWMessageBox;
class CWConnectionDlg;
class CWGameListDlg;
class CWJoinToGameDlg;
class CWCreateNewGameDlg;
class CWLobby;
class CWGame;

class CGameClient
{
public:
	CGameClient();
	virtual ~CGameClient();

	void Run();

	void ShowConnectionDlg();
	void OnDisconnected(const std::wstring& rsError);
	void OnGameInfoListChanged(const std::map<DWORD, CGameInfo>& rmapGameInfo);
	void OnGameStateChanged(const CGameState& rGameState);
	void OnChatChanged(const CChat& rChat);

	void ShowGameListDlg();
	void ShowCreateNewGameDlg();
	void ShowJoinToGameDlg();
	void ShowLobbyDlg();
	void ShowGameView();

private:
	std::unique_ptr<INetClient> m_pNetClient;
	CScene m_Scene;
	sf::Font m_Font;
	CWMessageBox* m_pMessageBox;
	CWConnectionDlg* m_pConnectionDlg;
	CWGameListDlg* m_pGameListDlg;
	CWCreateNewGameDlg* m_pCreateNewGameDlg;
	CWJoinToGameDlg* m_pJoinToGameDlg;
	CWLobby* m_pLobby;
	CWGame* m_pWGame;
};
