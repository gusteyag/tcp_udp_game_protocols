#pragma once
#include "WContainer.h"
#include <functional>
#include <map>
#include "GameDefinitions.h"


class CWText;
class CWListBox;
class CWButton;
class CWEditBox;

class CWCreateNewGameDlg :
	public CWContainer
{
public:
	CWCreateNewGameDlg(CScene& rScene);
	virtual ~CWCreateNewGameDlg();

	void SetOnCreateNewGameHandler(
		const std::function<void(
			const std::wstring& rsGameName,
			sf::Uint32 uiSpecifiedPlayersCount,
			const std::wstring& rsPlayerName,
			eCharacters eCharacter
			)>& rHandler
	) { m_OnCreateNewGameHandler = rHandler; }
	void SetOnBackHandler(const std::function<void(void)>& rHandler) { m_OnBackHandler = rHandler; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
		CWText* m_pTextEditGameName;
		CWEditBox* m_pEditGameName;
		CWText* m_pTextEditPlayersCount;
		CWEditBox* m_pEditPlayersCount;
		CWText* m_pTextEditPlayerName;
		CWEditBox* m_pEditPlayerName;
		CWText* m_pTextListCharacters;
		CWListBox* m_pListCharacters;
		CWButton* m_pButtonCreate;
		CWButton* m_pButtonBack;
		std::function<void(
			const std::wstring& rsGameName,
			sf::Uint32 uiSpecifiedPlayersCount,
			const std::wstring& rsPlayerName,
			eCharacters eCharacter
			)> m_OnCreateNewGameHandler;
		std::function<void(void)> m_OnBackHandler;
};

