#include "GameClient.h"
#include "Scene.h"
#include "Widget.h"
#include "WButton.h"
#include "WScrollBar.h"
#include "WEditBox.h"
#include "WTextBox.h"
#include "WText.h"
#include "WContainer.h"
#include "WChat.h"
#include "WListBox.h"
#include "WConnectionDlg.h"
#include "Client.h"
#include "P2PClient.h"
#include "WMessageBox.h"
#include "WGameListDlg.h"
#include "WJoinToGameDlg.h"
#include "WCreateNewGameDlg.h"
#include "WLobby.h"
#include "WGame.h"


CGameClient::CGameClient()
	: m_pConnectionDlg(new CWConnectionDlg(m_Scene))
	, m_pMessageBox(new CWMessageBox(m_Scene))
	, m_pGameListDlg(new CWGameListDlg(m_Scene))
	, m_pJoinToGameDlg(new CWJoinToGameDlg(m_Scene))
	, m_pCreateNewGameDlg(new CWCreateNewGameDlg(m_Scene))
	, m_pLobby(new CWLobby(m_Scene))
	, m_pWGame(new CWGame(m_Scene))
{
	m_Scene.GetMainWidget()->AddChild(m_pWGame);
	m_pWGame->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pLobby);
	m_pLobby->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pCreateNewGameDlg);
	m_pCreateNewGameDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pJoinToGameDlg);
	m_pJoinToGameDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pGameListDlg);
	m_pGameListDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pConnectionDlg);
	m_pConnectionDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pMessageBox);
	m_pMessageBox->SetVisible(false);
}

CGameClient::~CGameClient()
{
}

void CGameClient::Run()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Ventanita");
	window.setVerticalSyncEnabled(true);

	m_Font.loadFromFile("courbd.ttf");

	m_pMessageBox->SetFont(m_Font);

	m_Scene.SetRenderWindow(&window);

	ShowConnectionDlg();

	while (window.isOpen())
	{
		while (m_pNetClient.get() && m_pNetClient->Process());

		sf::Event Event;
		while (window.pollEvent(Event))
		{
			if ((Event.type == sf::Event::Closed) || ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape)))
			{
				window.close();
				return;
			}

			m_Scene.OnEvent(Event);
		}

		m_Scene.Draw();

		Sleep(1);
	}
}

void CGameClient::ShowConnectionDlg()
{
	m_pMessageBox->SetVisible(false);
	m_pGameListDlg->UpdateGameList(std::map<DWORD, CGameInfo>());
	m_pGameListDlg->SetVisible(false);
	m_pCreateNewGameDlg->SetVisible(false);
	m_pJoinToGameDlg->UpdatePlayersList(CGameState());
	m_pJoinToGameDlg->SetVisible(false);
	m_pLobby->UpdatePlayersList(CGameState());
	m_pLobby->UpdateChat(CChat());
	m_pLobby->SetVisible(false);
	m_pWGame->SetCharacter(eCharacters_None);
	m_pWGame->SetNetworkClient(NULL);
	m_pWGame->UpdateGameState(CGameState());
	m_pWGame->UpdateChat(CChat());
	m_pWGame->SetVisible(false);

	m_pConnectionDlg->SetFont(m_Font);
	m_pConnectionDlg->SetOnConnectHandler([this](
		eNetworkType eType,
		const std::wstring& rsServerOrBSSAddress,
		sf::Uint16 uiServerOrBSSPort,
		const std::wstring& rsLocalAddress,
		sf::Uint16 uiLocalPort,
		sf::Uint32 uiMinPlayersCount
		)
	{
		m_pConnectionDlg->SetVisible(false);
		if (eType == eNetworkType_ClientServer)
		{
			m_pNetClient.reset(new CClient(rsServerOrBSSAddress, uiServerOrBSSPort));
		}
		else
		{
			m_pNetClient.reset(new CP2PClient(rsLocalAddress, uiLocalPort, rsServerOrBSSAddress, uiServerOrBSSPort, uiMinPlayersCount));
		}

		m_pMessageBox->Show(eMessageType_Info, L"Waiting for Connection...", [this]() {
			m_pNetClient->Disconnect();
			m_pNetClient.reset(NULL);
			ShowConnectionDlg();
		});
		m_pMessageBox->SetButtonText(L"Cancel");

		if (!m_pNetClient->Connect(
			[this](const std::wstring& rsError) { OnDisconnected(rsError); },
			[this](const std::map<DWORD, CGameInfo>& rmapGameInfo) { OnGameInfoListChanged(rmapGameInfo); },
			[this](const CGameState& rGameState) { OnGameStateChanged(rGameState); },
			[this](const CChat& rChat) { OnChatChanged(rChat); }
		))
		{
			OnDisconnected(L"Failed to connect to server. Check parameters.");
		}
	});
	m_pConnectionDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pConnectionDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pConnectionDlg->SetVisible(true);
}

void CGameClient::OnDisconnected(const std::wstring& rsError)
{
	m_pMessageBox->SetVisible(false);
	m_pGameListDlg->UpdateGameList(std::map<DWORD, CGameInfo>());
	m_pGameListDlg->SetVisible(false);
	m_pCreateNewGameDlg->SetVisible(false);
	m_pJoinToGameDlg->UpdatePlayersList(CGameState());
	m_pJoinToGameDlg->SetVisible(false);
	m_pLobby->UpdatePlayersList(CGameState());
	m_pLobby->UpdateChat(CChat());
	m_pLobby->SetVisible(false);
	m_pWGame->SetCharacter(eCharacters_None);
	m_pWGame->SetNetworkClient(NULL);
	m_pWGame->UpdateGameState(CGameState());
	m_pWGame->UpdateChat(CChat());
	m_pWGame->SetVisible(false);

	wchar_t szBuff[256] = {};
	_snwprintf_s(szBuff, _TRUNCATE, L"Connection error:\n\"%s\"\n\nTry connecting again.", rsError.c_str());
	m_pMessageBox->Show(eMessageType_Error, szBuff, [this]() {
		m_pNetClient->Disconnect();
		m_pNetClient.reset(NULL);
		ShowConnectionDlg();
	});
}

void CGameClient::OnGameInfoListChanged(const std::map<DWORD, CGameInfo>& rmapGameInfo)
{
	m_pMessageBox->SetVisible(false);

	m_pGameListDlg->UpdateGameList(rmapGameInfo);
	if(rmapGameInfo.count(m_pJoinToGameDlg->GetGameId()))
		m_pJoinToGameDlg->UpdatePlayersList(rmapGameInfo.at(m_pJoinToGameDlg->GetGameId()));
	
	if (!m_pCreateNewGameDlg->IsVisible() && !m_pJoinToGameDlg->IsVisible() && !m_pLobby->IsVisible() && !m_pWGame->IsVisible() && !m_pGameListDlg->IsVisible())
		ShowGameListDlg();
}

void CGameClient::OnGameStateChanged(const CGameState& rGameState)
{
	m_pMessageBox->SetVisible(false);

	if (m_pNetClient->GetNetworkType() == eNetworkType_P2P)
	{
		if (!m_pJoinToGameDlg->GetGameId())
			ShowJoinToGameDlg();
	}

	m_pJoinToGameDlg->UpdatePlayersList(rGameState);
	m_pLobby->UpdatePlayersList(rGameState);
	m_pWGame->UpdateGameState(rGameState);

	if (rGameState.GetGameState() != eGameStates_Created)
	{
		m_pConnectionDlg->SetVisible(false);
		m_pGameListDlg->SetVisible(false);
		m_pCreateNewGameDlg->SetVisible(false);
		m_pJoinToGameDlg->SetVisible(false);
		m_pLobby->SetVisible(false);

		if (!m_pWGame->IsVisible())
			ShowGameView();
	}
}

void CGameClient::OnChatChanged(const CChat& rChat)
{
	m_pLobby->UpdateChat(rChat);
	m_pWGame->UpdateChat(rChat);
}

void CGameClient::ShowGameListDlg()
{
	m_pGameListDlg->SetFont(m_Font);
	m_pGameListDlg->SetOnGameSelectedHandler([this](const CGameInfo& rGameInfo) {
		m_pGameListDlg->SetVisible(false);
		m_pJoinToGameDlg->UpdatePlayersList(rGameInfo);
		ShowJoinToGameDlg();
	});
	m_pGameListDlg->SetOnCreateNewGameHandler([this]() {
		m_pGameListDlg->SetVisible(false);
		ShowCreateNewGameDlg();
	});
	m_pGameListDlg->SetOnBackHandler([this]() {
		m_pGameListDlg->SetVisible(false);
		m_pNetClient->Disconnect();
		m_pNetClient.reset(NULL);
		ShowConnectionDlg();
	});
	m_pGameListDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pGameListDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pGameListDlg->SetVisible(true);
}

void CGameClient::ShowCreateNewGameDlg()
{
	m_pCreateNewGameDlg->SetFont(m_Font);
	m_pCreateNewGameDlg->SetOnCreateNewGameHandler([this](
		const std::wstring& rsGameName,
		sf::Uint32 uiSpecifiedPlayersCount,
		const std::wstring& rsPlayerName,
		eCharacters eCharacter
		)
	{
		m_pCreateNewGameDlg->SetVisible(false);
		m_pWGame->SetCharacter(eCharacter);
		m_pWGame->SetNetworkClient(m_pNetClient.get());
		m_pNetClient->Request_CreateNewGameAndJoin([this, eCharacter](eResult eRes, DWORD GameId, DWORD PlayerId) {
			if (eRes != eResult_Success)
			{
				m_pWGame->SetCharacter(eCharacters_None);
				m_pWGame->SetNetworkClient(NULL);
				m_pMessageBox->Show(eMessageType_Error, L"Failed to create the game.", [this]() {
					ShowGameListDlg();
				});
			}
			else
			{
				ShowLobbyDlg();
			}
		}, rsGameName, uiSpecifiedPlayersCount, rsPlayerName, eCharacter);
	});
	m_pCreateNewGameDlg->SetOnBackHandler([this]() {
		m_pCreateNewGameDlg->SetVisible(false);
		ShowGameListDlg();
	});
	m_pCreateNewGameDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pCreateNewGameDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pCreateNewGameDlg->SetVisible(true);
}

void CGameClient::ShowJoinToGameDlg()
{
	m_pJoinToGameDlg->SetFont(m_Font);
	m_pJoinToGameDlg->SetOnJoinToGameHandler([this](sf::Uint32 uiGameId, const std::wstring& rsPlayerName, eCharacters eCharacter) {
		m_pJoinToGameDlg->SetVisible(false);
		m_pWGame->SetCharacter(eCharacter);
		m_pWGame->SetNetworkClient(m_pNetClient.get());
		m_pNetClient->Request_Join([this, eCharacter](eResult eRes, DWORD GameId, DWORD PlayerId) {
			if (eRes != eResult_Success)
			{
				m_pWGame->SetCharacter(eCharacters_None);
				m_pWGame->SetNetworkClient(NULL);
				m_pMessageBox->Show(eMessageType_Error, L"Failed to join to the game.", [this]() {
					if (m_pNetClient->GetNetworkType() == eNetworkType_P2P)
					{
						m_pNetClient->Disconnect();
						m_pNetClient.reset(NULL);
						ShowConnectionDlg();
					}
					else
					{
						ShowGameListDlg();
					}
				});
			}
			else
			{
				if (!m_pWGame->IsVisible())
					ShowLobbyDlg();
			}
		}, uiGameId, rsPlayerName, eCharacter);
	});
	m_pJoinToGameDlg->SetOnBackHandler([this]() {
		m_pJoinToGameDlg->SetVisible(false);
		if (m_pNetClient->GetNetworkType() == eNetworkType_P2P)
		{
			m_pNetClient->Disconnect();
			m_pNetClient.reset(NULL);
			ShowConnectionDlg();
		}
		else
		{
			ShowGameListDlg();
		}
	});
	m_pJoinToGameDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pJoinToGameDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pJoinToGameDlg->SetVisible(true);
}

void CGameClient::ShowLobbyDlg()
{
	m_pLobby->SetFont(m_Font);
	m_pLobby->SetOnChatMessageHandler([this](const std::wstring& rsText) {
		if (m_pNetClient)
			m_pNetClient->Request_ChatMessage(rsText);
	});
	m_pLobby->SetOnQuitHandler([this]() {
		m_pLobby->SetVisible(false);
		if (m_pNetClient->GetNetworkType() == eNetworkType_P2P)
		{
			m_pNetClient->Disconnect();
			m_pNetClient.reset(NULL);
			ShowConnectionDlg();
		}
		else
		{
			if (m_pNetClient)
				m_pNetClient->Request_LeftGame([this](eResult eRes) {
				ShowGameListDlg();
			});
		}
	});
	m_pLobby->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pLobby),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pLobby->SetVisible(true);
}

void CGameClient::ShowGameView()
{
	m_pWGame->SetFont(m_Font);
	m_pWGame->SetOnQuitHandler([this]() {
		m_pWGame->SetVisible(false);
		m_pWGame->SetCharacter(eCharacters_None);
		m_pWGame->SetNetworkClient(NULL);
		m_pWGame->UpdateGameState(CGameState());
		m_pWGame->UpdateChat(CChat());
		if (m_pNetClient->GetNetworkType() == eNetworkType_P2P)
		{
			m_pNetClient->Disconnect();
			m_pNetClient.reset(NULL);
			ShowConnectionDlg();
		}
		else
		{
			if (m_pNetClient)
				m_pNetClient->Request_LeftGame([this](eResult eRes) {
				ShowGameListDlg();
			});
		}
	});
	m_pWGame->SetOnChatMessageHandler([this](const std::wstring& rsText) {
		if (m_pNetClient)
			m_pNetClient->Request_ChatMessage(rsText);
	});
	m_pWGame->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pWGame),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pWGame->SetVisible(true);
}
