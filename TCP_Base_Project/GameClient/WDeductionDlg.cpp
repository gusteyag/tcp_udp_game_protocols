#include "WDeductionDlg.h"
#include "WText.h"
#include "WListBox.h"
#include "WButton.h"


CWDeductionDlg::CWDeductionDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pTextDescription(new CWText(rScene))
	, m_pTextDeduction(new CWText(rScene))
	, m_pListCharacters(new CWListBox(rScene))
	, m_pListWeapons(new CWListBox(rScene))
	, m_pListRooms(new CWListBox(rScene))
	, m_pButtonMake(new CWButton(rScene))
	, m_eRoom(eRooms_None)
{
	AddChild(m_pTextDescription);
	AddChild(m_pTextDeduction);
	AddChild(m_pListCharacters);
	AddChild(m_pListWeapons);
	AddChild(m_pListRooms);
	AddChild(m_pButtonMake);

	SetVisible(false);
	SetFillColor(sf::Color::Black);

	m_pTextDescription->SetFontSize(15);
	m_pTextDeduction->SetFontSize(12);

	m_pTextDescription->SetTextHAlignment(eHAlignment_Center);
	m_pTextDescription->SetTextVAlignment(eVAlignment_Center);
	m_pTextDeduction->SetTextHAlignment(eHAlignment_Center);
	m_pTextDeduction->SetTextVAlignment(eVAlignment_Center);

	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Amapola), eCharacters_Amapola));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Rubio), eCharacters_Rubio));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Orquidea), eCharacters_Orquidea));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Prado), eCharacters_Prado));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Celeste), eCharacters_Celeste));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Mora), eCharacters_Mora));

	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Punyal), eWeapons_Punyal));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Cuerda), eWeapons_Cuerda));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Candelabro), eWeapons_Candelabro));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Pistola), eWeapons_Pistola));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Tuberia), eWeapons_Tuberia));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Herramienta), eWeapons_Herramienta));

	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Cocina), eRooms_Cocina));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Comedor), eRooms_Comedor));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Salon), eRooms_Salon));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Estudio), eRooms_Estudio));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Biblioteca), eRooms_Biblioteca));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Billar), eRooms_Billar));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Terraza), eRooms_Terraza));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Baile), eRooms_Baile));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Escaleras), eRooms_Escaleras));

	m_pButtonMake->SetOnClickedHandler([this]() {
		SetVisible(false);
		if (m_OnMakeHandler)
			m_OnMakeHandler(
				(eCharacters)(m_pListCharacters->GetSelectedLine(NULL) ? m_pListCharacters->GetSelectedLine(NULL)->back().ui64UserData : eCharacters_None),
				(eWeapons)(m_pListWeapons->GetSelectedLine(NULL) ? m_pListWeapons->GetSelectedLine(NULL)->back().ui64UserData : eWeapons_None),
				(eRooms)(m_pListRooms->GetSelectedLine(NULL) ? m_pListRooms->GetSelectedLine(NULL)->back().ui64UserData : eRooms_None)
			);
	});
	m_pButtonMake->SetText(L"Make");
}

CWDeductionDlg::~CWDeductionDlg()
{
}

void CWDeductionDlg::Show(const std::function<void(eCharacters eCharacter, eWeapons eWeapon, eRooms eRoom)>& rHandler, eRooms eRoom/* = eRooms_None*/)
{
	m_OnMakeHandler = rHandler;
	m_eRoom = eRoom;
	if (m_eRoom != eRooms_None)
	{
		int index = 0;
		for (auto& line : m_pListRooms->GetLines())
		{
			if (line.back().ui64UserData == m_eRoom)
			{
				m_pListRooms->SetSelectedLine(index);
				m_pListRooms->SetEnabled(false);
			}
			++index;
		}
	}
	else
	{
		m_pListRooms->SetSelectedLine(-1);
		m_pListRooms->SetEnabled(true);
	}
	m_pButtonMake->SetEnabled(false);
	m_pListCharacters->SetSelectedLine(-1);
	m_pListWeapons->SetSelectedLine(-1);
	SetVisible(true);
}

void CWDeductionDlg::NormalizeSize()
{
	float fListColumnWidth = 100;
	float fListVSpacing = 50;
	float fTextDescriptionHeight = 50;
	float fTextDeductionHeight = 50;
	float fButtonsWidth = 100;
	float fButtonsHeight = 30;

	m_pTextDescription->SetFont(GetFont());
	m_pTextDeduction->SetFont(GetFont());
	m_pListCharacters->SetFont(GetFont());
	m_pListWeapons->SetFont(GetFont());
	m_pListRooms->SetFont(GetFont());
	m_pButtonMake->SetFont(GetFont());

	std::wstring sDeduction;
	if (m_pListCharacters->GetSelectedLine(NULL) && m_pListWeapons->GetSelectedLine(NULL) && m_pListRooms->GetSelectedLine(NULL))
	{
		wchar_t szBuff[256] = {};
		_snwprintf_s(szBuff, _TRUNCATE, L"\"%s killed with the %s in the %s room\".",
			enum_desc((eCharacters)m_pListCharacters->GetSelectedLine(NULL)->back().ui64UserData),
			enum_desc((eWeapons)m_pListWeapons->GetSelectedLine(NULL)->back().ui64UserData),
			enum_desc((eRooms)m_pListRooms->GetSelectedLine(NULL)->back().ui64UserData)
			);
		sDeduction = szBuff;
		m_pButtonMake->SetEnabled(true);
	}
	else
	{
		m_pButtonMake->SetEnabled(false);
	}

	if (m_eRoom == eRooms_None)
	{
		SetText(L"Make Accusation");
		m_pTextDescription->SetText(L"Select items to make Accusation.");
		m_pTextDeduction->SetText(L"");
		if (!sDeduction.empty())
			m_pTextDeduction->SetText(L"Your Accusation is:\n" + sDeduction);
	}
	else
	{
		SetText(L"Make Deduction");
		m_pTextDescription->SetText(L"Select items to make Deduction.");
		m_pTextDeduction->SetText(L"");
		if (!sDeduction.empty())
			m_pTextDeduction->SetText(L"Your Deduction is:\n" + sDeduction);
	}

	if (m_pListCharacters->GetColumns().empty())
	{
		m_pListCharacters->GetColumns().push_back(ListBoxColumn(L"Characters", fListColumnWidth));
		m_pListCharacters->NormalizeSize();
	}
	if (m_pListWeapons->GetColumns().empty())
	{
		m_pListWeapons->GetColumns().push_back(ListBoxColumn(L"Weapons", fListColumnWidth));
		m_pListWeapons->NormalizeSize();
	}
	if (m_pListRooms->GetColumns().empty())
	{
		m_pListRooms->GetColumns().push_back(ListBoxColumn(L"Rooms", fListColumnWidth));
		m_pListRooms->NormalizeSize();
	}

	SetSize(sf::Vector2f(
		(fListColumnWidth + fListVSpacing) * 3 + fListVSpacing + (GetInBorderWidth() + GetPadding()) * 2,
		GetReservedIndentTop() + fTextDescriptionHeight + m_pListRooms->GetSize().y + fTextDeductionHeight + GetPadding() + fButtonsHeight + (GetInBorderWidth() + GetPadding()) * 2 + GetPadding()
		));
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*this),
		sf::FloatRect(0, 0, GetScene().GetRenderWindow()->getDefaultView().getSize().x, GetScene().GetRenderWindow()->getDefaultView().getSize().y));

	m_pTextDescription->SetSize(sf::Vector2f(GetWorkingSize().x, fTextDescriptionHeight));
	m_pTextDescription->SetPosition(sf::Vector2f(0, 0));

	m_pListCharacters->SetPosition(sf::Vector2f(fListVSpacing, fTextDescriptionHeight));
	m_pListWeapons->SetPosition(sf::Vector2f(fListVSpacing + fListColumnWidth + fListVSpacing, fTextDescriptionHeight));
	m_pListRooms->SetPosition(sf::Vector2f(fListVSpacing + fListColumnWidth + fListVSpacing + fListColumnWidth + fListVSpacing, fTextDescriptionHeight));

	m_pTextDeduction->SetSize(sf::Vector2f(GetWorkingSize().x, fTextDeductionHeight));
	m_pTextDeduction->SetPosition(sf::Vector2f(0, fTextDescriptionHeight + m_pListRooms->GetSize().y));

	m_pButtonMake->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonMake->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - fButtonsWidth / 2,
		fTextDescriptionHeight + m_pListRooms->GetSize().y + fTextDeductionHeight + GetPadding()
	));
}

void CWDeductionDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
