#include "WGameLocation.h"
#include "WChip.h"


CWGameLocation::CWGameLocation(CScene& rScene)
	: CWContainer(rScene)
	, m_fTileSize(0)
	, m_clrTile(100, 50, 50)
	, m_clrTileLight(150, 70, 70)
	, m_clrTileBorder(50, 20, 20)
	, m_bBlinking(false)
	, m_clrBorderNormal(GetBorderColor())
	, m_clrBorderBlink(sf::Color::Yellow)
	, m_bMovementDone(false)
{
	SetBorderWidth(-1);
	SetHeaderHeight(0);

	m_v2iLocationSize.x = g_rcLocation.right - g_rcLocation.left;
	m_v2iLocationSize.y = g_rcLocation.bottom - g_rcLocation.top;

	FillPlaces();
}

CWGameLocation::~CWGameLocation()
{
}

void CWGameLocation::FillPlaces()
{
	for (int y = 0; y < m_v2iLocationSize.y; ++y)
	{
		for (int x = 0; x < m_v2iLocationSize.x; ++x)
		{
			eRooms eRoom = PtInRoom(Point(x, y));
			if (eRoom == eRooms_None)
			{
				m_lstPlaces.push_back(std::make_pair(Point(x, y), EmptyPlace));
			}
		}
	}

	for (auto& room : g_RoomLocations.map)
	{
		bool bFirst = true;
		for (auto& rc : room.second)
		{
			sf::IntRect rect(rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top);
			if (bFirst)
			{
				// Reserved for text
				rect.top += 2;
				bFirst = false;
			}
			for (int y = rect.top; y < (rect.top + rect.height); ++y)
			{
				for (int x = rect.left; x < (rect.left + rect.width); ++x)
				{
					m_mapRoomPlaces[room.first].push_back(std::make_pair(Point(x, y), EmptyPlace));
				}
			}
		}
	}
}

bool CWGameLocation::IsEmptyPlace(const Point& rPoint)
{
	for (auto& place : m_lstPlaces)
	{
		if (place.first == rPoint)
			return (place.second == EmptyPlace);
	}
	return false;
}

void CWGameLocation::Move(ULONG64 ullChip, const Point& rPoint)
{
	if (!IsEmptyPlace(rPoint))
		return;

	bool bFound = false;
	for (auto& room : m_mapRoomPlaces)
	{
		for (auto& place : room.second)
		{
			if (place.second == ullChip)
			{
				place.second = EmptyPlace;
				bFound = true;
				break;
			}
		}
		if (bFound)
			break;
	}

	for (auto& place : m_lstPlaces)
	{
		if (place.second == ullChip)
			place.second = EmptyPlace;
		else if ((place.first == rPoint) && (place.second == EmptyPlace))
			place.second = ullChip;
	}

	if (!m_mapChips.count(ullChip))
		m_mapChips[ullChip].reset(new CWChip(GetScene(), ullChip));
}

void CWGameLocation::MoveToRoom(ULONG64 ullChip, eRooms eRoom)
{
	bool bFound = false;
	for (auto& place : m_lstPlaces)
	{
		if (place.second == ullChip)
		{
			place.second = EmptyPlace;
			bFound = true;
			break;
		}
	}

	if (!bFound)
	{
		for (auto& room : m_mapRoomPlaces)
		{
			for (auto& place : room.second)
			{
				if (place.second == ullChip)
				{
					if (room.first == eRoom)
						return;
					place.second = EmptyPlace;
					bFound = true;
					break;
				}
			}
			if (bFound)
				break;
		}
	}

	for (auto& place : m_mapRoomPlaces.at(eRoom))
	{
		if (place.second == EmptyPlace)
		{
			place.second = ullChip;
			break;
		}
	}

	if (!m_mapChips.count(ullChip))
		m_mapChips[ullChip].reset(new CWChip(GetScene(), ullChip));
}

void CWGameLocation::SetBlinking(bool bBlinking)
{
	m_bBlinking = bBlinking;
	if (!m_bBlinking)
		SetBorderColor(m_clrBorderNormal);
	else
		SetBorderColor(m_clrBorderBlink);
}

void CWGameLocation::Blink()
{
	for (auto& chip : m_mapChips)
		chip.second->Blink();

	if (!m_bBlinking)
		return;

	if (GetBorderColor() == m_clrBorderNormal)
		SetBorderColor(m_clrBorderBlink);
	else
		SetBorderColor(m_clrBorderNormal);
}

void CWGameLocation::NormalizeSize()
{
	m_fTileSize = GetWorkingSize().y / m_v2iLocationSize.y;
	if ((m_fTileSize * m_v2iLocationSize.x) > GetWorkingSize().x)
		m_fTileSize = GetWorkingSize().x / m_v2iLocationSize.x;

	m_v2fLocationOffset.x = GetWorkingSize().x / 2 - m_fTileSize * m_v2iLocationSize.x / 2;
	m_v2fLocationOffset.y = GetWorkingSize().y / 2 - m_fTileSize * m_v2iLocationSize.y / 2;
}

void CWGameLocation::Draw()
{
	NormalizeSize();

	for (int y = 0; y < m_v2iLocationSize.y; ++y)
	{
		for (int x = 0; x < m_v2iLocationSize.x; ++x)
		{
			sf::Vector2f pos = PointToAbsolutePosition(Point(x, y));
			sf::RectangleShape rcShape(sf::Vector2f(m_fTileSize, m_fTileSize));
			rcShape.setPosition(pos);
			eRooms eRoom = PtInRoom(Point(x, y));
			if (eRoom != eRooms_None)
			{
				rcShape.setFillColor(GetRoomColor(eRoom));
				rcShape.setOutlineThickness(0);
			}
			else
			{
				if (InTrack(Point(x, y)))
					rcShape.setFillColor(m_clrTileLight);
				else
					rcShape.setFillColor(m_clrTile);
				rcShape.setOutlineThickness(-1);
				rcShape.setOutlineColor(m_clrTileBorder);
			}
			GetRenderTarget()->draw(rcShape);
		}
	}

	for (auto& room : m_mapRoomPlaces)
	{
		sf::Text text(enum_desc(room.first), GetFont(), 15);
		text.setPosition(GetRoomTextAbsolutePosition(room.first));
		text.setFillColor(sf::Color::Black);
		GetRenderTarget()->draw(text);

		for (auto& place : room.second)
		{
			if (place.second != EmptyPlace)
				DrawChip(place.second, place.first);
		}
	}

	for (auto& place : m_lstPlaces)
	{
		if (place.second != EmptyPlace)
			DrawChip(place.second, place.first);
	}

	__super::Draw();
}

void CWGameLocation::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonPressed(button, x, y);

	if ((button != sf::Mouse::Button::Left)/* || !GetScene().GetRenderWindow()*/)
		return;

	//sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (m_OnMoveHandler && !m_lstTrack.empty() && !m_bMovementDone)
	{
		m_eTrackCharacter = eCharacters_None;
		m_uiStepsLeft = 0;
		m_OnMoveHandler(m_lstTrack);
		m_lstTrack.clear();
		m_bMovementDone = true;
	}
}

void CWGameLocation::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonReleased(button, x, y);

	//if (button != sf::Mouse::Button::Left)
	//	return;
}

void CWGameLocation::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseMoved(x, y);

	if (!GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetAbsoluteWorkingRect().contains(pos))
		CheckTrack(AbsolutePositionToPoint(pos));
}

sf::Color CWGameLocation::GetRoomColor(eRooms eRoom)
{
	switch (eRoom)
	{
	case eRooms_Cocina: return sf::Color::Green;
	case eRooms_Comedor: return sf::Color(150, 0, 150);
	case eRooms_Salon: return sf::Color(0, 150, 150);
	case eRooms_Estudio: return sf::Color::Magenta;
	case eRooms_Biblioteca: return sf::Color::Cyan;
	case eRooms_Billar: return sf::Color::Red;
	case eRooms_Terraza: return sf::Color::Yellow;
	case eRooms_Baile: return sf::Color::Blue;
	case eRooms_Escaleras: return sf::Color::White;
	}
	return sf::Color::Black;
}

sf::Vector2f CWGameLocation::PointToPosition(const Point& rPoint)
{
	return sf::Vector2f(m_fTileSize * rPoint.x, m_fTileSize * rPoint.y);
}

sf::Vector2f CWGameLocation::PointToAbsolutePosition(const Point& rPoint)
{
	return sf::Vector2f(
		GetAbsoluteWorkingPosition().x + m_fTileSize * rPoint.x + m_v2fLocationOffset.x,
		GetAbsoluteWorkingPosition().y + m_fTileSize * rPoint.y + m_v2fLocationOffset.y
	);
}

Point CWGameLocation::PositionToPoint(const sf::Vector2f& rv2fPosition)
{
	for (int y = 0; y < m_v2iLocationSize.y; ++y)
	{
		for (int x = 0; x < m_v2iLocationSize.x; ++x)
		{
			sf::FloatRect rc(
				m_fTileSize * x,
				m_fTileSize * y,
				m_fTileSize,
				m_fTileSize
			);
			if (rc.contains(rv2fPosition))
				return Point(x, y);
		}
	}
	return Point(-1, -1);
}

Point CWGameLocation::AbsolutePositionToPoint(const sf::Vector2f& rv2fPosition)
{
	for (int y = 0; y < m_v2iLocationSize.y; ++y)
	{
		for (int x = 0; x < m_v2iLocationSize.x; ++x)
		{
			sf::FloatRect rc(
				GetAbsoluteWorkingPosition().x + m_fTileSize * x + m_v2fLocationOffset.x,
				GetAbsoluteWorkingPosition().y + m_fTileSize * y + m_v2fLocationOffset.y,
				m_fTileSize,
				m_fTileSize
			);
			if (rc.contains(rv2fPosition))
				return Point(x, y);
		}
	}
	return Point(-1, -1);
}

sf::Vector2f CWGameLocation::GetRoomTextAbsolutePosition(eRooms eRoom)
{
	return PointToAbsolutePosition(Point(g_RoomLocations.map.at(eRoom).begin()->left, g_RoomLocations.map.at(eRoom).begin()->top));
}

void CWGameLocation::DrawChip(sf::Uint64 ullChip, const Point& rPoint)
{
	if (!m_mapChips.count(ullChip))
		return;

	CWChip* pChip = m_mapChips.at(ullChip).get();
	pChip->SetFont(GetFont());
	pChip->SetRenderTarget(GetRenderTarget());
	pChip->SetSize(sf::Vector2f(m_fTileSize * 1.5f, m_fTileSize * 1.5f));
	pChip->SetPosition(sf::Vector2f(
		GetAbsoluteWorkingPosition().x - (pChip->GetSize().x - m_fTileSize) / 2 + m_v2fLocationOffset.x + m_fTileSize * rPoint.x,
		GetAbsoluteWorkingPosition().y - (pChip->GetSize().y - m_fTileSize) / 2 + m_v2fLocationOffset.y + m_fTileSize * rPoint.y
	));
	pChip->Draw();
}

void CWGameLocation::CheckTrack(const Point& rPoint)
{
	if (!m_uiStepsLeft || (m_eTrackCharacter == eCharacters_None) || m_bMovementDone)
	{
		m_lstTrack.clear();
		return;
	}

	sf::Uint64 ullCharacter = make_CharacterCard(m_eTrackCharacter);
	Point ptCharacter;
	eRooms eCharacterRoom = eRooms_None;
	bool bFound = false;
	for (auto& place : m_lstPlaces)
	{
		if (place.second == ullCharacter)
		{
			ptCharacter = place.first;
			bFound = true;
			break;
		}
	}
	if (!bFound)
	{
		for (auto& room : m_mapRoomPlaces)
		{
			for (auto& place : room.second)
			{
				if (place.second == ullCharacter)
				{
					ptCharacter = place.first;
					eCharacterRoom = room.first;
					bFound = true;
					break;
				}
			}
			if (bFound)
				break;
		}
	}

	m_lstTrack.clear();
	if (!bFound)
		return;

	bool bLastPointInRoom = false;
	bool bPointLeavesRoom = false;
	bool bPointInSecondRoom = false;
	for (int x = rPoint.x; ; --x)
	{
		if ((x < 0) || (m_lstTrack.size() > m_uiStepsLeft))
		{
			m_lstTrack.clear();
			break;
		}
		if (Point(x, rPoint.y) == ptCharacter)
			break;
		if (PtInRoom(Point(x, rPoint.y)) != eRooms_None) // In room
		{
			if (!m_lstTrack.size())
				bLastPointInRoom = true;
			if (bPointLeavesRoom)
			{
				bPointInSecondRoom = true;
			}
			else
			{
				if (((x - 1) >= 0) && (PtInRoom(Point(x - 1, rPoint.y)) == eRooms_None))
					m_lstTrack.push_front(Point(x, rPoint.y));
			}
		}
		else // Out room
		{
			if (bPointInSecondRoom)
			{
				m_lstTrack.clear();
				break;
			}
			if (bLastPointInRoom)
				bPointLeavesRoom = true;
			m_lstTrack.push_front(Point(x, rPoint.y));
		}
	}
	if (!m_lstTrack.empty())
		return;

	bLastPointInRoom = false;
	bPointLeavesRoom = false;
	bPointInSecondRoom = false;
	for (int x = rPoint.x; ; ++x)
	{
		if ((x >= m_v2iLocationSize.x) || (m_lstTrack.size() > m_uiStepsLeft))
		{
			m_lstTrack.clear();
			break;
		}
		if (Point(x, rPoint.y) == ptCharacter)
			break;
		if (PtInRoom(Point(x, rPoint.y)) != eRooms_None) // In room
		{
			if (!m_lstTrack.size())
				bLastPointInRoom = true;
			if (bPointLeavesRoom)
			{
				bPointInSecondRoom = true;
			}
			else
			{
				if (((x + 1) < m_v2iLocationSize.x) && (PtInRoom(Point(x + 1, rPoint.y)) == eRooms_None))
					m_lstTrack.push_front(Point(x, rPoint.y));
			}
		}
		else // Out room
		{
			if (bPointInSecondRoom)
			{
				m_lstTrack.clear();
				break;
			}
			if (bLastPointInRoom)
				bPointLeavesRoom = true;
			m_lstTrack.push_front(Point(x, rPoint.y));
		}
	}
	if (!m_lstTrack.empty())
		return;

	bLastPointInRoom = false;
	bPointLeavesRoom = false;
	bPointInSecondRoom = false;
	for (int y = rPoint.y; ; --y)
	{
		if ((y < 0) || (m_lstTrack.size() > m_uiStepsLeft))
		{
			m_lstTrack.clear();
			break;
		}
		if (Point(rPoint.x, y) == ptCharacter)
			break;
		if (PtInRoom(Point(rPoint.x, y)) != eRooms_None) // In room
		{
			if (!m_lstTrack.size())
				bLastPointInRoom = true;
			if (bPointLeavesRoom)
			{
				bPointInSecondRoom = true;
			}
			else
			{
				if (((y - 1) >= 0) && (PtInRoom(Point(rPoint.x, y - 1)) == eRooms_None))
					m_lstTrack.push_front(Point(rPoint.x, y));
			}
		}
		else // Out room
		{
			if (bPointInSecondRoom)
			{
				m_lstTrack.clear();
				break;
			}
			if (bLastPointInRoom)
				bPointLeavesRoom = true;
			m_lstTrack.push_front(Point(rPoint.x, y));
		}
	}
	if (!m_lstTrack.empty())
		return;

	bLastPointInRoom = false;
	bPointLeavesRoom = false;
	bPointInSecondRoom = false;
	for (int y = rPoint.y; ; ++y)
	{
		if ((y >= m_v2iLocationSize.y) || (m_lstTrack.size() > m_uiStepsLeft))
		{
			m_lstTrack.clear();
			break;
		}
		if (Point(rPoint.x, y) == ptCharacter)
			break;
		if (PtInRoom(Point(rPoint.x, y)) != eRooms_None) // In room
		{
			if (!m_lstTrack.size())
				bLastPointInRoom = true;
			if (bPointLeavesRoom)
			{
				bPointInSecondRoom = true;
			}
			else
			{
				if (((y + 1) < m_v2iLocationSize.y) && (PtInRoom(Point(rPoint.x, y + 1)) == eRooms_None))
					m_lstTrack.push_front(Point(rPoint.x, y));
			}
		}
		else // Out room
		{
			if (bPointInSecondRoom)
			{
				m_lstTrack.clear();
				break;
			}
			if (bLastPointInRoom)
				bPointLeavesRoom = true;
			m_lstTrack.push_front(Point(rPoint.x, y));
		}
	}
}

bool CWGameLocation::InTrack(const Point& rPoint) const
{
	for (auto& point : m_lstTrack)
	{
		if (rPoint == point)
			return true;
	}
	return false;
}
