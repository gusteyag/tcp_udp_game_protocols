#pragma once
#include "WContainer.h"
#include <functional>
#include "GameDefinitions.h"


class CWText;
class CWListBox;
class CWButton;

class CWDeductionDlg :
	public CWContainer
{
public:
	CWDeductionDlg(CScene& rScene);
	virtual ~CWDeductionDlg();

	void Show(const std::function<void(eCharacters eCharacter, eWeapons eWeapon, eRooms eRoom)>& rHandler, eRooms eRoom = eRooms_None);

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pTextDescription;
	CWText* m_pTextDeduction;
	CWListBox* m_pListCharacters;
	CWListBox* m_pListWeapons;
	CWListBox* m_pListRooms;
	CWButton* m_pButtonMake;
	std::function<void(eCharacters eCharacter, eWeapons eWeapon, eRooms eRoom)> m_OnMakeHandler;

	eRooms m_eRoom;
};

