#pragma once
#include "WContainer.h"
#include "GameDefinitions.h"
#include <map>
#include <list>
#include <functional>
#include <memory>
#include "WChip.h"


class CWGameLocation :
	public CWContainer
{
public:
	CWGameLocation(CScene& rScene);
	virtual ~CWGameLocation();

	void Move(ULONG64 ullChip, const Point& rPoint);
	void MoveToRoom(ULONG64 ullChip, eRooms eRoom);

	void MoveRequest(eCharacters eCharacter, sf::Uint32 uiStepsLeft, const std::function<void(const std::list<POINT>& rlstTrack)>& rHandler)
	{ m_eTrackCharacter = eCharacter; m_uiStepsLeft = uiStepsLeft; m_OnMoveHandler = rHandler; m_bMovementDone = false; }

	void SetBlinking(bool bBlinking);
	void Blink();

	void SetChipBlinking(sf::Uint64 ullChip, bool bBlinking) { if (m_mapChips.count(ullChip)) m_mapChips.at(ullChip)->SetBlinking(bBlinking); }
	void SetChipInactive(sf::Uint64 ullChip, bool bInactive) { if (m_mapChips.count(ullChip)) m_mapChips.at(ullChip)->SetInactive(bInactive); }
	void SetChipDisabled(sf::Uint64 ullChip, bool bDisabled) { if (m_mapChips.count(ullChip)) m_mapChips.at(ullChip)->SetEnabled(!bDisabled); }

	virtual void NormalizeSize();
	virtual void Draw();

	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	sf::Vector2i m_v2iLocationSize;
	float m_fTileSize;
	sf::Vector2f m_v2fLocationOffset;
	sf::Color m_clrTile;
	sf::Color m_clrTileLight;
	sf::Color m_clrTileBorder;
	std::list<std::pair<Point, sf::Uint64> > m_lstPlaces;
	std::map<eRooms, std::list<std::pair<Point, sf::Uint64> > > m_mapRoomPlaces;
	std::map<sf::Uint64, std::unique_ptr<CWChip> > m_mapChips;

	eCharacters m_eTrackCharacter;
	sf::Uint32 m_uiStepsLeft;
	std::function<void(const std::list<POINT>& rlstTrack)> m_OnMoveHandler;
	std::list<POINT> m_lstTrack;
	bool m_bMovementDone;

	bool m_bBlinking;
	sf::Color m_clrBorderNormal;
	sf::Color m_clrBorderBlink;

private:
	sf::Color GetRoomColor(eRooms eRoom);
	const sf::Uint64 EmptyPlace = (sf::Uint64)-1;
	void FillPlaces();
	bool IsEmptyPlace(const Point& rPoint);
	sf::Vector2f PointToPosition(const Point& rPoint);
	sf::Vector2f PointToAbsolutePosition(const Point& rPoint);
	Point PositionToPoint(const sf::Vector2f& rv2fPosition);
	Point AbsolutePositionToPoint(const sf::Vector2f& rv2fPosition);
	sf::Vector2f GetRoomTextAbsolutePosition(eRooms eRoom);
	void DrawChip(sf::Uint64 ullChip, const Point& rPoint);
	void CheckTrack(const Point& rPoint);
	bool InTrack(const Point& rPoint) const;
};

