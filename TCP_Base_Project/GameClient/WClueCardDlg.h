#pragma once
#include "WContainer.h"
#include <functional>
#include "GameDefinitions.h"


class CWText;
class CWListBox;
class CWButton;

class CWClueCardDlg :
	public CWContainer
{
public:
	CWClueCardDlg(CScene& rScene);
	virtual ~CWClueCardDlg();

	void Show(eCardTypes eCardType, const std::function<void(sf::Uint64 ullAssumedCard)>& rSelectHandler);

	virtual void NormalizeSize();

	virtual void Draw();

private:
	eCardTypes m_eCardType;
	CWText* m_pTextDescription;
	CWListBox* m_pListCharacters;
	CWListBox* m_pListWeapons;
	CWListBox* m_pListRooms;
	CWButton* m_pButtonSelect;
	std::function<void(sf::Uint64 ullAssumedCard)> m_OnSelectHandler;
};

