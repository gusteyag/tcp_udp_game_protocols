#pragma once
#include <Windows.h>
#include <map>
#include <functional>
#include "GameInfo.h"
#include "GameState.h"
#include "Chat.h"


typedef std::function<void()> net_event_cb_t;
typedef std::function<void(eResult)> net_answer_cb_t;
typedef std::function<void(const std::wstring& rsError)> net_message_cb_t;
typedef std::function<void(const std::map<DWORD, CGameInfo>&)> net_game_info_list_cb_t;
typedef std::function<void(const CGameState& rGameState)> net_game_state_cb_t;
typedef std::function<void(const CChat& rChat)> net_chat_cb_t;
typedef std::function<void(eResult, DWORD GameId, DWORD PlayerId)> net_join_answer_cb_t;

enum eNetworkType
{
	eNetworkType_None, // Disconnected from any network
	eNetworkType_ClientServer,
	eNetworkType_P2P,
};

class INetClient
{
public:
	INetClient() {}
	virtual ~INetClient() {}

	virtual eNetworkType GetNetworkType() = 0;

	virtual bool Connect(
		const net_message_cb_t& rOnDisconnectedCB,
		const net_game_info_list_cb_t& rOnGameInfoListChangedCB,
		const net_game_state_cb_t& rOnGameStateChangedCB,
		const net_chat_cb_t& rOnChatChangedCB
	) = 0;
	virtual void Disconnect() = 0;

	virtual bool Process() = 0;


	virtual void Request_CreateNewGameAndJoin(
		const net_join_answer_cb_t& rAnswerCB,
		const std::wstring& rsGameName,
		DWORD dwSpecifiedPlayersCount,
		const std::wstring& rsPlayerName,
		eCharacters eCharacter
	) = 0;
	virtual void Request_Join(const net_join_answer_cb_t& rAnswerCB, DWORD dwGameId, const std::wstring& rsPlayerName, eCharacters eCharacter) = 0;
	virtual void Request_LeftGame(const net_answer_cb_t& rAnswerCB) = 0;

	virtual void Request_RollDices(const net_answer_cb_t& rAnswerCB) = 0;
	virtual void Request_ClueCard(const net_answer_cb_t& rAnswerCB, ULONG64 nAssumedCard) = 0;
	virtual void Request_ClueCardAnswer(const net_answer_cb_t& rAnswerCB) = 0;
	virtual void Request_ReceiveClueCardAnswer(const net_answer_cb_t& rAnswerCB) = 0;
	virtual void Request_Move(const net_answer_cb_t& rAnswerCB, const std::list<POINT>& rlstTrack) = 0;
	virtual void Request_MakeDeduction(const net_answer_cb_t& rAnswerCB, eWeapons eWeapon, eCharacters eCharacter) = 0;
	virtual void Request_AnswerToDeduction(const net_answer_cb_t& rAnswerCB) = 0;
	virtual void Request_ReceiveDeductionAnswers(const net_answer_cb_t& rAnswerCB) = 0;
	virtual void Request_MakeAccusation(const net_answer_cb_t& rAnswerCB, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter) = 0;
	virtual void Request_SkipAccusation(const net_answer_cb_t& rAnswerCB) = 0;

	virtual void Request_ChatMessage(const std::wstring& rsMessage) = 0;
};
