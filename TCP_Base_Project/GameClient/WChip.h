#pragma once
#include "Widget.h"
#include "GameDefinitions.h"

class CWChip :
	public CWidget
{
public:
	CWChip(CScene& rScene);
	CWChip(CScene& rScene, sf::Uint64 ullChip);
	virtual ~CWChip();

	void SetChip(sf::Uint64 ullChip) { m_ullChip = ullChip; }
	sf::Uint64 GetChip() const { return m_ullChip; }

	void SetInactive(bool bInactive) { m_bInactive = bInactive; }
	
	void CWChip::SetBlinking(bool bBlinking);
	void CWChip::Blink();

	virtual void Draw();

private:
	sf::Uint64 m_ullChip;
	bool m_bInactive;
	bool m_bBlinking;
	bool m_bBlinkColors;
	sf::Color m_clrInactive;
	sf::Color m_clrBlink;
};
