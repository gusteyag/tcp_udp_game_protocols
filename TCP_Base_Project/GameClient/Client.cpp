#include "Client.h"


CClient::CClient(const std::wstring& rsServerAddress, USHORT nServerPort)
	: m_sServerAddress(rsServerAddress)
	, m_nServerPort(nServerPort)
	, m_bOnceConnected(false)
	, m_bDisconnected(true)
	, m_bHandshakeSended(false)
	, m_bHandshake(false)
{
}

CClient::~CClient()
{
	Disconnect();
}

bool CClient::Connect(
	const net_message_cb_t& rOnDisconnectedCB,
	const net_game_info_list_cb_t& rOnGameInfoListChangedCB,
	const net_game_state_cb_t& rOnGameStateChangedCB,
	const net_chat_cb_t& rOnChatChangedCB
)
{
	m_Socket.setBlocking(false);

	m_bOnceConnected = false;
	m_bDisconnected = true;
	if (!_Connect())
	{
		Disconnect();
		return false;
	}

	m_OnDisconnectedCB = rOnDisconnectedCB;
	m_OnGameInfoListChangedCB = rOnGameInfoListChangedCB;
	m_OnGameStateChangedCB = rOnGameStateChangedCB;
	m_OnChatChangedCB = rOnChatChangedCB;
	return true;
}

void CClient::Disconnect()
{
	m_bDisconnected = true;
	m_Socket.disconnect();
	m_bHandshakeSended = false;
	m_bHandshake = false;
	m_InPacket.clear();
	m_lstOutPackets.clear();

	m_mapGameInfoList.clear();

	//m_OnDisconnectedCB = std::nullptr_t();
	m_OnGameInfoListChangedCB = std::nullptr_t();
	m_OnGameStateChangedCB = std::nullptr_t();
	m_OnChatChangedCB = std::nullptr_t();

	m_CreateAndJoin_AnswerCB = std::nullptr_t();
	m_Join_AnswerCB = std::nullptr_t();
	m_mapCallbacks.clear();
}

bool CClient::_Connect()
{
	char szBuff[256] = {};
	_snprintf_s(szBuff, _TRUNCATE, "%S", m_sServerAddress.c_str());
	sf::TcpSocket::Status status = m_Socket.connect(szBuff, m_nServerPort);
	if (status == sf::TcpSocket::Error)
		return false;
	m_bOnceConnected = true;
	m_bDisconnected = false;
	m_ConnectionClock.restart();
	return true;
}

bool CClient::Process()
{
	if (!m_OnDisconnectedCB || !m_OnGameInfoListChangedCB || !m_OnGameStateChangedCB)
		return false;

	if (!m_bOnceConnected && m_bDisconnected)
	{
		if (m_ConnectionClock.getElapsedTime().asSeconds() >= 5)
		{
			if (!_Connect())
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to connect to server.");
				return false;
			}
		}
		return false;
	}

	if (!m_bHandshakeSended)
	{
		sf::Packet* pPacket = new sf::Packet();
		(*pPacket) << (sf::Uint32)ePacketType_Handshake << (sf::Uint32)CURRENT_PROTOCOL_VERSION_BCD;
		m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));

		m_bHandshakeSended = true;
	}

	bool bSomethingHappened = false;

	sf::TcpSocket::Status status = m_Socket.receive(m_InPacket);
	if (status != sf::TcpSocket::Done)
	{
		if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
		{
			Disconnect();
			if(m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Failed to receive data from server.");
			return false;
		}
	}
	else
	{
		bSomethingHappened = true;
		if (!ProcessProtocol())
			return false;
		m_InPacket.clear();
	}

	if (!m_lstOutPackets.empty())
	{
		sf::TcpSocket::Status status = m_Socket.send(*m_lstOutPackets.front());
		if (status != sf::TcpSocket::Done)
		{
			if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to send data to server.");
				return false;
			}
		}
		else
		{
			bSomethingHappened = true;
			m_lstOutPackets.pop_front();
		}
	}

	return bSomethingHappened;
}

bool CClient::ProcessProtocol()
{
	if (!m_bHandshake)
	{
		sf::Uint32 nData = 0;
		m_InPacket >> nData;
		if (nData != ePacketType_Handshake)
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Unexpected packet type from server. Expected to ePacketType_Handshake.");
			return false;
		}
		m_InPacket >> nData;
		if (nData != (sf::Uint32)eResult_Success)
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Handshake failed.");
			return false;
		}
		m_bHandshake = true;
		return true;
	}

	sf::Uint32 nPacketType = 0;
	m_InPacket >> nPacketType;
	switch (nPacketType)
	{
	case ePacketType_GameInfoList:
	{
		m_mapGameInfoList.clear();
		sf::Uint32 nData = 0;
		m_InPacket >> nData;
		for (sf::Uint32 i = 0; i < nData; ++i)
		{
			CGameInfo info;
			if (!info.Deserialize(m_InPacket))
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to deserialize GameInfo.");
				return false;
				//break;
			}
			m_mapGameInfoList[info.GetGameId()] = info;
		}
		m_OnGameInfoListChangedCB(m_mapGameInfoList);
	}
	break;

	case ePacketType_GameState:
	{
		CGameState state;
		if (!state.Deserialize(m_InPacket))
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Failed to deserialize GameState.");
			return false;
			//break;
		}
		m_OnGameStateChangedCB(state);
	}
	break;

	case ePacketType_CreateNewGameAndJoin:
	{
		sf::Uint32 nResult = eResult_Fail;
		m_InPacket >> nResult;
		if (nResult == eResult_Success)
		{
			sf::Uint32 nGameId = 0;
			sf::Uint32 nPlayerId = 0;
			m_InPacket >> nGameId >> nPlayerId;
			m_CreateAndJoin_AnswerCB((eResult)nResult, nGameId, nPlayerId);
		}
		else
		{
			m_CreateAndJoin_AnswerCB((eResult)nResult, 0, 0);
		}
		m_CreateAndJoin_AnswerCB = std::nullptr_t();
	}
	break;

	case ePacketType_JoinToGame:
	{
		sf::Uint32 nResult = eResult_Fail;
		sf::Uint32 nGameId = 0;
		sf::Uint32 nPlayerId = 0;
		m_InPacket >> nResult >> nGameId;
		if (nResult == eResult_Success)
		{
			m_InPacket >> nPlayerId;
		}
		if (!m_mapGameInfoList.count(nGameId))
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"GameInfo not found.");
			return false;
		}
		else
		{
			m_Join_AnswerCB((eResult)nResult, nGameId, nPlayerId);
		}
		m_Join_AnswerCB = std::nullptr_t();
	}
	break;

	case ePacketType_LeftGame:
	case ePacketType_RollDices:
	case ePacketType_RequestClueCard:
	case ePacketType_AnswerClueCard:
	case ePacketType_ReceiveClueCardAnswer:
	case ePacketType_Move:
	case ePacketType_MakeDeduction:
	case ePacketType_AnswerToDeduction:
	case ePacketType_ReceiveDeductionAnswers:
	case ePacketType_MakeAccusation:
	case ePacketType_SkipAccusation:
	{
		sf::Uint32 nResult = eResult_Fail;
		m_InPacket >> nResult;
		if (m_mapCallbacks.count((ePacketType)nPacketType))
		{
			m_mapCallbacks.at((ePacketType)nPacketType)((eResult)nResult);
			m_mapCallbacks.erase((ePacketType)nPacketType);
		}
		else
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Answer without request.");
			return false;
		}
	}
	break;

	case ePacketType_ChatMessage:
	{
		sf::Uint32 nResult = eResult_Fail;
		m_InPacket >> nResult;
		if (nResult != eResult_Success)
		{
			// Do nothing
		}
	}
	break;

	case ePacketType_ChatState:
	{
		CChat chat;
		if (!chat.Deserialize(m_InPacket))
		{
			// Do nothing
			break;
		}
		if(m_OnChatChangedCB)
			m_OnChatChangedCB(chat);
	}
	break;

	default:
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Unknown packet type.");
		return false;
	}
	break;
	}

	return true;
}

void CClient::Request_CreateNewGameAndJoin(
	const net_join_answer_cb_t& rAnswerCB,
	const std::wstring& rsGameName,
	DWORD dwSpecifiedPlayersCount,
	const std::wstring& rsPlayerName,
	eCharacters eCharacter
)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_CreateNewGameAndJoin << (sf::Uint32)dwSpecifiedPlayersCount << rsGameName << eCharacter << rsPlayerName;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_CreateAndJoin_AnswerCB = rAnswerCB;
}

void CClient::Request_Join(const net_join_answer_cb_t& rAnswerCB, DWORD dwGameId, const std::wstring& rsPlayerName, eCharacters eCharacter)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_JoinToGame << (sf::Uint32)dwGameId << (sf::Uint32)eCharacter << rsPlayerName;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_Join_AnswerCB = rAnswerCB;
}

void CClient::Request_LeftGame(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_LeftGame;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_LeftGame] = rAnswerCB;
}

void CClient::Request_RollDices(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_RollDices;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_RollDices] = rAnswerCB;
}

void CClient::Request_ClueCard(const net_answer_cb_t& rAnswerCB, ULONG64 nAssumedCard)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_RequestClueCard << nAssumedCard;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_RequestClueCard] = rAnswerCB;
}

void CClient::Request_ClueCardAnswer(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_AnswerClueCard;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_AnswerClueCard] = rAnswerCB;
}

void CClient::Request_ReceiveClueCardAnswer(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_ReceiveClueCardAnswer;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_ReceiveClueCardAnswer] = rAnswerCB;
}

void CClient::Request_Move(const net_answer_cb_t& rAnswerCB, const std::list<POINT>& rlstTrack)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_Move << (sf::Uint32)rlstTrack.size();
	for (auto& point : rlstTrack)
		(*pPacket) << (sf::Int32)point.x << (sf::Int32)point.y;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_Move] = rAnswerCB;
}

void CClient::Request_MakeDeduction(const net_answer_cb_t& rAnswerCB, eWeapons eWeapon, eCharacters eCharacter)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_MakeDeduction << (sf::Uint32)eWeapon << (sf::Uint32)eCharacter;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_MakeDeduction] = rAnswerCB;
}

void CClient::Request_AnswerToDeduction(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_AnswerToDeduction;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_AnswerToDeduction] = rAnswerCB;
}

void CClient::Request_ReceiveDeductionAnswers(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_ReceiveDeductionAnswers;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_ReceiveDeductionAnswers] = rAnswerCB;
}

void CClient::Request_MakeAccusation(const net_answer_cb_t& rAnswerCB, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_MakeAccusation << (sf::Uint32)eRoom << (sf::Uint32)eWeapon << (sf::Uint32)eCharacter;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_MakeAccusation] = rAnswerCB;
}

void CClient::Request_SkipAccusation(const net_answer_cb_t& rAnswerCB)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_SkipAccusation;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	m_mapCallbacks[ePacketType_SkipAccusation] = rAnswerCB;
}

void CClient::Request_ChatMessage(const std::wstring& rsMessage)
{
	sf::Packet* pPacket = new sf::Packet();
	(*pPacket) << (sf::Uint32)ePacketType_ChatMessage << rsMessage;
	m_lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
}
