#pragma once
#include "WContainer.h"
#include "GameState.h"
#include <functional>


class CWGameLocation;
class CChat;
class CWChat;
class CWPlayerStatus;
class CWButton;
class CWListBox;
class INetClient;
class CWMessageBox;
class CWWinnerDlg;
class CWDeductionDlg;
class CWClueCardDlg;

class CWGame :
	public CWContainer
{
public:
	CWGame(CScene& rScene);
	virtual ~CWGame();

	void SetOnQuitHandler(const std::function<void(void)>& rHandler) { m_OnQuitHandler = rHandler; }
	void SetOnChatMessageHandler(const std::function<void(const std::wstring& rsText)>& rHandler) { m_OnChatMessageHandler = rHandler; }

	void SetCharacter(eCharacters eCharacter) { m_eCharacter = eCharacter; }
	void SetNetworkClient(INetClient* pNetClient) { m_pNetClient = pNetClient; }
	void UpdateGameState(const CGameState& rGameState);
	void UpdateChat(const CChat& rChat);

	virtual void NormalizeSize();

	virtual void Draw();

	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	eCharacters m_eCharacter;
	INetClient* m_pNetClient;
	CGameState m_GameState;
	CWGameLocation* m_pGameLocation;
	CWChat* m_pChat;
	CWPlayerStatus* m_pPlayer1Status;
	CWPlayerStatus* m_pPlayer2Status;
	CWButton* m_pButtonQuit;
	CWListBox* m_pListOpenedCards;
	CWListBox* m_pListYourCards;
	CWMessageBox* m_pMessageBox;
	CWWinnerDlg* m_pWinnerDlg;
	CWDeductionDlg* m_pDeductionDlg;
	CWClueCardDlg* m_pClueCardDlg;
	std::function<void(void)> m_OnQuitHandler;
	std::function<void(const std::wstring& rsText)> m_OnChatMessageHandler;
	sf::Clock m_ClockBlinking;
	std::set<DWORD> m_setPlayersLeftGame;
	std::set<DWORD> m_setPlayersInactive;
	float m_fChatUpSize;
};
