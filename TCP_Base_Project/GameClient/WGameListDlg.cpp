#include "WGameListDlg.h"
#include "WText.h"
#include "WListBox.h"
#include "WButton.h"


CWGameListDlg::CWGameListDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pText(new CWText(rScene))
	, m_pList(new CWListBox(rScene))
	, m_pButtonSelect(new CWButton(rScene))
	, m_pButtonCreate(new CWButton(rScene))
	, m_pButtonBack(new CWButton(rScene))
{
	AddChild(m_pText);
	AddChild(m_pList);
	AddChild(m_pButtonSelect);
	AddChild(m_pButtonCreate);
	AddChild(m_pButtonBack);

	SetText(L"Game List");
	m_pText->SetText(L"Select a game from the list or create new one.");
	m_pText->SetWordsWrap(true);
	m_pButtonSelect->SetText(L"Select Game");
	m_pButtonCreate->SetText(L"Create New Game");
	m_pButtonBack->SetText(L"Back");

	m_pButtonSelect->SetEnabled(false);

	m_pButtonSelect->SetOnClickedHandler([this]() {
		if(m_pList->GetSelectedLine(NULL) && m_OnGameSelectedHandler)
			m_OnGameSelectedHandler(m_mapGameInfo[(DWORD)m_pList->GetSelectedLine(NULL)->front().ui64UserData]);
	});

	m_pButtonCreate->SetOnClickedHandler([this]() {
		if (m_OnCreateNewGameHandler)
			m_OnCreateNewGameHandler();
	});

	m_pButtonBack->SetOnClickedHandler([this]() {
		if (m_OnBackHandler)
			m_OnBackHandler();
	});
}

CWGameListDlg::~CWGameListDlg()
{
}

void CWGameListDlg::UpdateGameList(const std::map<DWORD, CGameInfo>& rmapGameInfo)
{
	m_mapGameInfo = rmapGameInfo;
	m_pList->GetLines().clear();
	for (auto& info : m_mapGameInfo)
	{
		m_pList->GetLines().push_back(std::list<ListBoxItem>());
		m_pList->GetLines().back().push_back(ListBoxItem(info.second.GetGameName(), info.first));
		m_pList->GetLines().back().push_back(ListBoxItem(enum_desc(info.second.GetGameState())));
		m_pList->GetLines().back().push_back(ListBoxItem(std::to_wstring(info.second.GetSpecifiedPlayersCount())));
		m_pList->GetLines().back().push_back(ListBoxItem(std::to_wstring(info.second.GetActualPlayersCount())));
	}
}

void CWGameListDlg::NormalizeSize()
{
	float fGameNameWidth = 350;
	float fGameStateWidth = 100;
	float fGameNeedPlayersWidth = 100;
	float fGamePlayersNowWidth = 100;
	float fWidth = fGameNameWidth + fGameStateWidth + fGameNeedPlayersWidth + fGamePlayersNowWidth + m_pList->GetInBorderWidth() * 2;
	float fTextHeight = 30;
	float fListHeight = 300;
	float fButtonsTopSpacing = 20;
	float fButtonsWidth = 150;
	float fButtonsHeight = 30;
	float fButtonsHSpacing = 20;

	if (m_pList->GetSelectedLine(NULL))
	{
		if (m_mapGameInfo.count((sf::Uint32)m_pList->GetSelectedLine(NULL)->front().ui64UserData) &&
			(m_mapGameInfo.at((sf::Uint32)m_pList->GetSelectedLine(NULL)->front().ui64UserData).GetGameState() != eGameStates_Created))
			m_pList->SetSelectedLine(-1);
		else
			m_pButtonSelect->SetEnabled(true);
	}
	else
	{
		m_pButtonSelect->SetEnabled(false);
	}

	if (!m_pList->GetColumns().size())
	{
		m_pList->GetColumns().push_back(ListBoxColumn(L"Name", fGameNameWidth));
		m_pList->GetColumns().push_back(ListBoxColumn(L"State", fGameStateWidth));
		m_pList->GetColumns().push_back(ListBoxColumn(L"Need Players", fGameNeedPlayersWidth));
		m_pList->GetColumns().push_back(ListBoxColumn(L"Players Now", fGamePlayersNowWidth));
	}

	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fWidth,
		GetWorkingPosition().y + fTextHeight + fListHeight + fButtonsTopSpacing + fButtonsHeight + GetPadding() + GetInBorderWidth()
	));

	m_pText->SetFont(GetFont());
	m_pText->SetFontSize(GetFontSize() + 4);
	m_pText->SetSize(sf::Vector2f(fWidth, fTextHeight));
	m_pText->SetPosition(sf::Vector2f(0, 0));

	m_pList->SetFont(GetFont());
	m_pList->SetFontSize(GetFontSize());
	m_pList->SetSize(sf::Vector2f(fWidth, fListHeight));
	m_pList->SetPosition(sf::Vector2f(0, fTextHeight));

	m_pButtonSelect->SetFont(GetFont());
	m_pButtonSelect->SetFontSize(GetFontSize());
	m_pButtonSelect->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonSelect->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 3 + fButtonsHSpacing * 2) / 2,
		fTextHeight + fListHeight + fButtonsTopSpacing
	));

	m_pButtonCreate->SetFont(GetFont());
	m_pButtonCreate->SetFontSize(GetFontSize());
	m_pButtonCreate->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonCreate->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 3 + fButtonsHSpacing * 2) / 2 + fButtonsWidth + fButtonsHSpacing,
		fTextHeight + fListHeight + fButtonsTopSpacing
	));

	m_pButtonBack->SetFont(GetFont());
	m_pButtonBack->SetFontSize(GetFontSize());
	m_pButtonBack->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonBack->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 3 + fButtonsHSpacing * 2) / 2 + (fButtonsWidth + fButtonsHSpacing) * 2,
		fTextHeight + fListHeight + fButtonsTopSpacing
	));
}

void CWGameListDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
