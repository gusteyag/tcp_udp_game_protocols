#pragma once
#include "WContainer.h"
#include "GameDefinitions.h"
#include <functional>


class CWChip;
class CWText;
class CWButton;

class CWPlayerStatus :
	public CWContainer
{
public:
	CWPlayerStatus(CScene& rScene);
	virtual ~CWPlayerStatus();

	void ClearStatus();

	void SetPlayerStatus(
		eCharacters eCharacter,
		const std::wstring& rsName,
		const std::wstring& rsStatus,
		const sf::Vector2u& rv2uDices = sf::Vector2u(),
		const std::wstring& rsButton1 = std::wstring(),
		const std::function<void(void)>& Button1CB = nullptr,
		const std::wstring& rsButton2 = std::wstring(),
		const std::function<void(void)>& Button2CB = nullptr
	);

	void SetBlinking(bool bBlinking);
	void Blink();

	void SetActiveView(bool bActiveView) { m_bActiveView = bActiveView; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWChip* m_pChipPlayer;
	CWText* m_pTextPlayerName;
	CWText* m_pTextPlayerStatus;
	CWText* m_pTextPlayerDices;

	CWButton* m_pButton1;
	CWButton* m_pButton2;

	bool m_bBlinking;
	bool m_bBlinkColors;
	sf::Color m_clrBorderNormal;
	sf::Color m_clrBorderBlink;

	bool m_bActiveView;
	sf::Color m_clrBorderActive;
};
