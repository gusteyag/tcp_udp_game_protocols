#pragma once
#include <functional>
#include "WContainer.h"
#include "INetClient.h"

class CWButton;
class CWEditBox;

typedef std::function<void(
	eNetworkType eType,
	const std::wstring& rsServerOrBSSAddress,
	sf::Uint16 uiServerOrBSSPort,
	const std::wstring& rsLocalAddress,
	sf::Uint16 uiLocalPort,
	sf::Uint32 uiMinPlayersCount
	)> connection_dlg_handler_t;

class CWConnectionDlg :
	public CWContainer
{
public:
	CWConnectionDlg(CScene& rScene);
	virtual ~CWConnectionDlg();

	void SetOnConnectHandler(const connection_dlg_handler_t& rHandler) { m_OnConnectHandler = rHandler; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWButton* m_pRadioServer;
	CWButton* m_pRadioP2P;
	CWEditBox* m_pEditServer;
	CWEditBox* m_pEditServerPort;
	CWEditBox* m_pEditBSS;
	CWEditBox* m_pEditBSSPort;
	CWEditBox* m_pEditLocal;
	CWEditBox* m_pEditLocalPort;
	CWEditBox* m_pEditMinPlayersCount;
	CWText* m_pTextServerOrBSS;
	CWText* m_pTextServerOrBSSPort;
	CWText* m_pTextLocal;
	CWText* m_pTextLocalPort;
	CWText* m_pTextMinPlayersCount;
	CWButton* m_pButtonConnect;
	connection_dlg_handler_t m_OnConnectHandler;

private:
	void CallOnConnectHandler();
};
