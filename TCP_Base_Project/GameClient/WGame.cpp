#include "WGame.h"
#include "WChat.h"
#include "Chat.h"
#include "WGameLocation.h"
#include "WPlayerStatus.h"
#include "WListBox.h"
#include "WButton.h"
#include "WMessageBox.h"
#include "INetClient.h"
#include "WWinnerDlg.h"
#include "WDeductionDlg.h"
#include "WClueCardDlg.h"


CWGame::CWGame(CScene& rScene)
	: CWContainer(rScene)
	, m_eCharacter(eCharacters_None)
	, m_pNetClient(NULL)
	, m_pGameLocation(new CWGameLocation(rScene))
	, m_pChat(new CWChat(rScene))
	, m_pPlayer1Status(new CWPlayerStatus(rScene))
	, m_pPlayer2Status(new CWPlayerStatus(rScene))
	, m_pButtonQuit(new CWButton(rScene))
	, m_pListOpenedCards(new CWListBox(rScene))
	, m_pListYourCards(new CWListBox(rScene))
	, m_pMessageBox(new CWMessageBox(rScene))
	, m_pWinnerDlg(new CWWinnerDlg(rScene))
	, m_pDeductionDlg(new CWDeductionDlg(rScene))
	, m_pClueCardDlg(new CWClueCardDlg(rScene))
	, m_fChatUpSize(0)
{
	AddChild(m_pGameLocation);
	AddChild(m_pChat);
	AddChild(m_pPlayer1Status);
	AddChild(m_pPlayer2Status);
	AddChild(m_pButtonQuit);
	AddChild(m_pListOpenedCards);
	AddChild(m_pListYourCards);
	AddChild(m_pClueCardDlg);
	AddChild(m_pDeductionDlg);
	AddChild(m_pMessageBox);
	AddChild(m_pWinnerDlg);

	m_pChat->SetFillColor(sf::Color(0, 0, 0, 200));

	m_pMessageBox->SetVisible(false);

	SetBorderWidth(0);
	SetHeaderHeight(0);
	SetPadding(5);

	m_pGameLocation->SetPadding(5);
	m_pPlayer1Status->SetPadding(5);
	m_pPlayer2Status->SetPadding(5);

	m_pListOpenedCards->SetFontSize(8);
	m_pListYourCards->SetFontSize(8);

	m_pButtonQuit->SetText(L"Quit");

	m_pChat->SetBorderWidth(-1);
	m_pChat->SetHeaderHeight(0);
	m_pChat->SetFontSize(8);
	m_pChat->SetPadding(5);
	m_pChat->SetOnTextEnteredHandler([this](const std::wstring& rsText) {
		if (m_OnChatMessageHandler)
			m_OnChatMessageHandler(rsText);
	});

	m_pButtonQuit->SetOnClickedHandler([this]() {
		if (m_OnQuitHandler)
			m_OnQuitHandler();
	});
}

CWGame::~CWGame()
{
}

void CWGame::UpdateGameState(const CGameState& rGameState)
{
	m_GameState = rGameState;
	if (!m_GameState.GetGameId())
	{
		m_setPlayersLeftGame.clear();
		m_setPlayersInactive.clear();
		return;
	}

	DWORD dwCurrentPlayerId = 0;
	bool bPlayerLeftGame = false;
	for (auto& player : rGameState.GetPlayers())
	{
		if (player.second->GetCharacter() == m_eCharacter)
		{
			dwCurrentPlayerId = player.second->GetId();
			if (player.second->GetState() == ePlayerStates_LeftGame)
				bPlayerLeftGame = true;
			break;
		}
	}
	if (!dwCurrentPlayerId)
		return;

	m_pListOpenedCards->GetLines().clear();
	for (auto& hint : rGameState.GetPlayers().at(dwCurrentPlayerId)->GetHints())
	{
		m_pListOpenedCards->GetLines().push_back(listbox_items_t());
		m_pListOpenedCards->GetLines().back().push_back(ListBoxItem(get_CardName(hint.first), hint.first));
		m_pListOpenedCards->GetLines().back().push_back(ListBoxItem(get_CardTypeName(hint.first)));
		std::wstring sPlayer = L"Unknown";
		if (rGameState.GetPlayers().count(hint.second))
		{
			sPlayer = enum_desc(rGameState.GetPlayers().at(hint.second)->GetCharacter());
			sPlayer += L"(";
			sPlayer += rGameState.GetPlayers().at(hint.second)->GetName();
			sPlayer += L")";
		}
		m_pListOpenedCards->GetLines().back().push_back(ListBoxItem(sPlayer));
	}

	m_pListYourCards->GetLines().clear();
	for (auto& card : rGameState.GetPlayers().at(dwCurrentPlayerId)->GetCards())
	{
		m_pListYourCards->GetLines().push_back(listbox_items_t());
		m_pListYourCards->GetLines().back().push_back(ListBoxItem(get_CardName(card), card));
		m_pListYourCards->GetLines().back().push_back(ListBoxItem(get_CardTypeName(card)));
	}

	for (auto& player : rGameState.GetPlayers())
	{
		if (player.second->GetRoom() == eRooms_None)
			m_pGameLocation->Move(make_CharacterCard(player.second->GetCharacter()), player.second->GetPosition());
		else
			m_pGameLocation->MoveToRoom(make_CharacterCard(player.second->GetCharacter()), player.second->GetRoom());
	}
	for (auto& room : rGameState.GetRooms())
	{
		for (auto& weapon : room.second->GetWeapons())
			m_pGameLocation->MoveToRoom(make_WeaponCard(weapon), room.second->GetType());
	}

	m_pPlayer1Status->ClearStatus();
	m_pPlayer2Status->ClearStatus();

	bool bWinnerExists = false;
	for (auto& player : rGameState.GetPlayers())
	{
		if (player.second->GetState() == ePlayerStates_Winner)
		{
			bWinnerExists = true;
			break;
		}
	}
	if ((rGameState.GetGameState() == eGameStates_Ended) && !bWinnerExists)
	{
		m_pWinnerDlg->Show([this]() {
			if (m_OnQuitHandler)
				m_OnQuitHandler();
		}, true);
		return;
	}

	for (auto& player : rGameState.GetPlayers())
	{
		std::wstring sPlayerName = enum_desc(player.second->GetCharacter());
		sPlayerName += L"(";
		sPlayerName += player.second->GetName();
		sPlayerName += L")";

		m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), false);

		if (player.second->IsInactive())
		{
			m_pGameLocation->SetChipInactive(make_CharacterCard(player.second->GetCharacter()), true);

			if (!m_setPlayersInactive.count(player.second->GetId()))
			{
				m_setPlayersInactive.insert(player.second->GetId());
				if (m_eCharacter == player.second->GetCharacter())
				{
					m_pMessageBox->Show(eMessageType_Info, L"You made wrong Accusation and you are in Inactive state.");
				}
				else
				{
					wchar_t szBuff[256] = {};
					_snwprintf_s(szBuff, _TRUNCATE, L"Player \"%s\" made wrong Accusation and he is in Inactive state.", sPlayerName.c_str());
					m_pMessageBox->Show(eMessageType_Info, szBuff);
				}
			}
		}

		switch (player.second->GetState())
		{
		case ePlayerStates_InLobby:
		{
			//Do nothing
		}
		break;

		case ePlayerStates_Normal:
		{
			//Do nothing
		}
		break;

		case ePlayerStates_RollDices:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				m_pPlayer1Status->SetBlinking(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Your Turn now.", sf::Vector2u(), L"Roll Dices", [this]() {
					m_pPlayer1Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_RollDices([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to roll dices.");
					});
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Rolling Dices.", sf::Vector2u());
			}
		}
		break;

		case ePlayerStates_RequestingClueCard:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Congratulations! You can request a Clue Card of type \"%s\".", enum_desc(rGameState.GetClueCardType()));
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff,
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));

				m_pClueCardDlg->Show(rGameState.GetClueCardType(), [this](sf::Uint64 ullAssumedCard) {
					m_pNetClient->Request_ClueCard([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to make a Clue Card request.");
					}, ullAssumedCard);
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Requesting a Clue Card.",
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
			}
		}
		break;

		case ePlayerStates_WaitForClueCardAnswer:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Waiting for a Clue Card answer.",
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Waiting for a Clue Card answer.",
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
			}
		}
		break;

		case ePlayerStates_ReceivingClueCardAnswer:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				std::wstring sButton = L"Receive Answer";
				wchar_t szBuff[256] = {};
				if (rGameState.GetAnsweredPlayerId())
				{
					CPlayer* pPlayer = rGameState.GetPlayers().at(rGameState.GetAnsweredPlayerId()).get();
					std::wstring sOtherPlayerName = enum_desc(pPlayer->GetCharacter());
					sOtherPlayerName += L"(";
					sOtherPlayerName += pPlayer->GetName();
					sOtherPlayerName += L")";
					_snwprintf_s(szBuff, _TRUNCATE, L"Player \"%s\" has a Clue Card.\nPlease receive an answer.", sOtherPlayerName.c_str());
				}
				else
				{
					_snwprintf_s(szBuff, _TRUNCATE, L"No player has a Clue Card.");
					sButton = L"Continue";
				}
				m_pPlayer1Status->SetBlinking(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff, sf::Vector2u(), sButton, [this]() {
					m_pPlayer1Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_ReceiveClueCardAnswer([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to receive a Clue Card answer.");
					});
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Receiving a Clue Card answer.",
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
			}
		}
		break;

		case ePlayerStates_AnsweringClueCard:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer2Status->SetActiveView(true);
				bool bClueCardFound = false;
				int index = 0;
				for (auto& line : m_pListYourCards->GetLines())
				{
					if (line.size() && line.front().ui64UserData == rGameState.GetRequestedClueCard())
					{
						bClueCardFound = true;
						m_pListYourCards->SetSelectedLine(index);
						break;
					}
					++index;
				}
				std::wstring sOtherPlayerName = L"Unknown";
				for (auto& player : rGameState.GetPlayers())
				{
					if (player.second->GetState() == ePlayerStates_WaitForClueCardAnswer)
					{
						sOtherPlayerName = enum_desc(player.second->GetCharacter());
						sOtherPlayerName += L"(";
						sOtherPlayerName += player.second->GetName();
						sOtherPlayerName += L")";
						break;
					}
				}
				wchar_t szBuff[256] = {};
				if (bClueCardFound)
					_snwprintf_s(szBuff, _TRUNCATE, L"Please answer to \"%s\" about a Clue Card that you have.", sOtherPlayerName.c_str());
				else
					m_pMessageBox->Show(eMessageType_Error, L"Clue Card not found locally.");
				m_pPlayer2Status->SetBlinking(true);
				m_pPlayer2Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff, sf::Vector2u(), L"Answer", [this]() {
					m_pListYourCards->SetSelectedLine(-1);
					m_pPlayer2Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_ClueCardAnswer([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to answer for a Clue Card request.");
					});
				});
			}
			else
			{
				m_pPlayer2Status->SetActiveView(false);
				m_pPlayer2Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"Answering for a Clue Card request.");
			}
		}
		break;

		case ePlayerStates_Move:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Please move your chip in a Game Location area. You have %u steps left.", player.second->GetMovesLeft());
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff,
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
				m_pGameLocation->SetBlinking(true);

				m_pGameLocation->MoveRequest(m_eCharacter, player.second->GetMovesLeft(), [this](const std::list<POINT>& rlstTrack) {
					if (m_pNetClient)
						m_pNetClient->Request_Move([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to move. Try again.");
						else
							m_pGameLocation->SetBlinking(false);
					}, rlstTrack);
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					L"Moving.",
					sf::Vector2u(rGameState.GetDicesResult().cx, rGameState.GetDicesResult().cy));
			}
		}
		break;

		case ePlayerStates_MakeDeduction:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"You went into the room. Now you can make a deduction.");
				m_pDeductionDlg->Show([this](eCharacters eCharacter, eWeapons eWeapon, eRooms eRoom) {
					if (m_pNetClient)
						m_pNetClient->Request_MakeDeduction([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to make Deduction.");
						}, eWeapon, eCharacter);
				}, player.second->GetRoom());
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"Making a deduction.");
			}
		}
		break;

		case ePlayerStates_WaitForDeductionAnswers:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Made deduction: \"%s killed with the %s in the %s room\".\nWaiting for a deduction answer.",
					enum_desc(rGameState.GetDeductionCharacter()), enum_desc(rGameState.GetDeductionWeapon()), enum_desc(rGameState.GetDeductionRoom()));
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, szBuff);
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Made deduction: \"%s killed with the %s in the %s room\".\nWaiting for a deduction answer.",
					enum_desc(rGameState.GetDeductionCharacter()), enum_desc(rGameState.GetDeductionWeapon()), enum_desc(rGameState.GetDeductionRoom()));
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, szBuff);
			}
		}
		break;

		case ePlayerStates_ReceivingDeductionAnswers:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				wchar_t szBuff[256] = {};
				std::wstring sOtherPlayerName = L"Unknown";
				if (rGameState.GetDeductionAnswer().first)
				{
					CPlayer* pPlayer = rGameState.GetPlayers().at(rGameState.GetDeductionAnswer().first).get();
					sOtherPlayerName = enum_desc(pPlayer->GetCharacter());
					sOtherPlayerName += L"(";
					sOtherPlayerName += pPlayer->GetName();
					sOtherPlayerName += L")";
					
				}

				std::wstring sButton = L"Receive Answer";
				if (rGameState.GetDeductionAnswer().second)
				{
					_snwprintf_s(szBuff, _TRUNCATE, L"Made deduction: \"%s killed with the %s in the %s room\".\nPlayer \"%s\" has a card \"%s(%s)\".\nPlease receive an answer.",
						enum_desc(rGameState.GetDeductionCharacter()), enum_desc(rGameState.GetDeductionWeapon()), enum_desc(rGameState.GetDeductionRoom()),
						sOtherPlayerName.c_str(),
						get_CardName(rGameState.GetDeductionAnswer().second), get_CardTypeName(rGameState.GetDeductionAnswer().second));
				}
				else
				{
					_snwprintf_s(szBuff, _TRUNCATE, L"Made deduction: \"%s killed with the %s in the %s room\".\nNo player could deny a Deduction.",
						enum_desc(rGameState.GetDeductionCharacter()), enum_desc(rGameState.GetDeductionWeapon()), enum_desc(rGameState.GetDeductionRoom()));
					sButton = L"Continue";
				}

				m_pPlayer1Status->SetBlinking(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff, sf::Vector2u(), sButton, [this]() {
					m_pPlayer1Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_ReceiveDeductionAnswers([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to receive a Deduction answer.");
					});
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Made deduction: \"%s killed with the %s in the %s room\".\nReceiving a Deduction answer.",
					enum_desc(rGameState.GetDeductionCharacter()), enum_desc(rGameState.GetDeductionWeapon()), enum_desc(rGameState.GetDeductionRoom()));
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, szBuff);
			}
		}
		break;

		case ePlayerStates_AnswersToDeduction:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer2Status->SetActiveView(true);
				bool bDeductionCardFound = false;
				int index = 0;
				for (auto& line : m_pListYourCards->GetLines())
				{
					if (line.size() &&
						(line.front().ui64UserData == make_RoomCard(rGameState.GetDeductionRoom())) ||
						(line.front().ui64UserData == make_WeaponCard(rGameState.GetDeductionWeapon())) ||
						(line.front().ui64UserData == make_CharacterCard(rGameState.GetDeductionCharacter())))
					{
						bDeductionCardFound = true;
						m_pListYourCards->SetSelectedLine(index);
						break;
					}
					++index;
				}
				std::wstring sOtherPlayerName = L"Unknown";
				for (auto& player : rGameState.GetPlayers())
				{
					if (player.second->GetState() == ePlayerStates_WaitForDeductionAnswers)
					{
						sOtherPlayerName = enum_desc(player.second->GetCharacter());
						sOtherPlayerName += L"(";
						sOtherPlayerName += player.second->GetName();
						sOtherPlayerName += L")";
						break;
					}
				}
				wchar_t szBuff[256] = {};
				if (bDeductionCardFound)
					_snwprintf_s(szBuff, _TRUNCATE, L"Please answer to \"%s\" about a Deduction Card that you have.", sOtherPlayerName.c_str());
				else
					_snwprintf_s(szBuff, _TRUNCATE, L"Please answer to \"%s\" that you can not deny a Deduction.", sOtherPlayerName.c_str());
				m_pPlayer2Status->SetBlinking(true);
				m_pPlayer2Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName,
					szBuff, sf::Vector2u(), L"Answer", [this]() {
					m_pListYourCards->SetSelectedLine(-1);
					m_pPlayer2Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_AnswerToDeduction([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to answer for a Deduction request.");
					});
				});
			}
			else
			{
				m_pPlayer2Status->SetActiveView(false);
				m_pPlayer2Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"Answering for a Deduction request.");
			}
		}
		break;

		case ePlayerStates_PromptToMakeAccusation:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				m_pPlayer1Status->SetBlinking(true);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"Would you like to make an Accusation?", sf::Vector2u(),
					L"Yes", [this]() {
					m_pPlayer1Status->SetBlinking(false);
					m_pDeductionDlg->Show([this](eCharacters eCharacter, eWeapons eWeapon, eRooms eRoom) {
						if (m_pNetClient)
							m_pNetClient->Request_MakeAccusation([this](eResult eRes) {
							if (eRes != eResult_Success)
								m_pMessageBox->Show(eMessageType_Error, L"Failed to make Accusation.");
							}, eRoom, eWeapon, eCharacter);
					});
				}, L"No", [this]() {
					m_pPlayer1Status->SetBlinking(false);
					if (m_pNetClient)
						m_pNetClient->Request_SkipAccusation([this](eResult eRes) {
						if (eRes != eResult_Success)
							m_pMessageBox->Show(eMessageType_Error, L"Failed to skip Accusation.");
					});
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pPlayer1Status->SetPlayerStatus(player.second->GetCharacter(), sPlayerName, L"Thinking about Accusation.");
			}
		}
		break;

		case ePlayerStates_Winner:
		{
			m_pGameLocation->SetChipBlinking(make_CharacterCard(player.second->GetCharacter()), true);
			if (m_eCharacter == player.second->GetCharacter())
			{
				m_pPlayer1Status->SetActiveView(true);
				m_pWinnerDlg->Show([this]() {
					if (m_OnQuitHandler)
						m_OnQuitHandler();
				});
			}
			else
			{
				m_pPlayer1Status->SetActiveView(false);
				m_pWinnerDlg->Show([this]() {
					if (m_OnQuitHandler)
						m_OnQuitHandler();
				}, false, sPlayerName, rGameState.GetMurderCharacter(), rGameState.GetMurderWeapon(), rGameState.GetMurderRoom());
			}
		}
		break;

		case ePlayerStates_LeftGame:
		{
			m_pGameLocation->SetChipDisabled(make_CharacterCard(player.second->GetCharacter()), true);
			if (!m_setPlayersLeftGame.count(player.second->GetId()))
			{
				m_setPlayersLeftGame.insert(player.second->GetId());
				wchar_t szBuff[256] = {};
				_snwprintf_s(szBuff, _TRUNCATE, L"Player \"%s\" left the Game.", sPlayerName.c_str());
				m_pMessageBox->Show(eMessageType_Info, szBuff);
			}
		}
		break;
		}
	}
}

void CWGame::UpdateChat(const CChat& rChat)
{
	m_pChat->ClearMessages();
	for (auto& msg : rChat.GetMessages())
	{
		std::wstring sAuthor = L"Unknown";
		if (m_GameState.GetPlayers().count(msg.first))
		{
			sAuthor = m_GameState.GetPlayers().at(msg.first)->GetName();
			sAuthor += L"(";
			sAuthor += enum_desc(m_GameState.GetPlayers().at(msg.first)->GetCharacter());
			sAuthor += L")";
		}
		m_pChat->AddMessage(sAuthor, msg.second);
	}
}

void CWGame::NormalizeSize()
{
	float fCardNameWidth = 70;
	float fCardTypeWidth = 50;
	float fCardPlayerWidth = 100;
	float fYourCardsHeight = 150;
	float fButonsHeight = 30;
	float fChatWidth = 300;
	float fChatHeight = 100;

	SetSize(sf::Vector2f(800, 600));

	m_pMessageBox->SetFont(GetFont());
	m_pWinnerDlg->SetFont(GetFont());
	m_pDeductionDlg->SetFont(GetFont());
	m_pClueCardDlg->SetFont(GetFont());

	m_pListYourCards->SetFont(GetFont());
	m_pListYourCards->SetHeaderHeight(15);
	m_pListYourCards->SetLineHeight(15);
	if (!m_pListYourCards->GetColumns().size())
	{
		m_pListYourCards->GetColumns().push_back(ListBoxColumn(L"Your Cards", fCardNameWidth));
		m_pListYourCards->GetColumns().push_back(ListBoxColumn(L"Type", fCardTypeWidth));
		m_pListYourCards->NormalizeSize();
		m_pListYourCards->SetSize(sf::Vector2f(m_pListYourCards->GetSize().x + 20, fYourCardsHeight));
	}

	m_pListOpenedCards->SetFont(GetFont());
	m_pListOpenedCards->SetHeaderHeight(15);
	m_pListOpenedCards->SetLineHeight(15);
	if (!m_pListOpenedCards->GetColumns().size())
	{
		m_pListOpenedCards->GetColumns().push_back(ListBoxColumn(L"Opened Cards", fCardNameWidth));
		m_pListOpenedCards->GetColumns().push_back(ListBoxColumn(L"Type", fCardTypeWidth));
		m_pListOpenedCards->GetColumns().push_back(ListBoxColumn(L"Player", fCardPlayerWidth));
		//m_pListOpenedCards->NormalizeSize();
	}
	m_pListOpenedCards->SetSize(sf::Vector2f(
		m_pListYourCards->GetSize().x,
		GetSize().y - fButonsHeight - fYourCardsHeight - fChatHeight - GetPadding() * 5
	));

	m_pButtonQuit->SetFont(GetFont());
	m_pButtonQuit->SetSize(sf::Vector2f(m_pListYourCards->GetSize().x, fButonsHeight));

	m_pChat->SetFont(GetFont());
	m_pChat->SetSize(sf::Vector2f(fChatWidth, fChatHeight + m_fChatUpSize));

	m_pPlayer1Status->SetFont(GetFont());
	m_pPlayer2Status->SetFont(GetFont());

	if (m_pPlayer2Status->IsVisible())
	{
		m_pPlayer1Status->SetSize(sf::Vector2f(
			(GetSize().x - fChatWidth - GetPadding() * 4) / 2,
			fChatHeight));
	}
	else
	{
		m_pPlayer1Status->SetSize(sf::Vector2f(
			GetSize().x - fChatWidth - GetPadding() * 3,
			fChatHeight));
	}
	m_pPlayer1Status->SetPosition(sf::Vector2f(fChatWidth + GetPadding(), m_pGameLocation->GetSize().y + GetPadding()));
	m_pPlayer2Status->SetSize(sf::Vector2f(
		(GetSize().x - fChatWidth - GetPadding() * 4) / 2,
		fChatHeight));
	m_pPlayer2Status->SetPosition(sf::Vector2f(
		fChatWidth + GetPadding() + m_pPlayer1Status->GetSize().x + GetPadding(),
		m_pGameLocation->GetSize().y + GetPadding()
	));

	m_pGameLocation->SetFont(GetFont());
	m_pGameLocation->SetSize(sf::Vector2f(
		GetSize().x - m_pListYourCards->GetSize().x - GetPadding() * 3,
		GetSize().y - fChatHeight - GetPadding() * 3
	));

	m_pGameLocation->SetPosition(sf::Vector2f(0, 0));
	m_pButtonQuit->SetPosition(sf::Vector2f(m_pGameLocation->GetSize().x + GetPadding(), 0));
	m_pListOpenedCards->SetPosition(sf::Vector2f(m_pGameLocation->GetSize().x + GetPadding(), fButonsHeight + GetPadding()));
	m_pListYourCards->SetPosition(sf::Vector2f(m_pGameLocation->GetSize().x + GetPadding(), fButonsHeight + m_pListOpenedCards->GetSize().y + GetPadding() * 2));
	m_pChat->SetPosition(sf::Vector2f(0, m_pGameLocation->GetSize().y + GetPadding() - m_fChatUpSize));
}

void CWGame::Draw()
{
	NormalizeSize();

	if (m_ClockBlinking.getElapsedTime().asMilliseconds() >= 500)
	{
		m_pPlayer1Status->Blink();
		m_pPlayer2Status->Blink();
		m_pGameLocation->Blink();
		m_ClockBlinking.restart();
	}

	__super::Draw();
}

void CWGame::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{

	if (!IsEnabled())
		return;

	__super::OnMouseButtonPressed(button, x, y);

	if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (m_pChat->GetAbsoluteBoundingRect().contains(pos))
		m_fChatUpSize = 400;
	else
		m_fChatUpSize = 0;
}
