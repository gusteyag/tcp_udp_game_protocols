#pragma once
#include "WContainer.h"
#include "GameState.h"
#include <functional>


class CWText;
class CWChat;
class CWListBox;
class CWButton;
class CChat;

class CWLobby :
	public CWContainer
{
public:
	CWLobby(CScene& rScene);
	virtual ~CWLobby();

	void SetOnQuitHandler(const std::function<void(void)>& rHandler) { m_OnQuitHandler = rHandler; }
	void SetOnChatMessageHandler(const std::function<void(const std::wstring& rsText)>& rHandler) { m_OnChatMessageHandler = rHandler; }

	void UpdatePlayersList(const CGameState& rGameState);
	void UpdateChat(const CChat& rChat);

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CGameState m_GameState;
	CWText* m_pTextChat;
	CWChat* m_pChat;
	CWText* m_pTextPlayersList;
	CWListBox* m_pPlayersList;
	CWButton* m_pButtonQuit;
	std::function<void(const std::wstring& rsText)> m_OnChatMessageHandler;
	std::function<void(void)> m_OnQuitHandler;
};

