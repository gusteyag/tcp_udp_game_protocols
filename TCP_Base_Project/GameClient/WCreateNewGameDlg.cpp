#include "WCreateNewGameDlg.h"
#include "WText.h"
#include "WListBox.h"
#include "WButton.h"
#include "WEditBox.h"



CWCreateNewGameDlg::CWCreateNewGameDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pTextEditGameName(new CWText(rScene))
	, m_pEditGameName(new CWEditBox(rScene))
	, m_pTextEditPlayersCount(new CWText(rScene))
	, m_pEditPlayersCount(new CWEditBox(rScene))
	, m_pTextEditPlayerName(new CWText(rScene))
	, m_pEditPlayerName(new CWEditBox(rScene))
	, m_pTextListCharacters(new CWText(rScene))
	, m_pListCharacters(new CWListBox(rScene))
	, m_pButtonCreate(new CWButton(rScene))
	, m_pButtonBack(new CWButton(rScene))
{
	AddChild(m_pTextEditGameName);
	AddChild(m_pEditGameName);
	AddChild(m_pTextEditPlayersCount);
	AddChild(m_pEditPlayersCount);
	AddChild(m_pTextEditPlayerName);
	AddChild(m_pEditPlayerName);
	AddChild(m_pTextListCharacters);
	AddChild(m_pListCharacters);
	AddChild(m_pButtonCreate);
	AddChild(m_pButtonBack);

	SetText(L"Create New Game");
	m_pTextEditGameName->SetText(L"Game Name: ");
	m_pTextEditGameName->SetTextHAlignment(eHAlignment_Right);
	m_pTextEditPlayersCount->SetText(L"Players Count: ");
	m_pTextEditPlayersCount->SetTextHAlignment(eHAlignment_Right);
	m_pTextEditPlayerName->SetText(L"Player Name: ");
	m_pTextEditPlayerName->SetTextHAlignment(eHAlignment_Right);
	m_pTextListCharacters->SetText(L"Character: ");
	m_pTextListCharacters->SetTextHAlignment(eHAlignment_Right);
	m_pButtonCreate->SetText(L"Create");
	m_pButtonBack->SetText(L"Back");

	m_pButtonCreate->SetEnabled(false);

	m_pButtonCreate->SetOnClickedHandler([this]() {
		if (m_OnCreateNewGameHandler && m_pEditGameName->GetText().size() && m_pEditPlayersCount->GetText().size() && m_pEditPlayerName->GetText().size() && m_pListCharacters->GetSelectedLine(NULL))
		{
			m_OnCreateNewGameHandler(
				m_pEditGameName->GetText(),
				(sf::Uint32)std::stoul(m_pEditPlayersCount->GetText()),
				m_pEditPlayerName->GetText(),
				(eCharacters)m_pListCharacters->GetSelectedLine(NULL)->front().ui64UserData
			);
		}
	});

	m_pButtonBack->SetOnClickedHandler([this]() {
		if (m_OnBackHandler)
			m_OnBackHandler();
	});
}

CWCreateNewGameDlg::~CWCreateNewGameDlg()
{
}

void CWCreateNewGameDlg::NormalizeSize()
{
	float fWidth = 300;
	float fVSpacing = 20;
	float fTextWidth = 120;
	float fEditWidth = 120;
	float fButtonsHeight = 30;
	float fButtonsWidth = 100;
	float fButtonsHSpacing = 20;

	if (m_pEditGameName->GetText().size() && m_pEditPlayersCount->GetText().size() && m_pEditPlayerName->GetText().size() && m_pListCharacters->GetSelectedLine(NULL))
		m_pButtonCreate->SetEnabled(true);
	else
		m_pButtonCreate->SetEnabled(false);

	m_pEditGameName->SetSize(sf::Vector2f(fEditWidth, 0));
	m_pEditGameName->NormalizeSize();
	m_pEditPlayersCount->SetSize(sf::Vector2f(fEditWidth, 0));
	m_pEditPlayersCount->NormalizeSize();
	m_pEditPlayerName->SetSize(sf::Vector2f(fEditWidth, 0));
	m_pEditPlayerName->NormalizeSize();
	if (!m_pListCharacters->GetColumns().size())
	{
		m_pListCharacters->GetColumns().push_back(ListBoxColumn(L"Character", fEditWidth - (m_pListCharacters->GetInBorderWidth() + m_pListCharacters->GetPadding()) * 2));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Amapola), eCharacters_Amapola));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Rubio), eCharacters_Rubio));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Orquidea), eCharacters_Orquidea));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Prado), eCharacters_Prado));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Celeste), eCharacters_Celeste));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Mora), eCharacters_Mora));
		m_pListCharacters->SetHeaderHeight(0);
		m_pListCharacters->NormalizeSize();
	}
	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fWidth,
		GetWorkingPosition().y + GetPadding() + GetInBorderWidth() +
		(fVSpacing + m_pEditGameName->GetSize().y) * 3 + 
		fVSpacing + m_pListCharacters->GetSize().y + 
		fVSpacing + fButtonsHeight
	));

	m_pTextEditGameName->SetFont(GetFont());
	m_pTextEditGameName->SetFontSize(GetFontSize());
	m_pTextEditGameName->SetSize(sf::Vector2f(fTextWidth, m_pEditGameName->GetSize().y));
	m_pTextEditGameName->SetPosition(sf::Vector2f(0, fVSpacing));

	m_pEditGameName->SetFont(GetFont());
	m_pEditGameName->SetFontSize(GetFontSize());
	m_pEditGameName->SetPosition(sf::Vector2f(fTextWidth, fVSpacing));

	m_pTextEditPlayersCount->SetFont(GetFont());
	m_pTextEditPlayersCount->SetFontSize(GetFontSize());
	m_pTextEditPlayersCount->SetSize(sf::Vector2f(fTextWidth, m_pEditGameName->GetSize().y));
	m_pTextEditPlayersCount->SetPosition(sf::Vector2f(0, fVSpacing + m_pEditGameName->GetSize().y + fVSpacing));

	m_pEditPlayersCount->SetFont(GetFont());
	m_pEditPlayersCount->SetFontSize(GetFontSize());
	m_pEditPlayersCount->SetPosition(sf::Vector2f(fTextWidth, fVSpacing + m_pEditGameName->GetSize().y + fVSpacing));

	m_pTextEditPlayerName->SetFont(GetFont());
	m_pTextEditPlayerName->SetFontSize(GetFontSize());
	m_pTextEditPlayerName->SetSize(sf::Vector2f(fTextWidth, m_pEditGameName->GetSize().y));
	m_pTextEditPlayerName->SetPosition(sf::Vector2f(0, (fVSpacing + m_pEditGameName->GetSize().y) * 2 + fVSpacing));

	m_pEditPlayerName->SetFont(GetFont());
	m_pEditPlayerName->SetFontSize(GetFontSize());
	m_pEditPlayerName->SetPosition(sf::Vector2f(fTextWidth, (fVSpacing + m_pEditGameName->GetSize().y) * 2 + fVSpacing));

	m_pTextListCharacters->SetFont(GetFont());
	m_pTextListCharacters->SetFontSize(GetFontSize());
	m_pTextListCharacters->SetSize(sf::Vector2f(fTextWidth, m_pEditGameName->GetSize().y));
	m_pTextListCharacters->SetPosition(sf::Vector2f(0, (fVSpacing + m_pEditGameName->GetSize().y) * 3 + fVSpacing));

	m_pListCharacters->SetFont(GetFont());
	m_pListCharacters->SetFontSize(GetFontSize());
	m_pListCharacters->SetPosition(sf::Vector2f(fTextWidth, (fVSpacing + m_pEditGameName->GetSize().y) * 3 + fVSpacing));

	m_pButtonCreate->SetFont(GetFont());
	m_pButtonCreate->SetFontSize(GetFontSize());
	m_pButtonCreate->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonCreate->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 2 + fButtonsHSpacing) / 2,
		(fVSpacing + m_pEditGameName->GetSize().y) * 3 + fVSpacing + m_pListCharacters->GetSize().y + fVSpacing
	));

	m_pButtonBack->SetFont(GetFont());
	m_pButtonBack->SetFontSize(GetFontSize());
	m_pButtonBack->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonBack->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 2 + fButtonsHSpacing) / 2 + fButtonsWidth + fButtonsHSpacing,
		(fVSpacing + m_pEditGameName->GetSize().y) * 3 + fVSpacing + m_pListCharacters->GetSize().y + fVSpacing
	));
}

void CWCreateNewGameDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
