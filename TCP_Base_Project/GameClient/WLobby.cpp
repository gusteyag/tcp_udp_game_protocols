#include "WLobby.h"
#include "WText.h"
#include "WChat.h"
#include "WListBox.h"
#include "WButton.h"
#include "Chat.h"


CWLobby::CWLobby(CScene& rScene)
	: CWContainer(rScene)
	, m_pTextChat(new CWText(rScene))
	, m_pChat(new CWChat(rScene))
	, m_pTextPlayersList(new CWText(rScene))
	, m_pPlayersList(new CWListBox(rScene))
	, m_pButtonQuit(new CWButton(rScene))
{
	AddChild(m_pTextChat);
	AddChild(m_pChat);
	AddChild(m_pTextPlayersList);
	AddChild(m_pPlayersList);
	AddChild(m_pButtonQuit);

	SetText(L"Waiting for all Players");
	m_pTextChat->SetText(L"You can chat with other players here:");
	m_pTextChat->SetTextHAlignment(eHAlignment_Left);
	m_pTextChat->SetTextVAlignment(eVAlignment_Bottom);
	m_pTextPlayersList->SetText(L"Existing players list:");
	m_pTextPlayersList->SetTextHAlignment(eHAlignment_Left);
	m_pTextPlayersList->SetTextVAlignment(eVAlignment_Bottom);
	m_pButtonQuit->SetText(L"Quit");

	m_pChat->SetBorderWidth(-1);
	m_pChat->SetHeaderHeight(0);
	m_pChat->SetOnTextEnteredHandler([this](const std::wstring& rsText) {
		if (m_OnChatMessageHandler)
			m_OnChatMessageHandler(rsText);
	});

	m_pButtonQuit->SetOnClickedHandler([this]() {
		if (m_OnQuitHandler)
			m_OnQuitHandler();
	});
}

CWLobby::~CWLobby()
{
}

void CWLobby::UpdatePlayersList(const CGameState& rGameState)
{
	m_GameState = rGameState;
	m_pPlayersList->GetLines().clear();
	for (auto& player : rGameState.GetPlayers())
	{
		m_pPlayersList->GetLines().push_back(std::list<ListBoxItem>());
		m_pPlayersList->GetLines().back().push_back(ListBoxItem(player.second->GetName(), player.first));
		m_pPlayersList->GetLines().back().push_back(ListBoxItem(enum_desc(player.second->GetCharacter())));
	}
}

void CWLobby::UpdateChat(const CChat& rChat)
{
	m_pChat->ClearMessages();
	for (auto& msg : rChat.GetMessages())
	{
		std::wstring sAuthor = L"Unknown";
		if (m_GameState.GetPlayers().count(msg.first))
		{
			sAuthor = m_GameState.GetPlayers().at(msg.first)->GetName();
			sAuthor += L"(";
			sAuthor += enum_desc(m_GameState.GetPlayers().at(msg.first)->GetCharacter());
			sAuthor += L")";
		}
		m_pChat->AddMessage(sAuthor, msg.second);
	}
}

void CWLobby::NormalizeSize()
{
	float fPlayerNameWidth = 300;
	float fPlayerCharacterWidth = 150;
	float fWidth = fPlayerNameWidth + fPlayerCharacterWidth + (m_pPlayersList->GetInBorderWidth() + m_pPlayersList->GetPadding()) * 2;
	float fTextListHeight = 20;
	float fListHeight = 120;
	float fTextChatHeight = 30;
	float fChatHeight = 250;
	float fVSpacing = 20;
	float fButtonsWidth = 100;
	float fButtonsHeight = 30;

	if (!m_pPlayersList->GetColumns().size())
	{
		m_pPlayersList->GetColumns().push_back(ListBoxColumn(L"Name", fPlayerNameWidth));
		m_pPlayersList->GetColumns().push_back(ListBoxColumn(L"Character", fPlayerCharacterWidth));
	}

	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fWidth,
		GetWorkingPosition().y + GetPadding() + GetInBorderWidth() +
		fTextListHeight + fListHeight +
		fTextChatHeight + fChatHeight +
		fVSpacing + fButtonsHeight
	));

	m_pTextPlayersList->SetFont(GetFont());
	m_pTextPlayersList->SetFontSize(GetFontSize() + 2);
	m_pTextPlayersList->SetSize(sf::Vector2f(fWidth, fTextListHeight));
	m_pTextPlayersList->SetPosition(sf::Vector2f(0, 0));

	m_pPlayersList->SetFont(GetFont());
	m_pPlayersList->SetFontSize(GetFontSize());
	m_pPlayersList->SetSize(sf::Vector2f(fWidth, fListHeight));
	m_pPlayersList->SetPosition(sf::Vector2f(0, fTextListHeight));

	m_pTextChat->SetFont(GetFont());
	m_pTextChat->SetFontSize(GetFontSize() + 2);
	m_pTextChat->SetSize(sf::Vector2f(fWidth, fTextChatHeight));
	m_pTextChat->SetPosition(sf::Vector2f(0, fTextListHeight + fListHeight));

	m_pChat->SetFont(GetFont());
	m_pChat->SetFontSize(GetFontSize());
	m_pChat->SetSize(sf::Vector2f(fWidth, fChatHeight));
	m_pChat->SetPosition(sf::Vector2f(0, fTextListHeight + fListHeight + fTextChatHeight));

	m_pButtonQuit->SetFont(GetFont());
	m_pButtonQuit->SetFontSize(GetFontSize());
	m_pButtonQuit->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonQuit->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - fButtonsWidth / 2,
		fTextListHeight + fListHeight + fTextChatHeight + fChatHeight + fVSpacing
	));
}

void CWLobby::Draw()
{
	NormalizeSize();
	__super::Draw();
}
