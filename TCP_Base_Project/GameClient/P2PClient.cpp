#include "P2PClient.h"
#include "GameDefinitions.h"


CP2PClient::CP2PClient(const std::wstring& rsP2PLocalAddress, USHORT nP2PLocalPort, const std::wstring& rsBSSAddress, USHORT nBSSPort, DWORD dwMinimumPlayersCount)
	: m_sP2PLocalAddress(rsP2PLocalAddress)
	, m_nP2PLocalPort(nP2PLocalPort)
	, m_sBSSAddress(rsBSSAddress)
	, m_nBSSPort(nBSSPort)
	, m_dwMinimumPlayersCount(dwMinimumPlayersCount)
	, m_bBSSClientOnceConnected(false)
	, m_bBSSClientDisconnected(true)
	, m_bBSSClientHandshakeSended(false)
	, m_bBSSClientHandshake(false)
	, m_dwP2PPlayerId(0)
	, m_bGameState_InProgress(false)
	, m_bGameState_Processed(false)
	, m_bGameState_NeedToSendOneMoreTime(false)
{
}

CP2PClient::~CP2PClient()
{
	Disconnect();
}

bool CP2PClient::Connect(
	const net_message_cb_t& rOnDisconnectedCB,
	const net_game_info_list_cb_t& rOnGameInfoListChangedCB,
	const net_game_state_cb_t& rOnGameStateChangedCB,
	const net_chat_cb_t& rOnChatChangedCB
)
{
	char szBuff[256] = {};

	m_pP2PListener.reset(new sf::TcpListener());
	m_pP2PListener->setBlocking(false);
	_snprintf_s(szBuff, _TRUNCATE, "%S", m_sP2PLocalAddress.c_str());
	if (m_pP2PListener->listen(m_nP2PLocalPort, szBuff) != sf::Socket::Done)
	{
		Disconnect();
		return false;
	}
	m_pP2PSocketToAccept.reset(new sf::TcpSocket());
	
	if (!ConnectBSSClient())
	{
		Disconnect();
		return false;
	}

	m_OnDisconnectedCB = rOnDisconnectedCB;
	//m_OnGameInfoListChangedCB = rOnGameInfoListChangedCB;
	m_OnGameInfoListChangedCB = std::nullptr_t();
	m_OnGameStateChangedCB = rOnGameStateChangedCB;
	m_OnChatChangedCB = rOnChatChangedCB;
	return true;
}

void CP2PClient::Disconnect()
{
	if (m_pP2PListener)
	{
		m_pP2PListener->close();
		m_pP2PListener.reset(NULL);
	}
	if (m_pP2PSocketToAccept)
		m_pP2PSocketToAccept.reset(NULL);

	DisconnectBSSClient();

	m_dwP2PPlayerId = 0;

	m_mapP2PClientInfo.clear();

	for (auto& connection : m_lstP2PHandshake)
		connection->pSocket->disconnect();
	m_lstP2PHandshake.clear();

	for (auto& connection : m_mapP2PConnections)
		connection.second->pSocket->disconnect();
	m_mapP2PConnections.clear();

	m_pGame.reset(NULL);

	m_Join_AnswerCB = std::nullptr_t();

	m_bGameState_InProgress = false;
	m_setGameState_ConfirmedPlayersId.clear();
	m_GameState_AnswerCB = std::nullptr_t();

	//m_OnDisconnectedCB = std::nullptr_t();
	m_OnGameInfoListChangedCB = std::nullptr_t();
	m_OnGameStateChangedCB = std::nullptr_t();
	m_OnChatChangedCB = std::nullptr_t();
}

bool CP2PClient::Process()
{
	if (!m_OnDisconnectedCB || !m_OnGameStateChangedCB)
		return false;

	bool bSomethingHappened = false;

	// BSSClient processing
	////////////////////////////////////////////////////////////////////////////////////////////////
	if (m_bBSSClientDisconnected)
	{
		if (!m_bBSSClientOnceConnected && m_BSSClientConnectionClock.getElapsedTime().asSeconds() >= 5)
		{
			if (!_ConnectBSSClient())
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to connect to BSS server.");
				return false;
			}
		}
	}
	else
	{
		if (!m_bBSSClientHandshakeSended)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)eBSSPacketType_Handshake << (sf::Uint32)BSS_CURRENT_PROTOCOL_VERSION_BCD;
			(*pPacket) << m_sP2PLocalAddress << m_nP2PLocalPort << (sf::Uint32)m_dwMinimumPlayersCount;
			m_lstBSSClientOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));

			m_bBSSClientHandshakeSended = true;
		}

		sf::TcpSocket::Status status = m_BSSClientSocket.receive(m_BSSClientInPacket);
		if (status != sf::TcpSocket::Done)
		{
			if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to receive data from BSS server.");
				return false;
			}
		}
		else
		{
			bSomethingHappened = true;
			if (!ProcessBSSClientProtocol())
				return false;
			m_BSSClientInPacket.clear();
		}

		if (!m_lstBSSClientOutPackets.empty())
		{
			sf::TcpSocket::Status status = m_BSSClientSocket.send(*m_lstBSSClientOutPackets.front());
			if (status != sf::TcpSocket::Done)
			{
				if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
				{
					Disconnect();
					if (m_OnDisconnectedCB)
						m_OnDisconnectedCB(L"Failed to send data to BSS server.");
					return false;
				}
			}
			else
			{
				bSomethingHappened = true;
				m_lstBSSClientOutPackets.pop_front();
			}
		}
	}

	// P2PConnection accepting
	////////////////////////////////////////////////////////////////////////////////////////////////
	sf::TcpSocket::Status status = m_pP2PListener->accept(*m_pP2PSocketToAccept.get());
	if (status != sf::TcpSocket::Done)
	{
		if (status == sf::TcpSocket::Error)
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Failed to accept peer connection.");
			return false;
		}
		else
		{
			// Do nothing
		}
	}
	else
	{
		m_pP2PSocketToAccept->setBlocking(false);
		std::unique_ptr<P2PConnection> pConnection(new P2PConnection());
		pConnection->pSocket.reset(m_pP2PSocketToAccept.release());
		m_pP2PSocketToAccept.reset(new sf::TcpSocket());
		pConnection->eType = eP2PConnectionType_In;
		m_lstP2PHandshake.push_back(std::move(pConnection));
	}

	// P2PConnection handshake
	////////////////////////////////////////////////////////////////////////////////////////////////
	std::list<std::unique_ptr<P2PConnection> >::iterator itP2PHandshake = m_lstP2PHandshake.begin();
	while (itP2PHandshake != m_lstP2PHandshake.end())
	{
		sf::TcpSocket::Status status = (*itP2PHandshake)->pSocket->receive((*itP2PHandshake)->InPacket);
		if (status != sf::TcpSocket::Done)
		{
			if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
			{
				bSomethingHappened = true;
				(*itP2PHandshake)->pSocket->disconnect();
				itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
				if (!(*itP2PHandshake)->bRejected || (status == sf::TcpSocket::Error))
				{
					Disconnect();
					if (m_OnDisconnectedCB)
						m_OnDisconnectedCB(L"Other peer disconnected unexpectedly while handshake.");
					return false;
				}
				continue;
			}
		}
		else
		{
			if ((*itP2PHandshake)->bRejected)
			{
				(*itP2PHandshake)->pSocket->disconnect();
				itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
				continue;
			}

			bSomethingHappened = true;
			sf::Uint32 nData = 0;
			(*itP2PHandshake)->InPacket >> nData;
			if (nData != eP2PPacketType_Handshake)
			{
				(*itP2PHandshake)->pSocket->disconnect();
				itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
				continue;
			}

			if ((*itP2PHandshake)->eType == eP2PConnectionType_In)
			{
				eResult eRes = eResult_Success;
				(*itP2PHandshake)->InPacket >> nData;
				if (nData != P2P_CURRENT_PROTOCOL_VERSION_BCD)
				{
					eRes = eResult_Fail;
				}
				else
				{
					(*itP2PHandshake)->InPacket >> nData; // PlayerId
					if ((nData == m_dwP2PPlayerId) || m_mapP2PConnections.count(nData)) // Already exists
					{
						eRes = eResult_AlreadyExists;
					}
				}
				(*itP2PHandshake)->InPacket.clear();

				std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
				(*pPacket) << (sf::Uint32)eP2PPacketType_Handshake << (sf::Uint32)eRes;
				(*itP2PHandshake)->lstOutPackets.push_back(std::move(pPacket));

				if (eRes == eResult_Success)
				{
					(*itP2PHandshake)->nPlayerId = nData;
				}
				else
				{
					(*itP2PHandshake)->bRejected = true;
					++itP2PHandshake;
					continue;
				}
			}
			else if ((*itP2PHandshake)->eType == eP2PConnectionType_Out)
			{
				(*itP2PHandshake)->InPacket >> nData;
				(*itP2PHandshake)->InPacket.clear();

				if (nData != eResult_Success)
				{
					(*itP2PHandshake)->pSocket->disconnect();
					itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
					Disconnect();
					if (m_OnDisconnectedCB)
						m_OnDisconnectedCB(L"Handshake to peer failed.");
					return false;
				}
			}

			m_mapP2PConnections[(*itP2PHandshake)->nPlayerId].reset(itP2PHandshake->release());
			itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
			CheckP2PConnections();
			continue;
		}

		if (!(*itP2PHandshake)->lstOutPackets.empty())
		{
			sf::TcpSocket::Status status = (*itP2PHandshake)->pSocket->send(*(*itP2PHandshake)->lstOutPackets.front());
			if (status != sf::TcpSocket::Done)
			{
				if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
				{
					bSomethingHappened = true;
					(*itP2PHandshake)->pSocket->disconnect();
					itP2PHandshake = m_lstP2PHandshake.erase(itP2PHandshake);
					if (!(*itP2PHandshake)->bRejected || (status == sf::TcpSocket::Error))
					{
						Disconnect();
						if (m_OnDisconnectedCB)
							m_OnDisconnectedCB(L"Other peer disconnected unexpectedly while handshake.");
						return false;
					}
					continue;
				}
			}
			else
			{
				bSomethingHappened = true;
				(*itP2PHandshake)->lstOutPackets.pop_front();
			}
		}
	}

	// P2PConnection processing
	////////////////////////////////////////////////////////////////////////////////////////////////
	std::map<DWORD, std::unique_ptr<P2PConnection> >::iterator itP2PConnection = m_mapP2PConnections.begin();
	while (itP2PConnection != m_mapP2PConnections.end())
	{
		sf::TcpSocket::Status status = itP2PConnection->second->pSocket->receive(itP2PConnection->second->InPacket);
		if (status != sf::TcpSocket::Done)
		{
			if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
			{
				bSomethingHappened = true;
				DWORD dwPlayerId = itP2PConnection->second->nPlayerId;
				itP2PConnection->second->pSocket->disconnect();
				itP2PConnection = m_mapP2PConnections.erase(itP2PConnection);
				if (!OnPeerDisconnected(dwPlayerId))
					return false;
				else
					continue;
			}
		}
		else
		{
			bSomethingHappened = true;
			if (!ProcessP2PConnectionProtocol(*itP2PConnection->second))
				return false;
			itP2PConnection->second->InPacket.clear();
		}

		if (!itP2PConnection->second->lstOutPackets.empty())
		{
			sf::TcpSocket::Status status = itP2PConnection->second->pSocket->send(*itP2PConnection->second->lstOutPackets.front());
			if (status != sf::TcpSocket::Done)
			{
				if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
				{
					bSomethingHappened = true;
					DWORD dwPlayerId = itP2PConnection->second->nPlayerId;
					itP2PConnection->second->pSocket->disconnect();
					itP2PConnection = m_mapP2PConnections.erase(itP2PConnection);
					if (!OnPeerDisconnected(dwPlayerId))
						return false;
					else
						continue;
				}
			}
			else
			{
				bSomethingHappened = true;
				itP2PConnection->second->lstOutPackets.pop_front();
			}
		}

		++itP2PConnection;
	}

	return bSomethingHappened;
}

bool CP2PClient::ConnectBSSClient()
{
	m_BSSClientSocket.setBlocking(false);

	m_bBSSClientOnceConnected = false;
	m_bBSSClientDisconnected = true;
	if (!_ConnectBSSClient())
	{
		Disconnect();
		return false;
	}
	return true;
}

bool CP2PClient::_ConnectBSSClient()
{
	char szBuff[256] = {};
	_snprintf_s(szBuff, _TRUNCATE, "%S", m_sBSSAddress.c_str());
	sf::TcpSocket::Status status = m_BSSClientSocket.connect(szBuff, m_nBSSPort);
	if (status == sf::TcpSocket::Error)
		return false;
	m_bBSSClientOnceConnected = true;
	m_bBSSClientDisconnected = false;
	m_BSSClientConnectionClock.restart();
	return true;
}

void CP2PClient::DisconnectBSSClient()
{
	m_bBSSClientDisconnected = true;
	m_BSSClientSocket.disconnect();
	m_bBSSClientHandshakeSended = false;
	m_bBSSClientHandshake = false;
	m_BSSClientInPacket.clear();
	m_lstBSSClientOutPackets.clear();
}

void CP2PClient::CheckP2PConnections()
{
	if (!m_mapP2PClientInfo.size())
		return;
	std::map<DWORD, std::unique_ptr<P2PConnection> >::iterator itConnection = m_mapP2PConnections.begin();
	while (itConnection != m_mapP2PConnections.end())
	{
		if (!m_mapP2PClientInfo.count(itConnection->second->nPlayerId))
		{
			itConnection->second->pSocket->disconnect();
			itConnection = m_mapP2PConnections.erase(itConnection);
		}
		else
		{
			++itConnection;
		}
	}
	if (m_mapP2PClientInfo.size() == m_mapP2PConnections.size())
	{
		if (!m_pGame.get())
		{
			m_setLeftGamePlayerIds.clear();
			m_pGame.reset(new CGameWrapper(1, L"P2P Network Game", (DWORD)m_mapP2PClientInfo.size(), (*this)));
			for (auto& connection : m_mapP2PConnections)
			{
				std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
				(*pPacket) << (sf::Uint32)eP2PPacketType_ReadyToStart;
				connection.second->lstOutPackets.push_back(std::move(pPacket));
			}
		}
		bool bReadyToStart = true;
		for (auto& connection : m_mapP2PConnections)
		{
			if (!connection.second->bReadyToStart)
			{
				bReadyToStart = false;
				break;
			}
		}
		if (bReadyToStart)
			m_OnGameStateChangedCB(m_pGame->GetState());
	}
}

bool CP2PClient::ProcessBSSClientProtocol()
{
	if (!m_bBSSClientHandshake)
	{
		sf::Uint32 nData = 0;
		m_BSSClientInPacket >> nData;
		if (nData != eBSSPacketType_Handshake)
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Unexpected packet type. Expected to eBSSPacketType_Handshake.");
			return false;
		}
		m_BSSClientInPacket >> nData;
		if (nData != (sf::Uint32)eResult_Success)
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Incompatible protocol version on BSS server.");
			return false;
		}
		m_BSSClientInPacket >> nData;
		m_dwP2PPlayerId = nData;
		m_bBSSClientHandshake = true;
		return true;
	}

	sf::Uint32 nPacketType = 0;
	m_BSSClientInPacket >> nPacketType;
	switch (nPacketType)
	{
	case eBSSPacketType_StartGame:
	{
		m_mapP2PClientInfo.clear();
		sf::Uint32 nCount = 0;
		m_BSSClientInPacket >> nCount;
		for (sf::Uint32 i = 0; i < nCount; ++i)
		{
			CP2PClientInfo info;
			if (!info.Deserialize(m_BSSClientInPacket))
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to deserialize P2PClientInfo.");
				return false;
			}
			else
			{
				m_mapP2PClientInfo[info.GetPlayerId()] = info;
			}
		}

		for (auto& info : m_mapP2PClientInfo)
		{
			if (info.second.GetPlayerId() == m_dwP2PPlayerId) // Just in case
				continue;
			if (info.second.GetPlayerId() > m_dwP2PPlayerId) // eP2PConnectionType_In connections
				continue;

			std::unique_ptr<P2PConnection> pConnection(new P2PConnection());
			pConnection->eType = eP2PConnectionType_Out;
			pConnection->nPlayerId = info.second.GetPlayerId();
			pConnection->pSocket.reset(new sf::TcpSocket());
			pConnection->pSocket->setBlocking(false);
			char szBuff[256] = {};
			_snprintf_s(szBuff, _TRUNCATE, "%S", info.second.GetAddress().c_str());
			sf::TcpSocket::Status status = pConnection->pSocket->connect(szBuff, info.second.GetPort());
			if (status == sf::TcpSocket::Error)
			{
				Disconnect();
				if (m_OnDisconnectedCB)
					m_OnDisconnectedCB(L"Failed to connect to peer.");
				return false;
			}
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint32)eP2PPacketType_Handshake << (sf::Uint32)P2P_CURRENT_PROTOCOL_VERSION_BCD << (sf::Uint32)m_dwP2PPlayerId;
			pConnection->lstOutPackets.push_back(std::move(pPacket));
			m_lstP2PHandshake.push_back(std::move(pConnection));
		}

		DisconnectBSSClient();
		CheckP2PConnections();
	}
	break;

	default:
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Unknown packet type.");
		return false;
	}
	break;
	}

	return true;
}

bool CP2PClient::ProcessP2PConnectionProtocol(P2PConnection& rConnection)
{
	sf::Uint32 nPacketType = 0;
	rConnection.InPacket >> nPacketType;
	switch (nPacketType)
	{
	case eP2PPacketType_ReadyToStart:
	{
		rConnection.bReadyToStart = true;
		CheckP2PConnections();
	}
	break;

	case eP2PPacketType_JoinToGame_Request:
	{
		if (!m_pGame.get())
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Failed to accept JoinToGame request. Game object not exists.");
			return false;
		}
		else
		{
			sf::Uint32 nPlayerId = 0;
			std::wstring sPlayerName;
			sf::Uint32 nCharacter = 0;
			rConnection.InPacket >> nPlayerId >> sPlayerName >> nCharacter;
			eResult eRes = m_pGame->PlayerTriesToJoin(nPlayerId, sPlayerName, enum_check((eCharacters)nCharacter));
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint32)eP2PPacketType_JoinToGame_Answer << (sf::Uint32)eRes;
			rConnection.lstOutPackets.push_back(std::move(pPacket));

			if (eRes == eResult_Success)
			{
				if (!SendGameStateToOthers(net_answer_cb_t([this](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); })))
					return false;
			}
		}
	}
	break;

	case eP2PPacketType_JoinToGame_Answer:
	{
		sf::Uint32 nResult = eResult_Fail;
		rConnection.InPacket >> nResult;
		m_Join_AnswerCB((eResult)nResult, 1, ((nResult == eResult_Success) ? m_dwP2PPlayerId : 0));
		m_Join_AnswerCB = std::nullptr_t();
	}
	break;

	case eP2PPacketType_GameState:
	{
		if (!ReceiveGameState(rConnection.nPlayerId, rConnection.InPacket))
			return false;
	}
	break;

	case eP2PPacketType_GameState_Confirmation:
	{
		if (!ReceiveGameStateConfirmation(rConnection.nPlayerId))
			return false;
	}
	break;

	case eP2PPacketType_ChatMessage:
	{
		if (!m_pGame.get())
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Failed to accept ChatMessage request. Game object not exists.");
			return false;
		}
		else
		{
			sf::Uint32 nPlayerId = 0;
			std::wstring sMessage;
			rConnection.InPacket >> nPlayerId >> sMessage;
			m_pGame->AddChatMessage(nPlayerId, sMessage);
		}
	}
	break;

	default:
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Unknown packet type.");
		return false;
	}
	break;
	}

	return true;
}

void CP2PClient::Request_Join(const net_join_answer_cb_t& rAnswerCB, DWORD dwGameId, const std::wstring& rsPlayerName, eCharacters eCharacter)
{
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady, 0, 0);
		return;
	}
	if (m_pGame->GetState().GetGameState() != eGameStates_Created)
	{
		rAnswerCB(eResult_InappropriateGameState, 0, 0);
		return;
	}
	DWORD dwMinPlayerId = 0;
	for (auto& connection : m_mapP2PConnections)
	{
		if (!dwMinPlayerId || connection.second->nPlayerId < dwMinPlayerId)
			dwMinPlayerId = connection.second->nPlayerId;
	}
	if (m_dwP2PPlayerId < dwMinPlayerId)
	{
		eResult eRes = m_pGame->PlayerTriesToJoin(m_dwP2PPlayerId, rsPlayerName, eCharacter);
		if (eRes == eResult_Success)
		{
			SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success, m_pGame->GetId(), m_dwP2PPlayerId); }));
		}
		else
		{
			rAnswerCB(eRes, m_pGame->GetId(), m_dwP2PPlayerId);
		}
	}
	else
	{
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		(*pPacket) << (sf::Uint32)eP2PPacketType_JoinToGame_Request << (sf::Uint32)m_dwP2PPlayerId << rsPlayerName << (sf::Uint32)eCharacter;
		m_mapP2PConnections.at(dwMinPlayerId)->lstOutPackets.push_back(std::move(pPacket));

		m_Join_AnswerCB = rAnswerCB;
	}
}

void CP2PClient::Request_LeftGame(const net_answer_cb_t& rAnswerCB)
{
	Disconnect();
	rAnswerCB(eResult_Success);
	//if (m_bGameState_InProgress)
	//{
	//	rAnswerCB(eResult_NotReady);
	//	return;
	//}
	//if (!m_pGame.get())
	//{
	//	rAnswerCB(eResult_NotReady);
	//	return;
	//}
	//eResult eRes = m_pGame->PlayerLeftGame(m_dwP2PPlayerId, FALSE);
	//if (eRes != eResult_Success)
	//{
	//	rAnswerCB(eRes);
	//	return;
	//}
	//SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_RollDices(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToRollDices(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_ClueCard(const net_answer_cb_t& rAnswerCB, ULONG64 nAssumedCard)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToRequestClueCard(m_dwP2PPlayerId, nAssumedCard);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_ClueCardAnswer(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToAnswerClueCard(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_ReceiveClueCardAnswer(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToReceiveClueCardAnswer(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_Move(const net_answer_cb_t& rAnswerCB, const std::list<POINT>& rlstTrack)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToMove(m_dwP2PPlayerId, rlstTrack);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_MakeDeduction(const net_answer_cb_t& rAnswerCB, eWeapons eWeapon, eCharacters eCharacter)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToMakeDeduction(m_dwP2PPlayerId, eWeapon, eCharacter);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_AnswerToDeduction(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToAnswerToDeduction(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_ReceiveDeductionAnswers(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToReceiveDeductionAnswers(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_MakeAccusation(const net_answer_cb_t& rAnswerCB, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToMakeAccusation(m_dwP2PPlayerId, eRoom, eWeapon, eCharacter);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_SkipAccusation(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	if (!m_pGame.get())
	{
		rAnswerCB(eResult_NotReady);
		return;
	}
	eResult eRes = m_pGame->PlayerTriesToSkipAccusation(m_dwP2PPlayerId);
	if (eRes != eResult_Success)
	{
		rAnswerCB(eRes);
		return;
	}
	SendGameStateToOthers(net_answer_cb_t([this, rAnswerCB](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); rAnswerCB(eResult_Success); }));
}

void CP2PClient::Request_ChatMessage(const std::wstring& rsMessage)
{
	if (!m_pGame.get())
		return;
	for (auto& connection : m_mapP2PConnections)
	{
		if (connection.second->nPlayerId == m_dwP2PPlayerId)
			continue;
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		(*pPacket) << (sf::Uint32)eP2PPacketType_ChatMessage << (sf::Uint32)m_dwP2PPlayerId << rsMessage;
		connection.second->lstOutPackets.push_back(std::move(pPacket));
	}
	m_pGame->AddChatMessage(m_dwP2PPlayerId, rsMessage);
}

bool CP2PClient::SendGameStateToOthers(const net_answer_cb_t& rAnswerCB)
{
	if (m_bGameState_InProgress)
	{
		m_bGameState_NeedToSendOneMoreTime = true;
		//Disconnect();
		//if (m_OnDisconnectedCB)
		//	m_OnDisconnectedCB(L"Game state sending failed.");
		//return false;
		return true;
	}
	if (!m_pGame.get())
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Game state sending failed.");
		return false;
	}
	bool bSended = false;
	for (auto& connection : m_mapP2PConnections)
	{
		if (connection.second->nPlayerId == m_dwP2PPlayerId)
			continue;

		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		(*pPacket) << (sf::Uint32)eP2PPacketType_GameState;
		if (m_pGame->GetStateUnsafe().Serialize(*pPacket))
		{
			connection.second->lstOutPackets.push_back(std::move(pPacket));
			bSended = true;
		}
		else
		{
			Disconnect();
			if (m_OnDisconnectedCB)
				m_OnDisconnectedCB(L"Game state serialization failed.");
			return false;
		}
	}
	if (bSended)
	{
		m_setGameState_ConfirmedPlayersId.insert(m_dwP2PPlayerId);
		m_GameState_AnswerCB = rAnswerCB;
		m_bGameState_InProgress = true;
		m_bGameState_Processed = true;
	}
	else
	{
		rAnswerCB(eResult_Success);
	}
	return true;
}

bool CP2PClient::ReceiveGameState(DWORD dwPlayerId, sf::Packet& rPacket)
{
	if (m_bGameState_InProgress && m_bGameState_Processed)
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Game state receiving failed.");
		return false;
	}

	m_bGameState_InProgress = true;

	if (!m_pGame.get())
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Game state receiving failed.");
		return false;
	}
	if (m_pGame.get() && m_pGame->GetStateUnsafe().Deserialize(rPacket))
	{
		for (auto& id : m_setLeftGamePlayerIds)
			m_pGame->PlayerLeftGame(id, TRUE);
		
		for (auto& connection : m_mapP2PConnections)
		{
			if (connection.second->nPlayerId == m_dwP2PPlayerId)
				continue;
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint32)eP2PPacketType_GameState_Confirmation;
			connection.second->lstOutPackets.push_back(std::move(pPacket));
		}
		m_setGameState_ConfirmedPlayersId.insert(m_dwP2PPlayerId);
		m_setGameState_ConfirmedPlayersId.insert(dwPlayerId);
		m_GameState_AnswerCB = net_answer_cb_t([this](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); });
		m_bGameState_Processed = true;
		if (!CheckConfirmation())
			return false;
	}
	else
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Game state deserialization failed.");
	}
	return true;
}

bool CP2PClient::ReceiveGameStateConfirmation(DWORD dwPlayerId)
{
	m_bGameState_InProgress = true;

	if (!m_pGame.get())
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Game state confirmation receiving failed.");
		return false;
	}
	m_setGameState_ConfirmedPlayersId.insert(dwPlayerId);

	
	return CheckConfirmation();
}

bool CP2PClient::CheckConfirmation()
{
	if (!m_bGameState_Processed)
		return true;

	std::list<DWORD> lstExpectedPlayersId;
	for (auto& connection : m_mapP2PConnections)
	{
		lstExpectedPlayersId.push_back(connection.second->nPlayerId);
	}
	if (m_setGameState_ConfirmedPlayersId.size() >= lstExpectedPlayersId.size())
	{
		bool bConfirmed = true;
		for (auto& id : lstExpectedPlayersId)
		{
			if (!m_setGameState_ConfirmedPlayersId.count(id))
			{
				bConfirmed = false;
				break;
			}
		}
		if (bConfirmed)
		{
			m_setGameState_ConfirmedPlayersId.clear();
			m_bGameState_InProgress = false;
			m_bGameState_Processed = false;
			m_GameState_AnswerCB(eResult_Success);
			m_GameState_AnswerCB = std::nullptr_t();
			if (m_bGameState_NeedToSendOneMoreTime)
			{
				m_bGameState_NeedToSendOneMoreTime = false;
				if (!SendGameStateToOthers(net_answer_cb_t([this](eResult eRes) { m_OnGameStateChangedCB(m_pGame->GetState()); })))
					return false;
			}
		}
	}
	return true;
}

bool CP2PClient::OnPeerDisconnected(DWORD dwPlayerId)
{
	if (m_pGame.get() &&
		(m_pGame->GetState().GetGameState() != eGameStates_Created) &&
		m_pGame->GetState().GetPlayers().count(dwPlayerId))
	{
		m_setLeftGamePlayerIds.insert(dwPlayerId);
		m_pGame->PlayerLeftGame(dwPlayerId, TRUE);
		m_OnGameStateChangedCB(m_pGame->GetState());
		CheckConfirmation();

		//if (m_pGame->GetState().GetPlayers().at(dwPlayerId)->GetState() != ePlayerStates_LeftGame)
		//{
		//	Disconnect();
		//	if (m_OnDisconnectedCB)
		//		m_OnDisconnectedCB(L"Other peer disconnected unexpectedly.");
		//	return false;
		//}
	}
	else
	{
		Disconnect();
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(L"Other peer disconnected unexpectedly.");
		return false;
	}
	return true;
}
