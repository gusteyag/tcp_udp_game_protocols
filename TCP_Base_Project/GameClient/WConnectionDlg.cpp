#include "WConnectionDlg.h"
#include "WText.h"
#include "WButton.h"
#include "WEditBox.h"


CWConnectionDlg::CWConnectionDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pRadioServer(new CWButton(rScene))
	, m_pRadioP2P(new CWButton(rScene))
	, m_pEditServer(new CWEditBox(rScene))
	, m_pEditServerPort(new CWEditBox(rScene))
	, m_pEditBSS(new CWEditBox(rScene))
	, m_pEditBSSPort(new CWEditBox(rScene))
	, m_pEditLocal(new CWEditBox(rScene))
	, m_pEditLocalPort(new CWEditBox(rScene))
	, m_pEditMinPlayersCount(new CWEditBox(rScene))
	, m_pTextServerOrBSS(new CWText(rScene))
	, m_pTextServerOrBSSPort(new CWText(rScene))
	, m_pTextLocal(new CWText(rScene))
	, m_pTextLocalPort(new CWText(rScene))
	, m_pTextMinPlayersCount(new CWText(rScene))
	, m_pButtonConnect(new CWButton(rScene))
{
	AddChild(m_pRadioServer);
	AddChild(m_pRadioP2P);
	AddChild(m_pTextServerOrBSS);
	AddChild(m_pEditServer);
	AddChild(m_pEditBSS);
	AddChild(m_pTextServerOrBSSPort);
	AddChild(m_pEditServerPort);
	AddChild(m_pEditBSSPort);
	AddChild(m_pTextLocal);
	AddChild(m_pEditLocal);
	AddChild(m_pTextLocalPort);
	AddChild(m_pEditLocalPort);
	AddChild(m_pTextMinPlayersCount);
	AddChild(m_pEditMinPlayersCount);
	AddChild(m_pButtonConnect);

	SetText(L"Connection Parameters");
	m_pRadioServer->SetText(L"Client/Server");
	m_pRadioP2P->SetText(L"P2P");
	m_pTextServerOrBSS->SetText(L"Server Address: ");
	m_pEditServer->SetText(L"127.0.0.1");
	m_pEditBSS->SetText(L"127.0.0.1");
	m_pTextServerOrBSSPort->SetText(L"Server Port: ");
	m_pEditServerPort->SetText(L"55555");
	m_pEditBSSPort->SetText(L"33333");
	m_pTextLocal->SetText(L"Local Address: ");
	m_pEditLocal->SetText(L"127.0.0.1");
	m_pTextLocalPort->SetText(L"Local Port: ");
	m_pEditLocalPort->SetText(L"7777");
	m_pTextMinPlayersCount->SetText(L"Minimum Players Count: ");
	m_pEditMinPlayersCount->SetText(L"3");
	m_pButtonConnect->SetText(L"Connect");

	m_pRadioServer->SetStyleRadio(true);
	m_pRadioP2P->SetStyleRadio(true);
	m_pRadioServer->SetSelected(true);

	m_pEditServer->SetVisible(true);
	m_pEditServerPort->SetVisible(true);
	m_pEditBSS->SetVisible(false);
	m_pEditBSSPort->SetVisible(false);
	m_pTextLocal->SetVisible(false);
	m_pEditLocal->SetVisible(false);
	m_pTextLocalPort->SetVisible(false);
	m_pEditLocalPort->SetVisible(false);
	m_pTextMinPlayersCount->SetVisible(false);
	m_pEditMinPlayersCount->SetVisible(false);

	m_pRadioServer->SetOnSelectedHandler([this]() {
		m_pTextServerOrBSS->SetText(L"Server Address: ");
		m_pTextServerOrBSSPort->SetText(L"Server Port: ");
		m_pEditServer->SetVisible(true);
		m_pEditServerPort->SetVisible(true);
		m_pEditBSS->SetVisible(false);
		m_pEditBSSPort->SetVisible(false);
		m_pTextLocal->SetVisible(false);
		m_pEditLocal->SetVisible(false);
		m_pTextLocalPort->SetVisible(false);
		m_pEditLocalPort->SetVisible(false);
		m_pTextMinPlayersCount->SetVisible(false);
		m_pEditMinPlayersCount->SetVisible(false);
	});
	m_pRadioP2P->SetOnSelectedHandler([this]() {
		m_pTextServerOrBSS->SetText(L"BSS Address: ");
		m_pTextServerOrBSSPort->SetText(L"BSS Port: ");
		m_pEditServer->SetVisible(false);
		m_pEditServerPort->SetVisible(false);
		m_pEditBSS->SetVisible(true);
		m_pEditBSSPort->SetVisible(true);
		m_pTextLocal->SetVisible(true);
		m_pEditLocal->SetVisible(true);
		m_pTextLocalPort->SetVisible(true);
		m_pEditLocalPort->SetVisible(true);
		m_pTextMinPlayersCount->SetVisible(true);
		m_pEditMinPlayersCount->SetVisible(true);
	});
	m_pButtonConnect->SetOnClickedHandler([this]() { CallOnConnectHandler(); });
}

CWConnectionDlg::~CWConnectionDlg()
{
}

void CWConnectionDlg::CallOnConnectHandler()
{
	try
	{
		if (m_OnConnectHandler)
		{
			eNetworkType eType = m_pRadioServer->IsSelected() ? eNetworkType_ClientServer : eNetworkType_P2P;
			std::wstring sServerOrBSSAddress = (eType == eNetworkType_ClientServer) ? m_pEditServer->GetText() : m_pEditBSS->GetText();
			sf::Uint16 uiServerOrBSSPort = (sf::Uint16)std::stoul((eType == eNetworkType_ClientServer) ? m_pEditServerPort->GetText() : m_pEditBSSPort->GetText());
			std::wstring sLocalAddress = m_pEditLocal->GetText();
			sf::Uint16 uiLocalPort = (sf::Uint16)std::stoul(m_pEditLocalPort->GetText());
			sf::Uint32 uiMinPlayersCount = (sf::Uint32)std::stoul(m_pEditMinPlayersCount->GetText());
			m_OnConnectHandler(eType, sServerOrBSSAddress, uiServerOrBSSPort, sLocalAddress, uiLocalPort, uiMinPlayersCount);
		}
	}
	catch (std::exception&)
	{
		// TODO: Error message
	}
}

void CWConnectionDlg::NormalizeSize()
{
	float fButtonsHeight = 30;
	float fLeftSpacing = 20;
	float fTextWidth = 200;
	float fEditWidth = 300;
	float fEditPortWidth = 100;
	float fTopSpacing = 40;
	float fHeight = 20;
	float fVSpacing = 20;

	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fLeftSpacing * 2 + fTextWidth + fEditWidth,
		GetWorkingPosition().y + fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 5 - fVSpacing + fTopSpacing + fButtonsHeight + GetPadding() + GetInBorderWidth()
	));

	m_pRadioServer->SetFont(GetFont());
	m_pRadioServer->SetFontSize(GetFontSize());
	m_pRadioServer->SetSize(sf::Vector2f(GetWorkingSize().x / 2, fButtonsHeight));
	m_pRadioServer->SetPosition(sf::Vector2f(0, 0));

	m_pRadioP2P->SetFont(GetFont());
	m_pRadioP2P->SetFontSize(GetFontSize());
	m_pRadioP2P->SetSize(sf::Vector2f(GetWorkingSize().x / 2, fButtonsHeight));
	m_pRadioP2P->SetPosition(sf::Vector2f(GetWorkingSize().x / 2, 0));

	m_pTextServerOrBSS->SetFont(GetFont());
	m_pTextServerOrBSS->SetFontSize(GetFontSize());
	m_pTextServerOrBSS->SetTextHAlignment(eHAlignment_Right);
	m_pTextServerOrBSS->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextServerOrBSS->SetPosition(sf::Vector2f(fLeftSpacing, fButtonsHeight + fTopSpacing));

	m_pEditServer->SetFont(GetFont());
	m_pEditServer->SetFontSize(GetFontSize());
	m_pEditServer->SetSize(sf::Vector2f(fEditWidth, fHeight));
	m_pEditServer->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing));

	m_pEditBSS->SetFont(GetFont());
	m_pEditBSS->SetFontSize(GetFontSize());
	m_pEditBSS->SetSize(sf::Vector2f(fEditWidth, fHeight));
	m_pEditBSS->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing));

	m_pTextServerOrBSSPort->SetFont(GetFont());
	m_pTextServerOrBSSPort->SetFontSize(GetFontSize());
	m_pTextServerOrBSSPort->SetTextHAlignment(eHAlignment_Right);
	m_pTextServerOrBSSPort->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextServerOrBSSPort->SetPosition(sf::Vector2f(fLeftSpacing, fButtonsHeight + fTopSpacing + fHeight + fVSpacing));

	m_pEditServerPort->SetFont(GetFont());
	m_pEditServerPort->SetFontSize(GetFontSize());
	m_pEditServerPort->SetSize(sf::Vector2f(fEditPortWidth, fHeight));
	m_pEditServerPort->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing + fHeight + fVSpacing));

	m_pEditBSSPort->SetFont(GetFont());
	m_pEditBSSPort->SetFontSize(GetFontSize());
	m_pEditBSSPort->SetSize(sf::Vector2f(fEditPortWidth, fHeight));
	m_pEditBSSPort->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing + fHeight + fVSpacing));

	m_pTextLocal->SetFont(GetFont());
	m_pTextLocal->SetFontSize(GetFontSize());
	m_pTextLocal->SetTextHAlignment(eHAlignment_Right);
	m_pTextLocal->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextLocal->SetPosition(sf::Vector2f(fLeftSpacing, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 2));

	m_pEditLocal->SetFont(GetFont());
	m_pEditLocal->SetFontSize(GetFontSize());
	m_pEditLocal->SetSize(sf::Vector2f(fEditWidth, fHeight));
	m_pEditLocal->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 2));

	m_pTextLocalPort->SetFont(GetFont());
	m_pTextLocalPort->SetFontSize(GetFontSize());
	m_pTextLocalPort->SetTextHAlignment(eHAlignment_Right);
	m_pTextLocalPort->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextLocalPort->SetPosition(sf::Vector2f(fLeftSpacing, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 3));

	m_pEditLocalPort->SetFont(GetFont());
	m_pEditLocalPort->SetFontSize(GetFontSize());
	m_pEditLocalPort->SetSize(sf::Vector2f(fEditPortWidth, fHeight));
	m_pEditLocalPort->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 3));

	m_pTextMinPlayersCount->SetFont(GetFont());
	m_pTextMinPlayersCount->SetFontSize(GetFontSize());
	m_pTextMinPlayersCount->SetTextHAlignment(eHAlignment_Right);
	m_pTextMinPlayersCount->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextMinPlayersCount->SetPosition(sf::Vector2f(fLeftSpacing, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 4));

	m_pEditMinPlayersCount->SetFont(GetFont());
	m_pEditMinPlayersCount->SetFontSize(GetFontSize());
	m_pEditMinPlayersCount->SetSize(sf::Vector2f(fEditPortWidth, fHeight));
	m_pEditMinPlayersCount->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 4));

	m_pButtonConnect->SetFont(GetFont());
	m_pButtonConnect->SetFontSize(GetFontSize());
	m_pButtonConnect->SetSize(sf::Vector2f(GetWorkingSize().x / 3, fButtonsHeight));
	m_pButtonConnect->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - m_pButtonConnect->GetSize().x / 2,
		fButtonsHeight + fTopSpacing + (fHeight + fVSpacing) * 5 - fVSpacing + fTopSpacing
	));
}

void CWConnectionDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
