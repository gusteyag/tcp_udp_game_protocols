#pragma once
#include "WContainer.h"
#include <functional>
#include <map>
#include "GameInfo.h"


class CWText;
class CWListBox;
class CWButton;

class CWGameListDlg :
	public CWContainer
{
public:
	CWGameListDlg(CScene& rScene);
	virtual ~CWGameListDlg();

	void SetOnGameSelectedHandler(const std::function<void(const CGameInfo& rGameInfo)>& rHandler) { m_OnGameSelectedHandler = rHandler; }
	void SetOnCreateNewGameHandler(const std::function<void(void)>& rHandler) { m_OnCreateNewGameHandler = rHandler; }
	void SetOnBackHandler(const std::function<void(void)>& rHandler) { m_OnBackHandler = rHandler; }

	void UpdateGameList(const std::map<DWORD, CGameInfo>& rmapGameInfo);

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pText;
	CWListBox* m_pList;
	CWButton* m_pButtonSelect;
	CWButton* m_pButtonCreate;
	CWButton* m_pButtonBack;
	std::map<DWORD, CGameInfo> m_mapGameInfo;
	std::function<void(const CGameInfo& rGameInfo)> m_OnGameSelectedHandler;
	std::function<void(void)> m_OnCreateNewGameHandler;
	std::function<void(void)> m_OnBackHandler;
};

