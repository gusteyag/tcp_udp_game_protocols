#include "WJoinToGameDlg.h"
#include "WText.h"
#include "WListBox.h"
#include "WButton.h"
#include "WEditBox.h"


CWJoinToGameDlg::CWJoinToGameDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pText(new CWText(rScene))
	, m_pTextListPlayers(new CWText(rScene))
	, m_pListPlayers(new CWListBox(rScene))
	, m_pTextEditName(new CWText(rScene))
	, m_pEditName(new CWEditBox(rScene))
	, m_pTextListCharacters(new CWText(rScene))
	, m_pListCharacters(new CWListBox(rScene))
	, m_pButtonJoin(new CWButton(rScene))
	, m_pButtonBack(new CWButton(rScene))
	, m_dwGameId(0)
{
	AddChild(m_pTextListPlayers);
	AddChild(m_pListPlayers);
	AddChild(m_pText);
	AddChild(m_pTextEditName);
	AddChild(m_pEditName);
	AddChild(m_pTextListCharacters);
	AddChild(m_pListCharacters);
	AddChild(m_pButtonJoin);
	AddChild(m_pButtonBack);

	SetText(L"Join to Game");
	m_pTextListPlayers->SetText(L"Existing players list:");
	m_pTextListPlayers->SetTextHAlignment(eHAlignment_Left);
	m_pText->SetText(L"Select a player name and character to join to the game.");
	m_pText->SetWordsWrap(true);
	m_pTextEditName->SetText(L"Name: ");
	m_pTextEditName->SetTextHAlignment(eHAlignment_Right);
	m_pTextListCharacters->SetText(L"Character: ");
	m_pTextListCharacters->SetTextHAlignment(eHAlignment_Right);
	m_pButtonJoin->SetText(L"Join");
	m_pButtonBack->SetText(L"Back");

	m_pButtonJoin->SetEnabled(false);

	m_pButtonJoin->SetOnClickedHandler([this]() {
		if (m_OnJoinToGameHandler && m_pEditName->GetText().size() && m_pListCharacters->GetSelectedLine(NULL))
			m_OnJoinToGameHandler(m_dwGameId, m_pEditName->GetText(), (eCharacters)m_pListCharacters->GetSelectedLine(NULL)->front().ui64UserData);
	});

	m_pButtonBack->SetOnClickedHandler([this]() {
		if (m_OnBackHandler)
			m_OnBackHandler();
	});
}

CWJoinToGameDlg::~CWJoinToGameDlg()
{
}

void CWJoinToGameDlg::UpdatePlayersList(const CGameInfo& rGameInfo)
{
	m_dwGameId = rGameInfo.GetGameId();
	m_pListPlayers->GetLines().clear();
	for (auto& player : rGameInfo.GetPlayers())
	{
		m_pListPlayers->GetLines().push_back(std::list<ListBoxItem>());
		m_pListPlayers->GetLines().back().push_back(ListBoxItem(player.second.GetName(), player.first));
		m_pListPlayers->GetLines().back().push_back(ListBoxItem(enum_desc(player.second.GetCharacter())));
	}
}

void CWJoinToGameDlg::UpdatePlayersList(const CGameState& rGameState)
{
	m_dwGameId = rGameState.GetGameId();
	m_pListPlayers->GetLines().clear();
	for (auto& player : rGameState.GetPlayers())
	{
		m_pListPlayers->GetLines().push_back(std::list<ListBoxItem>());
		m_pListPlayers->GetLines().back().push_back(ListBoxItem(player.second->GetName(), player.first));
		m_pListPlayers->GetLines().back().push_back(ListBoxItem(enum_desc(player.second->GetCharacter())));
	}
}

void CWJoinToGameDlg::NormalizeSize()
{
	float fPlayerNameWidth = 350;
	float fPlayerCharacterWidth = 100;
	float fWidth = fPlayerNameWidth + fPlayerCharacterWidth + m_pListPlayers->GetInBorderWidth() * 2;
	float fTextListPlayersHeight = 20;
	float fListPlayersHeight = 200;
	float fTextHeight = 50;
	float fVSpacing = 20;
	float fLeftSpacing = 20;
	float fTextWidth = 120;
	float fEditWidth = 120;
	float fButtonsHeight = 30;
	float fButtonsWidth = 100;
	float fButtonsHSpacing = 20;

	if (m_pEditName->GetText().size() && m_pListCharacters->GetSelectedLine(NULL))
		m_pButtonJoin->SetEnabled(true);
	else
		m_pButtonJoin->SetEnabled(false);

	if (!m_pListPlayers->GetColumns().size())
	{
		m_pListPlayers->GetColumns().push_back(ListBoxColumn(L"Name", fPlayerNameWidth));
		m_pListPlayers->GetColumns().push_back(ListBoxColumn(L"Character", fPlayerCharacterWidth));
	}

	m_pEditName->SetSize(sf::Vector2f(fEditWidth, 0));
	m_pEditName->NormalizeSize();
	if (!m_pListCharacters->GetColumns().size())
	{
		m_pListCharacters->GetColumns().push_back(ListBoxColumn(L"Character", fEditWidth - (m_pListCharacters->GetInBorderWidth() + m_pListCharacters->GetPadding()) * 2));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Amapola), eCharacters_Amapola));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Rubio), eCharacters_Rubio));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Orquidea), eCharacters_Orquidea));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Prado), eCharacters_Prado));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Celeste), eCharacters_Celeste));
		m_pListCharacters->GetLines().push_back(std::list<ListBoxItem>());
		m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Mora), eCharacters_Mora));
		m_pListCharacters->SetHeaderHeight(0);
		m_pListCharacters->NormalizeSize();
	}
	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fWidth,
		GetWorkingPosition().y + GetPadding() + GetInBorderWidth() +
		fTextListPlayersHeight + fListPlayersHeight + fVSpacing +
		fTextHeight + 
		m_pEditName->GetSize().y + fVSpacing +
		m_pListCharacters->GetSize().y + fVSpacing +
		fButtonsHeight
	));

	m_pTextListPlayers->SetFont(GetFont());
	m_pTextListPlayers->SetFontSize(GetFontSize());
	m_pTextListPlayers->SetSize(sf::Vector2f(fWidth, fTextListPlayersHeight));
	m_pTextListPlayers->SetPosition(sf::Vector2f(0, 0));

	m_pListPlayers->SetFont(GetFont());
	m_pListPlayers->SetFontSize(GetFontSize());
	m_pListPlayers->SetSize(sf::Vector2f(fWidth, fListPlayersHeight));
	m_pListPlayers->SetPosition(sf::Vector2f(0, fTextListPlayersHeight));

	m_pText->SetFont(GetFont());
	m_pText->SetFontSize(GetFontSize() + 2);
	m_pText->SetSize(sf::Vector2f(fWidth, fTextHeight));
	m_pText->SetPosition(sf::Vector2f(0, fTextListPlayersHeight + fListPlayersHeight));
	
	m_pTextEditName->SetFont(GetFont());
	m_pTextEditName->SetFontSize(GetFontSize());
	m_pTextEditName->SetSize(sf::Vector2f(fTextWidth, m_pEditName->GetSize().y));
	m_pTextEditName->SetPosition(sf::Vector2f(fLeftSpacing, fTextListPlayersHeight + fListPlayersHeight + fTextHeight));

	m_pEditName->SetFont(GetFont());
	m_pEditName->SetFontSize(GetFontSize());
	m_pEditName->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fTextListPlayersHeight + fListPlayersHeight + fTextHeight));

	m_pTextListCharacters->SetFont(GetFont());
	m_pTextListCharacters->SetFontSize(GetFontSize());
	m_pTextListCharacters->SetSize(sf::Vector2f(fTextWidth, m_pEditName->GetSize().y));
	m_pTextListCharacters->SetPosition(sf::Vector2f(
		fLeftSpacing,
		fTextListPlayersHeight + fListPlayersHeight + fTextHeight + m_pEditName->GetSize().y + fVSpacing
	));

	m_pListCharacters->SetFont(GetFont());
	m_pListCharacters->SetFontSize(GetFontSize());
	m_pListCharacters->SetPosition(sf::Vector2f(
		fLeftSpacing + fTextWidth,
		fTextListPlayersHeight + fListPlayersHeight + fTextHeight + m_pEditName->GetSize().y + fVSpacing
	));

	m_pButtonJoin->SetFont(GetFont());
	m_pButtonJoin->SetFontSize(GetFontSize());
	m_pButtonJoin->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonJoin->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 2 + fButtonsHSpacing) / 2,
		fTextListPlayersHeight + fListPlayersHeight + fTextHeight + m_pEditName->GetSize().y + fVSpacing + m_pListCharacters->GetSize().y + fVSpacing
	));

	m_pButtonBack->SetFont(GetFont());
	m_pButtonBack->SetFontSize(GetFontSize());
	m_pButtonBack->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonBack->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth * 2 + fButtonsHSpacing) / 2 + fButtonsWidth + fButtonsHSpacing,
		fTextListPlayersHeight + fListPlayersHeight + fTextHeight + m_pEditName->GetSize().y + fVSpacing + m_pListCharacters->GetSize().y + fVSpacing
	));
}

void CWJoinToGameDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
