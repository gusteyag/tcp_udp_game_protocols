#include "WPlayerStatus.h"
#include "WChip.h"
#include "WText.h"
#include "WButton.h"


CWPlayerStatus::CWPlayerStatus(CScene& rScene)
	: CWContainer(rScene)
	, m_pChipPlayer(new CWChip(rScene))
	, m_pTextPlayerName(new CWText(rScene))
	, m_pTextPlayerStatus(new CWText(rScene))
	, m_pTextPlayerDices(new CWText(rScene))
	, m_pButton1(new CWButton(rScene))
	, m_pButton2(new CWButton(rScene))
	, m_bBlinking(false)
	, m_bBlinkColors(false)
	, m_clrBorderNormal(GetBorderColor())
	, m_clrBorderBlink(sf::Color::Yellow)
	, m_bActiveView(false)
	, m_clrBorderActive(sf::Color::Green)
{
	AddChild(m_pChipPlayer);
	AddChild(m_pTextPlayerName);
	AddChild(m_pTextPlayerStatus);
	AddChild(m_pTextPlayerDices);
	AddChild(m_pButton1);
	AddChild(m_pButton2);

	SetBorderWidth(-1);
	SetHeaderHeight(0);
	SetPadding(5);
	SetVisible(false);

	m_pChipPlayer->SetFontSize(12);
	m_pTextPlayerName->SetFontSize(12);
	m_pTextPlayerStatus->SetFontSize(10);
	m_pTextPlayerDices->SetFontSize(30);
	m_pButton1->SetFontSize(10);
	m_pButton2->SetFontSize(10);

	m_pTextPlayerName->SetPadding(5);
	m_pTextPlayerStatus->SetPadding(2);
	m_pTextPlayerDices->SetPadding(2);

	m_pTextPlayerName->SetTextHAlignment(eHAlignment_Left);
	m_pTextPlayerName->SetTextVAlignment(eVAlignment_Center);
	m_pTextPlayerDices->SetTextHAlignment(eHAlignment_Center);
	m_pTextPlayerDices->SetTextVAlignment(eVAlignment_Center);
	m_pTextPlayerStatus->SetTextHAlignment(eHAlignment_Center);
	m_pTextPlayerStatus->SetTextVAlignment(eVAlignment_Center);
	m_pTextPlayerStatus->SetWordsWrap(true);

	m_pTextPlayerName->SetTextColor(sf::Color(200, 200, 0));
}

CWPlayerStatus::~CWPlayerStatus()
{
}

void CWPlayerStatus::ClearStatus()
{
	m_pChipPlayer->SetChip(0);
	m_pTextPlayerName->SetText(L"");
	m_pTextPlayerStatus->SetText(L"");
	m_pTextPlayerDices->SetText(L"");

	m_pButton1->SetEnabled(true);
	m_pButton1->SetText(L"");
	m_pButton1->SetOnClickedHandler(nullptr);
	m_pButton2->SetEnabled(true);
	m_pButton2->SetText(L"");
	m_pButton2->SetOnClickedHandler(nullptr);

	SetActiveView(false);
	SetBlinking(false);

	SetVisible(false);
}

void CWPlayerStatus::SetPlayerStatus(
	eCharacters eCharacter,
	const std::wstring& rsName,
	const std::wstring& rsStatus,
	const sf::Vector2u& rv2uDices/* = sf::Vector2u()*/,
	const std::wstring& rsButton1/* = std::wstring()*/,
	const std::function<void(void)>& Button1CB/* = nullptr*/,
	const std::wstring& rsButton2 /*= std::wstring()*/,
	const std::function<void(void)>& Button2CB/* = nullptr*/
)
{
	if (eCharacter == eCharacters_None)
	{
		m_pChipPlayer->SetChip(0);
		m_pTextPlayerName->SetText(L"");
		SetVisible(false);
	}
	else
	{
		m_pChipPlayer->SetChip(make_CharacterCard(eCharacter));
		m_pTextPlayerName->SetText(rsName);
		SetVisible(true);
	}

	m_pTextPlayerStatus->SetText(rsStatus);
	if (m_pTextPlayerStatus->GetText().empty())
		m_pTextPlayerStatus->SetVisible(false);
	else
		m_pTextPlayerStatus->SetVisible(true);

	if (rv2uDices == sf::Vector2u())
	{
		m_pTextPlayerDices->SetText(L"");
		m_pTextPlayerDices->SetVisible(false);

		m_pButton1->SetText(rsButton1);
		if (m_pButton1->GetText().empty())
		{
			m_pButton1->SetVisible(false);
			m_pButton1->SetOnClickedHandler(nullptr);
		}
		else
		{
			m_pButton1->SetVisible(true);
			m_pButton1->SetOnClickedHandler([this, Button1CB]() {
				m_pButton1->SetEnabled(false);
				if (Button1CB)
					Button1CB();
			});
		}
		m_pButton2->SetText(rsButton2);
		if (m_pButton2->GetText().empty())
		{
			m_pButton2->SetVisible(false);
			m_pButton2->SetOnClickedHandler(nullptr);
		}
		else
		{
			m_pButton2->SetVisible(true);
			m_pButton2->SetOnClickedHandler([this, Button2CB]() {
				m_pButton2->SetEnabled(false);
				if (Button2CB)
					Button2CB();
			});
		}
	}
	else
	{
		wchar_t szBuff[10] = {};
		_snwprintf_s(szBuff, _TRUNCATE, L"%u:%u", rv2uDices.x, rv2uDices.y);
		m_pTextPlayerDices->SetText(szBuff);
		m_pTextPlayerDices->SetVisible(true);

		m_pButton1->SetVisible(false);
		m_pButton2->SetVisible(false);
	}
}

void CWPlayerStatus::SetBlinking(bool bBlinking)
{
	m_bBlinking = bBlinking;
	m_bBlinkColors = m_bBlinking;
}

void CWPlayerStatus::Blink()
{
	if (!m_bBlinking)
		return;
	m_bBlinkColors = !m_bBlinkColors;
}

void CWPlayerStatus::NormalizeSize()
{
	float fChipSize = 25;
	float fStatusMinHeight = 30;
	float fButtonsWidth = 100;
	float fButtonsHeight = 20;
	float fButtonsHSpacing = 5;

	if (m_bBlinkColors)
	{
		SetBorderColor(m_clrBorderBlink);
	}
	else
	{
		if (m_bActiveView)
			SetBorderColor(m_clrBorderActive);
		else
			SetBorderColor(m_clrBorderNormal);
	}
	m_pChipPlayer->SetFont(GetFont());
	m_pChipPlayer->SetSize(sf::Vector2f(fChipSize, fChipSize));
	m_pTextPlayerName->SetFont(GetFont());
	m_pTextPlayerStatus->SetFont(GetFont());
	m_pTextPlayerDices->SetFont(GetFont());
	m_pButton1->SetFont(GetFont());
	m_pButton1->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButton2->SetFont(GetFont());
	m_pButton2->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));

	m_pChipPlayer->SetPosition(sf::Vector2f(0, 0));

	m_pTextPlayerName->SetSize(sf::Vector2f(GetWorkingSize().x - fChipSize, fChipSize));
	m_pTextPlayerName->SetPosition(sf::Vector2f(fChipSize, 0));

	m_pTextPlayerDices->SetSize(sf::Vector2f(GetWorkingSize().x, GetWorkingSize().y - fChipSize - fStatusMinHeight));
	m_pTextPlayerDices->SetPosition(sf::Vector2f(0, fChipSize));

	if (m_pTextPlayerDices->IsVisible())
	{
		m_pTextPlayerStatus->SetSize(sf::Vector2f(GetWorkingSize().x, fStatusMinHeight));
		m_pTextPlayerStatus->SetPosition(sf::Vector2f(0, fChipSize + m_pTextPlayerDices->GetSize().y));
	}
	else
	{
		if (m_pButton2->IsVisible() || m_pButton1->IsVisible())
			m_pTextPlayerStatus->SetSize(sf::Vector2f(GetWorkingSize().x, GetWorkingSize().y - fChipSize - fButtonsHeight));
		else
			m_pTextPlayerStatus->SetSize(sf::Vector2f(GetWorkingSize().x, GetWorkingSize().y - fChipSize));
		m_pTextPlayerStatus->SetPosition(sf::Vector2f(0, fChipSize));
	}

	if (m_pButton2->IsVisible())
	{
		m_pButton1->SetPosition(sf::Vector2f(
			GetWorkingSize().x / 2 - (fButtonsWidth * 2 + fButtonsHSpacing) / 2,
			fChipSize + m_pTextPlayerStatus->GetSize().y
		));
	}
	else
	{
		m_pButton1->SetPosition(sf::Vector2f(
			GetWorkingSize().x / 2 - fButtonsWidth / 2,
			fChipSize + m_pTextPlayerStatus->GetSize().y
		));
	}
	m_pButton2->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - (fButtonsWidth*2 + fButtonsHSpacing) / 2 + fButtonsWidth + fButtonsHSpacing,
		fChipSize + m_pTextPlayerStatus->GetSize().y
	));
}

void CWPlayerStatus::Draw()
{
	NormalizeSize();
	__super::Draw();
}
