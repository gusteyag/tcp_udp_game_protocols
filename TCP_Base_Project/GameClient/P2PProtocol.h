#pragma once
#include <Windows.h>
#include <string>
#include <SFML\Network.hpp>


#define P2P_CURRENT_PROTOCOL_VERSION_BCD			((sf::Uint32)0x00010000) // 1.0

enum eP2PPacketType
{
	eP2PPacketType_Handshake = 1,
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- BCD protocol version
	// 4 bytes	- sf::Uint32	- PlayerId
	// Answer:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult (eResult_Succes or disconnect)

	eP2PPacketType_ReadyToStart,
	// 4 bytes	- sf::Uint32	- ePacketType

	eP2PPacketType_JoinToGame_Request,
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- PlayerId
	// ???		- std::wstring	- player name
	// 4 bytes	- sf::Uint32	- player character

	eP2PPacketType_JoinToGame_Answer,
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	eP2PPacketType_GameState,
	// 4 bytes	- sf::Uint32	- ePacketType
	// ???		------------	- game state data

	eP2PPacketType_GameState_Confirmation,
	// 4 bytes	- sf::Uint32	- ePacketType

	eP2PPacketType_ChatMessage,
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- PlayerId
	// ???		- std::wstring	- message
};
