#include "WChip.h"
#include "GameDefinitions.h"


CWChip::CWChip(CScene& rScene)
	: CWidget(rScene)
	, m_ullChip(0)
	, m_bInactive(false)
	, m_bBlinking(false)
	, m_bBlinkColors(false)
	, m_clrInactive(sf::Color(70, 70, 70))
	, m_clrBlink(sf::Color(255, 70, 70))
{
	SetBorderWidth(0);
	SetPadding(0);
	SetFontSize(10);
}

CWChip::CWChip(CScene& rScene, sf::Uint64 ullChip)
	: CWidget(rScene)
	, m_ullChip(ullChip)
	, m_bInactive(false)
	, m_bBlinking(false)
	, m_bBlinkColors(false)
	, m_clrInactive(sf::Color(100, 100, 100))
	, m_clrBlink(sf::Color(255, 0, 0))
{
	SetBorderWidth(0);
	SetPadding(0);
	SetFontSize(10);
}

CWChip::~CWChip()
{
}

void CWChip::SetBlinking(bool bBlinking)
{
	m_bBlinking = bBlinking;
	m_bBlinkColors = m_bBlinking;
}

void CWChip::Blink()
{
	if (!m_bBlinking)
		return;
	m_bBlinkColors = !m_bBlinkColors;
}

void CWChip::Draw()
{
	if (!GetRenderTarget())
		return;

	switch (get_CardType(m_ullChip))
	{
	case eCardTypes_Character:
	{
		sf::CircleShape shape(((GetSize().x > GetSize().y) ? GetSize().y : GetSize().x) / 2, 100);
		shape.setOrigin(sf::Vector2f(
			shape.getLocalBounds().width / 2 + shape.getLocalBounds().left,
			shape.getLocalBounds().height / 2 + shape.getLocalBounds().top
		));
		shape.setPosition(sf::Vector2f(GetSize().x / 2 + GetAbsolutePosition().x, GetSize().y / 2 + GetAbsolutePosition().y));
		shape.setOutlineThickness(0);
		if (IsEnabled())
		{
			if (m_bBlinkColors)
			{
				shape.setFillColor(m_clrBlink);
			}
			else
			{
				if (m_bInactive)
					shape.setFillColor(m_clrInactive);
				else
					shape.setFillColor(sf::Color(100, 255, 100));
			}
		}
		else
		{
			shape.setFillColor(sf::Color::Black);
		}
		GetRenderTarget()->draw(shape);

		std::wstring desc = enum_desc(get_CharacterCardType(m_ullChip));
		sf::Text text(desc.substr(0, 2), GetFont(), GetFontSize());
		CScene::AlignText(eHAlignment_Center, eVAlignment_Center, text, GetAbsoluteBoundingRect());
		if(IsEnabled())
			text.setFillColor(sf::Color::Black);
		else
			text.setFillColor(sf::Color::White);
		GetRenderTarget()->draw(text);
	}
	break;

	case eCardTypes_Weapon:
	{
		sf::CircleShape shape(((GetSize().x > GetSize().y) ? GetSize().y : GetSize().x) / 2, 4);
		shape.setOrigin(sf::Vector2f(
			shape.getLocalBounds().width / 2 + shape.getLocalBounds().left,
			shape.getLocalBounds().height / 2 + shape.getLocalBounds().top
		));
		shape.setPosition(sf::Vector2f(GetSize().x / 2 + GetAbsolutePosition().x, GetSize().y / 2 + GetAbsolutePosition().y));
		shape.setOutlineThickness(0);
		if (IsEnabled())
		{
			if (m_bBlinkColors)
			{
				shape.setFillColor(m_clrBlink);
			}
			else
			{
				if (m_bInactive)
					shape.setFillColor(m_clrInactive);
				else
					shape.setFillColor(sf::Color(255, 100, 100));
			}
		}
		else
		{
			shape.setFillColor(sf::Color::Black);
		}
		GetRenderTarget()->draw(shape);

		std::wstring desc = enum_desc(get_WeaponCardType(m_ullChip));
		sf::Text text(desc.substr(0, 2), GetFont(), GetFontSize());
		CScene::AlignText(eHAlignment_Center, eVAlignment_Center, text, GetAbsoluteBoundingRect());
		text.setFillColor(sf::Color::White);
		GetRenderTarget()->draw(text);
	}
	break;

	case eCardTypes_Room:
	{
		sf::RectangleShape shape(GetSize());
		shape.setPosition(GetAbsolutePosition());
		shape.setOutlineThickness(0);
		if (IsEnabled())
		{
			if (m_bBlinkColors)
			{
				shape.setFillColor(m_clrBlink);
			}
			else
			{
				if (m_bInactive)
					shape.setFillColor(m_clrInactive);
				else
					shape.setFillColor(sf::Color(100, 100, 255));
			}
		}
		else
		{
			shape.setFillColor(sf::Color::Black);
		}
		GetRenderTarget()->draw(shape);

		std::wstring desc = enum_desc(get_RoomCardType(m_ullChip));
		sf::Text text(desc.substr(0, 2), GetFont(), GetFontSize());
		CScene::AlignText(eHAlignment_Center, eVAlignment_Center, text, GetAbsoluteBoundingRect());
		text.setFillColor(sf::Color::White);
		GetRenderTarget()->draw(text);
	}
	break;
	}
}
