#pragma once
#include <Windows.h>
#include <list>
#include <memory>
#include <string>
#include <SFML\Network.hpp>
#include "GameDefinitions.h"
#include "../GameServer/Protocol.h"
#include "INetClient.h"


class CClient : public INetClient
{
public:
	CClient(const std::wstring& rsServerAddress, USHORT nServerPort);
	virtual ~CClient();

	virtual eNetworkType GetNetworkType() { return eNetworkType_ClientServer; }

	virtual bool Connect(
		const net_message_cb_t& rOnDisconnectedCB,
		const net_game_info_list_cb_t& rOnGameInfoListChangedCB,
		const net_game_state_cb_t& rOnGameStateChangedCB,
		const net_chat_cb_t& rOnChatChangedCB
	);
	virtual void Disconnect();

	virtual bool Process();

	virtual void Request_CreateNewGameAndJoin(
		const net_join_answer_cb_t& rAnswerCB,
		const std::wstring& rsGameName,
		DWORD dwSpecifiedPlayersCount,
		const std::wstring& rsPlayerName,
		eCharacters eCharacter
	);
	virtual void Request_Join(const net_join_answer_cb_t& rAnswerCB, DWORD dwGameId, const std::wstring& rsPlayerName, eCharacters eCharacter);
	virtual void Request_LeftGame(const net_answer_cb_t& rAnswerCB);
	virtual void Request_RollDices(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ClueCard(const net_answer_cb_t& rAnswerCB, ULONG64 nAssumedCard);
	virtual void Request_ClueCardAnswer(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ReceiveClueCardAnswer(const net_answer_cb_t& rAnswerCB);
	virtual void Request_Move(const net_answer_cb_t& rAnswerCB, const std::list<POINT>& rlstTrack);
	virtual void Request_MakeDeduction(const net_answer_cb_t& rAnswerCB, eWeapons eWeapon, eCharacters eCharacter);
	virtual void Request_AnswerToDeduction(const net_answer_cb_t& rAnswerCB);
	virtual void Request_ReceiveDeductionAnswers(const net_answer_cb_t& rAnswerCB);
	virtual void Request_MakeAccusation(const net_answer_cb_t& rAnswerCB, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter);
	virtual void Request_SkipAccusation(const net_answer_cb_t& rAnswerCB);

	virtual void Request_ChatMessage(const std::wstring& rsMessage);

private:
	std::wstring m_sServerAddress;
	USHORT m_nServerPort;

	std::map<DWORD, CGameInfo> m_mapGameInfoList;

	net_message_cb_t m_OnDisconnectedCB;
	net_game_info_list_cb_t m_OnGameInfoListChangedCB;
	net_game_state_cb_t m_OnGameStateChangedCB;
	net_chat_cb_t m_OnChatChangedCB;

	sf::TcpSocket m_Socket;
	bool m_bOnceConnected;
	bool m_bDisconnected;
	bool m_bHandshakeSended;
	bool m_bHandshake;
	sf::Packet m_InPacket;
	std::list<std::unique_ptr<sf::Packet> > m_lstOutPackets;

	net_join_answer_cb_t m_CreateAndJoin_AnswerCB;
	net_join_answer_cb_t m_Join_AnswerCB;
	std::map<ePacketType, net_answer_cb_t> m_mapCallbacks;

	sf::Clock m_ConnectionClock;

private:
	bool _Connect();
	bool ProcessProtocol();
};
