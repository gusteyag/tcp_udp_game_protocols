#pragma once
#include "WContainer.h"
#include <functional>
#include "GameDefinitions.h"


class CWText;
class CWButton;

class CWWinnerDlg :
	public CWContainer
{
public:
	CWWinnerDlg(CScene& rScene);
	virtual ~CWWinnerDlg();

	void Show(
		const std::function<void(void)>& rOnQuitHandler,
		bool bNoWinner = false,
		std::wstring sWinner = std::wstring(),
		eCharacters eMurderCharacter = eCharacters_None,
		eWeapons eMurderWeapon = eWeapons_None,
		eRooms eMurderRoom = eRooms_None
	)
	{
		m_OnQuitHandler = rOnQuitHandler;
		m_bNoWinner = bNoWinner;
		m_sWinner = sWinner;
		m_eMurderCharacter = eMurderCharacter;
		m_eMurderWeapon = eMurderWeapon;
		m_eMurderRoom = eMurderRoom;
		SetVisible(true);
	}
	
	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pTextLarge;
	CWText* m_pTextSmall;
	CWButton* m_pButtonQuit;
	std::function<void(void)> m_OnQuitHandler;

	bool m_bNoWinner;
	std::wstring m_sWinner;
	eCharacters m_eMurderCharacter;
	eWeapons m_eMurderWeapon;
	eRooms m_eMurderRoom;
};

