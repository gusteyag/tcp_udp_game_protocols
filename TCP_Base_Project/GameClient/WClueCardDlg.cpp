#include "WClueCardDlg.h"
#include "WText.h"
#include "WListBox.h"
#include "WButton.h"


CWClueCardDlg::CWClueCardDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_eCardType(eCardTypes_None)
	, m_pTextDescription(new CWText(rScene))
	, m_pListCharacters(new CWListBox(rScene))
	, m_pListWeapons(new CWListBox(rScene))
	, m_pListRooms(new CWListBox(rScene))
	, m_pButtonSelect(new CWButton(rScene))
{
	AddChild(m_pTextDescription);
	AddChild(m_pListCharacters);
	AddChild(m_pListWeapons);
	AddChild(m_pListRooms);
	AddChild(m_pButtonSelect);

	SetVisible(false);
	SetFillColor(sf::Color::Black);

	m_pTextDescription->SetFontSize(15);
	m_pTextDescription->SetWordsWrap(true);
	m_pTextDescription->SetTextHAlignment(eHAlignment_Center);
	m_pTextDescription->SetTextVAlignment(eVAlignment_Center);

	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Amapola), eCharacters_Amapola));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Rubio), eCharacters_Rubio));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Orquidea), eCharacters_Orquidea));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Prado), eCharacters_Prado));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Celeste), eCharacters_Celeste));
	m_pListCharacters->GetLines().push_back(listbox_items_t());
	m_pListCharacters->GetLines().back().push_back(ListBoxItem(enum_desc(eCharacters_Mora), eCharacters_Mora));

	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Punyal), eWeapons_Punyal));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Cuerda), eWeapons_Cuerda));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Candelabro), eWeapons_Candelabro));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Pistola), eWeapons_Pistola));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Tuberia), eWeapons_Tuberia));
	m_pListWeapons->GetLines().push_back(listbox_items_t());
	m_pListWeapons->GetLines().back().push_back(ListBoxItem(enum_desc(eWeapons_Herramienta), eWeapons_Herramienta));

	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Cocina), eRooms_Cocina));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Comedor), eRooms_Comedor));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Salon), eRooms_Salon));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Estudio), eRooms_Estudio));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Biblioteca), eRooms_Biblioteca));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Billar), eRooms_Billar));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Terraza), eRooms_Terraza));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Baile), eRooms_Baile));
	m_pListRooms->GetLines().push_back(listbox_items_t());
	m_pListRooms->GetLines().back().push_back(ListBoxItem(enum_desc(eRooms_Escaleras), eRooms_Escaleras));

	m_pButtonSelect->SetOnClickedHandler([this]() {
		SetVisible(false);
		if (m_OnSelectHandler)
		{
			sf::Uint64 ullAssumedCard = 0;
			switch (m_eCardType)
			{
			case eCardTypes_Character:
				ullAssumedCard = make_CharacterCard((eCharacters)(m_pListCharacters->GetSelectedLine(NULL) ? m_pListCharacters->GetSelectedLine(NULL)->back().ui64UserData : eCharacters_None));
				break;

			case eCardTypes_Weapon:
				ullAssumedCard = make_WeaponCard((eWeapons)(m_pListWeapons->GetSelectedLine(NULL) ? m_pListWeapons->GetSelectedLine(NULL)->back().ui64UserData : eCharacters_None));
				break;

			case eCardTypes_Room:
				ullAssumedCard = make_RoomCard((eRooms)(m_pListRooms->GetSelectedLine(NULL) ? m_pListRooms->GetSelectedLine(NULL)->back().ui64UserData : eCharacters_None));
				break;
			}
			m_OnSelectHandler(ullAssumedCard);
		}
	});
	m_pButtonSelect->SetText(L"Select");
}

CWClueCardDlg::~CWClueCardDlg()
{
}

void CWClueCardDlg::Show(eCardTypes eCardType, const std::function<void(sf::Uint64 uiAssumedCard)>& rSelectHandler)
{
	m_eCardType = eCardType;
	m_OnSelectHandler = rSelectHandler;
	m_pButtonSelect->SetEnabled(false);
	m_pListCharacters->SetVisible(false);
	m_pListWeapons->SetVisible(false);
	m_pListRooms->SetVisible(false);
	switch (m_eCardType)
	{
	case eCardTypes_Character:
		m_pListCharacters->SetVisible(true);
		break;

	case eCardTypes_Weapon:
		m_pListWeapons->SetVisible(true);
		break;

	case eCardTypes_Room:
		m_pListRooms->SetVisible(true);
		break;
	}
	SetVisible(true);
}

void CWClueCardDlg::NormalizeSize()
{
	float fListColumnWidth = 100;
	float fListVSpacing = 50;
	float fTextDescriptionHeight = 100;
	float fButtonsWidth = 100;
	float fButtonsHeight = 30;

	m_pTextDescription->SetFont(GetFont());
	m_pListCharacters->SetFont(GetFont());
	m_pListWeapons->SetFont(GetFont());
	m_pListRooms->SetFont(GetFont());
	m_pButtonSelect->SetFont(GetFont());

	if (((m_eCardType == eCardTypes_Character) && m_pListCharacters->GetSelectedLine(NULL)) ||
		((m_eCardType == eCardTypes_Weapon) && m_pListWeapons->GetSelectedLine(NULL)) ||
		((m_eCardType == eCardTypes_Room) && m_pListRooms->GetSelectedLine(NULL)))
	{
		m_pButtonSelect->SetEnabled(true);
	}
	else
	{
		m_pButtonSelect->SetEnabled(false);
	}

	SetText(L"Select Clue Card");
	wchar_t szBuff[256] = {};
	_snwprintf_s(szBuff, _TRUNCATE, L"Select \"%s\" to make a Clue Card request.", enum_desc(m_eCardType));
	m_pTextDescription->SetText(szBuff);

	if (m_pListCharacters->GetColumns().empty())
	{
		m_pListCharacters->GetColumns().push_back(ListBoxColumn(L"Characters", fListColumnWidth));
		m_pListCharacters->NormalizeSize();
	}
	if (m_pListWeapons->GetColumns().empty())
	{
		m_pListWeapons->GetColumns().push_back(ListBoxColumn(L"Weapons", fListColumnWidth));
		m_pListWeapons->NormalizeSize();
	}
	if (m_pListRooms->GetColumns().empty())
	{
		m_pListRooms->GetColumns().push_back(ListBoxColumn(L"Rooms", fListColumnWidth));
		m_pListRooms->NormalizeSize();
	}

	SetSize(sf::Vector2f(
		fListVSpacing + fListColumnWidth + fListVSpacing + (GetInBorderWidth() + GetPadding()) * 2,
		GetReservedIndentTop() + fTextDescriptionHeight + m_pListRooms->GetSize().y + GetPadding() + fButtonsHeight + (GetInBorderWidth() + GetPadding()) * 2 + GetPadding()
	));
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*this),
		sf::FloatRect(0, 0, GetScene().GetRenderWindow()->getDefaultView().getSize().x, GetScene().GetRenderWindow()->getDefaultView().getSize().y));

	m_pTextDescription->SetSize(sf::Vector2f(GetWorkingSize().x, fTextDescriptionHeight));
	m_pTextDescription->SetPosition(sf::Vector2f(0, 0));

	m_pListCharacters->SetPosition(sf::Vector2f(fListVSpacing, fTextDescriptionHeight));
	m_pListWeapons->SetPosition(sf::Vector2f(fListVSpacing, fTextDescriptionHeight));
	m_pListRooms->SetPosition(sf::Vector2f(fListVSpacing, fTextDescriptionHeight));

	m_pButtonSelect->SetSize(sf::Vector2f(fButtonsWidth, fButtonsHeight));
	m_pButtonSelect->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - fButtonsWidth / 2,
		fTextDescriptionHeight + m_pListRooms->GetSize().y + GetPadding()
	));
}

void CWClueCardDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
