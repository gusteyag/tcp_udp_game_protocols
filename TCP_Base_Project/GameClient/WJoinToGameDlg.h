#pragma once
#include "WContainer.h"
#include <functional>
#include <map>
#include "GameInfo.h"


class CWText;
class CWListBox;
class CWButton;
class CWEditBox;

class CWJoinToGameDlg :
	public CWContainer
{
public:
	CWJoinToGameDlg(CScene& rScene);
	virtual ~CWJoinToGameDlg();

	void SetOnJoinToGameHandler(const std::function<void(sf::Uint32 uiGameId, const std::wstring& rsPlayerName, eCharacters eCharacter)>& rHandler) { m_OnJoinToGameHandler = rHandler; }
	void SetOnBackHandler(const std::function<void(void)>& rHandler) { m_OnBackHandler = rHandler; }

	void UpdatePlayersList(const CGameInfo& rGameInfo);
	void UpdatePlayersList(const CGameState& rGameState);
	DWORD GetGameId() const { return m_dwGameId; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pText;
	CWText* m_pTextListPlayers;
	CWListBox* m_pListPlayers;
	CWText* m_pTextEditName;
	CWEditBox* m_pEditName;
	CWText* m_pTextListCharacters;
	CWListBox* m_pListCharacters;
	CWButton* m_pButtonJoin;
	CWButton* m_pButtonBack;
	DWORD m_dwGameId;
	std::function<void(sf::Uint32 uiGameId, const std::wstring& rsPlayerName, eCharacters eCharacter)> m_OnJoinToGameHandler;
	std::function<void(void)> m_OnBackHandler;
};

