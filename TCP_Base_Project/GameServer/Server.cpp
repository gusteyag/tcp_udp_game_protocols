#include "Server.h"
#include "Logger.h"
#include "Protocol.h"
#include <GameDefinitions.h>

sf::Uint32 CServer::m_nClientIdCounter = 0;
sf::Uint32 CServer::m_nGameIdCounter = 0;
sf::Uint32 CServer::m_nPlayerIdCounter = 0;

CServer::CServer(const std::string& rsAddress, unsigned short nPort)
	: m_bStop(false)
	, m_sAddress(rsAddress)
	, m_nPort(nPort)
{
	m_Listener.setBlocking(false);
	if (m_Listener.listen(nPort, rsAddress) != sf::Socket::Done)
	{
		throw std::exception("Failed to call m_Listener.listen().");
	}
	m_pClientToAccept = new Client(++m_nClientIdCounter);
}

CServer::~CServer()
{
	Stop();
}

void CServer::Run()
{
	log_info("Server running. Address=\"%s\", Port=%u", m_sAddress.c_str(), (DWORD)m_nPort);

	while (!m_bStop)
	{
		bool bSomethingHappened = false;

		sf::TcpListener::Status status = m_Listener.accept(m_pClientToAccept->Socket);
		if (status != sf::Socket::Done)
		{
			if (status == sf::TcpSocket::Error)
			{
				log_error("Failed to accept new connection.");
				Stop();
				return;
			}
		}
		else
		{
			bSomethingHappened = true;
			m_pClientToAccept->Socket.setBlocking(false);
			m_mapClients[m_pClientToAccept->nId].reset(m_pClientToAccept);
			log_info("Client %u connected.", m_pClientToAccept->nId);
			m_pClientToAccept = new Client(++m_nClientIdCounter);
		}

		std::map<sf::Uint32, std::unique_ptr<Client> >::iterator itClient = m_mapClients.begin();
		while (itClient != m_mapClients.end())
		{
			sf::TcpSocket::Status status = itClient->second->Socket.receive(itClient->second->InPacket);
			if (status != sf::TcpSocket::Done)
			{
				if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
				{
					if(status == sf::TcpSocket::Error)
						log_error("Client %u disconnected with error.", itClient->second->nId);
					else
						log_warning("Client %u disconnected.", itClient->second->nId);

					bSomethingHappened = true;
					itClient->second->Socket.disconnect();
					DWORD dwGameId = itClient->second->nGameId;
					DWORD dwPlayerId = itClient->second->nPlayerId;
					bool bRejected = itClient->second->bRejected;
					itClient = m_mapClients.erase(itClient);
					if(!bRejected && dwGameId && dwPlayerId)
						RemoveDisconnectedClientFromGame(dwGameId, dwPlayerId);
					continue;
				}
			}
			else
			{
				bSomethingHappened = true;
				if (!ProcessProtocol(*itClient->second))
				{
					itClient->second->Socket.disconnect();
					DWORD dwGameId = itClient->second->nGameId;
					DWORD dwPlayerId = itClient->second->nPlayerId;
					bool bRejected = itClient->second->bRejected;
					itClient = m_mapClients.erase(itClient);
					if (!bRejected && dwGameId && dwPlayerId)
						RemoveDisconnectedClientFromGame(dwGameId, dwPlayerId);
					continue;
				}
				else
				{
					itClient->second->InPacket.clear();
				}
			}

			if (!itClient->second->lstOutPackets.empty())
			{
				sf::TcpSocket::Status status = itClient->second->Socket.send(*itClient->second->lstOutPackets.front());
				if (status != sf::TcpSocket::Done)
				{
					if ((status == sf::TcpSocket::Disconnected) || (status == sf::TcpSocket::Error))
					{
						if (status == sf::TcpSocket::Error)
							log_error("Client %u disconnected with error.", itClient->second->nId);
						else
							log_warning("Client %u disconnected.", itClient->second->nId);

						bSomethingHappened = true;
						itClient->second->Socket.disconnect();
						DWORD dwGameId = itClient->second->nGameId;
						DWORD dwPlayerId = itClient->second->nPlayerId;
						bool bRejected = itClient->second->bRejected;
						itClient = m_mapClients.erase(itClient);
						if (!bRejected && dwGameId && dwPlayerId)
							RemoveDisconnectedClientFromGame(dwGameId, dwPlayerId);
						continue;
					}
				}
				else
				{
					bSomethingHappened = true;
					itClient->second->lstOutPackets.pop_front();
				}
			}

			++itClient;
		}

		if (!bSomethingHappened)
			Sleep(1); // To avoid CPU load
	}

	m_cvWaitStopping.notify_all();
	log_info("Server stopped.");
}

void CServer::Stop()
{
	std::unique_lock<std::mutex> lock(m_mtxWaitStopping);

	if (!m_bStop)
	{
		m_bStop = true;
		m_cvWaitStopping.wait(lock);
	}

	m_Listener.close();
	for (auto& client : m_mapClients)
		client.second->Socket.disconnect();
	m_mapClients.clear();
	delete m_pClientToAccept;
	m_pClientToAccept = NULL;
}

bool CServer::ProcessProtocol(Client& rClient)
{
	if (rClient.bRejected)
		return false; // Disconnect client

	if (!rClient.bHandshake)
	{
		sf::Uint32 nData = 0;
		rClient.InPacket >> nData;
		if(nData != ePacketType_Handshake)
		{
			log_error("Unexpected packet type. ClientId=%u", rClient.nId);
			return false;
		}
		rClient.InPacket >> nData;
		if (nData != CURRENT_PROTOCOL_VERSION_BCD)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_Handshake << (sf::Uint32)eResult_Fail;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			rClient.bHandshake = true;
			rClient.bRejected = true;
			log_error("Incompatible protocol version on client. ClientId=%u", rClient.nId);
		}
		else
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_Handshake << (sf::Uint32)eResult_Success;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			SendGameInfoList(&rClient);
			rClient.bHandshake = true;
		}
		return true;
	}

	sf::Uint32 nPacketType = 0;
	rClient.InPacket >> nPacketType;
	switch (nPacketType)
	{
	case ePacketType_CreateNewGameAndJoin:
	{
		sf::Uint32 nSpecifiedPlayersCount = 0;
		std::wstring sGameName;
		sf::Uint32 nCharacter = 0;
		std::wstring sPlayerName;
		rClient.InPacket >> nSpecifiedPlayersCount >> sGameName >> nCharacter >> sPlayerName;
		bool bFound = false;
		for (auto& game : m_mapGames)
		{
			if (game.second->GetName() == sGameName)
			{
				bFound = true;
				break;
			}
		}
		if (bFound)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_CreateNewGameAndJoin << (sf::Uint32)eResult_GameNameAlreadyExists;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			break;
		}
		CGameWrapper* pGame = new CGameWrapper(++m_nGameIdCounter, sGameName, nSpecifiedPlayersCount, (*this));
		m_mapGames[pGame->GetId()].reset(pGame);
		DWORD dwPlayerId = ++m_nPlayerIdCounter;
		rClient.nGameId = pGame->GetId();
		rClient.nPlayerId = dwPlayerId;
		eResult eRes = pGame->PlayerTriesToJoin(dwPlayerId, sPlayerName, enum_check((eCharacters)nCharacter));
		sf::Packet* pPacket = new sf::Packet();
		(*pPacket) << (sf::Uint32)ePacketType_CreateNewGameAndJoin << (sf::Uint32)eRes;
		if (eRes == eResult_Success)
		{
			(*pPacket) << rClient.nGameId << rClient.nPlayerId;
		}
		else
		{
			m_mapGames.erase(pGame->GetId());
			rClient.nGameId = 0;
			rClient.nPlayerId = 0;
		}
		rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	}
	break;

	case ePacketType_JoinToGame:
	{
		sf::Uint32 nGameId = 0;
		sf::Uint32 nCharacter = 0;
		std::wstring sPlayerName;
		rClient.InPacket >> nGameId >> nCharacter >> sPlayerName;
		if (m_mapGames.count(nGameId))
		{
			DWORD dwPlayerId = ++m_nPlayerIdCounter;
			rClient.nGameId = nGameId;
			rClient.nPlayerId = dwPlayerId;
			eResult eRes = m_mapGames.at(nGameId)->PlayerTriesToJoin(dwPlayerId, sPlayerName, enum_check((eCharacters)nCharacter));
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_JoinToGame << (sf::Uint32)eRes << nGameId;
			if (eRes == eResult_Success)
			{
				(*pPacket) << rClient.nPlayerId;
			}
			else
			{
				rClient.nGameId = 0;
				rClient.nPlayerId = 0;
			}
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));

			if(m_mapGames.at(nGameId)->GetState().GetGameState() == eGameStates_Started)
				SendChatState(m_mapGames.at(nGameId)->GetChat(), m_mapGames.at(nGameId)->GetState(), NULL);
			else
				SendChatState(m_mapGames.at(nGameId)->GetChat(), m_mapGames.at(nGameId)->GetState(), &rClient);
		}
		else
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_JoinToGame << (sf::Uint32)eResult_NotFound << nGameId;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			log_error("Game not found. ClientId=%u, GameId=%u", rClient.nId, nGameId);
		}
	}
	break;

	case ePacketType_LeftGame:
	{
		sf::Uint32 nGameId = rClient.nGameId;
		if (m_mapGames.count(nGameId))
		{
			rClient.nGameId = 0; // To avoid GameState sending and to send GameInfoList
			eResult eRes = m_mapGames.at(nGameId)->PlayerLeftGame(rClient.nPlayerId, FALSE);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_LeftGame << (sf::Uint32)eRes;
			if (eRes == eResult_Success)
			{
				rClient.nGameId = 0;
				rClient.nPlayerId = 0;
			}
			else
			{
				rClient.nGameId = nGameId;
			}
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_LeftGame << (sf::Uint32)eResult_NotFound;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
			log_error("Game not found. ClientId=%u, GameId=%u", rClient.nId, nGameId);
		}
	}
	break;

	case ePacketType_RollDices:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_RollDices << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToRollDices(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_RollDices << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_RequestClueCard:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_RequestClueCard << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			sf::Uint64 nAssumedCard = 0;
			rClient.InPacket >> nAssumedCard;
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToRequestClueCard(rClient.nPlayerId, nAssumedCard);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_RequestClueCard << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_AnswerClueCard:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_AnswerClueCard << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToAnswerClueCard(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_AnswerClueCard << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_ReceiveClueCardAnswer:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ReceiveClueCardAnswer << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToReceiveClueCardAnswer(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ReceiveClueCardAnswer << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_Move:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_Move << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			std::list<POINT> lstTrack;
			sf::Uint32 nPointsListSize = 0;
			rClient.InPacket >> nPointsListSize;
			for (sf::Uint32 i = 0; i < nPointsListSize; ++i)
			{
				sf::Int32 x = 0;
				sf::Int32 y = 0;
				rClient.InPacket >> x >> y;
				POINT pt = {(LONG)x, (LONG)y};
				lstTrack.push_back(pt);
			}
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToMove(rClient.nPlayerId, lstTrack);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_Move << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_MakeDeduction:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_MakeDeduction << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			sf::Uint32 nWeapon = 0;
			sf::Uint32 nCharacter = 0;
			rClient.InPacket >> nWeapon >> nCharacter;
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToMakeDeduction(rClient.nPlayerId, enum_check((eWeapons)nWeapon), enum_check((eCharacters)nCharacter));
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_MakeDeduction << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_AnswerToDeduction:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_AnswerToDeduction << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToAnswerToDeduction(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_AnswerToDeduction << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_ReceiveDeductionAnswers:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ReceiveDeductionAnswers << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToReceiveDeductionAnswers(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ReceiveDeductionAnswers << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_MakeAccusation:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_MakeAccusation << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			sf::Uint32 nRoom = 0;
			sf::Uint32 nWeapon = 0;
			sf::Uint32 nCharacter = 0;
			rClient.InPacket >> nRoom >> nWeapon >> nCharacter;
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToMakeAccusation(rClient.nPlayerId, enum_check((eRooms)nRoom), enum_check((eWeapons)nWeapon), enum_check((eCharacters)nCharacter));
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_MakeAccusation << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_SkipAccusation:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_SkipAccusation << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			eResult eRes = m_mapGames.at(rClient.nGameId)->PlayerTriesToSkipAccusation(rClient.nPlayerId);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_SkipAccusation << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	case ePacketType_ChatMessage:
	{
		if (!rClient.nGameId)
		{
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ChatMessage << (sf::Uint32)eResult_PlayerNotInGame;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			std::wstring sMessage;
			rClient.InPacket >> sMessage;
			eResult eRes = m_mapGames.at(rClient.nGameId)->AddChatMessage(rClient.nPlayerId, sMessage);
			sf::Packet* pPacket = new sf::Packet();
			(*pPacket) << (sf::Uint32)ePacketType_ChatMessage << (sf::Uint32)eRes;
			rClient.lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
	}
	break;

	default:
	{
		log_error("Unknown packet type");
		return false;
	}
	break;
	}

	return true;
}

void CServer::RemoveDisconnectedClientFromGame(DWORD dwGameId, DWORD dwPlayerId)
{
	for (auto& game : m_mapGames)
	{
		if (game.second->GetId() == dwGameId && game.second->GetState().GetPlayers().count(dwPlayerId))
		{
			eResult eRes = game.second->PlayerLeftGame(dwPlayerId, TRUE);
			if(eRes != eResult_Success)
				log_error("Failed to call PlayerLeftGame(). Result=%u, PlayerId=%u, GameId=%u", (DWORD)eRes, dwGameId, dwPlayerId);
			break;
		}
	}
}

void CServer::SendGameInfoList(__in_opt Client* pClientToSend)
{
	for (auto& client : m_mapClients)
	{
		if (pClientToSend && (pClientToSend->nId != client.second->nId))
			continue;
		if (client.second->nGameId)
			continue;
		sf::Packet* pPacket = new sf::Packet();
		(*pPacket) << (sf::Uint32)ePacketType_GameInfoList;
		(*pPacket) << (sf::Uint32)m_mapGames.size();
		for (auto& game : m_mapGames)
			game.second->GetInfo().Serialize(*pPacket);
		client.second->lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
	}
}

void CServer::SendGameState(const CGameState& rState, __in_opt Client* pClientToSend)
{
	if (pClientToSend)
	{
		sf::Packet* pPacket = new sf::Packet();
		(*pPacket) << (sf::Uint32)ePacketType_GameState;
		if (rState.Serialize(*pPacket))
		{
			pClientToSend->lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			delete pPacket;
			log_error("Failed to serialize game state. GameId=%u, GameName=%S", rState.GetGameId(), rState.GetGameName().c_str());
		}
	}
	else
	{
		for (auto& player : rState.GetPlayers())
		{
			Client* pClient = NULL;
			for (auto& client : m_mapClients)
			{
				if ((client.second->nGameId == rState.GetGameId()) && (client.second->nPlayerId == player.second->GetId()))
				{
					pClient = client.second.get();
					break;
				}
			}
			if (pClient)
			{
				sf::Packet* pPacket = new sf::Packet();
				(*pPacket) << (sf::Uint32)ePacketType_GameState;
				if (rState.Serialize(*pPacket))
				{
					pClient->lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
				}
				else
				{
					delete pPacket;
					log_error("Failed to serialize game state. GameId=%u, GameName=%S", rState.GetGameId(), rState.GetGameName().c_str());
				}
			}
		}
	}
}

void CServer::SendChatState(const CChat& rChat, const CGameState& rState, __in_opt Client* pClientToSend)
{
	if (pClientToSend)
	{
		sf::Packet* pPacket = new sf::Packet();
		(*pPacket) << (sf::Uint32)ePacketType_ChatState;
		if (rChat.Serialize(*pPacket))
		{
			pClientToSend->lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
		}
		else
		{
			delete pPacket;
			log_error("Failed to serialize chat state. GameId=%u, GameName=%S", rState.GetGameId(), rState.GetGameName().c_str());
		}
	}
	else
	{
		for (auto& player : rState.GetPlayers())
		{
			Client* pClient = NULL;
			for (auto& client : m_mapClients)
			{
				if ((client.second->nGameId == rState.GetGameId()) && (client.second->nPlayerId == player.second->GetId()))
				{
					pClient = client.second.get();
					break;
				}
			}
			if (pClient)
			{
				sf::Packet* pPacket = new sf::Packet();
				(*pPacket) << (sf::Uint32)ePacketType_ChatState;
				if (rChat.Serialize(*pPacket))
				{
					pClient->lstOutPackets.push_back(std::unique_ptr<sf::Packet>(pPacket));
				}
				else
				{
					delete pPacket;
					log_error("Failed to serialize chat state. GameId=%u, GameName=%S", rState.GetGameId(), rState.GetGameName().c_str());
				}
			}
		}
	}
}
