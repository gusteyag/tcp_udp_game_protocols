#pragma once
#include <Windows.h>


#define CURRENT_PROTOCOL_VERSION_BCD			((sf::Uint32)0x00010000) // 1.0

enum ePacketType
{
	ePacketType_Handshake = 1,
	// Request from client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- BCD protocol version
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_GameInfoList,
	// Sends from server to client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- GameInfo list size
	// ???		------------	- GameInfo list data

	ePacketType_GameState,
	// Sends from server to client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// ???		------------	- GameState data

	ePacketType_CreateNewGameAndJoin,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- specified players count
	// ???		- std::wstring	- new game name
	// 4 bytes	- sf::Uint32	- player character
	// ???		- std::wstring	- player name
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult
	//   if eResult_Success:
	// 4 bytes	- sf::Uint32	- GameId
	// 4 bytes	- sf::Uint32	- PlayerId

	ePacketType_JoinToGame,
	// Request from client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- GameId to join
	// 4 bytes	- sf::Uint32	- player character
	// ???		- std::wstring	- player name
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult
	// 4 bytes	- sf::Uint32	- GameId
	//   if eResult_Success:
	// 4 bytes	- sf::Uint32	- PlayerId

	ePacketType_LeftGame,
	// Request from client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_RollDices,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_RequestClueCard,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// 8 bytes	- sf::Uint64	- AssumedCard
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_AnswerClueCard,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_ReceiveClueCardAnswer,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_Move,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- points list size
	// ???		------------	- list of the moving points (x, y, ..., x, y)
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_MakeDeduction,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eWeapon
	// 4 bytes	- sf::Uint32	- eCharacter
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_AnswerToDeduction,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_ReceiveDeductionAnswers,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_MakeAccusation,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eRoom
	// 4 bytes	- sf::Uint32	- eWeapon
	// 4 bytes	- sf::Uint32	- eCharacter
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_SkipAccusation,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_ChatMessage,
	// Request (from client):
	// 4 bytes	- sf::Uint32	- ePacketType
	// ???		- std::wstring	- player message
	// Answer from server:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- eResult

	ePacketType_ChatState,
	// Sends from server to client:
	// 4 bytes	- sf::Uint32	- ePacketType
	// 4 bytes	- sf::Uint32	- chat message list size
	// ???		------------	- chat message list data
};
