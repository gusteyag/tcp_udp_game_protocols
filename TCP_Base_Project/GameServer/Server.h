#pragma once
#include <windows.h>
#include <memory>
#include <list>
#include <condition_variable>
#include <SFML\Network.hpp>
#include <Game.h>

class CServer
{
public:
	CServer(const std::string& rsAddress, unsigned short nPort);
	virtual ~CServer();

	void Run();
	void Stop();

private:
	bool m_bStop;
	std::condition_variable m_cvWaitStopping;
	std::mutex m_mtxWaitStopping;
	static sf::Uint32 m_nClientIdCounter;
	sf::TcpListener m_Listener;
	std::string m_sAddress;
	unsigned short m_nPort;

	struct Client
	{
		sf::Uint32 nId;
		sf::TcpSocket Socket;
		sf::Packet InPacket;
		std::list<std::unique_ptr<sf::Packet> > lstOutPackets;
		bool bHandshake;
		bool bRejected;
		sf::Uint32 nGameId;
		sf::Uint32 nPlayerId;

		Client(sf::Uint32 nId)
			: nId(nId)
			, bHandshake(false)
			, bRejected(false)
			, nGameId(0)
			, nPlayerId(0)
		{}
	};

	Client* m_pClientToAccept;
	std::map<sf::Uint32, std::unique_ptr<Client> > m_mapClients;

	class CGameWrapper : public CGame
	{
		CServer& m_rServer;
		virtual void OnStateChanged(const CGameState& rState) { m_rServer.SendGameState(rState, NULL); }
		virtual void OnInfoChanged(const CGameInfo& rState) { m_rServer.SendGameInfoList(NULL); }
		virtual void OnChatChanged(const CChat& rChat, const CGameState& rState) { m_rServer.SendChatState(rChat, rState, NULL); }
		virtual void OnGameRemoved(const CGameState& rState)
		{
			for (auto& client : m_rServer.m_mapClients)
			{
				if (client.second->nGameId != rState.GetGameId())
					continue;
				client.second->nGameId = 0;
				client.second->nPlayerId = 0;
			}
			CServer& rServer = m_rServer; // Before deleting
			m_rServer.m_mapGames.erase(rState.GetGameId());
			rServer.SendGameInfoList(NULL);
		}

	public:
		CGameWrapper(DWORD dwId, const std::wstring& rsName, DWORD dwSpecifiedPlayersCount, CServer& rServer)
			: CGame(dwId, rsName, dwSpecifiedPlayersCount)
			, m_rServer(rServer)
		{}
		virtual ~CGameWrapper() {}
	};

	void RemoveDisconnectedClientFromGame(DWORD dwGameId, DWORD dwPlayerId);
	void SendGameInfoList(__in_opt Client* pClientToSend);
	void SendGameState(const CGameState& rState, __in_opt Client* pClientToSend);
	void SendChatState(const CChat& rChat, const CGameState& rState, __in_opt Client* pClientToSend);

	static sf::Uint32 m_nGameIdCounter;
	std::map<sf::Uint32, std::unique_ptr<CGame> > m_mapGames;

	static sf::Uint32 m_nPlayerIdCounter;

private:
	bool ProcessProtocol(Client& rClient);
};
