#pragma once
#include <Windows.h>
#include "Server.h"
#include "Logger.h"
#include <memory>
#include <condition_variable>

std::unique_ptr<CServer> g_pServer;

BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
	case CTRL_C_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
	{
		if(g_pServer)
			g_pServer->Stop();
	}
	break;

	default:
		return FALSE;
	}

	return TRUE;
}

int main(int argc, char **argv)
{
	std::string sAddress = "0.0.0.0";
	std::string sPort = "55555";

	if (argc <= 1)
	{
		puts("Usage: module.exe -p <port> -a <ip address>\n");
	}
	else
	{
		int icmd = 1;
		while (icmd < argc)
		{
			if (!strcmp(argv[icmd], "-p") || !strcmp(argv[icmd], "--url") || !strcmp(argv[icmd], "--port"))
			{
				if (++icmd >= argc)
					break;
				if (!strncmp(argv[icmd], "-", 1) || !strncmp(argv[icmd], "--", 2))
					continue;
				sPort = argv[icmd];
			}
			else if (!strcmp(argv[icmd], "-a") || !strcmp(argv[icmd], "--address"))
			{
				if (++icmd >= argc)
					break;
				if (!strncmp(argv[icmd], "-", 1) || !strncmp(argv[icmd], "--", 2))
					continue;
				sAddress = argv[icmd];
			}
			++icmd;
		}
	}

	try
	{
		if (sPort.empty() || (sPort.find_first_not_of("0123456789") != std::string::npos))
		{
			puts("Invalid port.");
			SetConsoleCtrlHandler(CtrlHandler, FALSE);
			return 0;
		}
		if (sAddress.empty() || (sAddress.find_first_not_of("0123456789.") != std::string::npos))
		{
			puts("Invalid address.");
			SetConsoleCtrlHandler(CtrlHandler, FALSE);
			return 0;
		}
		unsigned short nPort = std::stoi(sPort);

		g_pServer.reset(new CServer(sAddress, nPort));

		if (!SetConsoleCtrlHandler(CtrlHandler, TRUE))
		{
			DWORD dwErr = GetLastError();
			log_error("Failed to call SetConsoleCtrlHandler(). Error=%u", dwErr);
			return 0;
		}

		g_pServer->Run();
	}
	catch (const std::exception& ex)
	{
		log_error("Exception occurred: \"%s\"", ex.what());
	}

	SetConsoleCtrlHandler(CtrlHandler, FALSE);
	g_pServer.reset(NULL);
	return 0;
}
