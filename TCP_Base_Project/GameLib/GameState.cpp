#include "GameState.h"

CGameState::CGameState()
	: m_dwId(0)
	, m_dwSpecifiedPlayersCount(0)
	, m_eGameState(eGameStates_Created)
	, m_eMurderRoom(eRooms_None)
	, m_eMurderWeapon(eWeapons_None)
	, m_eMurderCharacter(eCharacters_None)
	, m_dwRollDicesLastPlayerId(0)
	, m_eClueCardType(eCardTypes_None)
	, m_ullRequestedClueCard(0)
	, m_ullAnsweredClueCard(0)
	, m_dwAnsweredClueCardPlayerId(0)
	, m_eDeductionRoom(eRooms_None)
	, m_eDeductionWeapon(eWeapons_None)
	, m_eDeductionCharacter(eCharacters_None)
{
	m_DicesResult = g_ptEmptyDicesResult;
}

CGameState::CGameState(const CGameState& obj)
{
	(*this) = obj;
}

CGameState::~CGameState()
{
}

CGameState& CGameState::operator=(const CGameState& obj)
{
	m_dwId = obj.m_dwId;
	m_sName = obj.m_sName;
	m_dwSpecifiedPlayersCount = obj.m_dwSpecifiedPlayersCount;
	m_eGameState = obj.m_eGameState;
	for (auto& room : obj.m_mapRooms)
		m_mapRooms[room.first].reset(new CRoom(*room.second));
	for (auto& player : obj.m_mapPlayers)
		m_mapPlayers[player.first].reset(new CPlayer(*player.second));
	m_eMurderRoom = obj.m_eMurderRoom;
	m_eMurderWeapon = obj.m_eMurderWeapon;
	m_eMurderCharacter = obj.m_eMurderCharacter;

	m_DicesResult = obj.m_DicesResult;
	m_dwRollDicesLastPlayerId = obj.m_dwRollDicesLastPlayerId;

	m_eClueCardType = obj.m_eClueCardType;
	m_ullRequestedClueCard = obj.m_ullRequestedClueCard;
	m_ullAnsweredClueCard = obj.m_ullAnsweredClueCard;
	m_dwAnsweredClueCardPlayerId = obj.m_dwAnsweredClueCardPlayerId;

	m_eDeductionRoom = obj.m_eDeductionRoom;
	m_eDeductionWeapon = obj.m_eDeductionWeapon;
	m_eDeductionCharacter = obj.m_eDeductionCharacter;
	m_DeductionAnswer = obj.m_DeductionAnswer;

	m_mapRooms.clear();
	for (auto& room : obj.m_mapRooms)
		m_mapRooms[room.first].reset(new CRoom(*room.second));

	for (auto& player : obj.m_mapPlayers)
		m_mapPlayers[player.first].reset(new CPlayer(*player.second));

	return (*this);
}

BOOL CGameState::Serialize(__out sf::Packet& rPacket) const
{
	rPacket << (sf::Uint32)m_dwId;
	rPacket << m_sName;
	rPacket << (sf::Uint32)m_dwSpecifiedPlayersCount;
	rPacket << (sf::Uint32)m_eGameState;
	rPacket << (sf::Uint32)m_eMurderRoom;
	rPacket << (sf::Uint32)m_eMurderWeapon;
	rPacket << (sf::Uint32)m_eMurderCharacter;
	rPacket << (sf::Int32)m_DicesResult.cx;
	rPacket << (sf::Int32)m_DicesResult.cy;
	rPacket << (sf::Uint32)m_dwRollDicesLastPlayerId;
	rPacket << (sf::Uint32)m_eClueCardType;
	rPacket << (sf::Uint64)m_ullRequestedClueCard;
	rPacket << (sf::Uint64)m_ullAnsweredClueCard;
	rPacket << (sf::Uint32)m_dwAnsweredClueCardPlayerId;
	rPacket << (sf::Uint32)m_eDeductionRoom;
	rPacket << (sf::Uint32)m_eDeductionWeapon;
	rPacket << (sf::Uint32)m_eDeductionCharacter;
	rPacket << (sf::Uint32)m_DeductionAnswer.first;
	rPacket << (sf::Uint64)m_DeductionAnswer.second;

	rPacket << (sf::Uint32)m_mapRooms.size();
	for (auto& room : m_mapRooms)
	{
		rPacket << (sf::Uint32)room.second->GetType();
		rPacket << (sf::Uint32)room.second->GetPlayers().size();
		for (auto& player : room.second->GetPlayers())
			rPacket << (sf::Uint32)player;
		rPacket << (sf::Uint32)room.second->GetWeapons().size();
		for (auto& weapon : room.second->GetWeapons())
			rPacket << (sf::Uint32)weapon;
	}

	rPacket << (sf::Uint32)m_mapPlayers.size();
	for (auto& player : m_mapPlayers)
	{
		rPacket << (sf::Uint32)player.second->GetId();
		rPacket << player.second->GetName();
		rPacket << (sf::Uint32)player.second->GetCharacter();
		rPacket << (sf::Uint32)player.second->GetState();
		rPacket << (sf::Int32)player.second->GetPosition().x;
		rPacket << (sf::Int32)player.second->GetPosition().y;
		rPacket << (sf::Uint32)player.second->GetRoom();
		rPacket << (sf::Uint32)player.second->GetMovesLeft();
		rPacket << (sf::Uint32)player.second->WasMovedToRoom();
		rPacket << (sf::Uint32)player.second->IsInactive();
		rPacket << (sf::Uint32)player.second->IsDisconnected();
		rPacket << (sf::Uint32)player.second->GetCards().size();
		for (auto& card : player.second->GetCards())
			rPacket << (sf::Uint64)card;
		rPacket << (sf::Uint32)player.second->GetHints().size();
		for (auto& hint : player.second->GetHints())
		{
			rPacket << (sf::Uint64)hint.first;
			rPacket << (sf::Uint32)hint.second;
		}
	}

	return TRUE;
}

BOOL CGameState::Deserialize(__in sf::Packet& rPacket)
{
	sf::Int32 i32Data = 0;
	sf::Uint32 u32Data = 0;
	sf::Uint64 u64Data = 0;

	rPacket >> u32Data; m_dwId = (DWORD)u32Data;
	rPacket >> m_sName;
	rPacket >> u32Data; m_dwSpecifiedPlayersCount = (DWORD)u32Data;
	rPacket >> u32Data; m_eGameState = (eGameStates)u32Data;
	rPacket >> u32Data; m_eMurderRoom = (eRooms)u32Data;
	rPacket >> u32Data; m_eMurderWeapon = (eWeapons)u32Data;
	rPacket >> u32Data; m_eMurderCharacter = (eCharacters)u32Data;
	sf::Int32 iDicesResultCX = 0;
	sf::Int32 iDicesResultCY = 0;
	rPacket >> iDicesResultCX >> iDicesResultCY;
	m_DicesResult.cx = iDicesResultCX;
	m_DicesResult.cy = iDicesResultCY;
	rPacket >> u32Data; m_dwRollDicesLastPlayerId = (DWORD)u32Data;
	rPacket >> u32Data; m_eClueCardType = (eCardTypes)u32Data;
	rPacket >> u64Data; m_ullRequestedClueCard = (ULONG64)u64Data;
	rPacket >> u64Data; m_ullAnsweredClueCard = (ULONG64)u64Data;
	rPacket >> u32Data; m_dwAnsweredClueCardPlayerId = (DWORD)u32Data;
	rPacket >> u32Data; m_eDeductionRoom = (eRooms)u32Data;
	rPacket >> u32Data; m_eDeductionWeapon = (eWeapons)u32Data;
	rPacket >> u32Data; m_eDeductionCharacter = (eCharacters)u32Data;
	rPacket >> u32Data; m_DeductionAnswer.first = u32Data;
	rPacket >> u64Data; m_DeductionAnswer.second = u64Data;

	rPacket >> u32Data; m_mapRooms.clear();
	for (sf::Uint32 i = 0; i < u32Data; ++i)
	{
		sf::Uint32 u32Data2 = 0;
		rPacket >> u32Data2;
		CRoom* pRoom = new CRoom((eRooms)u32Data2);
		rPacket >> u32Data2;
		for (sf::Uint32 i = 0; i < u32Data2; ++i)
		{
			sf::Uint32 u32Data3 = 0;
			rPacket >> u32Data3;
			pRoom->AddPlayer(u32Data3);
		}
		rPacket >> u32Data2;
		for (sf::Uint32 i = 0; i < u32Data2; ++i)
		{
			sf::Uint32 u32Data3 = 0;
			rPacket >> u32Data3;
			pRoom->AddWeapon((eWeapons)u32Data3);
		}
		m_mapRooms[pRoom->GetType()].reset(pRoom);
	}

	rPacket >> u32Data; m_mapPlayers.clear();
	for (sf::Uint32 i = 0; i < u32Data; ++i)
	{
		sf::Uint32 nId = 0;
		std::wstring sName;
		sf::Uint32 nCharacter = 0;
		sf::Uint32 nState = 0;
		rPacket >> nId >> sName >> nCharacter >> nState;
		CPlayer* pPlayer = new CPlayer((DWORD)nId, sName, (eCharacters)nCharacter, (ePlayerStates)nState);
		sf::Int32 iPositionX = 0;
		sf::Int32 iPositionY = 0;
		rPacket >> iPositionX >> iPositionY;
		POINT pt = { (LONG)iPositionX, (LONG)iPositionY };
		pPlayer->SetPosition(pt);
		sf::Uint32 nRoom = 0;
		rPacket >> nRoom; pPlayer->SetRoom((eRooms)nRoom);
		sf::Uint32 nMovesLeft = 0;
		rPacket >> nMovesLeft; pPlayer->SetMovesLeft((DWORD)nMovesLeft);
		sf::Uint32 nWasMovedToRoom = 0;
		rPacket >> nWasMovedToRoom; if(nWasMovedToRoom) pPlayer->SetWasMovedToRoom();
		sf::Uint32 nInactive = 0;
		rPacket >> nInactive; if (nInactive) pPlayer->SetInactive();
		sf::Uint32 nDisconnected = 0;
		rPacket >> nDisconnected; if (nDisconnected) pPlayer->SetDisconnected();
		sf::Uint32 nCount = 0;
		rPacket >> nCount;
		for (sf::Uint32 i = 0; i < nCount; ++i)
		{
			sf::Uint64 nCard = 0;
			rPacket >> nCard;
			pPlayer->AddCard(nCard);
		}
		rPacket >> nCount;
		for (sf::Uint32 i = 0; i < nCount; ++i)
		{
			sf::Uint64 nCard = 0;
			sf::Uint32 nPlayerId = 0;
			rPacket >> nCard >> nPlayerId;
			pPlayer->AddHint(nCard, nPlayerId);
		}
		m_mapPlayers[pPlayer->GetId()].reset(pPlayer);
	}

	return TRUE;
}
