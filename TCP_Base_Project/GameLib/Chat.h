#pragma once
#include <Windows.h>
#include <string>
#include <list>
#include <SFML\Network.hpp>


class CChat
{
public:
	CChat();
	virtual ~CChat();

	BOOL Serialize(__out sf::Packet& rPacket) const;
	BOOL Deserialize(__in sf::Packet& rPacket);

	void AddMessage(DWORD dwPlayerId, const std::wstring& rsMessage);
	const std::list<std::pair<DWORD, std::wstring> >& GetMessages() const { return m_lstMessages; }

private:
	DWORD m_dwCurrentSize;
	std::list<std::pair<DWORD, std::wstring> > m_lstMessages;
};

