#pragma once
#include <windows.h>
#include <string>
#include "GameState.h"
#include "GameInfo.h"
#include "Chat.h"

class CGame
{
public:
	CGame(DWORD dwId, const std::wstring& rsName, DWORD dwSpecifiedPlayersCount);
	virtual ~CGame();

	virtual void OnStateChanged(const CGameState& rState) = 0;
	virtual void OnInfoChanged(const CGameInfo& rInfo) = 0;
	virtual void OnChatChanged(const CChat& rChat, const CGameState& rState) = 0;
	virtual void OnGameRemoved(const CGameState& rState) = 0;

	DWORD GetId() const { return m_State.GetGameId(); }
	const CGameState& GetState() const { return m_State; }
	CGameState& GetStateUnsafe() { return m_State; }
	const CGameInfo& GetInfo() const { return m_Info; }
	const CChat& GetChat() const { return m_Chat; }

	eResult AddChatMessage(DWORD dwPlayerId, const std::wstring& rsMessage)
	{
		if (!m_State.GetPlayers().count(dwPlayerId))
			return eResult_InvalidPlayerId;
		m_Chat.AddMessage(dwPlayerId, rsMessage);
		OnChatChanged(m_Chat, m_State);
		return eResult_Success;
	}

	const std::wstring& GetName() const { return m_State.GetGameName(); }
	DWORD GetSpecifiedPlayersCount() const { return m_State.GetSpecifiedPlayersCount(); }
	DWORD GetActualPlayersCount() const { return (DWORD)m_State.GetPlayers().size(); }

	eResult PlayerTriesToJoin(DWORD dwPlayerId, const std::wstring& rsName, eCharacters eCharacter);
	eResult PlayerLeftGame(DWORD dwPlayerId, BOOL bDisconnected);

	eResult PlayerTriesToRollDices(DWORD dwPlayerId);

	eResult PlayerTriesToRequestClueCard(DWORD dwPlayerId, ULONG64 ullAssumedCard);
	eResult PlayerTriesToAnswerClueCard(DWORD dwPlayerId);
	eResult PlayerTriesToReceiveClueCardAnswer(DWORD dwPlayerId);

	eResult PlayerTriesToMove(DWORD dwPlayerId, std::list<POINT> lstTrack);
	eResult PlayerTriesToMakeDeduction(DWORD dwPlayerId, eWeapons eWeapon, eCharacters eCharacter);
	eResult PlayerTriesToAnswerToDeduction(DWORD dwPlayerId);
	eResult PlayerTriesToReceiveDeductionAnswers(DWORD dwPlayerId);
	eResult PlayerTriesToMakeAccusation(DWORD dwPlayerId, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter);
	eResult PlayerTriesToSkipAccusation(DWORD dwPlayerId);

private:
	void StartGame();

	BOOL CheckNewPlayerIdAndName(DWORD dwPlayerId, const std::wstring& rsName) const;
	BOOL CheckNewPlayerCharacter(eCharacters eCharacter) const;
	BOOL CheckPlayerTrack(const std::list<POINT>& lstTrack, eRooms ePlayerInRoom) const;
	DWORD GetPlayerIdOnLeft(DWORD dwCurrentPlayerId) const;
	DWORD GetPlayerIdOnRight(DWORD dwCurrentPlayerId) const;

private:
	static DWORD m_nPlayerIdCounter;
	CGameState m_State;
	CGameInfo m_Info;
	CChat m_Chat;
};
