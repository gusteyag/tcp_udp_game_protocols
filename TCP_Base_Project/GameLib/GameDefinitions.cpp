#include "GameDefinitions.h"

extern const RECT g_rcLocation = {0, 0, 40, 30};
extern const POINT g_ptEmptyPosition = {-1, -1};
extern const SIZE g_ptEmptyDicesResult = { 0, 0 };
extern RoomLocations g_RoomLocations = RoomLocations();

eRooms PtInRoom(const Point& pt)
{
	for (auto& room : g_RoomLocations.map)
	{
		for (auto& rect : room.second)
		{
			if (PtInRect(&rect, pt))
				return room.first;
		}
	}
	return eRooms_None;
}

BOOL PtAroundRoom(const Point& pt, eRooms eRoom)
{
	if (PtInRoom(pt) != eRooms_None)
		return FALSE;
	for (auto& rect : g_RoomLocations.map.at(eRoom))
	{
		RECT rc = rect;
		InflateRect(&rc, 1, 1);
		if (PtInRect(&rc, pt))
			return TRUE;
	}
	return FALSE;
}
