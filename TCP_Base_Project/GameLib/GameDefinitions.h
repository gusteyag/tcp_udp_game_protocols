#pragma once
#include <windows.h>
#include <map>
#include <list>
#include <SFML/Graphics.hpp>

#define PLAYERS_COUNT_MIN			3
#define PLAYERS_COUNT_MAX			6
#define CHAT_SIZE_MAX				4096 //bytes

extern const RECT g_rcLocation;
extern const POINT g_ptEmptyPosition;
extern const SIZE g_ptEmptyDicesResult;

enum eResult
{
	eResult_Success,
	eResult_Fail,

	eResult_InvalidParameter,
	eResult_AlreadyExists,
	eResult_InappropriateGameState,
	eResult_ExcessAmount,
	eResult_NameAlreadyExists,
	eResult_NotFound,
	eResult_GameNameAlreadyExists,
	eResult_CharacterIsBusy,
	eResult_PositionIsBusy,
	eResult_InvalidPlayerId,
	eResult_InappropriatePlayerState,
	eResult_InvalidPlayerTrack,
	eResult_PlayerHasNoMoves,
	eResult_PlayerNotInGame,
	eResult_NotReady,
	eResult_NotImplemented,
};

enum eCardTypes
{
	eCardTypes_None, // placeholder
	eCardTypes_Character,
	eCardTypes_Weapon,
	eCardTypes_Room,
};
inline LPCWSTR enum_desc(eCardTypes eValue)
{
	switch (eValue)
	{
	case eCardTypes_Character: return L"Character";
	case eCardTypes_Weapon: return L"Weapon";
	case eCardTypes_Room: return L"Room";
	}
	return L"Unknown";
}
inline eCardTypes enum_check(eCardTypes eValue)
{
	switch (eValue)
	{
	case eCardTypes_Character: return eCardTypes_Character;
	case eCardTypes_Weapon: return eCardTypes_Weapon;
	case eCardTypes_Room: return eCardTypes_Room;
	}
	return eCardTypes_None;
}

enum eCharacters
{
	eCharacters_None, // plaseholder
	eCharacters_Amapola,
	eCharacters_Rubio,
	eCharacters_Orquidea,
	eCharacters_Prado,
	eCharacters_Celeste,
	eCharacters_Mora,
};
inline LPCWSTR enum_desc(eCharacters eValue)
{
	switch (eValue)
	{
	case eCharacters_Amapola: return L"Amapola";
	case eCharacters_Rubio: return L"Rubio";
	case eCharacters_Orquidea: return L"Orquidea";
	case eCharacters_Prado: return L"Prado";
	case eCharacters_Celeste: return L"Celeste";
	case eCharacters_Mora: return L"Mora";
	}
	return L"Unknown";
}
inline eCharacters enum_check(eCharacters eValue)
{
	switch (eValue)
	{
	case eCharacters_Amapola: return eCharacters_Amapola;
	case eCharacters_Rubio: return eCharacters_Rubio;
	case eCharacters_Orquidea: return eCharacters_Orquidea;
	case eCharacters_Prado: return eCharacters_Prado;
	case eCharacters_Celeste: return eCharacters_Celeste;
	case eCharacters_Mora: return eCharacters_Mora;
	}
	return eCharacters_None;
}

enum eWeapons
{
	eWeapons_None, // plaseholder
	eWeapons_Punyal,
	eWeapons_Cuerda,
	eWeapons_Candelabro,
	eWeapons_Pistola,
	eWeapons_Tuberia,
	eWeapons_Herramienta,
};
inline LPCWSTR enum_desc(eWeapons eValue)
{
	switch (eValue)
	{
	case eWeapons_Punyal: return L"Punyal";
	case eWeapons_Cuerda: return L"Cuerda";
	case eWeapons_Candelabro: return L"Candelabro";
	case eWeapons_Pistola: return L"Pistola";
	case eWeapons_Tuberia: return L"Tuberia";
	case eWeapons_Herramienta: return L"Herramienta";
	}
	return L"Unknown";
}
inline eWeapons enum_check(eWeapons eValue)
{
	switch (eValue)
	{
	case eWeapons_Punyal: return eWeapons_Punyal;
	case eWeapons_Cuerda: return eWeapons_Cuerda;
	case eWeapons_Candelabro: return eWeapons_Candelabro;
	case eWeapons_Pistola: return eWeapons_Pistola;
	case eWeapons_Tuberia: return eWeapons_Tuberia;
	case eWeapons_Herramienta: return eWeapons_Herramienta;
	}
	return eWeapons_None;
}

enum eRooms
{
	eRooms_None, // plaseholder
	eRooms_Cocina,
	eRooms_Comedor,
	eRooms_Salon,
	eRooms_Estudio,
	eRooms_Biblioteca,
	eRooms_Billar,
	eRooms_Terraza,
	eRooms_Baile,
	eRooms_Escaleras,
};
inline LPCWSTR enum_desc(eRooms eValue)
{
	switch (eValue)
	{
	case eRooms_Cocina: return L"Cocina";
	case eRooms_Comedor: return L"Comedor";
	case eRooms_Salon: return L"Salon";
	case eRooms_Estudio: return L"Estudio";
	case eRooms_Biblioteca: return L"Biblioteca";
	case eRooms_Billar: return L"Billar";
	case eRooms_Terraza: return L"Terraza";
	case eRooms_Baile: return L"Baile";
	case eRooms_Escaleras: return L"Escaleras";
	}
	return L"Unknown";
}
inline eRooms enum_check(eRooms eValue)
{
	switch (eValue)
	{
	case eRooms_Cocina: return eRooms_Cocina;
	case eRooms_Comedor: return eRooms_Comedor;
	case eRooms_Salon: return eRooms_Salon;
	case eRooms_Estudio: return eRooms_Estudio;
	case eRooms_Biblioteca: return eRooms_Biblioteca;
	case eRooms_Billar: return eRooms_Billar;
	case eRooms_Terraza: return eRooms_Terraza;
	case eRooms_Baile: return eRooms_Baile;
	case eRooms_Escaleras: return eRooms_Escaleras;
	}
	return eRooms_None;
}

// helper functions
inline RECT make_RECT(LONG l, LONG t, LONG r, LONG b)
{
	RECT rc = {l, t, r, b};
	return rc;
}
inline POINT make_POINT(LONG x, LONG y)
{
	POINT pt = { x, y };
	return pt;
}
inline SIZE make_SIZE(LONG x, LONG y)
{
	SIZE sz = { x, y };
	return sz;
}

inline ULONG64 make_CharacterCard(eCharacters eCharacter)
{
	return (((ULONG64)eCardTypes_Character << 32) | (ULONG64)eCharacter);
}
inline ULONG64 make_WeaponCard(eWeapons eWeapon)
{
	return (((ULONG64)eCardTypes_Weapon << 32) | (ULONG64)eWeapon);
}
inline ULONG64 make_RoomCard(eRooms eRoom)
{
	return (((ULONG64)eCardTypes_Room << 32) | (ULONG64)eRoom);
}
inline eCardTypes get_CardType(ULONG64 ullCard)
{
	return enum_check((eCardTypes)(ullCard >> 32));
}
inline ULONG get_Card(ULONG64 ullCard)
{
	return (ULONG)(ullCard & 0x00000000ffffffff);
}
inline eCharacters get_CharacterCardType(ULONG64 ullCard)
{
	if (enum_check((eCardTypes)(ullCard >> 32)) != eCardTypes_Character)
		return eCharacters_None;
	return enum_check((eCharacters)(ullCard & 0x00000000ffffffff));
}
inline eWeapons get_WeaponCardType(ULONG64 ullCard)
{
	if (enum_check((eCardTypes)(ullCard >> 32)) != eCardTypes_Weapon)
		return eWeapons_None;
	return enum_check((eWeapons)(ullCard & 0x00000000ffffffff));
}
inline eRooms get_RoomCardType(ULONG64 ullCard)
{
	if (enum_check((eCardTypes)(ullCard >> 32)) != eCardTypes_Room)
		return eRooms_None;
	return enum_check((eRooms)(ullCard & 0x00000000ffffffff));
}
inline LPCWSTR get_CardTypeName(ULONG64 ullCard)
{
	return enum_desc(get_CardType(ullCard));
}
inline LPCWSTR get_CardName(ULONG64 ullCard)
{
	switch (get_CardType(ullCard))
	{
	case eCardTypes_Character: return enum_desc(get_CharacterCardType(ullCard));
	case eCardTypes_Weapon: return enum_desc(get_WeaponCardType(ullCard));
	case eCardTypes_Room: return enum_desc(get_RoomCardType(ullCard));
	}
	return L"Unknown";
}

// Map of the room coordinates
struct RoomLocations
{
	std::map<eRooms, std::list<RECT> > map;

	RoomLocations()
	{
		map[eRooms_Cocina].push_back(make_RECT(0, 20, 10, 30));
		map[eRooms_Comedor].push_back(make_RECT(13, 20, 26, 30));
		map[eRooms_Salon].push_back(make_RECT(30, 22, 40, 30));
		map[eRooms_Estudio].push_back(make_RECT(32, 0, 40, 10));
		map[eRooms_Biblioteca].push_back(make_RECT(22, 0, 28, 10));
		map[eRooms_Billar].push_back(make_RECT(12, 0, 18, 10));
		map[eRooms_Terraza].push_back(make_RECT(0, 0, 8, 10));
		map[eRooms_Baile].push_back(make_RECT(0, 12, 8, 18));
		map[eRooms_Escaleras].push_back(make_RECT(30, 12, 40, 20));
	}
};
extern RoomLocations g_RoomLocations;

class Point : public sf::Vector2i
{
public:
	Point()
	{}
	Point(int x, int y)
		: sf::Vector2i(x, y)
	{}
	Point(const POINT& pt)
		: sf::Vector2i(pt.x, pt.y)
	{}
	operator POINT() const
	{
		POINT pt = {x, y};
		return pt;
	}
	bool operator==(const POINT& pt) const
	{
		return ((x == pt.x) && (y == pt.y));
	}
	bool operator==(const Point& pt) const
	{
		return ((x == pt.x) && (y == pt.y));
	}
};

eRooms PtInRoom(const Point& pt);
BOOL PtAroundRoom(const Point& pt, eRooms eRoom);
