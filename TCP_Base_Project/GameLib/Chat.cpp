#include "Chat.h"
#include "GameDefinitions.h"


CChat::CChat()
	: m_dwCurrentSize(0)
{
}

CChat::~CChat()
{
}

BOOL CChat::Serialize(__out sf::Packet& rPacket) const
{
	rPacket << (sf::Uint32)m_lstMessages.size();
	for (auto& message : m_lstMessages)
		rPacket << (sf::Uint32)message.first << message.second;
	return TRUE;
}

BOOL CChat::Deserialize(__in sf::Packet& rPacket)
{
	sf::Uint32 nCount = 0;
	rPacket >> nCount; m_lstMessages.clear(); m_dwCurrentSize = 0;
	for (sf::Uint32 i = 0; i < nCount; ++i)
	{
		sf::Uint32 nPlayerId = 0;
		std::wstring sMessage;
		rPacket >> nPlayerId >> sMessage;
		m_lstMessages.push_back(make_pair((DWORD)nPlayerId, sMessage));
		m_dwCurrentSize += ((DWORD)sMessage.size() + 1) * sizeof(wchar_t);
	}
	return TRUE;
}

void CChat::AddMessage(DWORD dwPlayerId, const std::wstring& rsMessage)
{
	DWORD dwNewSize = m_dwCurrentSize + ((DWORD)rsMessage.size() + 1) * sizeof(wchar_t);
	while (dwNewSize > CHAT_SIZE_MAX)
	{
		dwNewSize -= ((DWORD)m_lstMessages.front().second.size() + 1) * sizeof(wchar_t);
		m_lstMessages.pop_front();
	}
	m_lstMessages.push_back(std::make_pair(dwPlayerId, rsMessage));
}
