#pragma once
#include <windows.h>
#include <set>
#include "GameDefinitions.h"
#include "Player.h"


class CRoom
{
public:
	eRooms GetType() const { return m_eType; }

	void AddPlayer(DWORD dwPlayerId) { m_setPlayers.insert(dwPlayerId); }
	void RemovePlayer(DWORD dwPlayerId) { m_setPlayers.erase(dwPlayerId); }
	BOOL IsPlayerInRoom(DWORD dwPlayerId) const { return (m_setPlayers.count(dwPlayerId) != 0); }
	const std::set<DWORD> GetPlayers() const { return m_setPlayers; }

	void AddWeapon(eWeapons eWeapon) { m_setWeapons.insert(eWeapon); }
	void RemoveWeapon(eWeapons eWeapon) { m_setWeapons.erase(eWeapon); }
	BOOL IsWeaponInRoom(eWeapons eWeapon) const { return (m_setWeapons.count(eWeapon) != 0); }
	const std::set<eWeapons> GetWeapons() const { return m_setWeapons; }

	CRoom(eRooms eType)
		: m_eType(eType)
	{}
	virtual ~CRoom() {};

private:
	eRooms m_eType;
	std::set<DWORD> m_setPlayers;
	std::set<eWeapons> m_setWeapons;
};
