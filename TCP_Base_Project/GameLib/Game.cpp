#include "Game.h"
#include <algorithm>
#include <vector>


DWORD CGame::m_nPlayerIdCounter = 0;

CGame::CGame(DWORD dwId, const std::wstring& rsName, DWORD dwSpecifiedPlayersCount)
{
	if (dwSpecifiedPlayersCount < PLAYERS_COUNT_MIN)
		dwSpecifiedPlayersCount = PLAYERS_COUNT_MIN;
	if (dwSpecifiedPlayersCount > PLAYERS_COUNT_MAX)
		dwSpecifiedPlayersCount = PLAYERS_COUNT_MAX;
	m_State.SetGameId(dwId);
	m_State.SetGameName(rsName);
	m_State.SetSpecifiedPlayersCount(dwSpecifiedPlayersCount);
	m_Info.SetGameId(dwId);
	m_Info.SetGameName(rsName);
	m_Info.SetSpecifiedPlayersCount(dwSpecifiedPlayersCount);
}

CGame::~CGame()
{
}

eResult CGame::PlayerTriesToJoin(DWORD dwPlayerId, const std::wstring& rsName, eCharacters eCharacter)
{
	if (m_State.GetGameState() != eGameStates_Created)
		return eResult_InappropriateGameState;
	if (GetActualPlayersCount() >= GetSpecifiedPlayersCount())
		return eResult_ExcessAmount;
	if(!CheckNewPlayerIdAndName(dwPlayerId, rsName))
		return eResult_NameAlreadyExists;
	if (eCharacter == eCharacters_None)
		return eResult_InvalidParameter;
	if (!CheckNewPlayerCharacter(eCharacter))
		return eResult_CharacterIsBusy;

	m_State.AddPlayer(new CPlayer(dwPlayerId, rsName, eCharacter, ePlayerStates_InLobby));
	if (GetActualPlayersCount() == GetSpecifiedPlayersCount())
		StartGame();

	m_Info.AddPlayer(dwPlayerId, rsName, eCharacter);
	m_Info.SetGameState(m_State.GetGameState());
	OnInfoChanged(m_Info);
	
	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerLeftGame(DWORD dwPlayerId, BOOL bDisconnected)
{
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	if(!m_State.GetPlayers().count(dwPlayerId) || (m_State.GetPlayers().at(dwPlayerId)->GetState() == ePlayerStates_LeftGame))
		return eResult_Success;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();

	if (m_State.GetGameState() == eGameStates_Created)
	{
		m_State.GetPlayers().erase(pPlayer->GetId());
		if (m_State.GetPlayers().size() == 0)
		{
			OnGameRemoved(m_State);
			return eResult_Success;
		}
	}
	else
	{
		int nCount = 0;
		for (auto& player : m_State.GetPlayers())
		{
			if ((player.second->GetId() != pPlayer->GetId()) &&
				(player.second->GetState() != ePlayerStates_LeftGame))
			{
				++nCount;
			}
		}
		if (!nCount)
		{
			OnGameRemoved(m_State);
			return eResult_Success;
		}

		if (m_State.GetGameState() == eGameStates_Started)
		{
			for (auto& player : m_State.GetPlayers())
			{
				if ((player.second->GetId() != pPlayer->GetId()) &&
					(player.second->GetState() != ePlayerStates_LeftGame))
				{
					for (auto& card : pPlayer->GetCards())
						player.second->AddHint(card, pPlayer->GetId());
				}
			}

			switch (pPlayer->GetState())
			{
			case ePlayerStates_RollDices:
			{
				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_RequestingClueCard:
			{
				m_State.SetClueCardType(eCardTypes_None);

				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_WaitForClueCardAnswer:
			{
				m_State.SetClueCardType(eCardTypes_None);
				m_State.SetRequestedClueCard(0);

				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_ReceivingClueCardAnswer:
			{
				if (m_State.GetAnsweredClueCard())
				{
					for (auto& player : m_State.GetPlayers())
					{
						if (player.second->GetId() != m_State.GetAnsweredPlayerId())
							player.second->AddHint(m_State.GetAnsweredClueCard(), m_State.GetAnsweredPlayerId());
					}
				}
				m_State.SetClueCardType(eCardTypes_None);
				m_State.SetRequestedClueCard(0);
				m_State.SetAnsweredClueCard(0, 0);

				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_AnsweringClueCard:
			{
				CPlayer* pWaitForAnswerPlayer = NULL;
				for (auto& player : m_State.GetPlayers())
				{
					if (player.second->GetState() == ePlayerStates_WaitForClueCardAnswer)
					{
						pWaitForAnswerPlayer = player.second.get();
						break;
					}
				}
				if (pWaitForAnswerPlayer)
				{
					ULONG64 ullCard = m_State.GetRequestedClueCard();
					BOOL bFound = FALSE;
					for (auto& card : pPlayer->GetCards())
					{
						if (card == ullCard)
						{
							bFound = TRUE;
							break;
						}
					}
					if (bFound)
						m_State.SetAnsweredClueCard(ullCard, pPlayer->GetId());
					else
						m_State.SetAnsweredClueCard(ullCard, 0);
					pWaitForAnswerPlayer->SetState(ePlayerStates_ReceivingClueCardAnswer);
				}
			}
			break;
			
			case ePlayerStates_Move:
			{
				pPlayer->SetMovesLeft(0);
				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_MakeDeduction:
			{
				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_WaitForDeductionAnswers:
			case ePlayerStates_ReceivingDeductionAnswers:
			{
				m_State.ClearDeductionData();

				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;

			case ePlayerStates_AnswersToDeduction:
			{
				CPlayer* pWaitForDeductionPlayer = NULL;
				for (auto& player : m_State.GetPlayers())
				{
					if (player.second->GetState() == ePlayerStates_WaitForDeductionAnswers)
					{
						pWaitForDeductionPlayer = player.second.get();
						break;
					}
				}

				if (pWaitForDeductionPlayer)
				{
					ULONG64 ullRoomCard = make_RoomCard(m_State.GetDeductionRoom());
					ULONG64 ullWeaponCard = make_WeaponCard(m_State.GetDeductionWeapon());
					ULONG64 ullCharacterCard = make_CharacterCard(m_State.GetDeductionCharacter());

					ULONG64 ullCard = 0;
					if (pPlayer->DoesHaveCard(ullRoomCard))
						ullCard = ullRoomCard;
					else if (pPlayer->DoesHaveCard(ullWeaponCard))
						ullCard = ullWeaponCard;
					else if (pPlayer->DoesHaveCard(ullCharacterCard))
						ullCard = ullCharacterCard;

					if (ullCard)
					{
						m_State.SetDeductionAnswer(pPlayer->GetId(), ullCard);
						pWaitForDeductionPlayer->SetState(ePlayerStates_ReceivingDeductionAnswers);
					}
					else
					{
						m_State.SetDeductionAnswer(pPlayer->GetId(), 0);
						CPlayer* pRightPlayer = m_State.GetPlayers().at(GetPlayerIdOnRight(pPlayer->GetId())).get();
						if (pRightPlayer->GetState() == ePlayerStates_WaitForDeductionAnswers)
							pRightPlayer->SetState(ePlayerStates_ReceivingDeductionAnswers);
						else
							pRightPlayer->SetState(ePlayerStates_AnswersToDeduction);
					}
				}
				else
				{
					// Do nothing. Maybe a player left the game.
				}
			}
			break;

			case ePlayerStates_PromptToMakeAccusation:
			{
				CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
				while (pLeftPlayer->IsInactive())
					pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
				pLeftPlayer->SetState(ePlayerStates_RollDices);
			}
			break;
			}
		}

		pPlayer->SetState(ePlayerStates_LeftGame);
		if (bDisconnected)
			pPlayer->SetDisconnected();
	}

	m_Info.RemovePlayer(dwPlayerId);
	OnInfoChanged(m_Info);

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToRollDices(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_RollDices)
		return eResult_InappropriatePlayerState;

	pPlayer->ClearWasMovedToRoom();

	SIZE DiceResult = { (rand() % 6 + 1), (rand() % 6 + 1) };
	m_State.SetDicesResult(DiceResult, pPlayer->GetId());
	pPlayer->SetMovesLeft((DWORD)(DiceResult.cx + DiceResult.cy));
	if ((DiceResult.cx == 1) || (DiceResult.cy == 1))
	{
		std::vector<eCardTypes> vecClueCardTypes;
		vecClueCardTypes.push_back(eCardTypes_Character);
		vecClueCardTypes.push_back(eCardTypes_Weapon);
		vecClueCardTypes.push_back(eCardTypes_Room);
		std::random_shuffle(vecClueCardTypes.begin(), vecClueCardTypes.end());
		m_State.SetClueCardType(vecClueCardTypes.front());
		pPlayer->SetState(ePlayerStates_RequestingClueCard);
	}
	else
	{
		pPlayer->SetState(ePlayerStates_Move);
	}

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToRequestClueCard(DWORD dwPlayerId, ULONG64 ullAssumedCard)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_RequestingClueCard)
		return eResult_InappropriatePlayerState;

	switch (m_State.GetClueCardType())
	{
	case eCardTypes_Character:
	{
		eCharacters eCharacter = get_CharacterCardType(ullAssumedCard);
		if (eCharacter == eCharacters_None)
			return eResult_InvalidParameter;
	}
	break;
		
	case eCardTypes_Weapon:
	{
		eWeapons eWeapon = get_WeaponCardType(ullAssumedCard);
		if (eWeapon == eWeapons_None)
			return eResult_InvalidParameter;
	}
	break;

	case eCardTypes_Room:
	{
		eRooms eRoom = get_RoomCardType(ullAssumedCard);
		if (eRoom == eRooms_None)
			return eResult_InvalidParameter;
	}
	break;
	}

	m_State.SetRequestedClueCard(ullAssumedCard);
	CPlayer* pClueCardPlayer = NULL;
	for (auto& player : m_State.GetPlayers())
	{
		if ((player.second->GetState() != ePlayerStates_LeftGame) &&
			player.second->GetCards().count(ullAssumedCard) &&
			player.second->GetId() != pPlayer->GetId())
		{
			pClueCardPlayer = player.second.get();
			break;
		}
	}
	if (pClueCardPlayer)
	{
		//if (pClueCardPlayer->GetState() == ePlayerStates_LeftGame)
		//{
		//	m_State.SetAnsweredClueCard(ullAssumedCard, pClueCardPlayer->GetId());
		//	pPlayer->SetState(ePlayerStates_ReceivingClueCardAnswer);
		//}
		//else
		{
			pClueCardPlayer->SetState(ePlayerStates_AnsweringClueCard);
			pPlayer->SetState(ePlayerStates_WaitForClueCardAnswer);
		}
	}
	else
	{
		m_State.SetAnsweredClueCard(0, 0);
		pPlayer->SetState(ePlayerStates_ReceivingClueCardAnswer);
	}

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToAnswerClueCard(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_AnsweringClueCard)
		return eResult_InappropriatePlayerState;

	CPlayer* pWaitForAnswerPlayer = NULL;
	for (auto& player : m_State.GetPlayers())
	{
		if (player.second->GetState() == ePlayerStates_WaitForClueCardAnswer)
		{
			pWaitForAnswerPlayer = player.second.get();
			break;
		}
	}
	if (pWaitForAnswerPlayer)
	{
		ULONG64 ullCard = m_State.GetRequestedClueCard();
		BOOL bFound = FALSE;
		for (auto& card : pPlayer->GetCards())
		{
			if (card == ullCard)
			{
				bFound = TRUE;
				break;
			}
		}
		if (!bFound)
			return eResult_Fail;
		m_State.SetAnsweredClueCard(ullCard, pPlayer->GetId());
		pWaitForAnswerPlayer->SetState(ePlayerStates_ReceivingClueCardAnswer);
	}
	else
	{
		// Do nothing. Maybe a player left the game.
	}
	
	pPlayer->SetState(ePlayerStates_Normal);

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToReceiveClueCardAnswer(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_ReceivingClueCardAnswer)
		return eResult_InappropriatePlayerState;

	if (m_State.GetAnsweredClueCard())
	{
		for (auto& player : m_State.GetPlayers())
		{
			if(player.second->GetId() != m_State.GetAnsweredPlayerId())
				player.second->AddHint(m_State.GetAnsweredClueCard(), m_State.GetAnsweredPlayerId());
		}
	}

	m_State.SetClueCardType(eCardTypes_None);
	m_State.SetRequestedClueCard(0);
	m_State.SetAnsweredClueCard(0, 0);
	pPlayer->SetState(ePlayerStates_Move);

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToMove(DWORD dwPlayerId, std::list<POINT> lstTrack)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_Move)
		return eResult_InappropriatePlayerState;

	if (!pPlayer->GetMovesLeft())
		return eResult_PlayerHasNoMoves;
	while (pPlayer->GetMovesLeft() < lstTrack.size())
		lstTrack.pop_back();

	if (!CheckPlayerTrack(lstTrack, pPlayer->GetRoom()))
		return eResult_InvalidPlayerTrack;
	if (pPlayer->IsInRoom())
	{
		m_State.GetRooms().at(pPlayer->GetRoom())->RemovePlayer(pPlayer->GetId());
		pPlayer->SetRoom(eRooms_None);
	}
	pPlayer->SetPosition(lstTrack.back());
	pPlayer->ReduceMovesLeft((DWORD)lstTrack.size());

	eRooms eRoom = PtInRoom(pPlayer->GetPosition());
	if (eRoom != eRooms_None)
	{
		pPlayer->SetMovesLeft(0);
		pPlayer->SetRoom(eRoom);
		m_State.GetRooms().at(eRoom)->AddPlayer(pPlayer->GetId());
		if (GetPlayerIdOnRight(pPlayer->GetId()) == pPlayer->GetId())
			pPlayer->SetState(ePlayerStates_PromptToMakeAccusation);
		else
			pPlayer->SetState(ePlayerStates_MakeDeduction);
	}
	else if (!pPlayer->GetMovesLeft())
	{
		pPlayer->SetState(ePlayerStates_Normal); // Set before because LeftPlayer can be this player
		CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
		while (pLeftPlayer->IsInactive())
			pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
		pLeftPlayer->SetState(ePlayerStates_RollDices);
	}

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToMakeDeduction(DWORD dwPlayerId, eWeapons eWeapon, eCharacters eCharacter)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;
	if (eWeapon == eWeapons_None)
		return eResult_InvalidParameter;
	if (eCharacter == eCharacters_None)
		return eResult_InvalidParameter;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if ((pPlayer->GetState() != ePlayerStates_MakeDeduction) && !(pPlayer->GetState() == ePlayerStates_RollDices && pPlayer->WasMovedToRoom() && !pPlayer->IsInactive()))
		return eResult_InappropriatePlayerState;

	if (GetPlayerIdOnRight(pPlayer->GetId()) == pPlayer->GetId())
	{
		pPlayer->SetState(ePlayerStates_PromptToMakeAccusation);
	}
	else
	{
		for (auto& room : m_State.GetRooms())
		{
			if (room.second->IsWeaponInRoom(eWeapon))
			{
				room.second->RemoveWeapon(eWeapon);
				m_State.GetRooms().at(pPlayer->GetRoom())->AddWeapon(eWeapon);
				break;
			}
		}
		for (auto& player : m_State.GetPlayers())
		{
			if (player.second->GetCharacter() == eCharacter)
			{
				if (player.second->IsInRoom())
					m_State.GetRooms().at(player.second->GetRoom())->RemovePlayer(player.second->GetId());
				player.second->SetPosition(g_ptEmptyPosition);
				player.second->SetRoom(pPlayer->GetRoom());
				m_State.GetRooms().at(pPlayer->GetRoom())->AddPlayer(player.second->GetId());
				player.second->SetWasMovedToRoom();
				break;
			}
		}
		m_State.SetDeductionData(pPlayer->GetRoom(), eWeapon, eCharacter);
		m_State.GetPlayers().at(GetPlayerIdOnRight(pPlayer->GetId()))->SetState(ePlayerStates_AnswersToDeduction);
		pPlayer->SetState(ePlayerStates_WaitForDeductionAnswers);
	}

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToAnswerToDeduction(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_AnswersToDeduction)
		return eResult_InappropriatePlayerState;

	CPlayer* pWaitForDeductionPlayer = NULL;
	for (auto& player : m_State.GetPlayers())
	{
		if (player.second->GetState() == ePlayerStates_WaitForDeductionAnswers)
		{
			pWaitForDeductionPlayer = player.second.get();
			break;
		}
	}

	if (pWaitForDeductionPlayer)
	{
		ULONG64 ullRoomCard = make_RoomCard(m_State.GetDeductionRoom());
		ULONG64 ullWeaponCard = make_WeaponCard(m_State.GetDeductionWeapon());
		ULONG64 ullCharacterCard = make_CharacterCard(m_State.GetDeductionCharacter());

		ULONG64 ullCard = 0;
		if (pPlayer->DoesHaveCard(ullRoomCard))
			ullCard = ullRoomCard;
		else if (pPlayer->DoesHaveCard(ullWeaponCard))
			ullCard = ullWeaponCard;
		else if (pPlayer->DoesHaveCard(ullCharacterCard))
			ullCard = ullCharacterCard;

		if (ullCard)
		{
			m_State.SetDeductionAnswer(pPlayer->GetId(), ullCard);
			pWaitForDeductionPlayer->SetState(ePlayerStates_ReceivingDeductionAnswers);
		}
		else
		{
			m_State.SetDeductionAnswer(pPlayer->GetId(), 0);
			CPlayer* pRightPlayer = m_State.GetPlayers().at(GetPlayerIdOnRight(pPlayer->GetId())).get();
			if (pRightPlayer->GetState() == ePlayerStates_WaitForDeductionAnswers)
				pRightPlayer->SetState(ePlayerStates_ReceivingDeductionAnswers);
			else
				pRightPlayer->SetState(ePlayerStates_AnswersToDeduction);
		}
	}
	else
	{
		// Do nothing. Maybe a player left the game.
	}

	pPlayer->SetState(ePlayerStates_Normal);

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToReceiveDeductionAnswers(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_ReceivingDeductionAnswers)
		return eResult_InappropriatePlayerState;

	if (m_State.GetDeductionAnswer().second)
	{
		pPlayer->AddHint(m_State.GetDeductionAnswer().second, m_State.GetDeductionAnswer().first);
		pPlayer->SetState(ePlayerStates_Normal); // Set before because LeftPlayer can be this player
		CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
		while (pLeftPlayer->IsInactive())
			pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
		pLeftPlayer->SetState(ePlayerStates_RollDices);
	}
	else
	{
		pPlayer->SetState(ePlayerStates_PromptToMakeAccusation);
	}
	m_State.ClearDeductionData();

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToMakeAccusation(DWORD dwPlayerId, eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_PromptToMakeAccusation)
		return eResult_InappropriatePlayerState;

	BOOL bGameOver = FALSE;
	if ((m_State.GetMurderRoom() == eRoom) && (m_State.GetMurderWeapon() == eWeapon) && (m_State.GetMurderCharacter() == eCharacter))
	{
		pPlayer->SetState(ePlayerStates_Winner);
		m_State.SetGameState(eGameStates_Ended);
		bGameOver = TRUE;
	}
	else
	{
		pPlayer->SetInactive();
		bGameOver = TRUE;
		for (auto& player : m_State.GetPlayers())
		{
			if (!player.second->IsInactive() && (player.second->GetState() != ePlayerStates_LeftGame))
			{
				bGameOver = FALSE;
				break;
			}
		}
		if (bGameOver)
		{
			m_State.SetGameState(eGameStates_Ended);
		}
		else
		{
			pPlayer->SetState(ePlayerStates_Normal); // Set before because LeftPlayer can be this player
			CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
			while (pLeftPlayer->IsInactive())
				pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
			pLeftPlayer->SetState(ePlayerStates_RollDices);
		}
	}

	if (bGameOver)
	{
		m_Info.SetGameState(m_State.GetGameState());
		OnInfoChanged(m_Info);
	}

	OnStateChanged(m_State);
	return eResult_Success;
}

eResult CGame::PlayerTriesToSkipAccusation(DWORD dwPlayerId)
{
	if (m_State.GetGameState() != eGameStates_Started)
		return eResult_InappropriateGameState;
	if (!m_State.GetPlayers().count(dwPlayerId))
		return eResult_InvalidPlayerId;

	CPlayer* pPlayer = m_State.GetPlayers().at(dwPlayerId).get();
	if (pPlayer->GetState() != ePlayerStates_PromptToMakeAccusation)
		return eResult_InappropriatePlayerState;

	pPlayer->SetState(ePlayerStates_Normal); // Set before because LeftPlayer can be this player
	CPlayer* pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pPlayer->GetId())).get();
	while (pLeftPlayer->IsInactive())
		pLeftPlayer = m_State.GetPlayers().at(GetPlayerIdOnLeft(pLeftPlayer->GetId())).get();
	pLeftPlayer->SetState(ePlayerStates_RollDices);

	OnStateChanged(m_State);
	return eResult_Success;
}

void CGame::StartGame()
{
	std::vector<POINT> vecPositions;
	for (int y = 0; y < (g_rcLocation.bottom - g_rcLocation.top); ++y)
	{
		for (int x = 0; x < (g_rcLocation.right - g_rcLocation.left); ++x)
		{
			eRooms eRoom = PtInRoom(Point(x, y));
			if (eRoom == eRooms_None)
			{
				POINT pt = {x, y};
				vecPositions.push_back(pt);
			}
		}
	}
	std::random_shuffle(vecPositions.begin(), vecPositions.end());
	std::map<DWORD, std::unique_ptr<CPlayer> >::iterator itPlayer = m_State.GetPlayers().begin();
	std::vector<POINT>::iterator itPosition = vecPositions.begin();
	for (; ((itPlayer != m_State.GetPlayers().end()) && (itPosition != vecPositions.end())); ++itPlayer, ++itPosition)
		itPlayer->second->SetPosition(*itPosition);

	std::vector<ULONG64> vecRoomCards;
	vecRoomCards.push_back(make_RoomCard(eRooms_Cocina));
	vecRoomCards.push_back(make_RoomCard(eRooms_Comedor));
	vecRoomCards.push_back(make_RoomCard(eRooms_Salon));
	vecRoomCards.push_back(make_RoomCard(eRooms_Estudio));
	vecRoomCards.push_back(make_RoomCard(eRooms_Biblioteca));
	vecRoomCards.push_back(make_RoomCard(eRooms_Billar));
	vecRoomCards.push_back(make_RoomCard(eRooms_Terraza));
	vecRoomCards.push_back(make_RoomCard(eRooms_Baile));
	vecRoomCards.push_back(make_RoomCard(eRooms_Escaleras));
	std::random_shuffle(vecRoomCards.begin(), vecRoomCards.end());

	std::vector<ULONG64> vecWeaponCards;
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Punyal));
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Cuerda));
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Candelabro));
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Pistola));
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Tuberia));
	vecWeaponCards.push_back(make_WeaponCard(eWeapons_Herramienta));
	std::random_shuffle(vecWeaponCards.begin(), vecWeaponCards.end());

	std::vector<ULONG64> vecCharacterCards;
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Amapola));
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Rubio));
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Orquidea));
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Prado));
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Celeste));
	vecCharacterCards.push_back(make_CharacterCard(eCharacters_Mora));
	std::random_shuffle(vecCharacterCards.begin(), vecCharacterCards.end());

	m_State.SetMurderData(get_RoomCardType(vecRoomCards.front()), get_WeaponCardType(vecWeaponCards.front()), get_CharacterCardType(vecCharacterCards.front()));
	vecRoomCards.erase(vecRoomCards.begin());
	vecWeaponCards.erase(vecWeaponCards.begin());
	vecCharacterCards.erase(vecCharacterCards.begin());

	std::vector<ULONG64> vecCards;
	vecCards.insert(vecCards.end(), vecCharacterCards.begin(), vecCharacterCards.end());
	vecCards.insert(vecCards.end(), vecWeaponCards.begin(), vecWeaponCards.end());
	vecCards.insert(vecCards.end(), vecRoomCards.begin(), vecRoomCards.end());
	std::random_shuffle(vecCards.begin(), vecCards.end());

	while (!vecCards.empty())
	{
		for (auto& it : m_State.GetPlayers())
		{
			it.second->AddCard(vecCards.front());
			vecCards.erase(vecCards.begin());
			if (vecCards.empty())
				break;
		}
	}

	std::vector<CRoom*> vecRooms;
	vecRooms.push_back(new CRoom(eRooms_Cocina));
	vecRooms.push_back(new CRoom(eRooms_Comedor));
	vecRooms.push_back(new CRoom(eRooms_Salon));
	vecRooms.push_back(new CRoom(eRooms_Estudio));
	vecRooms.push_back(new CRoom(eRooms_Biblioteca));
	vecRooms.push_back(new CRoom(eRooms_Billar));
	vecRooms.push_back(new CRoom(eRooms_Terraza));
	vecRooms.push_back(new CRoom(eRooms_Baile));
	vecRooms.push_back(new CRoom(eRooms_Escaleras));
	std::random_shuffle(vecRooms.begin(), vecRooms.end());
	vecRooms[0]->AddWeapon(eWeapons_Punyal);
	vecRooms[1]->AddWeapon(eWeapons_Cuerda);
	vecRooms[2]->AddWeapon(eWeapons_Candelabro);
	vecRooms[3]->AddWeapon(eWeapons_Pistola);
	vecRooms[4]->AddWeapon(eWeapons_Tuberia);
	vecRooms[5]->AddWeapon(eWeapons_Herramienta);
	for (auto& room : vecRooms)
		m_State.AddRoom(room);

	m_State.SetGameState(eGameStates_Started);

	DWORD dwFirstPlayerId = 0;
	for (auto& player : m_State.GetPlayers())
	{
		player.second->SetState(ePlayerStates_Normal);
		if (!dwFirstPlayerId || (player.first < dwFirstPlayerId))
			dwFirstPlayerId = player.first;
	}
	m_State.GetPlayers()[dwFirstPlayerId]->SetState(ePlayerStates_RollDices);
}

BOOL CGame::CheckNewPlayerIdAndName(DWORD dwPlayerId, const std::wstring& rsName) const
{
	for (auto& it : m_State.GetPlayers())
	{
		if ((it.second->GetId() == dwPlayerId) || (it.second->GetName() == rsName))
			return FALSE;
	}
	return TRUE;
}

BOOL CGame::CheckNewPlayerCharacter(eCharacters eCharacter) const
{
	for (auto& it : m_State.GetPlayers())
	{
		if (it.second->GetCharacter() == eCharacter)
			return FALSE;
	}
	return TRUE;
}

BOOL CGame::CheckPlayerTrack(const std::list<POINT>& lstTrack, eRooms ePlayerInRoom) const
{
	if (lstTrack.empty())
		return FALSE;

	if (ePlayerInRoom != eRooms_None)
	{
		if (!PtAroundRoom(lstTrack.front(), ePlayerInRoom))
			return FALSE;
	}

	for (auto& player : m_State.GetPlayers())
	{
		for (auto& position : lstTrack)
		{
			if ((player.second->GetPosition().x == position.x) &&
				(player.second->GetPosition().y == position.y))
				return FALSE;
		}
	}

	POINT ptPrev = g_ptEmptyPosition;
	for (auto& position1 : lstTrack)
	{
		if(!PtInRect(&g_rcLocation, position1))
			return FALSE;

		if ((ptPrev.x != g_ptEmptyPosition.x) && (ptPrev.y != g_ptEmptyPosition.y) &&
			(position1.x != ptPrev.x) && (position1.y != ptPrev.y))
		{
			return FALSE;
		}
		ptPrev = position1;

		int counter = 0;
		for (auto& position2 : lstTrack)
		{
			if ((position1.x == position2.x) && (position1.y == position2.y))
				++counter;
		}
		if (counter > 1)
			return FALSE;
	}

	return TRUE;
}

DWORD CGame::GetPlayerIdOnLeft(DWORD dwCurrentPlayerId) const
{
	std::map<DWORD, std::unique_ptr<CPlayer> >::const_iterator it = m_State.GetPlayers().begin();
	for (; it != m_State.GetPlayers().end(); ++it)
	{
		if (it->second->GetId() == dwCurrentPlayerId)
			break;
	}
	++it;
	if (it == m_State.GetPlayers().end())
		it = m_State.GetPlayers().begin();
	while (it->second->GetId() != dwCurrentPlayerId)
	{
		if (it->second->GetState() != ePlayerStates_LeftGame)
			break;
		++it;
		if (it == m_State.GetPlayers().end())
			it = m_State.GetPlayers().begin();
	}
	return it->second->GetId();
}

DWORD CGame::GetPlayerIdOnRight(DWORD dwCurrentPlayerId) const
{
	std::map<DWORD, std::unique_ptr<CPlayer> >::const_reverse_iterator rit = m_State.GetPlayers().rbegin();
	for (; rit != m_State.GetPlayers().rend(); ++rit)
	{
		if (rit->second->GetId() == dwCurrentPlayerId)
			break;
	}
	++rit;
	if (rit == m_State.GetPlayers().rend())
		rit = m_State.GetPlayers().rbegin();
	while (rit->second->GetId() != dwCurrentPlayerId)
	{
		if (rit->second->GetState() != ePlayerStates_LeftGame)
			break;
		++rit;
		if (rit == m_State.GetPlayers().rend())
			rit = m_State.GetPlayers().rbegin();
	}
	return rit->second->GetId();
}
