#pragma once
#include <Windows.h>
#include <string>
#include <SFML\Network.hpp>
#include "GameState.h"
#include "PlayerInfo.h"


class CGameInfo
{
public:
	CGameInfo()
		: m_dwId(0)
		, m_dwSpecifiedPlayersCount(0)
		, m_eGameState(eGameStates_Created)
	{}
	virtual ~CGameInfo() {}

	BOOL Serialize(__out sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint32)m_dwId;
		rPacket << m_sName;
		rPacket << (sf::Uint32)m_dwSpecifiedPlayersCount;
		rPacket << (sf::Uint32)m_eGameState;
		rPacket << (sf::Uint32)m_mapPlayers.size();
		for (auto& player : m_mapPlayers)
		{
			if (!player.second.Serialize(rPacket))
				return FALSE;
		}
		return TRUE;
	}

	BOOL Deserialize(__in sf::Packet& rPacket)
	{
		sf::Uint32 u32Data = 0;
		rPacket >> u32Data; m_dwId = (DWORD)u32Data;
		rPacket >> m_sName;
		rPacket >> u32Data; m_dwSpecifiedPlayersCount = (DWORD)u32Data;
		rPacket >> u32Data; m_eGameState = (eGameStates)u32Data;
		rPacket >> u32Data;
		for (sf::Uint32 i = 0; i < u32Data; ++i)
		{
			CPlayerInfo info;
			if (!info.Deserialize(rPacket))
				return FALSE;
			m_mapPlayers[info.GetPlayerId()] = info;
		}
		return TRUE;
	}

	void SetGameId(DWORD dwId) { m_dwId = dwId; }
	DWORD GetGameId() const { return m_dwId; }

	void SetGameName(const std::wstring& rsName) { m_sName = rsName; }
	const std::wstring& GetGameName() const { return m_sName; }

	void SetSpecifiedPlayersCount(DWORD dwSpecifiedPlayersCount) { m_dwSpecifiedPlayersCount = dwSpecifiedPlayersCount; }
	DWORD GetSpecifiedPlayersCount() const { return m_dwSpecifiedPlayersCount; }

	DWORD GetActualPlayersCount() const { return (DWORD)m_mapPlayers.size(); }

	void SetGameState(eGameStates eState) { m_eGameState = eState; }
	eGameStates GetGameState() const { return m_eGameState; }

	void AddPlayer(DWORD dwPlayerId, const std::wstring& rsName, eCharacters eCharacter) { m_mapPlayers[dwPlayerId] = CPlayerInfo(dwPlayerId, rsName, eCharacter); }
	void RemovePlayer(DWORD dwPlayerId) { m_mapPlayers.erase(dwPlayerId); }
	const std::map<DWORD, CPlayerInfo>& GetPlayers() const { return m_mapPlayers; }

private:
	DWORD m_dwId;
	std::wstring m_sName;
	DWORD m_dwSpecifiedPlayersCount;
	eGameStates m_eGameState;
	std::map<DWORD, CPlayerInfo> m_mapPlayers;
};
