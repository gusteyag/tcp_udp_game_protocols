#pragma once
#include <windows.h>
#include <set>
#include "GameDefinitions.h"


enum ePlayerStates
{
	ePlayerStates_InLobby,
	ePlayerStates_Normal, // Do nothing
	ePlayerStates_RollDices,
	ePlayerStates_RequestingClueCard,
	ePlayerStates_WaitForClueCardAnswer,
	ePlayerStates_ReceivingClueCardAnswer,
	ePlayerStates_AnsweringClueCard,
	ePlayerStates_Move,
	ePlayerStates_MakeDeduction,
	ePlayerStates_WaitForDeductionAnswers,
	ePlayerStates_ReceivingDeductionAnswers,
	ePlayerStates_AnswersToDeduction,
	ePlayerStates_PromptToMakeAccusation,
	ePlayerStates_Winner,
	ePlayerStates_LeftGame,
};

class CPlayer
{
public:
	DWORD GetId() const { return m_dwId; }
	const std::wstring& GetName() const { return m_sName; }
	eCharacters GetCharacter() const { return m_eCharacter; }

	void SetPosition(const POINT& rptPosition) { m_ptPosition = rptPosition; if ((m_ptPosition.x != g_ptEmptyPosition.x) && (m_ptPosition.y != g_ptEmptyPosition.y)) m_eRoom = eRooms_None; }
	const POINT& GetPosition() const { return m_ptPosition; }

	void SetRoom(eRooms eRoom) { m_eRoom = eRoom; if (m_eRoom != eRooms_None) m_ptPosition = g_ptEmptyPosition; }
	eRooms GetRoom() { return m_eRoom; }
	BOOL IsInRoom() const { return (m_eRoom != eRooms_None); }

	void SetState(ePlayerStates eState) { m_eState = eState; }
	ePlayerStates GetState() const { return m_eState; }

	void AddCard(ULONG64 ullCard) { m_setCards.insert(ullCard); }
	const std::set<ULONG64>& GetCards() const { return m_setCards; }
	const BOOL DoesHaveCard(ULONG64 ullCard) { return (m_setCards.count(ullCard) != 0); }

	void ReduceMovesLeft(DWORD dwCount) { (dwCount >= m_dwMovesLeft) ? m_dwMovesLeft = 0 : m_dwMovesLeft -= dwCount; }
	void SetMovesLeft(DWORD dwMovesLeft) { m_dwMovesLeft = dwMovesLeft; }
	const DWORD GetMovesLeft() const { return m_dwMovesLeft; }

	void SetWasMovedToRoom() { m_bWasMovedToRoom = TRUE; }
	void ClearWasMovedToRoom() { m_bWasMovedToRoom = FALSE; }
	BOOL WasMovedToRoom() { return m_bWasMovedToRoom; }

	void AddHint(ULONG64 ullCard, DWORD dwPlayerId) { m_mapHints[ullCard] = dwPlayerId; }
	const std::map<ULONG64, DWORD>& GetHints() const { return m_mapHints; }

	void SetInactive() { m_bInactive = TRUE; }
	BOOL IsInactive() const { return m_bInactive; }

	void SetDisconnected() { m_bDisconnected = TRUE; }
	BOOL IsDisconnected() const { return m_bDisconnected; }

	CPlayer(DWORD dwId, const std::wstring& rsName, eCharacters eCharacter, ePlayerStates eState)
		: m_dwId(dwId)
		, m_sName(rsName)
		, m_eCharacter(eCharacter)
		, m_eState(eState)
		, m_eRoom(eRooms_None)
		, m_dwMovesLeft(0)
		, m_bWasMovedToRoom(FALSE)
		, m_bInactive(FALSE)
		, m_bDisconnected(FALSE)
	{
		m_ptPosition = g_ptEmptyPosition;
	}
	virtual ~CPlayer() {};

private:
	DWORD m_dwId;
	std::wstring m_sName;
	eCharacters m_eCharacter;
	POINT m_ptPosition;
	eRooms m_eRoom;
	DWORD m_dwMovesLeft;
	BOOL m_bWasMovedToRoom;
	BOOL m_bInactive;
	BOOL m_bDisconnected;

	ePlayerStates m_eState;

	std::set<ULONG64> m_setCards;
	std::map<ULONG64, DWORD> m_mapHints;
};
