#pragma once
#include <Windows.h>
#include <string>
#include <SFML\Network.hpp>
#include "GameDefinitions.h"


class CPlayerInfo
{
public:
	CPlayerInfo()
		: m_dwPlayerId(0)
		, m_eCharacter(eCharacters_None)
	{}
	CPlayerInfo(DWORD dwPlayerId, const std::wstring& rsName, eCharacters eCharacter)
		: m_dwPlayerId(dwPlayerId)
		, m_sName(rsName)
		, m_eCharacter(eCharacter)
	{}
	virtual ~CPlayerInfo() {}

	BOOL Serialize(__out sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint32)m_dwPlayerId;
		rPacket << m_sName;
		rPacket << (sf::Uint32)m_eCharacter;
		return TRUE;
	}

	BOOL Deserialize(__in sf::Packet& rPacket)
	{
		sf::Uint32 u32Data = 0;
		rPacket >> u32Data; m_dwPlayerId = (DWORD)u32Data;
		rPacket >> m_sName;
		rPacket >> u32Data; m_eCharacter = (eCharacters)u32Data;
		return TRUE;
	}

	void SetPlayerId(DWORD dwPlayerId) { m_dwPlayerId = dwPlayerId; }
	DWORD GetPlayerId() const { return m_dwPlayerId; }

	void SetName(const std::wstring& rsName) { m_sName = rsName; }
	const std::wstring& GetName() const { return m_sName; }

	void SetCharacter(eCharacters eCharacter) { m_eCharacter = eCharacter; }
	eCharacters GetCharacter() const { return m_eCharacter; }

private:
	DWORD m_dwPlayerId;
	std::wstring m_sName;
	eCharacters m_eCharacter;
};

