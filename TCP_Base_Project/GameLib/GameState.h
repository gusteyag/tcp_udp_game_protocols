#pragma once
#include <Windows.h>
#include <string>
#include <memory>
#include <SFML\Network.hpp>
#include "Room.h"
#include "Player.h"

enum eGameStates
{
	eGameStates_Created,
	eGameStates_Started,
	eGameStates_Ended,
	eGameStates_Fail,
};
inline LPCWSTR enum_desc(eGameStates eValue)
{
	switch (eValue)
	{
	case eGameStates_Created: return L"New";
	case eGameStates_Started: return L"Started";
	case eGameStates_Ended: return L"Game Over";
	case eGameStates_Fail: return L"Error";
	}
	return L"Unknown";
}

class CGameState
{
public:
	CGameState();
	CGameState(const CGameState& obj);
	virtual ~CGameState();

	CGameState& operator=(const CGameState& obj);

	BOOL Serialize(__out sf::Packet& rPacket) const;
	BOOL Deserialize(__in sf::Packet& rPacket);

	void SetGameId(DWORD dwId) { m_dwId = dwId; }
	DWORD GetGameId() const { return m_dwId; }

	void SetGameName(const std::wstring& rsName) { m_sName = rsName; }
	const std::wstring& GetGameName() const { return m_sName; }

	void SetSpecifiedPlayersCount(DWORD dwSpecifiedPlayersCount) { m_dwSpecifiedPlayersCount = dwSpecifiedPlayersCount; }
	DWORD GetSpecifiedPlayersCount() const { return m_dwSpecifiedPlayersCount; }

	void SetGameState(eGameStates eState) { m_eGameState = eState; }
	eGameStates GetGameState() const { return m_eGameState; }

	void AddRoom(CRoom* pRoom) { m_mapRooms[pRoom->GetType()] = std::unique_ptr<CRoom>(pRoom); }
	const std::map<eRooms, std::unique_ptr<CRoom> >& GetRooms() const { return m_mapRooms; }
	std::map<eRooms, std::unique_ptr<CRoom> >& GetRooms() { return m_mapRooms; }

	void AddPlayer(CPlayer* pPlayer) { m_mapPlayers[pPlayer->GetId()] = std::unique_ptr<CPlayer>(pPlayer); }
	const std::map<DWORD, std::unique_ptr<CPlayer> >& GetPlayers() const { return m_mapPlayers; }
	std::map<DWORD, std::unique_ptr<CPlayer> >& GetPlayers() { return m_mapPlayers; }

	void SetMurderData(eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter) { m_eMurderRoom = eRoom; m_eMurderWeapon = eWeapon; m_eMurderCharacter = eCharacter; }
	eRooms GetMurderRoom() const { return m_eMurderRoom; }
	eWeapons GetMurderWeapon() const { return m_eMurderWeapon; }
	eCharacters GetMurderCharacter() const { return m_eMurderCharacter; }

	void SetDicesResult(const SIZE& rDicesResult, DWORD dwPlayerId) { m_DicesResult = rDicesResult; m_dwRollDicesLastPlayerId = dwPlayerId; }
	const SIZE& GetDicesResult() const { return m_DicesResult; }
	DWORD GetRollDicesLastPlayerId() const { return m_dwRollDicesLastPlayerId; }

	void SetClueCardType(eCardTypes eType) { m_eClueCardType = eType; }
	const eCardTypes GetClueCardType() const { return m_eClueCardType; }
	void SetRequestedClueCard(ULONG64 ullCard) { m_ullRequestedClueCard = ullCard; }
	ULONG64 GetRequestedClueCard() const { return m_ullRequestedClueCard; }
	void SetAnsweredClueCard(ULONG64 ullCard, DWORD dwAnsweringPlayerId) { m_ullAnsweredClueCard = ullCard; m_dwAnsweredClueCardPlayerId = dwAnsweringPlayerId; }
	ULONG64 GetAnsweredClueCard() const { return m_ullAnsweredClueCard; }
	DWORD GetAnsweredPlayerId() const { return m_dwAnsweredClueCardPlayerId; }

	void SetDeductionData(eRooms eRoom, eWeapons eWeapon, eCharacters eCharacter) { m_eDeductionRoom = eRoom; m_eDeductionWeapon = eWeapon; m_eDeductionCharacter = eCharacter; }
	eRooms GetDeductionRoom() const { return m_eDeductionRoom; }
	eWeapons GetDeductionWeapon() const { return m_eDeductionWeapon; }
	eCharacters GetDeductionCharacter() const { return m_eDeductionCharacter; }
	void SetDeductionAnswer(DWORD dwPlayerId, ULONG64 ullCard) { m_DeductionAnswer = std::make_pair(dwPlayerId, ullCard); }
	const std::pair<DWORD, ULONG64>& GetDeductionAnswer() const { return m_DeductionAnswer; }
	void ClearDeductionData() { m_eDeductionRoom = eRooms_None; m_eDeductionWeapon = eWeapons_None; m_eDeductionCharacter = eCharacters_None; m_DeductionAnswer = std::make_pair(0, 0); }

private:
	DWORD m_dwId;
	std::wstring m_sName;
	DWORD m_dwSpecifiedPlayersCount;
	eGameStates m_eGameState;
	std::map<eRooms, std::unique_ptr<CRoom> > m_mapRooms;
	std::map<DWORD, std::unique_ptr<CPlayer> > m_mapPlayers;
	eRooms m_eMurderRoom;
	eWeapons m_eMurderWeapon;
	eCharacters m_eMurderCharacter;

	SIZE m_DicesResult; // cx is Dice1 and cy is Dice2
	DWORD m_dwRollDicesLastPlayerId;

	eCardTypes m_eClueCardType;
	ULONG64 m_ullRequestedClueCard;
	ULONG64 m_ullAnsweredClueCard;
	DWORD m_dwAnsweredClueCardPlayerId;

	eRooms m_eDeductionRoom;
	eWeapons m_eDeductionWeapon;
	eCharacters m_eDeductionCharacter;
	std::pair<DWORD, ULONG64> m_DeductionAnswer;
};
