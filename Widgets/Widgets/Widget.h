#pragma once
#include <list>
#include <memory>
#include <algorithm>
#include <SFML/Graphics.hpp>
#include "Scene.h"

class CWidget: public CSceneObject
{
public:
	CWidget(CScene& rScene);
	virtual ~CWidget();

	void SetRenderTarget(sf::RenderTarget* pRenderTarget) { m_pRenderTarget = pRenderTarget; }
	sf::RenderTarget* GetRenderTarget() { return (m_pRenderTarget ? m_pRenderTarget : GetScene().GetRenderWindow()); }

	void SetFont(const sf::Font& rFont) { m_Font = rFont; }
	const sf::Font& GetFont() const { return m_Font; }

	void SetFontSize(unsigned int uiFontSize) { m_uiFontSize = uiFontSize; }
	unsigned int GetFontSize() const { return m_uiFontSize; }

	virtual void SetText(const std::wstring& rText) { m_sText = rText; }
	const std::wstring& GetText() const { return m_sText; }

	void SetTextColor(const sf::Color& clrText) { m_clrText = clrText; }
	const sf::Color& GetTextColor() const { return m_clrText; }

	void SetBorderWidth(float fBorderWidth) { m_fBorderWidth = fBorderWidth; }
	float GetBorderWidth() const { return m_fBorderWidth; }

	void SetBorderColor(const sf::Color& clrBorder) { m_clrBorder = clrBorder; }
	const sf::Color& GetBorderColor() const { return m_clrBorder; }

	void SetFillColor(const sf::Color& clrClientArea) { m_clrFillColor = clrClientArea; }
	const sf::Color& GetFillColor() const { return m_clrFillColor; }

	void SetPadding(float fPadding) { m_fPadding = fPadding; }
	float GetPadding() const { return m_fPadding; }

	void SetReservedIndentLeft(float fReservedIndentLeft) { m_fReservedIndentLeft = fReservedIndentLeft; }
	float GetReservedIndentLeft() const { return m_fReservedIndentLeft; }

	void SetReservedIndentTop(float fReservedIndentTop) { m_fReservedIndentTop = fReservedIndentTop; }
	float GetReservedIndentTop() const { return m_fReservedIndentTop; }

	void SetReservedIndentRight(float fReservedIndentRight) { m_fReservedIndentRight = fReservedIndentRight; }
	float GetReservedIndentRight() const { return m_fReservedIndentRight; }

	void SetReservedIndentBottom(float fReservedIndentBottom) { m_fReservedIndentBottom = fReservedIndentBottom; }
	float GetReservedIndentBottom() const { return m_fReservedIndentBottom; }

	void SetParent(CWidget* pParent) { m_pParent = pParent; }
	CWidget* GetParent() { return m_pParent; }
	const CWidget* GetParent() const { return m_pParent; }

	void SetPosition(const sf::Vector2f& rv2fPosition) { m_v2fPosition = rv2fPosition; }
	const sf::Vector2f& GetPosition() const  { return m_v2fPosition; }

	void Move(sf::Vector2f v2fDistance) { SetPosition(sf::Vector2f(GetPosition().x + v2fDistance.x, GetPosition().y + v2fDistance.y)); }

	float GetOutBorderWidth() const { return ((m_fBorderWidth > 0) ? m_fBorderWidth : 0); }
	float GetInBorderWidth() const { return ((m_fBorderWidth < 0) ? -m_fBorderWidth : 0); }

	sf::Vector2f GetBoundingPosition() const { return sf::Vector2f(-GetOutBorderWidth(), -GetOutBorderWidth()); }
	sf::Vector2f GetWorkingPosition() const { return sf::Vector2f((GetInBorderWidth() + GetReservedIndentLeft() + m_fPadding), (GetInBorderWidth() + GetReservedIndentTop() + m_fPadding)); }

	sf::Vector2f GetAbsolutePosition() const { return (m_pParent ? (m_pParent->GetAbsoluteWorkingPosition() + m_v2fPosition) : m_v2fPosition); }
	sf::Vector2f GetAbsoluteBoundingPosition() const { return sf::Vector2f((GetAbsolutePosition().x + GetBoundingPosition().x), (GetAbsolutePosition().y + GetBoundingPosition().y)); }
	sf::Vector2f GetAbsoluteWorkingPosition() const { return sf::Vector2f((GetAbsolutePosition().x + GetWorkingPosition().x), (GetAbsolutePosition().y + GetWorkingPosition().y)); }

	void SetSize(const sf::Vector2f& rv2fSize) { m_v2fSize = rv2fSize; }
	const sf::Vector2f& GetSize() const { return m_v2fSize; }
	virtual void NormalizeSize() {}

	sf::Vector2f GetBoundingSize() const { return sf::Vector2f((GetSize().x + GetOutBorderWidth() * 2), (GetSize().y + GetOutBorderWidth() * 2)); }
	sf::Vector2f GetWorkingSize() const {
		return sf::Vector2f(
			GetSize().x - GetInBorderWidth() * 2 - GetReservedIndentLeft() - GetReservedIndentRight() - m_fPadding * 2,
			GetSize().y - GetInBorderWidth() * 2 - GetReservedIndentTop() - GetReservedIndentBottom() - m_fPadding * 2
		);
	}
	void SetWorkingSize(const sf::Vector2f& rv2fWorkingSize) {
		SetSize(sf::Vector2f(
			rv2fWorkingSize.x + GetInBorderWidth() * 2 + GetReservedIndentLeft() + GetReservedIndentRight() + m_fPadding * 2,
			rv2fWorkingSize.y + GetInBorderWidth() * 2 + GetReservedIndentTop() + GetReservedIndentBottom() + m_fPadding * 2
		));
	}

	sf::FloatRect GetRect() const { return sf::FloatRect(0, 0, GetSize().x, GetSize().y); }
	sf::FloatRect GetBoundingRect() const { return sf::FloatRect(GetBoundingPosition().x, GetBoundingPosition().y, GetBoundingSize().x, GetBoundingSize().y); }
	sf::FloatRect GetWorkingRect() const { return sf::FloatRect(GetWorkingPosition().x, GetWorkingPosition().y, GetWorkingSize().x, GetWorkingSize().y); }

	sf::FloatRect GetAbsoluteRect() const { return sf::FloatRect(GetAbsolutePosition().x, GetAbsolutePosition().y, GetSize().x, GetSize().y); }
	sf::FloatRect GetAbsoluteBoundingRect() const { return sf::FloatRect(GetAbsoluteBoundingPosition().x, GetAbsoluteBoundingPosition().y, GetBoundingSize().x, GetBoundingSize().y); }
	sf::FloatRect GetAbsoluteWorkingRect() const { return sf::FloatRect(GetAbsoluteWorkingPosition().x, GetAbsoluteWorkingPosition().y, GetWorkingSize().x, GetWorkingSize().y); }

	void SetEnabled(bool bIsEnabled) { m_bIsEnabled = bIsEnabled; if (!m_bIsEnabled) SetFocused(false); }
	bool IsEnabled() const { return ((!m_pParent || m_pParent->IsEnabled()) ? m_bIsEnabled : false); }

	void SetVisible(bool bIsVisible) { m_bIsVisible = bIsVisible; if (!m_bIsVisible) SetFocused(false); }
	bool IsVisible() const { return m_bIsVisible; }

	void SetFocused(bool bIsFocused);
	bool IsFocused() const { return ((GetSceneFocusedWidget() && GetSceneFocusedWidget() == this)); }

	void AddChild(CWidget* pChild);
	bool RemoveChild(CWidget* pChild);
	CWidget* ReleaseChild(CWidget* pChild);
	bool IsChild(CWidget* pChild);
	const std::list<std::unique_ptr<CWidget> >& GetChildren() const { return m_lstZOrder; }
	std::list<std::unique_ptr<CWidget> >& GetChildren() { return m_lstZOrder; }

	bool IsExists(CWidget* pWidget);

	virtual void Draw();

	virtual void OnTextEntered(sf::Uint32 unicode);
	virtual void OnKeyPressed(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnKeyReleased(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnMouseWheelScrolled(
		sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
		float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
		int          x,			//< X position of the mouse pointer, relative to the left of the owner window
		int          y			//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	sf::RenderTarget* m_pRenderTarget;
	sf::Font m_Font;
	unsigned int m_uiFontSize;
	std::wstring m_sText;
	sf::Color m_clrText;
	float m_fBorderWidth;
	float m_fPadding;
	float m_fReservedIndentLeft;
	float m_fReservedIndentTop;
	float m_fReservedIndentRight;
	float m_fReservedIndentBottom;
	sf::Color m_clrBorder;
	sf::Color m_clrFillColor;
	CWidget* m_pParent;
	sf::Vector2f m_v2fPosition;
	sf::Vector2f m_v2fSize;
	bool m_bIsEnabled;
	bool m_bIsVisible;
	std::list<std::unique_ptr<CWidget> > m_lstZOrder;
};
