#pragma once
#include "Widget.h"
#include "WText.h"
#include <memory>
#include <functional>


class CWScrollBar;

struct ListBoxColumn
{
	std::wstring text;
	float width;

	ListBoxColumn()
		: width(20)
	{}
	ListBoxColumn(const std::wstring& text, float width)
		: text(text)
		, width(width)
	{}
};

struct ListBoxItem
{
	std::unique_ptr<CWidget> widget;
	std::wstring text;
	sf::Uint64 ui64UserData;

	ListBoxItem()
		: ui64UserData(0)
	{}
	ListBoxItem(sf::Uint64 ui64UserData)
		: ui64UserData(ui64UserData)
	{}
	ListBoxItem(const std::wstring& text)
		: text(text)
		, ui64UserData(0)
	{}
	ListBoxItem(const std::wstring& text, sf::Uint64 ui64UserData)
		: text(text)
		, ui64UserData(ui64UserData)
	{}
	ListBoxItem(CWidget* widget)
		: widget(widget)
		, ui64UserData(0)
	{}
	ListBoxItem(CWidget* widget, sf::Uint64 ui64UserData)
		: widget(widget)
		, ui64UserData(ui64UserData)
	{}
	ListBoxItem(CWidget* widget, const std::wstring& text)
		: widget(widget)
		, text(text)
		, ui64UserData(0)
	{}
	ListBoxItem(CWidget* widget, const std::wstring& text, sf::Uint64 ui64UserData)
		: widget(widget)
		, text(text)
		, ui64UserData(ui64UserData)
	{}
};

typedef std::list<ListBoxColumn> listbox_columns_t;
typedef std::list<ListBoxItem> listbox_items_t;
typedef std::list<listbox_items_t> listbox_lines_t;

class CWListBox :
	public CWidget
{
public:
	CWListBox(CScene& rScene);
	virtual ~CWListBox();

	listbox_columns_t& GetColumns() { return m_lstColumns; }
	listbox_lines_t& GetLines() { return m_lstLines; }

	void SetHeaderHeight(float fHeaderHeight) { m_fHeaderHeight = fHeaderHeight; }
	float GetHeaderHeight() const { return m_fHeaderHeight; }

	void SetLineHeight(float fLineHeight) { m_fLineHeight = fLineHeight; }
	float GetLineHeight() const { return m_fLineHeight; }

	void SetStartVScrollVeiwPos(float fPosition) { m_fStartVScrollVeiwPos = fPosition; }
	float GetStartVScrollVeiwPos() const { return m_fStartVScrollVeiwPos; }

	void SetStartHScrollVeiwPos(float fPosition) { m_fStartHScrollVeiwPos = fPosition; }
	float GetStartHScrollVeiwPos() const { return m_fStartHScrollVeiwPos; }

	void SetVScrollVeiwPos(float fPosition);
	void SetHScrollVeiwPos(float fPosition);

	void SetSelectedLine(sf::Int32 index) { m_iSelectedLineIndex = index; } // Set -1 to remove selection
	sf::Int32 GetSelectedLine();
	listbox_items_t* GetSelectedLine(__out_opt listbox_items_t** pplstItems);

	void MakeSelectionVisible();

	void SetOnLineSelectedHandler(const std::function<void(sf::Int32 index, const listbox_items_t& rlstItems)>& rHandler) { m_OnLineSelectedHandler = rHandler; }
	void SetOnLineUnselectedHandler(const std::function<void(void)>& rHandler) { m_OnLineUnselectedHandler = rHandler; }

	virtual void NormalizeSize();
	virtual void Draw();

	virtual void OnTextEntered(sf::Uint32 unicode);
	virtual void OnKeyPressed(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnKeyReleased(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnMouseWheelScrolled(
		sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
		float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
		int          x,			//< X position of the mouse pointer, relative to the left of the owner window
		int          y			//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	listbox_columns_t m_lstColumns;
	listbox_lines_t m_lstLines;
	float m_fHeaderHeight;
	float m_fLineHeight;
	sf::Vector2f m_v2fItemPaddings;
	std::unique_ptr<CWScrollBar> m_pVScrollBar;
	std::unique_ptr<CWScrollBar> m_pHScrollBar;
	sf::Vector2f m_v2fListSize;
	float m_fStartVScrollVeiwPos;
	float m_fStartHScrollVeiwPos;
	float m_fVScrollOffset;
	float m_fHScrollOffset;
	sf::Color m_clrHeaderText;
	sf::Color m_clrHeaderArea;
	sf::Color m_clrListBorders;
	sf::Int32 m_iSelectedLineIndex;
	sf::Color m_clrSelectedLineBackground;
	std::function<void(sf::Int32 index, const listbox_items_t& rlstItems)> m_OnLineSelectedHandler;
	std::function<void(void)> m_OnLineUnselectedHandler;

private:
	sf::FloatRect GetListWorkingRect();
	sf::FloatRect GetAbsoluteListWorkingRect();
	sf::Vector2f CalculateListBoundingSize();
};
