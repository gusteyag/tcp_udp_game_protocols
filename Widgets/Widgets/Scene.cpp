#include "Scene.h"
#include "Widget.h"


CScene::CScene()
	: m_pRenderWindow(NULL)
	, m_pFocusedWidget(NULL)
	, m_pMainWidget(new CWidget(*this))
{
}

CScene::~CScene()
{
}

void CScene::SetRenderWindow(sf::RenderWindow* pRenderWindow)
{
	m_pRenderWindow = pRenderWindow;
	if (m_pRenderWindow)
	{
		m_pMainWidget->SetSize(m_pRenderWindow->getView().getSize());
		m_pMainWidget->SetPosition(sf::Vector2f(0.f, 0.f));
	}
}

void CScene::OnEvent(const sf::Event& rEvent)
{
	switch (rEvent.type)
	{
	case sf::Event::TextEntered:
		GetMainWidget()->OnTextEntered(rEvent.text.unicode);
		break;

	case sf::Event::KeyPressed:
		GetMainWidget()->OnKeyPressed(rEvent.key.code, rEvent.key.alt, rEvent.key.control, rEvent.key.shift, rEvent.key.system);
		break;

	case sf::Event::KeyReleased:
		GetMainWidget()->OnKeyReleased(rEvent.key.code, rEvent.key.alt, rEvent.key.control, rEvent.key.shift, rEvent.key.system);
		break;

	case sf::Event::MouseWheelScrolled:
		GetMainWidget()->OnMouseWheelScrolled(rEvent.mouseWheelScroll.wheel, rEvent.mouseWheelScroll.delta, rEvent.mouseWheelScroll.x, rEvent.mouseWheelScroll.y);
		break;

	case sf::Event::MouseButtonPressed:
		GetMainWidget()->OnMouseButtonPressed(rEvent.mouseButton.button, rEvent.mouseButton.x, rEvent.mouseButton.y);
		break;

	case sf::Event::MouseButtonReleased:
		GetMainWidget()->OnMouseButtonReleased(rEvent.mouseButton.button, rEvent.mouseButton.x, rEvent.mouseButton.y);
		break;

	case sf::Event::MouseMoved:
		GetMainWidget()->OnMouseMoved(rEvent.mouseMove.x, rEvent.mouseMove.y);
		break;
	}
}

void CScene::Draw()
{
	if (!GetRenderWindow())
		return;
	GetRenderWindow()->clear();
	m_pMainWidget->Draw();
	GetRenderWindow()->display();
}

void CScene::AlignText(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Text& rTextToAlign, const sf::FloatRect& rcGlobalBounds)
{
	sf::FloatRect rcTextToH = rTextToAlign.getGlobalBounds();
	sf::Text text(rTextToAlign);
	text.setString(L"f"/*L"fy"*/);
	sf::FloatRect rcTextToV = text.getGlobalBounds();
	float fXOffset = 0;
	switch (eHAlignment_)
	{
	case eHAlignment_None:
		fXOffset = 0;
		break;

	case eHAlignment_Center:
		fXOffset = (rcGlobalBounds.width / 2 + rcGlobalBounds.left) - (rcTextToH.width / 2 + rcTextToH.left);
		break;

	case eHAlignment_Left:
		fXOffset = rcGlobalBounds.left - rcTextToH.left;
		break;

	case eHAlignment_Right:
		fXOffset = (rcGlobalBounds.width + rcGlobalBounds.left) - (rcTextToH.width + rcTextToH.left);
		break;
	}
	fXOffset = (float)(int)(fXOffset + 0.5f);
	float fYOffset = 0;
	switch (eVAlignment_)
	{
	case eVAlignment_None:
		fYOffset = 0;
		break;

	case eVAlignment_Center:
		fYOffset = (rcGlobalBounds.height / 2 + rcGlobalBounds.top) - (rcTextToV.height / 2 + rcTextToV.top);
		break;

	case eVAlignment_Top:
		fYOffset = rcGlobalBounds.top - rcTextToV.top;
		break;

	case eVAlignment_Bottom:
		fYOffset = (rcGlobalBounds.height + rcGlobalBounds.top) - (rcTextToV.height + rcTextToV.top);
		break;
	}
	fYOffset = (float)(int)(fYOffset + 0.5f);
	rTextToAlign.move(sf::Vector2f(fXOffset, fYOffset));
}

void CScene::AlignText(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Text& rTextToAlign, const sf::Shape& rBaseShape)
{
	AlignText(eHAlignment_, eVAlignment_, rTextToAlign, rBaseShape.getGlobalBounds());
}

void CScene::CenterText(__inout sf::Text& rTextToAlign, const sf::FloatRect& rcGlobalBounds)
{
	AlignText(eHAlignment_Center, eVAlignment_Center, rTextToAlign, rcGlobalBounds);
}

void CScene::CenterText(__inout sf::Text& rTextToAlign, const sf::Shape& rBaseShape)
{
	AlignText(eHAlignment_Center, eVAlignment_Center, rTextToAlign, rBaseShape);
}

void CScene::AlignShape(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Shape& rShapeToAlign, const sf::FloatRect& rcGlobalBounds)
{
	sf::FloatRect rcShape = rShapeToAlign.getGlobalBounds();
	float fXOffset = 0;
	switch (eHAlignment_)
	{
	case eHAlignment_None:
		fXOffset = 0;
		break;

	case eHAlignment_Center:
		fXOffset = (rcGlobalBounds.width / 2 + rcGlobalBounds.left) - (rcShape.width / 2 + rcShape.left);
		break;

	case eHAlignment_Left:
		fXOffset = rcGlobalBounds.left - rcShape.left;
		break;

	case eHAlignment_Right:
		fXOffset = (rcGlobalBounds.width + rcGlobalBounds.left) - (rcShape.width + rcShape.left);
		break;
	}
	float fYOffset = 0;
	switch (eVAlignment_)
	{
	case eVAlignment_None:
		fYOffset = 0;
		break;

	case eVAlignment_Center:
		fYOffset = (rcGlobalBounds.height / 2 + rcGlobalBounds.top) - (rcShape.height / 2 + rcShape.top);
		break;

	case eVAlignment_Top:
		fYOffset = rcGlobalBounds.top - rcShape.top;
		break;

	case eVAlignment_Bottom:
		fYOffset = (rcGlobalBounds.height + rcGlobalBounds.top) - (rcShape.height + rcShape.top);
		break;
	}
	rShapeToAlign.move(sf::Vector2f(fXOffset, fYOffset));
}

void CScene::AlignShape(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Shape& rShapeToAlign, const sf::Shape& rBaseShape)
{
	AlignShape(eHAlignment_, eVAlignment_, rShapeToAlign, rBaseShape.getGlobalBounds());
}

void CScene::CenterShape(__inout sf::Shape& rShapeToAlign, const sf::FloatRect& rcGlobalBounds)
{
	AlignShape(eHAlignment_Center, eVAlignment_Center, rShapeToAlign, rcGlobalBounds);
}

void CScene::CenterShape(__inout sf::Shape& rShapeToAlign, const sf::Shape& rBaseShape)
{
	AlignShape(eHAlignment_Center, eVAlignment_Center, rShapeToAlign, rBaseShape);
}

void CScene::AlignWidget(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout CWidget& rWidgetToAlign, const sf::FloatRect& rcGlobalBounds)
{
	sf::FloatRect rcWidget = rWidgetToAlign.GetAbsoluteRect();
	float fXOffset = 0;
	switch (eHAlignment_)
	{
	case eHAlignment_None:
		fXOffset = 0;
		break;

	case eHAlignment_Center:
		fXOffset = (rcGlobalBounds.width / 2 + rcGlobalBounds.left) - (rcWidget.width / 2 + rcWidget.left);
		break;

	case eHAlignment_Left:
		fXOffset = rcGlobalBounds.left - rcWidget.left;
		break;

	case eHAlignment_Right:
		fXOffset = (rcGlobalBounds.width + rcGlobalBounds.left) - (rcWidget.width + rcWidget.left);
		break;
	}
	float fYOffset = 0;
	switch (eVAlignment_)
	{
	case eVAlignment_None:
		fYOffset = 0;
		break;

	case eVAlignment_Center:
		fYOffset = (rcGlobalBounds.height / 2 + rcGlobalBounds.top) - (rcWidget.height / 2 + rcWidget.top);
		break;

	case eVAlignment_Top:
		fYOffset = rcGlobalBounds.top - rcWidget.top;
		break;

	case eVAlignment_Bottom:
		fYOffset = (rcGlobalBounds.height + rcGlobalBounds.top) - (rcWidget.height + rcWidget.top);
		break;
	}
	rWidgetToAlign.Move(sf::Vector2f(fXOffset, fYOffset));
}

sf::FloatRect CScene::GetIntersect(const sf::FloatRect& rc1, const sf::FloatRect& rc2)
{
	float left = rc1.left;
	float top = rc1.top;
	float right = rc1.left + rc1.width;
	float bottom = rc1.top + rc1.height;
	if (left < rc2.left)
		left = rc2.left;
	if (left > rc2.left + rc2.width)
		left = rc2.left + rc2.width;
	if (right < rc2.left)
		right = rc2.left;
	if (right > rc2.left + rc2.width)
		right = rc2.left + rc2.width;
	if (top < rc2.top)
		top = rc2.top;
	if (top > rc2.top + rc2.height)
		top = rc2.top + rc2.height;
	if (bottom < rc2.top)
		bottom = rc2.top;
	if (bottom > rc2.top + rc2.height)
		bottom = rc2.top + rc2.height;
	return sf::FloatRect(left, top, right - left, bottom - top);
}

void CScene::UpdateView()
{
	if (!GetRenderWindow())
		return;
	if (!m_lstClippingRects.size())
	{
		GetRenderWindow()->setView(GetRenderWindow()->getDefaultView());
		return;
	}
	sf::FloatRect rcClipping(0, 0, GetRenderWindow()->getDefaultView().getSize().x, GetRenderWindow()->getDefaultView().getSize().y);
	for (auto& rc : m_lstClippingRects)
		rcClipping = GetIntersect(rc, rcClipping);
	sf::View view(GetRenderWindow()->getView());
	sf::FloatRect rcViewPort(
		rcClipping.left / GetRenderWindow()->getDefaultView().getSize().x,
		rcClipping.top / GetRenderWindow()->getDefaultView().getSize().y,
		rcClipping.width / GetRenderWindow()->getDefaultView().getSize().x,
		rcClipping.height / GetRenderWindow()->getDefaultView().getSize().y
	);
	view.setViewport(rcViewPort);
	view.setSize(sf::Vector2f(rcClipping.width, rcClipping.height));
	view.setCenter(sf::Vector2f(rcClipping.width / 2 + rcClipping.left, rcClipping.height / 2 + rcClipping.top));
	GetRenderWindow()->setView(view);
}
