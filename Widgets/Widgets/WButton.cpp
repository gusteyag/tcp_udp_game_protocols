#include "WButton.h"


CWButton::CWButton(CScene& rScene)
	: CWidget(rScene)
	, m_bStyleRadio(false)
	, m_bStyleCheckBox(false)
	, m_bPushed(false)
	, m_bChecked(false)
	, m_BorderUpColor(150, 0, 0)
	, m_BorderPushColor(255, 0, 0)
	, m_UpColor(255, 0, 0)
	, m_PushColor(150, 0, 0)
	, m_TextUpColor(0, 0, 0)
	, m_TextPushColor(200, 200, 200)
	, m_UpColorDisabled(70, 30, 30)
	, m_TextUpColorDisabled(100, 100, 100)
{
	SetBorderWidth(-2.f);
	SetFontSize(12);
}

CWButton::~CWButton()
{
}

void CWButton::Draw()
{
	if (!GetRenderTarget())
		return;

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(m_bPushed ? m_BorderPushColor : m_BorderUpColor);
	border.setFillColor(sf::Color(0, 0, 0, 0));

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(m_bPushed ? m_PushColor : (IsEnabled() ? m_UpColor : m_UpColorDisabled));

	sf::Text text(GetText(), GetFont(), GetFontSize());
	CScene::CenterText(text, shape);
	text.setFillColor(m_bPushed ? m_TextPushColor : (IsEnabled() ? m_TextUpColor : m_TextUpColorDisabled));

	GetRenderTarget()->draw(shape);
	GetRenderTarget()->draw(text);
	__super::Draw();
	GetRenderTarget()->draw(border);
}

void CWButton::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonPressed(button, x, y);

	if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetAbsoluteRect().contains(pos))
	{
		if (m_bStyleRadio && !m_bPushed)
		{
			m_bPushed = true;
			CheckOthersRadio();
			if (m_OnSelectedHandler)
				m_OnSelectedHandler();
		}
		else if (m_bStyleCheckBox)
		{
			m_bChecked = !m_bChecked;
			m_bPushed = true;
			if (m_bChecked)
			{
				if (m_OnCheckedHandler)
					m_OnCheckedHandler();
			}
		}
		else
		{
			m_bPushed = true;
		}
		SetFocused(true);
	}
}

void CWButton::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonReleased(button, x, y);

	if (button != sf::Mouse::Button::Left)
		return;

	if (!m_bStyleRadio && !m_bStyleCheckBox && m_bPushed)
	{
		m_bPushed = false;
		if(m_OnClickedHandler)
			m_OnClickedHandler();
	}
	else if (m_bStyleCheckBox && !m_bChecked)
	{
		if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
			return;

		sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
		if (GetAbsoluteRect().contains(pos))
		{
			m_bPushed = false;
			if (m_OnUncheckedHandler)
				m_OnUncheckedHandler();
		}
	}
}

void CWButton::CheckOthersRadio()
{
	if (m_bStyleRadio && m_bPushed && GetParent())
	{
		for (auto& widget : GetParent()->GetChildren())
		{
			CWButton* pButton = dynamic_cast<CWButton*>(widget.get());
			if (pButton && (pButton != this) && pButton->m_bStyleRadio && pButton->m_bPushed)
			{
				pButton->m_bPushed = false;
			}
		}
	}
}
