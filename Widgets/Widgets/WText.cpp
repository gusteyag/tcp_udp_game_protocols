#include "WText.h"


CWText::CWText(CScene& rScene)
	: CWidget(rScene)
	, m_bWordsWrap(false)
	, m_eTextHAlignment(eHAlignment_Center)
	, m_eTextVAlignment(eVAlignment_Center)
	, m_fWidgetVsTextSpacing(5)
	, m_uiTextStyle(sf::Text::Regular)
{
	SetFontSize(12);
	SetTextColor(sf::Color(200, 200, 200));
	SetPadding(0);
	SetBorderWidth(0);
}

CWText::~CWText()
{
}

void CWText::Draw()
{
	if (!GetRenderTarget())
		return;

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(GetFillColor());
	GetRenderTarget()->draw(shape);

	GetScene().PushClipping(GetAbsoluteWorkingRect());

	sf::Vector2f v2fTextSize = CalculateTextBoundingSize();
	float fVOffset = 0;
	switch (m_eTextVAlignment)
	{
	case eVAlignment_None:
		fVOffset = 0;
		break;

	case eVAlignment_Center:
		fVOffset = (GetAbsoluteTextAreaWorkingRect().top + GetAbsoluteTextAreaWorkingRect().height / 2) - v2fTextSize.y / 2;
		break;

	case eVAlignment_Top:
		fVOffset = GetAbsoluteTextAreaWorkingRect().top;
		break;

	case eVAlignment_Bottom:
		fVOffset = (GetAbsoluteTextAreaWorkingRect().top + GetAbsoluteTextAreaWorkingRect().height) - v2fTextSize.y;
		break;
	}
	if (m_pWidget.get())
	{
		if (m_pWidget->GetSize().x > GetWorkingSize().x)
			m_pWidget->SetSize(sf::Vector2f(GetWorkingSize().x, m_pWidget->GetSize().y));
		if (m_pWidget->GetSize().y > GetWorkingSize().y)
			m_pWidget->SetSize(sf::Vector2f(m_pWidget->GetSize().x, GetWorkingSize().y));
		if (GetText().size())
			CScene::AlignWidget(eHAlignment_Left, m_eTextVAlignment, (*m_pWidget), GetAbsoluteWorkingRect());
		else
			CScene::AlignWidget(m_eTextHAlignment, m_eTextVAlignment, (*m_pWidget), GetAbsoluteWorkingRect());
		m_pWidget->SetRenderTarget(GetRenderTarget());
		m_pWidget->Draw();
		m_pWidget->SetRenderTarget(NULL);
	}
	DrawText(fVOffset);

	__super::Draw();

	GetScene().PopClipping();

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(GetBorderColor());
	border.setFillColor(sf::Color(0, 0, 0, 0));
	GetRenderTarget()->draw(border);
}

sf::FloatRect CWText::GetAbsoluteTextAreaWorkingRect()
{
	if (!m_pWidget.get())
		return GetAbsoluteWorkingRect();
	float fLeftOffset = m_pWidget->GetSize().x + m_fWidgetVsTextSpacing;
	float width = GetWorkingSize().x - fLeftOffset;
	if (width < 0)
		width = 0;
	return sf::FloatRect(GetAbsoluteWorkingRect().left + fLeftOffset, GetAbsoluteWorkingRect().top, width, GetAbsoluteWorkingRect().height);
}

bool CWText::CalculateSuitableLine(
	const sf::Text& rText,
	const std::wstring& rsAllText,
	__out sf::Uint32& ruiLineOffset,
	__out sf::Uint32& ruiLineCharsCount,
	__out float& rfLineWidth,
	__inout sf::Uint32& ruiNextIndex,
	__inout float& rfNextOffset
)
{
	rfLineWidth = 0;
	sf::Uint32 uiStartIndex = ruiNextIndex;
	float fStartOffset = rfNextOffset;
	sf::Int32 iLastSpaceIndex = -1;
	for (; ruiNextIndex < (sf::Uint32)rsAllText.size(); ++ruiNextIndex)
	{
		rfLineWidth = rfNextOffset;
		if (rsAllText[ruiNextIndex] == L'\n')
		{
			++ruiNextIndex;
			rfNextOffset = 0;
			ruiLineOffset = uiStartIndex;
			ruiLineCharsCount = (ruiNextIndex - 1) - uiStartIndex;
			return false;
		}
		if (isblank(rsAllText[ruiNextIndex]))
			iLastSpaceIndex = ruiNextIndex;
		rfNextOffset += GetFont().getGlyph(rsAllText[ruiNextIndex], GetFontSize(), (rText.getStyle() & sf::Text::Bold), rText.getOutlineThickness()).advance;
		if (m_bWordsWrap && (rfNextOffset > GetAbsoluteTextAreaWorkingRect().width) && GetSize().x)
		{
			if (iLastSpaceIndex >= 0)
			{
				if (iLastSpaceIndex != ruiNextIndex)
					ruiNextIndex = iLastSpaceIndex + 1;
			}
			else if (!uiStartIndex && (fStartOffset != 0.f))
			{
				ruiNextIndex = uiStartIndex;
			}
			rfNextOffset = 0;
			ruiLineOffset = uiStartIndex;
			ruiLineCharsCount = ruiNextIndex - uiStartIndex;
			return false;
		}
	}
	ruiNextIndex = (sf::Uint32)rsAllText.size();
	ruiLineOffset = uiStartIndex;
	ruiLineCharsCount = ruiNextIndex - uiStartIndex;
	return true;
}

sf::Vector2f CWText::CalculateTextBoundingSize()
{
	sf::Text Text(L"fy", GetFont(), GetFontSize());
	sf::Vector2f size;
	sf::Uint32 uiNextIndex = 0;
	bool bEndText = false;
	while (!bEndText)
	{
		sf::Uint32 uiLineOffset = 0;
		sf::Uint32 uiLineCharsCount = 0;
		float fLineWidth = 0;
		float fNextOffset = 0;
		bEndText = CalculateSuitableLine(Text, GetText(), uiLineOffset, uiLineCharsCount, fLineWidth, uiNextIndex, fNextOffset);
		if (fLineWidth > size.x)
			size.x = fLineWidth;
		if (!bEndText) // New line
			size.y += Text.getLocalBounds().height + Text.getLocalBounds().top;
	}
	size.y += Text.getLocalBounds().top + Text.getLocalBounds().height; // For last line
	return size;
}

void CWText::DrawText(float fVOffset)
{
	sf::Text Text(L"", GetFont(), GetFontSize());
	Text.setPosition(sf::Vector2f(0, fVOffset));
	Text.setFillColor(GetTextColor());
	Text.setStyle(m_uiTextStyle);
	sf::Uint32 uiNextIndex = 0;
	bool bEndText = false;
	while (!bEndText)
	{
		sf::Uint32 uiLineOffset = 0;
		sf::Uint32 uiLineCharsCount = 0;
		float fLineWidth = 0;
		float fNextOffset = 0;
		bEndText = CalculateSuitableLine(Text, GetText(), uiLineOffset, uiLineCharsCount, fLineWidth, uiNextIndex, fNextOffset);
		Text.setString(GetText().substr(uiLineOffset, uiLineCharsCount));
		CScene::AlignText(m_eTextHAlignment, eVAlignment_None, Text, GetAbsoluteTextAreaWorkingRect());
		GetRenderTarget()->draw(Text);
		if (!bEndText) // New line
		{
			sf::FloatRect rcText = sf::Text(L"fy", GetFont(), GetFontSize()).getLocalBounds();
			Text.move(sf::Vector2f(0, (rcText.top + rcText.height)));
		}
	}
}
