#include "WChat.h"
#include "WTextBox.h"
#include "WEditBox.h"


CWChat::CWChat(CScene& rScene)
	: CWContainer(rScene)
	, m_pWTextBox(new CWTextBox(rScene))
	, m_pWEditBox(new CWEditBox(rScene))
{
	SetFontSize(10);
	SetHeaderHeight(15);
	SetBorderColor(sf::Color(255, 0, 0));
	AddChild(m_pWTextBox);
	AddChild(m_pWEditBox);
	SetText(L"Chat");

	m_pWEditBox->SetCanTextEntered(true);
	m_pWEditBox->SetOnTextEnteredHandler([this](const std::wstring& rsText)	{
		if (m_OnTextEnteredHandler)
			m_OnTextEnteredHandler(rsText);
		return true;
	});
}

CWChat::~CWChat()
{
}

void CWChat::AddMessage(const std::wstring& rsAuthor, const std::wstring& rsMessage)
{
	if (!m_pWTextBox)
		return;
	TextChunk AuthorChunk(rsAuthor + L": ", sf::Color(200, 200, 0));
	TextChunk MessageChunk(rsMessage + L"\n\n");
	if (m_pWTextBox)
	{
		m_pWTextBox->AddText(AuthorChunk);
		m_pWTextBox->AddText(MessageChunk);
		m_pWTextBox->SetVScrollVeiwPos(1.0f);
	}
}

void CWChat::ClearEditText()
{
	if (m_pWEditBox)
		m_pWEditBox->SetText(L"");
}

void CWChat::ClearMessages()
{
	if (m_pWTextBox)
		m_pWTextBox->ClearText();
}

void CWChat::Draw()
{
	if (!GetRenderTarget())
		return;

	m_pWEditBox->SetBorderColor(sf::Color(150, 0, 0));
	m_pWEditBox->SetFont(GetFont());
	m_pWEditBox->SetFontSize(GetFontSize());
	m_pWEditBox->SetSize(sf::Vector2f(GetWorkingSize().x, 0));
	m_pWEditBox->NormalizeSize();
	m_pWEditBox->SetPosition(sf::Vector2f(0, (GetWorkingSize().y - m_pWEditBox->GetSize().y)));

	m_pWTextBox->SetBorderColor(sf::Color(150, 0, 0));
	m_pWTextBox->SetFont(GetFont());
	m_pWTextBox->SetFontSize(GetFontSize());
	m_pWTextBox->SetSize(sf::Vector2f(GetWorkingSize().x, (GetWorkingSize().y - m_pWEditBox->GetSize().y - GetPadding())));
	m_pWTextBox->SetPosition(sf::Vector2f(0, 0));

	__super::Draw();
}
