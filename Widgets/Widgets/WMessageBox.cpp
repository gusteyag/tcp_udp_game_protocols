#include "WMessageBox.h"
#include "WText.h"
#include "WButton.h"


CWMessageBox::CWMessageBox(CScene& rScene)
	: CWContainer(rScene)
	, m_pText(new CWText(rScene))
	, m_pButton(new CWButton(rScene))
{
	AddChild(m_pText);
	AddChild(m_pButton);
	m_pButton->SetOnClickedHandler([this]() {
		SetVisible(false);
		if (m_OnButtonClickedHandler)
			m_OnButtonClickedHandler();
	});
	SetFillColor(sf::Color::Black);
}

CWMessageBox::~CWMessageBox()
{
}

void CWMessageBox::Show(eMessageType eType, const std::wstring& rsMessage, const std::function<void(void)>& rHandler/* = nullptr*/)
{
	switch (eType)
	{
	case eMessageType_Error:
		SetText(L"ERROR");
		break;
	case eMessageType_Warning:
		SetText(L"WARNING");
		break;
	case eMessageType_Info:
		SetText(L"Informational Message");
		break;
	}
	SetMessageText(rsMessage);
	SetButtonText(L"");
	SetOnButtonClickedHandler(rHandler);
	NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*this),
		sf::FloatRect(0, 0, GetScene().GetRenderWindow()->getDefaultView().getSize().x, GetScene().GetRenderWindow()->getDefaultView().getSize().y));
	SetVisible(true);
}

void CWMessageBox::NormalizeSize()
{
	float fTextHeight = 100;
	float fTextWidth = 300;
	float fButtonHeight = 20;
	float fButtonWidth = 70;

	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fTextWidth,
		GetWorkingPosition().y + fTextHeight + GetPadding() + fButtonHeight + GetPadding() + GetInBorderWidth()
	));

	m_pText->SetWordsWrap(true);
	m_pText->SetFont(GetFont());
	m_pText->SetFontSize(GetFontSize());
	m_pText->SetText(m_sMessage);
	m_pText->SetTextHAlignment(eHAlignment_Left);
	m_pText->SetTextVAlignment(eVAlignment_Top);
	m_pText->SetSize(sf::Vector2f(fTextWidth, fTextHeight));
	m_pText->SetPosition(sf::Vector2f(0, 0));

	m_pButton->SetBorderWidth(-1);
	m_pButton->SetFont(GetFont());
	m_pButton->SetFontSize(GetFontSize());
	m_pButton->SetText(m_sButton.size() ? m_sButton : L"Continue");
	m_pButton->SetSize(sf::Vector2f(fButtonWidth, fButtonHeight));
	m_pButton->SetPosition(sf::Vector2f(GetWorkingSize().x / 2 - m_pButton->GetSize().x / 2, fTextHeight + GetPadding()));
}

void CWMessageBox::Draw()
{
	NormalizeSize();
	__super::Draw();
}
