#include "WScrollBar.h"
#include "WButton.h"


CWScrollBar::CWScrollBar(CScene& rScene, eScrollBarType eType)
	: CWidget(rScene)
	, m_eType(eType)
	, m_fMinSliderLength(10)
	, m_bUpLeftButtonPressed(false)
	, m_bDownRightButtonPressed(false)
	, m_bUpLeftScrollAreaPressed(false)
	, m_bDownRightScrollAreaPressed(false)
	, m_fScrollAreaPressedViewPosition(0)
	, m_bSliderPressed(false)
	, m_fSliderPressedSliderPos(0)
	, m_fMouseScrollAreaWidth(77)
	, m_clrBorderAndButtons(sf::Color(200, 0, 0))
	, m_clrArrowsUp(sf::Color(100, 0, 0))
	, m_clrArrowsDown(sf::Color(255, 100, 100))
	, m_clrScrollArea(sf::Color(0, 0, 0, 0))
	, m_clrSliderUp(sf::Color(255, 0, 0))
	, m_clrSliderDown(sf::Color(177, 0, 0))
	, m_fSmallStep(0.01f)
	, m_fBigStep(0.2f)
	, m_fButtonScrollSpeed(16)
{
	SetBorderWidth(-1.f);
}

CWScrollBar::~CWScrollBar()
{
}

void CWScrollBar::StickTo(CWidget* pWidget, __in_opt CWScrollBar* pSecondScrollBar)
{
	if (!pWidget)
		return;

	SetParent(NULL);
	NormalizeSize();
	SetParent(pWidget);

	if (GetType() == eScrollBarType_Vertical)
	{
		pWidget->SetReservedIndentRight(pWidget->GetReservedIndentRight() + GetWidth() - GetInBorderWidth());
		SetPosition(sf::Vector2f(
			pWidget->GetWorkingRect().width + pWidget->GetPadding() + GetOutBorderWidth(),
			-pWidget->GetPadding() + GetOutBorderWidth()
		));
		SetLength(pWidget->GetWorkingRect().height + pWidget->GetPadding() * 2 - GetOutBorderWidth() * 2);
	}
	else
	{
		pWidget->SetReservedIndentBottom(pWidget->GetReservedIndentBottom() + GetWidth() - GetInBorderWidth());
		SetPosition(sf::Vector2f(
			-pWidget->GetPadding() + GetOutBorderWidth(),
			pWidget->GetWorkingRect().height + pWidget->GetPadding() + GetOutBorderWidth()
		));
		SetLength(pWidget->GetWorkingRect().width + pWidget->GetPadding() * 2 - GetOutBorderWidth() * 2);
	}

	if (pSecondScrollBar)
	{
		pSecondScrollBar->UnstickFrom(pWidget, NULL);
		pSecondScrollBar->StickTo(pWidget, NULL);
	}
}

void CWScrollBar::UnstickFrom(CWidget* pWidget, __in_opt CWScrollBar* pSecondScrollBar)
{
	if (!pWidget)
		return;

	SetParent(NULL);

	if (GetType() == eScrollBarType_Vertical)
		pWidget->SetReservedIndentRight(pWidget->GetReservedIndentRight() - GetWidth() + GetInBorderWidth());
	else
		pWidget->SetReservedIndentBottom(pWidget->GetReservedIndentBottom() - GetWidth() + GetInBorderWidth());

	SetPosition(sf::Vector2f(0, 0));
	SetSize(sf::Vector2f(0, 0));
	NormalizeSize();

	if (pSecondScrollBar)
	{
		pSecondScrollBar->UnstickFrom(pWidget, NULL);
		pSecondScrollBar->StickTo(pWidget, NULL);
	}
}

void CWScrollBar::NormalizeSize()
{
	if (GetWidth() <= 10)
		SetWidth(15);
	if (GetLength() < (GetButtonsLength() * 2 + m_fMinSliderLength))
		SetLength(GetButtonsLength() * 2 + m_fMinSliderLength);

	if (GetParent())
	{
		if (GetType() == eScrollBarType_Vertical)
		{
			SetPosition(sf::Vector2f(
				GetParent()->GetWorkingRect().width + GetParent()->GetPadding() + GetOutBorderWidth(),
				-GetParent()->GetPadding() + GetOutBorderWidth()
			));
			SetLength(GetParent()->GetWorkingRect().height + GetParent()->GetPadding() * 2 - GetOutBorderWidth() * 2);
		}
		else
		{
			SetPosition(sf::Vector2f(
				-GetParent()->GetPadding() + GetOutBorderWidth(),
				GetParent()->GetWorkingRect().height + GetParent()->GetPadding() + GetOutBorderWidth()
			));
			SetLength(GetParent()->GetWorkingRect().width + GetParent()->GetPadding() * 2 - GetOutBorderWidth() * 2);
		}
	}
}

void CWScrollBar::Draw()
{
	if (!GetRenderTarget())
		return;

	NormalizeSize();

	if (m_bUpLeftButtonPressed)
	{
		m_fViewPosition -= m_fSmallStep * m_fButtonScrollSpeed * m_Clock.getElapsedTime().asSeconds();
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_Clock.restart();
	}
	if (m_bDownRightButtonPressed)
	{
		m_fViewPosition += m_fSmallStep * m_fButtonScrollSpeed * m_Clock.getElapsedTime().asSeconds();
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_Clock.restart();
	}
	if (m_bUpLeftScrollAreaPressed)
	{
		if (m_fViewPosition > m_fScrollAreaPressedViewPosition)
		{
			m_fViewPosition -= m_fBigStep * m_fButtonScrollSpeed * m_Clock.getElapsedTime().asSeconds();
			m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
			if (m_OnScrollHandler)
				m_OnScrollHandler(m_fViewPosition);
		}
		m_Clock.restart();
	}
	if (m_bDownRightScrollAreaPressed)
	{
		if (m_fViewPosition < m_fScrollAreaPressedViewPosition)
		{
			m_fViewPosition += m_fBigStep * m_fButtonScrollSpeed * m_Clock.getElapsedTime().asSeconds();
			m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
			if (m_OnScrollHandler)
				m_OnScrollHandler(m_fViewPosition);
		}
		m_Clock.restart();
	}

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setOutlineThickness(GetBorderWidth());
	shape.setOutlineColor(m_clrBorderAndButtons);
	shape.setFillColor(m_clrScrollArea);

	sf::FloatRect frcButtonUpLeft = GetUpLeftButtonAbsoluteRect();
	sf::RectangleShape shapeButtonUpLeft(sf::Vector2f(frcButtonUpLeft.width, frcButtonUpLeft.height));
	shapeButtonUpLeft.setPosition(sf::Vector2f(frcButtonUpLeft.left, frcButtonUpLeft.top));
	shapeButtonUpLeft.setFillColor(m_clrBorderAndButtons);

	sf::FloatRect frcButtonDownRight = GetDownRightButtonAbsoluteRect();
	sf::RectangleShape shapeButtonDownRight(sf::Vector2f(frcButtonDownRight.width, frcButtonDownRight.height));
	shapeButtonDownRight.setPosition(sf::Vector2f(frcButtonDownRight.left, frcButtonDownRight.top));
	shapeButtonDownRight.setFillColor(m_clrBorderAndButtons);

	sf::CircleShape shapeArrow(GetButtonsLength() / 2.f * 0.77f, 3);
	shapeArrow.setOrigin(sf::Vector2f(
		shapeArrow.getLocalBounds().width / 2 + shapeArrow.getLocalBounds().left,
		shapeArrow.getLocalBounds().height / 2 + shapeArrow.getLocalBounds().top
	));

	sf::CircleShape shapeArrowUpLeft(shapeArrow);
	shapeArrowUpLeft.setPosition(sf::Vector2f(frcButtonUpLeft.left + GetButtonsLength() / 2, frcButtonUpLeft.top + GetButtonsLength() / 2));
	shapeArrowUpLeft.setRotation((m_eType == eScrollBarType_Vertical) ? 0.f : -90.f);
	shapeArrowUpLeft.setFillColor(m_bUpLeftButtonPressed ? m_clrArrowsDown : m_clrArrowsUp);

	sf::CircleShape shapeArrowDownRight(shapeArrow);
	shapeArrowDownRight.setPosition(sf::Vector2f(frcButtonDownRight.left + GetButtonsLength() / 2, frcButtonDownRight.top + GetButtonsLength() / 2));
	shapeArrowDownRight.setRotation((m_eType == eScrollBarType_Vertical) ? 180.f : 90.f);
	shapeArrowDownRight.setFillColor(m_bDownRightButtonPressed ? m_clrArrowsDown : m_clrArrowsUp);

	sf::FloatRect frcSlider = GetSliderAbsoluteRect();
	sf::RectangleShape shapeSlider(sf::Vector2f(frcSlider.width, frcSlider.height));
	shapeSlider.setPosition(sf::Vector2f(frcSlider.left, frcSlider.top));
	shapeSlider.setFillColor(m_bSliderPressed ? m_clrSliderDown : m_clrSliderUp);

	GetRenderTarget()->draw(shape);
	GetRenderTarget()->draw(shapeButtonUpLeft);
	GetRenderTarget()->draw(shapeButtonDownRight);
	GetRenderTarget()->draw(shapeArrowUpLeft);
	GetRenderTarget()->draw(shapeArrowDownRight);
	GetRenderTarget()->draw(shapeSlider);
}

void CWScrollBar::OnMouseWheelScrolled(
	sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
	float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
	int          x,			//< X position of the mouse pointer, relative to the left of the owner window
	int          y			//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseWheelScrolled(wheel, delta, x, y);

	if (GetType() == eScrollBarType_Horizontal)
		return;

	if (!GetScene().GetRenderWindow() || (wheel != sf::Mouse::Wheel::VerticalWheel))
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetParent())
	{
		if (!GetAbsoluteRect().contains(pos) && !GetParent()->GetAbsoluteRect().contains(pos))
			return;
	}
	else
	{
		if (!GetAbsoluteRect().contains(pos))
			return;
	}
	m_fViewPosition -= delta / 100;
	m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
	if (m_OnScrollHandler)
		m_OnScrollHandler(m_fViewPosition);
}

void CWScrollBar::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonPressed(button, x, y);

	if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetUpLeftButtonAbsoluteRect().contains(pos))
	{
		m_fViewPosition -= m_fSmallStep;
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_bUpLeftButtonPressed = true;
		m_Clock.restart();
	}
	if (GetDownRightButtonAbsoluteRect().contains(pos))
	{
		m_fViewPosition += m_fSmallStep;
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_bDownRightButtonPressed = true;
		m_Clock.restart();
	}
	if (GetUpLeftScrollAreaAbsoluteRect().contains(pos))
	{
		m_fViewPosition -= m_fBigStep;
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_bUpLeftScrollAreaPressed = true;
		float fMouseOffset = (m_eType == eScrollBarType_Vertical) ? (pos.y - GetUpLeftScrollAreaAbsoluteRect().top) : (pos.x - GetUpLeftScrollAreaAbsoluteRect().left);
		m_fScrollAreaPressedViewPosition = CalculateViewPosition(fMouseOffset - GetSliderLength() / 2);
		m_Clock.restart();
	}
	if (GetDownRightScrollAreaAbsoluteRect().contains(pos))
	{
		m_fViewPosition += m_fBigStep;
		m_fViewPosition = NormalizeViewPosition(m_fViewPosition);
		if (m_OnScrollHandler)
			m_OnScrollHandler(m_fViewPosition);
		m_bDownRightScrollAreaPressed = true;
		float fMouseOffset = (m_eType == eScrollBarType_Vertical) ? (pos.y - GetUpLeftScrollAreaAbsoluteRect().top) : (pos.x - GetUpLeftScrollAreaAbsoluteRect().left);
		m_fScrollAreaPressedViewPosition = CalculateViewPosition(fMouseOffset - GetSliderLength() / 2);
		m_Clock.restart();
	}
	if (GetSliderAbsoluteRect().contains(pos))
	{
		m_bSliderPressed = true;
		m_v2fSliderPressedMousePos = pos;
		m_fSliderPressedSliderPos = GetSliderScrollPosition();
	}
}

void CWScrollBar::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonReleased(button, x, y);

	if (button != sf::Mouse::Button::Left)
		return;

	m_bUpLeftButtonPressed = false;
	m_bDownRightButtonPressed = false;
	m_bUpLeftScrollAreaPressed = false;
	m_bDownRightScrollAreaPressed = false;
	m_bSliderPressed = false;
	m_v2fSliderPressedMousePos = sf::Vector2f();
	m_fSliderPressedSliderPos = 0;
}

void CWScrollBar::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseMoved(x, y);
	
	if (!GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (!GetUpLeftButtonAbsoluteRect().contains(pos))
		m_bUpLeftButtonPressed = false;
	if (!GetDownRightButtonAbsoluteRect().contains(pos))
		m_bDownRightButtonPressed = false;
	if (m_bSliderPressed)
	{
		if (GetScrollMouseAreaAbsoluteRect().contains(pos))
		{
			float fMouseOffset = (m_eType == eScrollBarType_Vertical) ? (pos.y - m_v2fSliderPressedMousePos.y) : (pos.x - m_v2fSliderPressedMousePos.x);
			m_fViewPosition = CalculateViewPosition(m_fSliderPressedSliderPos + fMouseOffset);
			if (m_OnScrollHandler)
				m_OnScrollHandler(m_fViewPosition);
		}
		else
		{
			m_fViewPosition = CalculateViewPosition(m_fSliderPressedSliderPos);
			if (m_OnScrollHandler)
				m_OnScrollHandler(m_fViewPosition);
		}
	}

	if (m_bUpLeftScrollAreaPressed)
	{
		if (GetUpLeftScrollAreaAbsoluteRect().contains(pos))
		{
			float fMouseOffset = (m_eType == eScrollBarType_Vertical) ? (pos.y - GetUpLeftScrollAreaAbsoluteRect().top) : (pos.x - GetUpLeftScrollAreaAbsoluteRect().left);
			m_fScrollAreaPressedViewPosition = CalculateViewPosition(fMouseOffset - GetSliderLength() / 2);
		}
		else
		{
			m_fScrollAreaPressedViewPosition = m_fViewPosition;
		}
	}

	if (m_bDownRightScrollAreaPressed)
	{
		if (GetDownRightScrollAreaAbsoluteRect().contains(pos))
		{
			float fMouseOffset = (m_eType == eScrollBarType_Vertical) ? (pos.y - GetUpLeftScrollAreaAbsoluteRect().top) : (pos.x - GetUpLeftScrollAreaAbsoluteRect().left);
			m_fScrollAreaPressedViewPosition = CalculateViewPosition(fMouseOffset - GetSliderLength() / 2);
		}
		else
		{
			m_fScrollAreaPressedViewPosition = m_fViewPosition;
		}
	}
}

sf::FloatRect CWScrollBar::GetUpLeftButtonAbsoluteRect()
{
	return sf::FloatRect(
		GetAbsolutePosition().x,
		GetAbsolutePosition().y,
		GetButtonsLength(),
		GetButtonsLength()
	);
}

sf::FloatRect CWScrollBar::GetDownRightButtonAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x : GetAbsolutePosition().x + GetSize().x - GetButtonsLength(),
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().y + GetSize().y - GetButtonsLength() : GetAbsolutePosition().y,
		GetButtonsLength(),
		GetButtonsLength()
	);
}

sf::FloatRect CWScrollBar::GetScrollAreaAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x : GetAbsolutePosition().x + GetButtonsLength(),
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().y + GetButtonsLength() : GetAbsolutePosition().y,
		(m_eType == eScrollBarType_Vertical) ? GetButtonsLength() : GetScrollAreaLength(),
		(m_eType == eScrollBarType_Vertical) ? GetScrollAreaLength() : GetButtonsLength()
	);
}

sf::FloatRect CWScrollBar::GetUpLeftScrollAreaAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x : GetAbsolutePosition().x + GetButtonsLength(),
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().y + GetButtonsLength() : GetAbsolutePosition().y,
		(m_eType == eScrollBarType_Vertical) ? GetButtonsLength() : GetSliderScrollPosition(),
		(m_eType == eScrollBarType_Vertical) ? GetSliderScrollPosition() : GetButtonsLength()
	);
}

sf::FloatRect CWScrollBar::GetDownRightScrollAreaAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x : GetAbsolutePosition().x + GetButtonsLength() + GetSliderScrollPosition() + GetSliderLength(),
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().y + GetButtonsLength() + GetSliderScrollPosition() + GetSliderLength() : GetAbsolutePosition().y,
		(m_eType == eScrollBarType_Vertical) ? GetButtonsLength() : GetScrollAreaLength() - GetSliderScrollPosition() - GetSliderLength(),
		(m_eType == eScrollBarType_Vertical) ? GetScrollAreaLength() - GetSliderScrollPosition() - GetSliderLength() : GetButtonsLength()
	);
}

float CWScrollBar::GetSliderLength()
{
	float fLength = GetScrollAreaLength() * m_fViewAmount;
	return ((fLength < m_fMinSliderLength) ? m_fMinSliderLength : fLength);
}

float CWScrollBar::CalculateViewPosition(float fSliderScrollPos)
{
	float fViewPos = (fSliderScrollPos / (GetScrollAreaLength() - GetSliderLength()));
	return NormalizeViewPosition(fViewPos);
}

sf::FloatRect CWScrollBar::GetSliderAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x : GetAbsolutePosition().x + GetButtonsLength() + GetSliderScrollPosition(),
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().y + GetButtonsLength() + GetSliderScrollPosition() : GetAbsolutePosition().y,
		(m_eType == eScrollBarType_Vertical) ? GetButtonsLength() : GetSliderLength(),
		(m_eType == eScrollBarType_Vertical) ? GetSliderLength() : GetButtonsLength()
	);
}

sf::FloatRect CWScrollBar::GetScrollMouseAreaAbsoluteRect()
{
	return sf::FloatRect(
		(m_eType == eScrollBarType_Vertical) ? GetAbsolutePosition().x - m_fMouseScrollAreaWidth : (float)SHRT_MIN,
		(m_eType == eScrollBarType_Vertical) ? (float)SHRT_MIN : GetAbsolutePosition().y - m_fMouseScrollAreaWidth,
		(m_eType == eScrollBarType_Vertical) ? m_fMouseScrollAreaWidth * 2 + GetButtonsLength() : (float)USHRT_MAX,
		(m_eType == eScrollBarType_Vertical) ? (float)USHRT_MAX : m_fMouseScrollAreaWidth * 2 + GetButtonsLength()
	);
}
