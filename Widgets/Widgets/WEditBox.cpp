#include "WEditBox.h"


CWEditBox::CWEditBox(CScene& rScene)
	: CWidget(rScene)
	, m_bStyleMultiline(false)
	, m_uiCursorPosition(0)
	, m_fLeftTextOffset(0)
	, m_fCursorNewOffset(-1)
	, m_bCursorIsVisible(true)
	, m_bCanTextEntered(false)
{
	SetFontSize(12);
	SetTextColor(sf::Color(200, 200, 200));
	SetBorderWidth(-1.f);
	SetPadding(5);
	SetBorderColor(sf::Color(255, 0, 0));
}

CWEditBox::~CWEditBox()
{
}

void CWEditBox::NormalizeSize()
{
	SetWorkingSize(sf::Vector2f(GetWorkingSize().x, (float)GetFontSize()));
}

void CWEditBox::Draw()
{
	if (!GetRenderTarget())
		return;

	if (GetWorkingSize().x <= 0)
		return;

	NormalizeSize();

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(GetFillColor());
	GetRenderTarget()->draw(shape);

	GetScene().PushClipping(GetAbsoluteTextAreaWorkingRect());


	sf::Text text(L"f", GetFont(), GetFontSize());
	CScene::AlignText(eHAlignment_None, eVAlignment_Center, text, GetAbsoluteTextAreaWorkingRect());
	text.setString(__super::GetText());
	text.setFillColor(GetTextColor());
	text.move(sf::Vector2f(GetAbsoluteTextAreaWorkingRect().left + m_fLeftTextOffset, 0));

	sf::RectangleShape cursor(sf::Vector2f(1, GetFontSize() * 1.3f));
	cursor.setFillColor(GetTextColor());
	CScene::AlignShape(eHAlignment_None, eVAlignment_Center, cursor, GetAbsoluteTextAreaWorkingRect());
	if (m_fCursorNewOffset >= 0)
	{
		m_uiCursorPosition = FindNearestIndexByOffset(text, text.getString(), m_fCursorNewOffset);
		m_fCursorNewOffset = -1;
	}
	cursor.move(sf::Vector2f(GetAbsoluteTextAreaWorkingRect().left + m_fLeftTextOffset + CalculateCursorOffset(text, text.getString(), m_uiCursorPosition), 0));

	float fRightExcess = (GetAbsoluteTextAreaWorkingRect().left + GetAbsoluteTextAreaWorkingRect().width - 1) - cursor.getPosition().x;
	if (fRightExcess < 0)
	{
		cursor.move(sf::Vector2f(fRightExcess, 0));
		text.move(sf::Vector2f(fRightExcess, 0));
		m_fLeftTextOffset += fRightExcess;
	}
	float fLeftExcess = GetAbsoluteTextAreaWorkingRect().left - cursor.getPosition().x;
	if (fLeftExcess > 0)
	{
		cursor.move(sf::Vector2f(fLeftExcess, 0));
		text.move(sf::Vector2f(fLeftExcess, 0));
		m_fLeftTextOffset += fLeftExcess;
		if (m_fLeftTextOffset > 0)
			m_fLeftTextOffset = 0;
	}


	GetRenderTarget()->draw(text);

	__super::Draw();

	GetScene().PopClipping();

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(GetBorderColor());
	border.setFillColor(sf::Color(0, 0, 0, 0));
	GetRenderTarget()->draw(border);
	
	if (m_Clock.getElapsedTime() > sf::milliseconds(500))
	{
		m_bCursorIsVisible = !m_bCursorIsVisible;
		m_Clock.restart();
	}
	if (m_bCursorIsVisible && IsFocused())
		GetRenderTarget()->draw(cursor);
}

void CWEditBox::OnTextEntered(sf::Uint32 unicode)
{
	if (!IsFocused())
		return;
	if (iswprint(unicode))
	{
		__super::SetText(std::wstring(__super::GetText()).insert(m_uiCursorPosition, 1, unicode));
		m_uiCursorPosition += 1;
		if (m_OnTextChangedHandler)
			m_OnTextChangedHandler(__super::GetText());
	}
}

void CWEditBox::OnKeyPressed(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (!IsFocused())
		return;

	if (code == sf::Keyboard::Key::Right)
	{
		if (m_uiCursorPosition < (sf::Uint32)__super::GetText().size())
			m_uiCursorPosition += 1;
	}
	if (code == sf::Keyboard::Key::End)
	{
		m_uiCursorPosition = (sf::Uint32)__super::GetText().size();
	}
	if (code == sf::Keyboard::Key::Left)
	{
		if (m_uiCursorPosition > 0)
			m_uiCursorPosition -= 1;
	}
	if (code == sf::Keyboard::Key::Home)
	{
		m_uiCursorPosition = 0;
	}
	if (code == sf::Keyboard::Key::BackSpace)
	{
		if (m_uiCursorPosition > 0)
		{
			__super::SetText(std::wstring(__super::GetText()).erase((m_uiCursorPosition - 1), 1));
			m_uiCursorPosition -= 1;
			if (m_OnTextChangedHandler)
				m_OnTextChangedHandler(__super::GetText());
		}
	}
	if (code == sf::Keyboard::Key::Delete)
	{
		if (m_uiCursorPosition < (sf::Uint32)__super::GetText().size())
			__super::SetText(std::wstring(__super::GetText()).erase(m_uiCursorPosition, 1));
		if (m_OnTextChangedHandler)
			m_OnTextChangedHandler(__super::GetText());
	}
	if (m_bCanTextEntered && code == sf::Keyboard::Key::Enter)
	{
		if (m_OnTextEnteredHandler)
		{
			if (m_OnTextEnteredHandler(__super::GetText()))
			{
				SetText(L"");
				if (m_OnTextChangedHandler)
					m_OnTextChangedHandler(__super::GetText());
			}
		}
	}
}

void CWEditBox::OnKeyReleased(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (!IsFocused())
		return;
}

void CWEditBox::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonPressed(button, x, y);

	if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetAbsoluteRect().contains(pos))
	{
		SetFocused(true);
	}
	if (GetAbsoluteTextAreaWorkingRect().contains(pos))
	{
		m_fCursorNewOffset = pos.x - GetAbsoluteTextAreaWorkingRect().left - m_fLeftTextOffset;
	}
}

void CWEditBox::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseButtonReleased(button, x, y);

	//if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
	//	return;

	//sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
}

void CWEditBox::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!IsEnabled())
		return;

	__super::OnMouseMoved(x, y);

	//if (!GetScene().GetRenderWindow())
	//	return;

	//sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
}

sf::FloatRect CWEditBox::GetAbsoluteTextAreaWorkingRect()
{
	return sf::FloatRect(
		GetAbsoluteWorkingRect().left,
		GetAbsoluteWorkingRect().top - GetPadding(),
		GetAbsoluteWorkingRect().width,
		GetAbsoluteWorkingRect().height + GetPadding() * 2
	);
}

float CWEditBox::CalculateCursorOffset(const sf::Text& rText, const std::wstring& rsText, sf::Uint32 index)
{
	float fOffset = 0.f;
	for (sf::Uint32 i = 0; ((i < rsText.size()) && (i < index)); ++i)
		fOffset += GetFont().getGlyph(rsText[i], GetFontSize(), (rText.getStyle() & sf::Text::Bold), rText.getOutlineThickness()).advance;
	return fOffset;
}

sf::Uint32 CWEditBox::FindNearestIndexByOffset(const sf::Text& rText, const std::wstring& rsText, float fOffset)
{
	float fCharOffset = 0.f;
	sf::Uint32 i = 0;
	while (i < rsText.size())
	{
		fCharOffset += GetFont().getGlyph(rsText[i], GetFontSize(), (rText.getStyle() & sf::Text::Bold), rText.getOutlineThickness()).advance;
		if (fCharOffset > fOffset)
			return i;
		++i;
	}
	return (sf::Uint32)rsText.size();
}
