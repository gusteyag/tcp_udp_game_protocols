#include "WContainer.h"
#include "WText.h"


CWContainer::CWContainer(CScene& rScene)
	: CWidget(rScene)
	, m_fHeaderHeight(25)
	, m_clrHeader(200, 0, 0)
{
	SetFontSize(12);
	SetTextColor(sf::Color(255, 255, 255));
	SetPadding(10);
	SetBorderWidth(-2);
	SetBorderColor(sf::Color(255, 0, 0));

	SetReservedIndentTop(m_fHeaderHeight - GetInBorderWidth());
	m_pHeader.reset(new CWText(rScene));
	m_pHeader->SetPadding(0);
}

CWContainer::~CWContainer()
{
}

void CWContainer::Draw()
{
	if (!GetRenderTarget())
		return;

	SetReservedIndentTop(m_fHeaderHeight - GetInBorderWidth());

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(GetFillColor());
	GetRenderTarget()->draw(shape);

	GetScene().PushClipping(GetAbsoluteWorkingRect());

	__super::Draw();

	GetScene().PopClipping();

	if (m_fHeaderHeight)
	{
		m_pHeader->SetRenderTarget(GetRenderTarget());
		m_pHeader->SetText(GetText());
		m_pHeader->SetFont(GetFont());
		m_pHeader->SetFontSize(GetFontSize() + 2);
		//m_pHeader->SetTextStyle(sf::Text::Bold);
		m_pHeader->SetBorderWidth(GetBorderWidth());
		m_pHeader->SetBorderColor(GetBorderColor());
		m_pHeader->SetFillColor(m_clrHeader);
		m_pHeader->SetSize(sf::Vector2f(GetSize().x, m_fHeaderHeight));
		m_pHeader->SetPosition(GetAbsolutePosition());
		m_pHeader->Draw();
	}

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(GetBorderColor());
	border.setFillColor(sf::Color(0, 0, 0, 0));
	GetRenderTarget()->draw(border);
}
