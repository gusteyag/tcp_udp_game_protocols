#pragma once
#include <memory>
#include <list>
#include <SFML/Graphics.hpp>


class CWidget;

enum eHAlignment
{
	eHAlignment_None,
	eHAlignment_Center,
	eHAlignment_Left,
	eHAlignment_Right,
};

enum eVAlignment
{
	eVAlignment_None,
	eVAlignment_Center,
	eVAlignment_Top,
	eVAlignment_Bottom,
};

class CScene
{
	friend class CSceneObject;

public:
	CScene();
	virtual ~CScene();

	void SetRenderWindow(sf::RenderWindow* pRenderWindow);
	sf::RenderWindow* GetRenderWindow() { return m_pRenderWindow; }

	CWidget* GetMainWidget() { return m_pMainWidget.get(); }

	void OnEvent(const sf::Event& rEvent);
	void Draw();

	void PushClipping(sf::FloatRect& rcClippingGlobalRect) { m_lstClippingRects.push_back(rcClippingGlobalRect); UpdateView(); }
	void PopClipping() { m_lstClippingRects.pop_back(); UpdateView(); }

	static void CScene::AlignText(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Text& rTextToAlign, const sf::FloatRect& rcGlobalBounds);
	static void CScene::AlignText(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Text& rTextToAlign, const sf::Shape& rBaseShape);
	static void CScene::CenterText(__inout sf::Text& rTextToAlign, const sf::FloatRect& rcGlobalBounds);
	static void CScene::CenterText(__inout sf::Text& rTextToAlign, const sf::Shape& rBaseShape);
	static void CScene::AlignShape(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Shape& rShapeToAlign, const sf::FloatRect& rcGlobalBounds);
	static void CScene::AlignShape(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout sf::Shape& rShapeToAlign, const sf::Shape& rBaseShape);
	static void CScene::CenterShape(__inout sf::Shape& rShapeToAlign, const sf::FloatRect& rcGlobalBounds);
	static void CScene::CenterShape(__inout sf::Shape& rShapeToAlign, const sf::Shape& rBaseShape);
	static void AlignWidget(eHAlignment eHAlignment_, eVAlignment eVAlignment_, __inout CWidget& rWidgetToAlign, const sf::FloatRect& rcGlobalBounds);
	static sf::FloatRect GetIntersect(const sf::FloatRect& rc1, const sf::FloatRect& rc2);

private:
	sf::RenderWindow* m_pRenderWindow;
	CWidget* m_pFocusedWidget;
	std::unique_ptr<CWidget> m_pMainWidget;
	std::list<sf::FloatRect> m_lstClippingRects;

private:
	void UpdateView();
};

class CSceneObject
{
protected:
	CSceneObject(CScene& rScene)
		: m_rScene(rScene)
	{}
	virtual ~CSceneObject() {}

	CScene& GetScene() { return m_rScene; }

	sf::RenderWindow* GetRenderWindow() { return m_rScene.m_pRenderWindow; }

	void SetSceneFocusedWidget(CWidget* pWidget) { m_rScene.m_pFocusedWidget = pWidget; }
	CWidget* GetSceneFocusedWidget() const { return m_rScene.m_pFocusedWidget; }

private:
	CScene& m_rScene;
};
