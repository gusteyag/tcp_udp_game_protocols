#pragma once
#include <string>
#include <functional>
#include "Widget.h"


class CWButton :
	public CWidget
{
public:
	CWButton(CScene& rScene);
	virtual ~CWButton();

	void SetStyleRadio(bool bRadio) { m_bStyleRadio = bRadio; }
	bool IsStyleRadio() const { return m_bStyleRadio; }

	void SetStyleCheckBox(bool bCheckBox) { m_bStyleCheckBox = bCheckBox; }
	bool IsStyleCheckBox() const { return m_bStyleCheckBox; }

	void SetSelected(bool bSelected) { if (m_bStyleRadio) { m_bPushed = bSelected; CheckOthersRadio(); } }
	bool IsSelected() const { return (m_bStyleRadio ? m_bPushed : false); }

	void SetChecked(bool bChecked) { if (m_bStyleCheckBox) { m_bChecked = bChecked; m_bPushed = m_bChecked; }}
	bool IsChecked() const { return (m_bStyleCheckBox ? m_bChecked : false); }

	void SetOnClickedHandler(const std::function<void(void)>& rHandler) { m_OnClickedHandler = rHandler; }

	// For radio button
	void SetOnSelectedHandler(const std::function<void(void)>& rHandler) { m_OnSelectedHandler = rHandler; }

	// For chsckbox button
	void SetOnCheckedHandler(const std::function<void(void)>& rHandler) { m_OnCheckedHandler = rHandler; }
	void SetOnUncheckedHandler(const std::function<void(void)>& rHandler) { m_OnUncheckedHandler = rHandler; }

	virtual void Draw();

	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	bool m_bStyleRadio;
	bool m_bStyleCheckBox;
	bool m_bPushed;
	bool m_bChecked;
	sf::Color m_BorderUpColor;
	sf::Color m_BorderPushColor;
	sf::Color m_UpColor;
	sf::Color m_PushColor;
	sf::Color m_TextUpColor;
	sf::Color m_TextPushColor;
	sf::Color m_UpColorDisabled;
	sf::Color m_TextUpColorDisabled;
	std::function<void(void)> m_OnClickedHandler;
	std::function<void(void)> m_OnSelectedHandler;
	std::function<void(void)> m_OnCheckedHandler;
	std::function<void(void)> m_OnUncheckedHandler;

private:
	void CheckOthersRadio();
};
