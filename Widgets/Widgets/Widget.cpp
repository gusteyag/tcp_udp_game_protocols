#include "Widget.h"


CWidget::CWidget(CScene& rScene)
	: CSceneObject(rScene)
	, m_pRenderTarget(NULL)
	, m_uiFontSize(15)
	, m_fBorderWidth(-1.f)
	, m_fPadding(5.f)
	, m_fReservedIndentLeft(0)
	, m_fReservedIndentTop(0)
	, m_fReservedIndentRight(0)
	, m_fReservedIndentBottom(0)
	, m_clrFillColor(sf::Color(0, 0, 0, 0))
	, m_pParent(NULL)
	, m_v2fPosition(0, 0)
	, m_v2fSize(0, 0)
	, m_bIsEnabled(true)
	, m_bIsVisible(true)
{
}

CWidget::~CWidget()
{
	SetFocused(false);
}

void CWidget::SetFocused(bool bIsFocused)
{
	if (IsFocused() == bIsFocused)
		return;
	if (bIsFocused)
	{
		if (!IsEnabled())
			return;
		CWidget* pFocused = GetSceneFocusedWidget();
		if (pFocused)
			pFocused->SetFocused(false);
		SetSceneFocusedWidget(this);
	}
	else
	{
		CWidget* pFocused = GetSceneFocusedWidget();
		if (pFocused && pFocused == this)
			SetSceneFocusedWidget(NULL);
	}
}

void CWidget::AddChild(CWidget* pChild)
{
	if (!pChild)
		return;
	pChild->SetParent(this);
	m_lstZOrder.push_back(std::unique_ptr<CWidget>(pChild));
}

bool CWidget::RemoveChild(CWidget* pChild)
{
	bool bRemoved = false;
	m_lstZOrder.remove_if([pChild, &bRemoved](const std::unique_ptr<CWidget>& pChildItem)
	{
		return ((pChildItem.get() == pChild) ? (pChildItem->SetFocused(false), bRemoved = true) : false);
	});
	return bRemoved;
}

CWidget* CWidget::ReleaseChild(CWidget* pChild)
{
	m_lstZOrder.remove_if([pChild](std::unique_ptr<CWidget>& pChildItem)
	{
		return ((pChildItem.get() == pChild) ? (pChildItem->SetFocused(false), pChildItem.release(), true) : false);
	});
	return pChild;
}

bool CWidget::IsChild(CWidget* pChild)
{
	bool bFound = false;
	std::find_if(m_lstZOrder.begin(), m_lstZOrder.end(), [pChild, &bFound](const std::unique_ptr<CWidget>& pChildItem)
	{
		return ((pChildItem.get() == pChild) ? (bFound = true) : false);
	});
	return bFound;
}

bool CWidget::IsExists(CWidget* pWidget)
{
	bool bFound = NULL;
	for (auto& child : m_lstZOrder)
	{
		if (child.get() == pWidget)
		{
			bFound = true;
			break;
		}
		bFound = child->IsExists(pWidget);
		if (bFound)
			break;
	}
	return bFound;
}

void CWidget::Draw()
{
	for (auto& child : m_lstZOrder)
	{
		if(child->IsVisible())
			child->Draw();
	}
}

void CWidget::OnTextEntered(sf::Uint32 unicode)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnTextEntered(unicode);
	}
}

void CWidget::OnKeyPressed(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnKeyPressed(code, alt, control, shift, system);
	}
}

void CWidget::OnKeyReleased(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnKeyReleased(code, alt, control, shift, system);
	}
}

void CWidget::OnMouseWheelScrolled(
	sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
	float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
	int          x,			//< X position of the mouse pointer, relative to the left of the owner window
	int          y			//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnMouseWheelScrolled(wheel, delta, x, y);
	}
}

void CWidget::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (!GetScene().GetRenderWindow())
		return;
	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	
	std::list<std::unique_ptr<CWidget> >::reverse_iterator ritChild = m_lstZOrder.rbegin();
	for (; ritChild != m_lstZOrder.rend(); ++ritChild)
	{
		if ((*ritChild)->IsVisible() && (*ritChild)->GetAbsoluteBoundingRect().contains(pos))
		{
			(*ritChild)->OnMouseButtonPressed(button, x, y);
			break;
		}
	}
}

void CWidget::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnMouseButtonReleased(button, x, y);
	}
}

void CWidget::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	for (auto& child : m_lstZOrder)
	{
		if (child->IsVisible())
			child->OnMouseMoved(x, y);
	}
}
