#pragma once
#include "Widget.h"
#include <string>
#include <functional>


class CWEditBox :
	public CWidget
{
public:
	CWEditBox(CScene& rScene);
	virtual ~CWEditBox();

	virtual void SetText(const std::wstring& rText) { __super::SetText(rText); m_uiCursorPosition = (sf::Uint32)__super::GetText().size(); }

	void SetStyleMultiline(bool bMultiline) { m_bStyleMultiline = bMultiline; }
	bool IsStyleMultiline() const { return m_bStyleMultiline; }

	void SetCanTextEntered(bool bCanTextEntered) { m_bCanTextEntered = bCanTextEntered; }
	bool CanTextEntered() const { return m_bCanTextEntered; }

	void SetOnTextEnteredHandler(const std::function<bool(const std::wstring& rsText)>& rHandler) { m_OnTextEnteredHandler = rHandler; }
	void SetOnTextChangedHandler(const std::function<void(const std::wstring& rsText)>& rHandler) { m_OnTextChangedHandler = rHandler; }

	virtual void NormalizeSize();
	virtual void Draw();

	virtual void OnTextEntered(sf::Uint32 unicode);
	virtual void OnKeyPressed(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnKeyReleased(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	bool m_bStyleMultiline;
	sf::Uint32 m_uiCursorPosition;
	float m_fLeftTextOffset;
	float m_fCursorNewOffset;
	bool m_bCursorIsVisible;
	sf::Clock m_Clock;
	bool m_bCanTextEntered;
	std::function<bool(const std::wstring& rsText)> m_OnTextEnteredHandler;
	std::function<void(const std::wstring& rsText)> m_OnTextChangedHandler;

private:
	sf::FloatRect GetAbsoluteTextAreaWorkingRect();
	float CalculateCursorOffset(const sf::Text& rText, const std::wstring& rsText, sf::Uint32 index);
	sf::Uint32 FindNearestIndexByOffset(const sf::Text& rText, const std::wstring& rsText, float fOffset);
};
