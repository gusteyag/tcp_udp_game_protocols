#pragma once
#include "WContainer.h"
#include <functional>


class CWTextBox;
class CWEditBox;

class CWChat :
	public CWContainer
{
public:
	CWChat(CScene& rScene);
	virtual ~CWChat();

	void AddMessage(const std::wstring& rsAuthor, const std::wstring& rsMessage);
	void ClearEditText();
	void ClearMessages();

	void SetOnTextEnteredHandler(const std::function<void(const std::wstring& rsText)>& rHandler) { m_OnTextEnteredHandler = rHandler; }

	virtual void Draw();

private:
	CWTextBox* m_pWTextBox;
	CWEditBox* m_pWEditBox;
	std::function<void(const std::wstring& rsText)> m_OnTextEnteredHandler;
};

