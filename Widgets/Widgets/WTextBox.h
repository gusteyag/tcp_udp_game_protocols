#pragma once
#include "Widget.h"
#include <string>
#include <list>


class CWScrollBar;

struct TextChunk
{
	std::wstring text;
	sf::Color color;

	TextChunk()
		: color(sf::Color(0, 0, 0, 0))
	{}
	TextChunk(const std::wstring& text)
		: text(text)
		, color(sf::Color(0, 0, 0, 0))
	{}
	TextChunk(const std::wstring& text, const sf::Color& color)
		: text(text)
		, color(color)
	{}
};

class CWTextBox :
	public CWidget
{
public:
	CWTextBox(CScene& rScene);
	virtual ~CWTextBox();

	void SetWordsWrap(bool bWordsWrap) { m_bWordsWrap = bWordsWrap; }
	bool GetWordsWrap() const { return m_bWordsWrap; }

	void SetStartVScrollVeiwPos(float fPosition) { m_fStartVScrollVeiwPos = fPosition; }
	float GetStartVScrollVeiwPos() const { return m_fStartVScrollVeiwPos; }

	void SetStartHScrollVeiwPos(float fPosition) { m_fStartHScrollVeiwPos = fPosition; }
	float GetStartHScrollVeiwPos() const { return m_fStartHScrollVeiwPos; }

	void SetVScrollVeiwPos(float fPosition);
	void SetHScrollVeiwPos(float fPosition);

	void AddText(const TextChunk& rChunk) { m_lstChunks.push_back(rChunk); }
	void ClearText() { m_lstChunks.clear(); }

	virtual void Draw();

	virtual void OnTextEntered(sf::Uint32 unicode);
	virtual void OnKeyPressed(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnKeyReleased(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);
	virtual void OnMouseWheelScrolled(
		sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
		float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
		int          x,			//< X position of the mouse pointer, relative to the left of the owner window
		int          y			//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	bool m_bWordsWrap;
	std::list<TextChunk> m_lstChunks;
	std::unique_ptr<CWScrollBar> m_pVScrollBar;
	std::unique_ptr<CWScrollBar> m_pHScrollBar;
	sf::Vector2f m_v2fTextSize;
	float m_fStartVScrollVeiwPos;
	float m_fStartHScrollVeiwPos;
	float m_fVScrollOffset;
	float m_fHScrollOffset;

private:
	bool CalculateSuitableLine(
		const sf::Text& rText,
		const std::wstring& rsAllText,
		__out sf::Uint32& ruiLineOffset,
		__out sf::Uint32& ruiLineCharsCount,
		__out float& rfLineWidth,
		__inout sf::Uint32& ruiNextIndex,
		__inout float& rfNextOffset
	);
	sf::Vector2f CalculateTextBoundingSize();
	void DrawText();
};
