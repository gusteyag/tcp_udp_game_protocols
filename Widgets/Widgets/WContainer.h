#pragma once
#include "Widget.h"
#include <memory>


class CWText;

class CWContainer :
	public CWidget
{
public:
	CWContainer(CScene& rScene);
	virtual ~CWContainer();

	void SetHeaderHeight(float fHeaderHeight) { m_fHeaderHeight = fHeaderHeight; }
	float GetHeaderHeight() const { return m_fHeaderHeight; }

	void SetHeaderColor(const sf::Color& clrHeader) { m_clrHeader = clrHeader; }
	const sf::Color& GetHeaderColor() const { return m_clrHeader; }

	virtual void Draw();

private:
	float m_fHeaderHeight;
	sf::Color m_clrHeader;
	std::unique_ptr<CWText> m_pHeader;
};

