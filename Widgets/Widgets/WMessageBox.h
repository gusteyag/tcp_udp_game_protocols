#pragma once
#include <functional>
#include "WContainer.h"


class CWText;
class CWButton;

enum eMessageType
{
	eMessageType_Error,
	eMessageType_Warning,
	eMessageType_Info,
};

class CWMessageBox :
	public CWContainer
{
public:
	CWMessageBox(CScene& rScene);
	virtual ~CWMessageBox();
	
	void SetOnButtonClickedHandler(const std::function<void(void)>& rHandler) { m_OnButtonClickedHandler = rHandler; }

	void SetMessageText(const std::wstring& rText) { m_sMessage = rText; }
	const std::wstring& GetMessageText() const { return m_sMessage; }

	void SetButtonText(const std::wstring& rText) { m_sButton = rText; }
	const std::wstring& GetButtonText() const { return m_sButton; }

	void Show(eMessageType eType, const std::wstring& rsMessage, const std::function<void(void)>& rHandler = nullptr);

	virtual void NormalizeSize();

	virtual void Draw();

private:
	std::wstring m_sMessage;
	std::wstring m_sButton;
	CWText* m_pText;
	CWButton* m_pButton;
	std::function<void(void)> m_OnButtonClickedHandler;
};
