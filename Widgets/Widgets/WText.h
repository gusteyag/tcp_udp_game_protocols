#pragma once
#include "Widget.h"
#include <memory>


class CWText :
	public CWidget
{
public:
	CWText(CScene& rScene);
	virtual ~CWText();

	void SetWidget(CWidget* pWidget) { m_pWidget.reset(pWidget); }
	CWidget* GetWidget() { return m_pWidget.get(); }
	CWidget* ReleaseWidget() { return m_pWidget.release(); }
	void RemoveWidget() { m_pWidget.reset(NULL); }

	void SetWordsWrap(bool bWordsWrap) { m_bWordsWrap = bWordsWrap; }
	bool GetWordsWrap() const { return m_bWordsWrap; }

	void SetTextHAlignment(eHAlignment eTextHAlignment) { m_eTextHAlignment = eTextHAlignment; }
	eHAlignment GetTextHAlignment() const { return m_eTextHAlignment; }

	void SetTextVAlignment(eVAlignment eTextVAlignment) { m_eTextVAlignment = eTextVAlignment; }
	eVAlignment GetTextVAlignment() const { return m_eTextVAlignment; }

	void SetWidgetVsTextSpacing(float fSpacing) { m_fWidgetVsTextSpacing = fSpacing; }
	float GetWidgetVsTextSpacing() const { return m_fWidgetVsTextSpacing; }

	void SetTextStyle(sf::Uint32 uiTextStyle) { m_uiTextStyle = uiTextStyle; }
	sf::Uint32 GetTextStyle() const { return m_uiTextStyle; }

	virtual void Draw();

private:
	std::unique_ptr<CWidget> m_pWidget;
	bool m_bWordsWrap;
	eHAlignment m_eTextHAlignment;
	eVAlignment m_eTextVAlignment;
	float m_fWidgetVsTextSpacing;
	sf::Uint32 m_uiTextStyle;

private:
	sf::FloatRect GetAbsoluteTextAreaWorkingRect();
	bool CalculateSuitableLine(
		const sf::Text& rText,
		const std::wstring& rsAllText,
		__out sf::Uint32& ruiLineOffset,
		__out sf::Uint32& ruiLineCharsCount,
		__out float& rfLineWidth,
		__inout sf::Uint32& ruiNextIndex,
		__inout float& rfNextOffset
	);
	sf::Vector2f CalculateTextBoundingSize();
	void DrawText(float fVOffset);
};
