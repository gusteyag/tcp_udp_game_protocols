#include "WTextBox.h"
#include "WScrollBar.h"
#include <algorithm>


CWTextBox::CWTextBox(CScene& rScene)
	: CWidget(rScene)
	, m_bWordsWrap(true)
	, m_fStartVScrollVeiwPos(1.f)
	, m_fStartHScrollVeiwPos(0)
	, m_fVScrollOffset(0)
	, m_fHScrollOffset(0)
{
	SetFontSize(12);
	SetTextColor(sf::Color(200, 200, 200));
	SetBorderWidth(-1.f);
	SetPadding(5);
	SetBorderColor(sf::Color(255, 0, 0));
}

CWTextBox::~CWTextBox()
{
}

void CWTextBox::SetVScrollVeiwPos(float fPosition)
{
	if (!m_pVScrollBar.get())
		return;
	m_pVScrollBar->SetVeiwPosition(fPosition);
	m_fVScrollOffset = (GetWorkingSize().y - m_v2fTextSize.y) * m_pVScrollBar->GetViewPosition();
}

void CWTextBox::SetHScrollVeiwPos(float fPosition)
{
	if (!m_pHScrollBar.get())
		return;
	m_pHScrollBar->SetVeiwPosition(fPosition);
	m_fHScrollOffset = (GetWorkingSize().x - m_v2fTextSize.x) * m_pHScrollBar->GetViewPosition();
}

void CWTextBox::Draw()
{
	if (!GetRenderTarget())
		return;

	if (GetWorkingSize().x <= 0)
		return;

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(GetFillColor());
	GetRenderTarget()->draw(shape);

	GetScene().PushClipping(GetAbsoluteWorkingRect());


	m_v2fTextSize = CalculateTextBoundingSize();

	bool bSomeChanged = false;
	if (m_v2fTextSize.y > GetWorkingSize().y)
	{
		if (!m_pVScrollBar.get())
		{
			m_pVScrollBar.reset(new CWScrollBar(GetScene(), eScrollBarType_Vertical));
			m_pVScrollBar->StickTo(this, m_pHScrollBar.get());
			m_pVScrollBar->SetRenderTarget(GetRenderTarget());
			SetVScrollVeiwPos(m_fStartVScrollVeiwPos);
			m_pVScrollBar->SetOnScrollHandler([this](float fVeiwPosition) { SetVScrollVeiwPos(fVeiwPosition); });
			bSomeChanged = true;
		}
	}
	else
	{
		if (m_pVScrollBar.get())
		{
			m_pVScrollBar->UnstickFrom(this, m_pHScrollBar.get());
			m_pVScrollBar.reset(NULL);
			bSomeChanged = true;
		}
		m_fVScrollOffset = 0;
	}

	if (bSomeChanged)
		m_v2fTextSize = CalculateTextBoundingSize();

	bSomeChanged = false;
	if (m_v2fTextSize.x > GetWorkingSize().x)
	{
		if (!m_pHScrollBar.get())
		{
			m_pHScrollBar.reset(new CWScrollBar(GetScene(), eScrollBarType_Horizontal));
			m_pHScrollBar->StickTo(this, m_pVScrollBar.get());
			m_pHScrollBar->SetRenderTarget(GetRenderTarget());
			SetHScrollVeiwPos(m_fStartHScrollVeiwPos);
			m_pHScrollBar->SetOnScrollHandler([this](float fVeiwPosition) { SetHScrollVeiwPos(fVeiwPosition); });
			bSomeChanged = true;
		}
	}
	else
	{
		if (m_pHScrollBar.get())
		{
			m_pHScrollBar->UnstickFrom(this, m_pVScrollBar.get());
			m_pHScrollBar.reset(NULL);
			bSomeChanged = true;
		}
		m_fHScrollOffset = 0;
	}

	if (bSomeChanged)
		m_v2fTextSize = CalculateTextBoundingSize();

	if (m_pVScrollBar.get())
		m_pVScrollBar->SetVeiwAmount(GetWorkingSize().y / m_v2fTextSize.y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->SetVeiwAmount(GetWorkingSize().x / m_v2fTextSize.x);

	if (m_pVScrollBar.get())
		SetVScrollVeiwPos(m_pVScrollBar->GetViewPosition());
	if (m_pHScrollBar.get())
		SetHScrollVeiwPos(m_pHScrollBar->GetViewPosition());

	DrawText();


	__super::Draw();

	GetScene().PopClipping();

	if (m_pVScrollBar.get())
		m_pVScrollBar->Draw();
	if (m_pHScrollBar.get())
		m_pHScrollBar->Draw();

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(GetBorderColor());
	border.setFillColor(sf::Color(0, 0, 0, 0));
	GetRenderTarget()->draw(border);
}

void CWTextBox::OnTextEntered(sf::Uint32 unicode)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnTextEntered(unicode);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnTextEntered(unicode);

	__super::OnTextEntered(unicode);
}

void CWTextBox::OnKeyPressed(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnKeyPressed(code, alt, control, shift, system);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnKeyPressed(code, alt, control, shift, system);

	__super::OnKeyPressed(code, alt, control, shift, system);
}

void CWTextBox::OnKeyReleased(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnKeyReleased(code, alt, control, shift, system);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnKeyReleased(code, alt, control, shift, system);

	__super::OnKeyReleased(code, alt, control, shift, system);
}

void CWTextBox::OnMouseWheelScrolled(
	sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
	float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
	int          x,			//< X position of the mouse pointer, relative to the left of the owner window
	int          y			//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseWheelScrolled(wheel, delta, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseWheelScrolled(wheel, delta, x, y);

	__super::OnMouseWheelScrolled(wheel, delta, x, y);
}

void CWTextBox::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseButtonPressed(button, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseButtonPressed(button, x, y);

	__super::OnMouseButtonPressed(button, x, y);
}

void CWTextBox::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseButtonReleased(button, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseButtonReleased(button, x, y);

	__super::OnMouseButtonReleased(button, x, y);
}

void CWTextBox::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseMoved(x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseMoved(x, y);

	__super::OnMouseMoved(x, y);
}

bool CWTextBox::CalculateSuitableLine(
	const sf::Text& rText,
	const std::wstring& rsAllText,
	__out sf::Uint32& ruiLineOffset,
	__out sf::Uint32& ruiLineCharsCount,
	__out float& rfLineWidth,
	__inout sf::Uint32& ruiNextIndex,
	__inout float& rfNextOffset
)
{
	rfLineWidth = 0;
	sf::Uint32 uiStartIndex = ruiNextIndex;
	float fStartOffset = rfNextOffset;
	sf::Int32 iLastSpaceIndex = -1;
	for (; ruiNextIndex < (sf::Uint32)rsAllText.size(); ++ruiNextIndex)
	{
		rfLineWidth = rfNextOffset;
		if (rsAllText[ruiNextIndex] == L'\n')
		{
			++ruiNextIndex;
			rfNextOffset = 0;
			ruiLineOffset = uiStartIndex;
			ruiLineCharsCount = (ruiNextIndex - 1) - uiStartIndex;
			return false;
		}
		if (isblank(rsAllText[ruiNextIndex]))
			iLastSpaceIndex = ruiNextIndex;
		rfNextOffset += GetFont().getGlyph(rsAllText[ruiNextIndex], GetFontSize(), (rText.getStyle() & sf::Text::Bold), rText.getOutlineThickness()).advance;
		if (m_bWordsWrap && (rfNextOffset > GetWorkingSize().x) && GetSize().x)
		{
			if (iLastSpaceIndex >= 0)
			{
				if (iLastSpaceIndex != ruiNextIndex)
					ruiNextIndex = iLastSpaceIndex + 1;
			}
			else if (!uiStartIndex && (fStartOffset != 0.f))
			{
				ruiNextIndex = uiStartIndex;
			}
			rfNextOffset = 0;
			ruiLineOffset = uiStartIndex;
			ruiLineCharsCount = ruiNextIndex  - uiStartIndex;
			return false;
		}
	}
	ruiNextIndex = (sf::Uint32)rsAllText.size();
	ruiLineOffset = uiStartIndex;
	ruiLineCharsCount = ruiNextIndex - uiStartIndex;
	return true;
}

sf::Vector2f CWTextBox::CalculateTextBoundingSize()
{
	sf::Text Text(L"fy", GetFont(), GetFontSize());
	sf::Vector2f size;
	float fNextOffset = 0;
	for (auto& chunk : m_lstChunks)
	{
		sf::Uint32 uiNextIndex = 0;
		bool bEndText = false;
		while (!bEndText)
		{
			sf::Uint32 uiLineOffset = 0;
			float fLineWidth = 0;
			sf::Uint32 uiLineCharsCount = 0;
			bEndText = CalculateSuitableLine(Text, chunk.text, uiLineOffset, uiLineCharsCount, fLineWidth, uiNextIndex, fNextOffset);
			if (fLineWidth > size.x)
				size.x = fLineWidth;
			if (!bEndText) // New line
				size.y += Text.getLocalBounds().top + Text.getLocalBounds().height;
		}
	}
	size.y += Text.getLocalBounds().top + Text.getLocalBounds().height; // For last line
	return size;
}

void CWTextBox::DrawText()
{
	sf::Text Text(L"", GetFont(), GetFontSize());
	Text.setPosition(GetAbsoluteWorkingPosition());
	Text.move(sf::Vector2f(0, m_fVScrollOffset));
	float fNextOffset = 0;
	for (auto& chunk : m_lstChunks)
	{
		Text.setFillColor((chunk.color == sf::Color(0, 0, 0, 0)) ? GetTextColor() : chunk.color);
		sf::Uint32 uiNextIndex = 0;
		bool bEndText = false;
		while (!bEndText)
		{
			Text.setPosition(sf::Vector2f(GetAbsoluteWorkingPosition().x + m_fHScrollOffset + fNextOffset, Text.getPosition().y));
			sf::Uint32 uiLineOffset = 0;
			sf::Uint32 uiLineCharsCount = 0;
			float fLineWidth = 0;
			bEndText = CalculateSuitableLine(Text, chunk.text, uiLineOffset, uiLineCharsCount, fLineWidth, uiNextIndex, fNextOffset);
			Text.setString(chunk.text.substr(uiLineOffset, uiLineCharsCount));
			sf::FloatRect rcIntersect = CScene::GetIntersect(Text.getGlobalBounds(), GetAbsoluteWorkingRect());
			if (rcIntersect.width || rcIntersect.height)
				GetRenderTarget()->draw(Text);
			if (!bEndText)// New line
			{
				sf::FloatRect rcText = sf::Text(L"fy", GetFont(), GetFontSize()).getLocalBounds();
				Text.move(sf::Vector2f(0, (rcText.top + rcText.height)));
			}
		}
	}
}
