#pragma once
#include <memory>
#include <functional>
#include "Widget.h"


enum eScrollBarType
{
	eScrollBarType_Vertical,
	eScrollBarType_Horizontal,
};

class CWScrollBar :
	public CWidget
{
public:
	CWScrollBar(CScene& rScene, eScrollBarType eType);
	virtual ~CWScrollBar();

	void SetType(eScrollBarType eType) { m_eType = eType; }
	eScrollBarType GetType() const { return m_eType; }

	void SetVeiwAmount(float fAmount) { m_fViewAmount = fAmount; }
	float GetViewAmount() const { return m_fViewAmount; }

	// From up/left (0.0f) to down/right (1.0f) view position
	void SetVeiwPosition(float fPosition) { (fPosition < 0.0f) ? (m_fViewPosition = 0.0f) : ((fPosition > 1.0f) ? (m_fViewPosition = 1.0f) : (m_fViewPosition = fPosition)); }
	float GetViewPosition() const { return m_fViewPosition; }

	void SetLength(float fLength) { SetSize((GetType() == eScrollBarType_Vertical) ? sf::Vector2f(GetSize().x, fLength) : sf::Vector2f(fLength, GetSize().y)); }
	float GetLength() const { return (GetType() == eScrollBarType_Vertical) ? GetSize().y : GetSize().x; }

	void SetWidth(float fWidth) { SetSize((GetType() == eScrollBarType_Vertical) ? sf::Vector2f(fWidth, GetSize().y) : sf::Vector2f(GetSize().x, fWidth)); }
	float GetWidth() const { return (GetType() == eScrollBarType_Vertical) ? GetSize().x : GetSize().y; }

	void StickTo(CWidget* pWidget, __in_opt CWScrollBar* pSecondScrollBar);
	void UnstickFrom(CWidget* pWidget, __in_opt CWScrollBar* pSecondScrollBar);

	void SetOnScrollHandler(const std::function<void(float/*VeiwPosition*/)>& rHandler) { m_OnScrollHandler = rHandler; }

	virtual void NormalizeSize();
	virtual void Draw();

	virtual void OnMouseWheelScrolled(
		sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
		float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
		int          x,			//< X position of the mouse pointer, relative to the left of the owner window
		int          y			//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonPressed(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseButtonReleased(
		sf::Mouse::Button button,	//< Code of the button that has been pressed
		int x,						//< X position of the mouse pointer, relative to the left of the owner window
		int y						//< Y position of the mouse pointer, relative to the top of the owner window
	);
	virtual void OnMouseMoved(
		int x,	//< X position of the mouse pointer, relative to the left of the owner window
		int y	//< Y position of the mouse pointer, relative to the top of the owner window
	);

private:
	eScrollBarType m_eType;
	float m_fViewAmount;
	float m_fViewPosition;
	float m_fMinSliderLength;
	bool m_bUpLeftButtonPressed;
	bool m_bDownRightButtonPressed;
	bool m_bUpLeftScrollAreaPressed;
	bool m_bDownRightScrollAreaPressed;
	float m_fScrollAreaPressedViewPosition;
	bool m_bSliderPressed;
	float m_fMouseScrollAreaWidth;
	sf::Vector2f m_v2fSliderPressedMousePos;
	float m_fSliderPressedSliderPos;
	sf::Color m_clrBorderAndButtons;
	sf::Color m_clrArrowsUp;
	sf::Color m_clrArrowsDown;
	sf::Color m_clrScrollArea;
	sf::Color m_clrSliderUp;
	sf::Color m_clrSliderDown;
	float m_fSmallStep;
	float m_fBigStep;
	float m_fButtonScrollSpeed;
	sf::Clock m_Clock;
	std::function<void(float/*VeiwPosition*/)> m_OnScrollHandler;

private:
	float GetButtonsLength() { return ((m_eType == eScrollBarType_Vertical) ? GetSize().x : GetSize().y); }
	sf::FloatRect GetUpLeftButtonAbsoluteRect();
	sf::FloatRect GetDownRightButtonAbsoluteRect();
	float GetScrollAreaLength() { return (((m_eType == eScrollBarType_Vertical) ? GetSize().y : GetSize().x) - (GetButtonsLength() * 2)); }
	sf::FloatRect GetScrollAreaAbsoluteRect();
	sf::FloatRect GetUpLeftScrollAreaAbsoluteRect();
	sf::FloatRect GetDownRightScrollAreaAbsoluteRect();
	float GetSliderLength();
	float GetSliderScrollPosition() { return (GetScrollAreaLength() - GetSliderLength()) * m_fViewPosition; }
	float NormalizeViewPosition(float fViewPos) { return ((fViewPos < 0.0f) ? 0.0f : ((fViewPos > 1.0f) ? 1.0f : fViewPos)); }
	float CalculateViewPosition(float fSliderScrollPos);
	sf::FloatRect GetSliderAbsoluteRect();
	sf::FloatRect GetScrollMouseAreaAbsoluteRect();
};
