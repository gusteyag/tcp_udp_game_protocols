#include "WListBox.h"
#include "WScrollBar.h"


CWListBox::CWListBox(CScene& rScene)
	: CWidget(rScene)
	, m_fHeaderHeight(20)
	, m_fLineHeight(20)
	, m_v2fItemPaddings(3, 1)
	, m_fStartVScrollVeiwPos(1.f)
	, m_fStartHScrollVeiwPos(0)
	, m_fVScrollOffset(0)
	, m_fHScrollOffset(0)
	, m_clrHeaderText(0, 0, 0)
	, m_clrHeaderArea(200, 150, 150)
	, m_clrListBorders(150, 0, 0)
	, m_iSelectedLineIndex(-1)
	, m_clrSelectedLineBackground(100, 50, 50)
{
	SetFontSize(12);
	SetTextColor(sf::Color(200, 200, 200));
	SetBorderWidth(-1.f);
	SetPadding(0);
	SetBorderColor(sf::Color(255, 0, 0));
}

CWListBox::~CWListBox()
{
}

void CWListBox::SetVScrollVeiwPos(float fPosition)
{
	if (!m_pVScrollBar.get())
		return;
	m_pVScrollBar->SetVeiwPosition(fPosition);
	m_fVScrollOffset = (GetListWorkingRect().height - m_v2fListSize.y) * m_pVScrollBar->GetViewPosition();
}

void CWListBox::SetHScrollVeiwPos(float fPosition)
{
	if (!m_pHScrollBar.get())
		return;
	m_pHScrollBar->SetVeiwPosition(fPosition);
	m_fHScrollOffset = (GetListWorkingRect().width - m_v2fListSize.x) * m_pHScrollBar->GetViewPosition();
}

sf::Int32 CWListBox::GetSelectedLine()
{
	if ((m_iSelectedLineIndex < 0) || m_iSelectedLineIndex >= m_lstLines.size())
		return -1;
	listbox_lines_t::iterator itLine = m_lstLines.begin();
	for (int i = 0; (i < m_lstLines.size()) && (itLine != m_lstLines.end()); ++i, ++itLine)
	{
		if (i == m_iSelectedLineIndex)
			return m_iSelectedLineIndex;
	}
	return -1;
}

listbox_items_t* CWListBox::GetSelectedLine(__out_opt listbox_items_t** pplstItems)
{
	if ((m_iSelectedLineIndex < 0) || m_iSelectedLineIndex >= m_lstLines.size())
	{
		if (pplstItems)
			(*pplstItems) = NULL;
		return NULL;
	}
	listbox_lines_t::iterator itLine = m_lstLines.begin();
	for (int i = 0; (i < m_lstLines.size()) && (itLine != m_lstLines.end()); ++i, ++itLine)
	{
		if (i == m_iSelectedLineIndex)
		{
			if (pplstItems)
				(*pplstItems) = &(*itLine);
			return &(*itLine);
		}
	}
	if (pplstItems)
		(*pplstItems) = NULL;
	return NULL;
}

void CWListBox::MakeSelectionVisible()
{
	if ((m_iSelectedLineIndex < 0) || (m_iSelectedLineIndex >= m_lstLines.size()))
		return;
	if (!m_pVScrollBar.get())
		return;
	float fVOffset = GetListWorkingRect().top + m_fVScrollOffset + m_iSelectedLineIndex * m_fLineHeight;
	if (fVOffset < GetListWorkingRect().top)
	{
		m_fVScrollOffset += GetListWorkingRect().top - fVOffset;
	}
	else if ((fVOffset + m_fLineHeight) > (GetListWorkingRect().top + GetListWorkingRect().height))
	{
		m_fVScrollOffset -= (fVOffset + m_fLineHeight) - (GetListWorkingRect().top + GetListWorkingRect().height);
	}
	m_pVScrollBar->SetVeiwPosition(m_fVScrollOffset / (GetListWorkingRect().height - m_v2fListSize.y));
}

void CWListBox::NormalizeSize()
{
	float fWidth = (GetInBorderWidth() + GetPadding()) * 2;
	for (auto& column : m_lstColumns)
		fWidth += column.width;
	float fHeight = (GetInBorderWidth() + GetPadding()) * 2 + m_fHeaderHeight + m_fLineHeight * m_lstLines.size();
	SetSize(sf::Vector2f(fWidth, fHeight));
}

void CWListBox::Draw()
{
	if (!GetRenderTarget())
		return;

	sf::RectangleShape shape(GetSize());
	shape.setPosition(GetAbsolutePosition());
	shape.setFillColor(GetFillColor());
	GetRenderTarget()->draw(shape);


	m_v2fListSize = CalculateListBoundingSize();

	bool bSomeChanged = false;
	if (m_v2fListSize.y > GetAbsoluteListWorkingRect().height)
	{
		if (!m_pVScrollBar.get())
		{
			m_pVScrollBar.reset(new CWScrollBar(GetScene(), eScrollBarType_Vertical));
			m_pVScrollBar->StickTo(this, m_pHScrollBar.get());
			m_pVScrollBar->SetRenderTarget(GetRenderTarget());
			SetVScrollVeiwPos(m_fStartVScrollVeiwPos);
			m_pVScrollBar->SetOnScrollHandler([this](float fVeiwPosition) { SetVScrollVeiwPos(fVeiwPosition); });
			bSomeChanged = true;
		}
	}
	else
	{
		if (m_pVScrollBar.get())
		{
			m_pVScrollBar->UnstickFrom(this, m_pHScrollBar.get());
			m_pVScrollBar.reset(NULL);
			bSomeChanged = true;
		}
		m_fVScrollOffset = 0;
	}

	if (bSomeChanged)
		m_v2fListSize = CalculateListBoundingSize();

	bSomeChanged = false;
	if (m_v2fListSize.x > GetAbsoluteListWorkingRect().width)
	{
		if (!m_pHScrollBar.get())
		{
			m_pHScrollBar.reset(new CWScrollBar(GetScene(), eScrollBarType_Horizontal));
			m_pHScrollBar->StickTo(this, m_pVScrollBar.get());
			m_pHScrollBar->SetRenderTarget(GetRenderTarget());
			SetHScrollVeiwPos(m_fStartHScrollVeiwPos);
			m_pHScrollBar->SetOnScrollHandler([this](float fVeiwPosition) { SetHScrollVeiwPos(fVeiwPosition); });
			bSomeChanged = true;
		}
	}
	else
	{
		if (m_pHScrollBar.get())
		{
			m_pHScrollBar->UnstickFrom(this, m_pVScrollBar.get());
			m_pHScrollBar.reset(NULL);
			bSomeChanged = true;
		}
		m_fHScrollOffset = 0;
	}

	if (bSomeChanged)
		m_v2fListSize = CalculateListBoundingSize();

	if (m_pVScrollBar.get())
		m_pVScrollBar->SetVeiwAmount(GetAbsoluteListWorkingRect().height / m_v2fListSize.y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->SetVeiwAmount(GetAbsoluteListWorkingRect().width / m_v2fListSize.x);

	if (m_pVScrollBar.get())
		SetVScrollVeiwPos(m_pVScrollBar->GetViewPosition());
	if (m_pHScrollBar.get())
		SetHScrollVeiwPos(m_pHScrollBar->GetViewPosition());

	CWText text(GetScene());
	text.SetRenderTarget(GetRenderTarget());
	text.SetFont(GetFont());
	text.SetFontSize(GetFontSize());
	text.SetPadding(0);
	text.SetWidgetVsTextSpacing(m_v2fItemPaddings.x);
	text.SetWordsWrap(false);


	if (m_fHeaderHeight > 0)
	{
		GetScene().PushClipping(sf::FloatRect(GetAbsoluteWorkingRect().left, GetAbsoluteWorkingRect().top, GetAbsoluteWorkingRect().width, m_fHeaderHeight));
		text.SetBorderColor(m_clrListBorders);
		text.SetBorderWidth(-1);
		text.SetTextColor(m_clrHeaderText);
		text.SetFillColor(m_clrHeaderArea);
		text.SetTextHAlignment(eHAlignment_Center);
		text.SetTextVAlignment(eVAlignment_Center);
		text.SetTextStyle(sf::Text::Regular);
		float fXOffset = m_fHScrollOffset;
		for (auto& column : m_lstColumns)
		{
			sf::FloatRect rcColumn(
				GetAbsoluteWorkingRect().left + fXOffset,
				GetAbsoluteWorkingRect().top,
				column.width,
				m_fHeaderHeight
			);
			sf::FloatRect rcColumnIntersect = CScene::GetIntersect(rcColumn, GetAbsoluteWorkingRect());
			if (rcColumnIntersect.width && rcColumnIntersect.height)
			{
				text.SetText(column.text);
				text.SetSize(sf::Vector2f(rcColumn.width, rcColumn.height));
				text.SetPosition(sf::Vector2f(rcColumn.left, rcColumn.top));
				text.Draw();
			}
			fXOffset += column.width;
		}
		GetScene().PopClipping();
	}


	GetScene().PushClipping(GetAbsoluteListWorkingRect());
	sf::FloatRect rcListWorking(0, 0, GetAbsoluteListWorkingRect().width, GetAbsoluteListWorkingRect().height);
	if ((m_iSelectedLineIndex >= 0) && (m_iSelectedLineIndex < m_lstLines.size()))
	{
		sf::FloatRect rcLine(
			GetAbsoluteListWorkingRect().left + m_fHScrollOffset,
			GetAbsoluteListWorkingRect().top + m_fVScrollOffset + m_iSelectedLineIndex * m_fLineHeight,
			m_v2fListSize.x,
			m_fLineHeight
		);
		rcLine = CScene::GetIntersect(rcLine, GetAbsoluteListWorkingRect());
		if (rcLine.width && rcLine.height)
		{
			sf::RectangleShape lineBackground(sf::Vector2f(rcLine.width, rcLine.height));
			lineBackground.setFillColor(m_clrSelectedLineBackground);
			lineBackground.setPosition(sf::Vector2f(rcLine.left, rcLine.top));
			GetRenderTarget()->draw(lineBackground);
		}
	}
	text.SetBorderWidth(0);
	text.SetTextColor(GetTextColor());
	text.SetFillColor(GetFillColor());
	text.SetTextHAlignment(eHAlignment_Left);
	text.SetTextVAlignment(eVAlignment_Center);
	text.SetTextStyle(sf::Text::Regular);
	float fYOffset = m_fVScrollOffset;
	for (auto& line : m_lstLines)
	{
		float fXOffset = m_fHScrollOffset;
		listbox_items_t::iterator itItem = line.begin();
		listbox_columns_t::iterator itColumn = m_lstColumns.begin();
		for (; ((itItem != line.end()) && (itColumn != m_lstColumns.end())); ++itItem, ++itColumn)
		{
			if (itItem->widget.get() || itItem->text.size())
			{
				sf::FloatRect rcText(
					GetAbsoluteListWorkingRect().left + m_v2fItemPaddings.x + fXOffset,
					GetAbsoluteListWorkingRect().top + m_v2fItemPaddings.y + fYOffset,
					itColumn->width - m_v2fItemPaddings.x * 2,
					m_fLineHeight - m_v2fItemPaddings.y * 2
				);
				sf::FloatRect rcTextIntersect = CScene::GetIntersect(rcText, GetAbsoluteListWorkingRect());
				if (rcTextIntersect.width && rcTextIntersect.height)
				{
					text.SetWidget(itItem->widget.get());
					text.SetText(itItem->text);
					text.SetSize(sf::Vector2f(rcText.width, rcText.height));
					text.SetPosition(sf::Vector2f(rcText.left, rcText.top));
					text.Draw();
					text.ReleaseWidget();
				}
			}
			fXOffset += itColumn->width;
		}
		fYOffset += m_fLineHeight;
	}
	sf::RectangleShape VLine;
	VLine.setOutlineThickness(-1);
	VLine.setOutlineColor(m_clrListBorders);
	VLine.setFillColor(sf::Color(0, 0, 0, 0));
	sf::RectangleShape HLine;
	HLine.setOutlineThickness(-1);
	HLine.setOutlineColor(m_clrListBorders);
	HLine.setFillColor(sf::Color(0, 0, 0, 0));
	fYOffset = m_fVScrollOffset;
	for (auto& line : m_lstLines)
	{
		float fXOffset = m_fHScrollOffset;

		listbox_columns_t::iterator itColumn = m_lstColumns.begin();
		for (; itColumn != m_lstColumns.end(); ++itColumn)
		{
			fXOffset += itColumn->width;

			sf::FloatRect rcVLine(GetAbsoluteListWorkingRect().left + fXOffset, GetAbsoluteListWorkingRect().top + m_fVScrollOffset, 1, m_v2fListSize.y);
			rcVLine = CScene::GetIntersect(rcVLine, GetAbsoluteListWorkingRect());
			if (rcVLine.width && rcVLine.height)
			{
				VLine.setSize(sf::Vector2f(rcVLine.width, rcVLine.height));
				VLine.setPosition(sf::Vector2f(rcVLine.left, rcVLine.top));
				GetRenderTarget()->draw(VLine);
			}
		}

		fYOffset += m_fLineHeight;

		sf::FloatRect rcHLine(GetAbsoluteListWorkingRect().left + m_fHScrollOffset, GetAbsoluteListWorkingRect().top + fYOffset, m_v2fListSize.x, 1);
		rcHLine = CScene::GetIntersect(rcHLine, GetAbsoluteListWorkingRect());
		if (rcHLine.width && rcHLine.height)
		{
			HLine.setSize(sf::Vector2f(rcHLine.width, rcHLine.height));
			HLine.setPosition(sf::Vector2f(rcHLine.left, rcHLine.top));
			GetRenderTarget()->draw(HLine);
		}
	}
	GetScene().PopClipping();
	

	GetScene().PushClipping(GetAbsoluteWorkingRect());

	__super::Draw();

	GetScene().PopClipping();

	if (m_pVScrollBar.get())
		m_pVScrollBar->Draw();
	if (m_pHScrollBar.get())
		m_pHScrollBar->Draw();

	sf::RectangleShape border(GetSize());
	border.setPosition(GetAbsolutePosition());
	border.setOutlineThickness(GetBorderWidth());
	border.setOutlineColor(GetBorderColor());
	border.setFillColor(sf::Color(0, 0, 0, 0));
	GetRenderTarget()->draw(border);
}

void CWListBox::OnTextEntered(sf::Uint32 unicode)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnTextEntered(unicode);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnTextEntered(unicode);

	__super::OnTextEntered(unicode);
}

void CWListBox::OnKeyPressed(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnKeyPressed(code, alt, control, shift, system);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnKeyPressed(code, alt, control, shift, system);

	__super::OnKeyPressed(code, alt, control, shift, system);
}

void CWListBox::OnKeyReleased(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnKeyReleased(code, alt, control, shift, system);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnKeyReleased(code, alt, control, shift, system);

	__super::OnKeyReleased(code, alt, control, shift, system);
}

void CWListBox::OnMouseWheelScrolled(
	sf::Mouse::Wheel wheel,	//< Which wheel (for mice with multiple ones)
	float        delta,		//< Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
	int          x,			//< X position of the mouse pointer, relative to the left of the owner window
	int          y			//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseWheelScrolled(wheel, delta, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseWheelScrolled(wheel, delta, x, y);

	__super::OnMouseWheelScrolled(wheel, delta, x, y);
}

void CWListBox::OnMouseButtonPressed(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseButtonPressed(button, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseButtonPressed(button, x, y);

	__super::OnMouseButtonPressed(button, x, y);

	if (!IsEnabled())
		return;

	if ((button != sf::Mouse::Button::Left) || !GetScene().GetRenderWindow())
		return;

	sf::Vector2f pos = GetScene().GetRenderWindow()->mapPixelToCoords(sf::Vector2i(x, y));
	if (GetAbsoluteListWorkingRect().contains(pos))
	{
		bool bFound = false;
		listbox_lines_t::iterator itLine = m_lstLines.begin();
		for (int i = 0; (i < m_lstLines.size()) && (itLine != m_lstLines.end()); ++i, ++itLine)
		{
			sf::FloatRect rcLine(
				GetAbsoluteListWorkingRect().left + m_fHScrollOffset,
				GetAbsoluteListWorkingRect().top + m_fVScrollOffset + i * m_fLineHeight,
				m_v2fListSize.x,
				m_fLineHeight
			);
			if (rcLine.contains(pos))
			{
				bFound = true;
				m_iSelectedLineIndex = i;
				if (m_OnLineSelectedHandler)
					m_OnLineSelectedHandler(m_iSelectedLineIndex, (*itLine));
				break;
			}
		}
		if (!bFound && ((m_iSelectedLineIndex >= 0) && (m_iSelectedLineIndex < m_lstLines.size())))
		{
			m_iSelectedLineIndex = -1;
			if (m_OnLineUnselectedHandler)
				m_OnLineUnselectedHandler();
		}
	}
}

void CWListBox::OnMouseButtonReleased(
	sf::Mouse::Button button,	//< Code of the button that has been pressed
	int x,						//< X position of the mouse pointer, relative to the left of the owner window
	int y						//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseButtonReleased(button, x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseButtonReleased(button, x, y);

	__super::OnMouseButtonReleased(button, x, y);
}

void CWListBox::OnMouseMoved(
	int x,	//< X position of the mouse pointer, relative to the left of the owner window
	int y	//< Y position of the mouse pointer, relative to the top of the owner window
)
{
	if (m_pVScrollBar.get())
		m_pVScrollBar->OnMouseMoved(x, y);
	if (m_pHScrollBar.get())
		m_pHScrollBar->OnMouseMoved(x, y);

	__super::OnMouseMoved(x, y);
}

sf::FloatRect CWListBox::GetListWorkingRect()
{
	float fHeight = GetWorkingSize().y - m_fHeaderHeight;
	if (fHeight < 0)
		fHeight = 0;
	return sf::FloatRect(0, m_fHeaderHeight, GetWorkingSize().x, fHeight);
}

sf::FloatRect CWListBox::GetAbsoluteListWorkingRect()
{
	sf::FloatRect rc = GetListWorkingRect();
	return sf::FloatRect(GetAbsoluteWorkingPosition().x + rc.left, GetAbsoluteWorkingPosition().y + rc.top, rc.width, rc.height);
}

sf::Vector2f CWListBox::CalculateListBoundingSize()
{

	float fHeight = (float)m_lstLines.size() * m_fLineHeight;
	float fWidth = 0;
	for (auto& column : m_lstColumns)
		fWidth += column.width;
	return sf::Vector2f(fWidth, fHeight);
}
