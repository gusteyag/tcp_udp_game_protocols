#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <map>
#include <set>
#include "MovingObj.h"
#include "GameDefinitions.h"


class CPlayer;

class CPlayerInfo : public CMovingObj
{
public:
	CPlayerInfo()
		: m_nPlayerId(0)
		, m_nLives(PLAYERS_NUMBER_OF_LIVES)
		, m_nLastMotionId(0)
	{}
	CPlayerInfo(sf::Uint32 nObjectId, sf::Uint32 nPlayerId, const std::wstring& rsName)
		: CMovingObj(eMovingObjType_Player, nObjectId)
		, m_nPlayerId(nPlayerId)
		, m_sPlayerName(rsName)
		, m_nLives(PLAYERS_NUMBER_OF_LIVES)
		, m_nLastMotionId(0)
	{}
	virtual ~CPlayerInfo() {}

	sf::Uint32 GetPlayerId() const { return m_nPlayerId; }
	const std::wstring& GetPlayerName() const { return m_sPlayerName; }

	sf::Uint32 GetLeaves() const { return m_nLives; }
	void DecrementLives() { if(--m_nLives < 0) m_nLives = 0; }

	sf::Uint32 GetLastMotionId() const { return m_nLastMotionId; }
	void SetLastMotionId(sf::Uint32 nLastMotionId) { m_nLastMotionId = nLastMotionId; }

	bool Serialize(sf::Packet& rPacket) const
	{
		if (!__super::Serialize(rPacket))
			return false;
		rPacket << (sf::Uint32)m_nPlayerId;
		rPacket << m_sPlayerName;
		rPacket << (sf::Uint32)m_nLives;
		rPacket << (sf::Uint32)m_nLastMotionId;
		return true;
	}

	bool Deserialize(sf::Packet& rPacket)
	{
		if (!__super::Deserialize(rPacket))
			return false;
		sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
		m_nPlayerId = ui32Data;
		rPacket >> m_sPlayerName;
		ui32Data = 0; rPacket >> ui32Data;
		m_nLives = ui32Data;
		ui32Data = 0; rPacket >> ui32Data;
		m_nLastMotionId = ui32Data;
		return true;
	}

private:
	sf::Uint32 m_nPlayerId;
	std::wstring m_sPlayerName;
	sf::Uint32 m_nLives;
	sf::Uint32 m_nLastMotionId;
};
