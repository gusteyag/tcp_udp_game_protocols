#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>


class MotionUnit
{
public:
	sf::Uint32 obj_id;
	sf::Uint32 id;
	sf::Vector2f diff;
	sf::Vector2f pos;

	MotionUnit()
		: obj_id(0)
		, id(0)
		, diff(0, 0)
		, pos(0, 0)
	{}

	bool Serialize(sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint32)obj_id;
		rPacket << (sf::Uint32)id;
		rPacket << (float)diff.x;
		rPacket << (float)diff.y;
		rPacket << (float)pos.x;
		rPacket << (float)pos.y;
		return true;
	}

	bool Deserialize(sf::Packet& rPacket)
	{

		sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
		obj_id = ui32Data;
		ui32Data = 0; rPacket >> ui32Data;
		id = ui32Data;
		float x = 0, y = 0; rPacket >> x >> y;
		diff = sf::Vector2f(x, y);
		x = 0, y = 0; rPacket >> x >> y;
		pos = sf::Vector2f(x, y);
		return true;
	}
};
