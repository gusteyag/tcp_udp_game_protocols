#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <map>
#include "PlayerInfo.h"
#include "..\GameServer\Game.h"


class CGame;

class CGameInfo
{
public:
	CGameInfo() {}
	CGameInfo(const CGame& rGame)
	{
		m_mapPlayers = rGame.GetPlayers();
	}
	virtual ~CGameInfo() {}

	const std::map<sf::Uint32, CPlayerInfo>& GetPlayers() const { return m_mapPlayers; }

	bool Serialize(sf::Packet& rPacket)
	{
		rPacket << (sf::Uint32)m_mapPlayers.size();
		for (auto& player : m_mapPlayers)
		{
			if (!player.second.Serialize(rPacket))
				return false;
		}
		return true;
	}

	bool Deserialize(sf::Packet& rPacket)
	{
		m_mapPlayers.clear();
		sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
		for (sf::Uint32 i = 0; i < ui32Data; ++i)
		{
			CPlayerInfo player;
			if (!player.Deserialize(rPacket))
				return false;
			m_mapPlayers[player.GetPlayerId()] = player;
		}
		return true;
	}

private:
	std::map<sf::Uint32, CPlayerInfo> m_mapPlayers;
};
