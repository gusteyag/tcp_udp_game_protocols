#pragma once
#include "PlayerInfo.h"

class CPlayerState
{
public:
	CPlayerState()
		: m_nPlayerId(0)
		, m_nObjectId(0)
		, m_nLives(0)
		, m_nLastMotionId(0)
	{}
	CPlayerState(const CPlayerInfo& rPlayerInfo)
		: m_nPlayerId(rPlayerInfo.GetPlayerId())
		, m_nObjectId(rPlayerInfo.GetObjectId())
		, m_nLives(rPlayerInfo.GetLeaves())
		, m_nLastMotionId(rPlayerInfo.GetLastMotionId())
	{}

	virtual ~CPlayerState() {}

	sf::Uint32 GetPlayerId() const { return m_nPlayerId; }
	sf::Uint32 GetObjectId() const { return m_nObjectId; }
	sf::Uint32 GetLeaves() const { return m_nLives; }
	sf::Uint32 GetLastMotionId() const { return m_nLastMotionId; }

	bool Serialize(sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint32)m_nPlayerId;
		rPacket << (sf::Uint32)m_nObjectId;
		rPacket << (sf::Uint32)m_nLives;
		rPacket << (sf::Uint32)m_nLastMotionId;
		return true;
	}

	bool Deserialize(sf::Packet& rPacket)
	{
		sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
		m_nPlayerId = ui32Data;
		ui32Data = 0; rPacket >> ui32Data;
		m_nObjectId = ui32Data;
		ui32Data = 0; rPacket >> ui32Data;
		m_nLives = ui32Data;
		ui32Data = 0; rPacket >> ui32Data;
		m_nLastMotionId = ui32Data;
		return true;
	}

private:
	sf::Uint32 m_nPlayerId;
	sf::Uint32 m_nObjectId;
	sf::Uint32 m_nLives;
	sf::Uint32 m_nLastMotionId;
};

