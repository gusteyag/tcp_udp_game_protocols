#pragma once
#include "MovingObj.h"
#include "GameDefinitions.h"

class CShot :
	public CMovingObj
{
public:
	CShot()
		: m_eDirection(ePlayerShotDir_None)
		, m_nPlayerId(0)
	{
		m_clk.restart();
	}
	CShot(sf::Uint32 nObjectId, sf::Uint32 nPlayerId, ePlayerShotDir eDirection)
		: CMovingObj(eMovingObjType_Shot, nObjectId)
		, m_eDirection(eDirection)
		, m_nPlayerId(nPlayerId)
	{
		m_clk.restart();
	}
	virtual ~CShot() {}

	ePlayerShotDir GetDirection() const { return m_eDirection; }
	sf::Uint32 GetPlayerId() const { return m_nPlayerId; }

	void CalculatePosition()
	{
		switch (m_eDirection)
		{
		case ePlayerShotDir_Left:
		{
			sf::Vector2f pos = GetPos();
			pos.x -= m_clk.getElapsedTime().asMilliseconds() * GetVelocity() / 1000.f;
			SetPos(pos);
		}
		break;

		case ePlayerShotDir_Right:
		{
			sf::Vector2f pos = GetPos();
			pos.x += m_clk.getElapsedTime().asMilliseconds() * GetVelocity() / 1000.f;
			SetPos(pos);
		}
		break;

		case ePlayerShotDir_Up:
		{
			sf::Vector2f pos = GetPos();
			pos.y -= m_clk.getElapsedTime().asMilliseconds() * GetVelocity() / 1000.f;
			SetPos(pos);
		}
		break;

		case ePlayerShotDir_Down:
		{
			sf::Vector2f pos = GetPos();
			pos.y += m_clk.getElapsedTime().asMilliseconds() * GetVelocity() / 1000.f;
			SetPos(pos);
		}
		break;
		}

		m_clk.restart();
	}

private:
	ePlayerShotDir m_eDirection;
	sf::Uint32 m_nPlayerId;
	sf::Clock m_clk;
};

