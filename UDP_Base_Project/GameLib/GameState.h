#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <map>
#include "MovingObj.h"
#include "PlayerState.h"
#include "..\GameServer\Game.h"

class CGameState
{
public:
	CGameState()
	{}
	CGameState(const CGame& rGame)
	{
		for (auto& player : rGame.GetPlayers())
		{
			m_mapPlayers[player.second.GetPlayerId()] = CPlayerState(player.second);
			m_mapMovingObjects[player.second.GetObjectId()] = CMovingObj(player.second);
		}
		for (auto& shot : rGame.GetShots())
			m_mapMovingObjects[shot.second.GetObjectId()] = CMovingObj(shot.second);
	}
	virtual ~CGameState() {}

	const std::map<sf::Uint32, CPlayerState>& GetPlayers() const { return m_mapPlayers; }
	const std::map<sf::Uint32, CMovingObj>& GetMovingObjects() const { return m_mapMovingObjects; }

	bool Serialize(sf::Packet& rPacket);

	bool Deserialize(sf::Packet& rPacket);

private:
	std::map<sf::Uint32, CPlayerState> m_mapPlayers;
	std::map<sf::Uint32, CMovingObj> m_mapMovingObjects;
};

