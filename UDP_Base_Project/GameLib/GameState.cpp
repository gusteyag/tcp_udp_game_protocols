#include "PlayerInfo.h"
#include "GameState.h"

bool CGameState::Serialize(sf::Packet& rPacket)
{
	rPacket << (sf::Uint32)m_mapPlayers.size();
	for (auto& player : m_mapPlayers)
	{
		if (!player.second.Serialize(rPacket))
			return false;
	}
	rPacket << (sf::Uint32)m_mapMovingObjects.size();
	for (auto& obj : m_mapMovingObjects)
	{
		if (!obj.second.Serialize(rPacket))
			return false;
	}
	return true;
}

bool CGameState::Deserialize(sf::Packet& rPacket)
{
	m_mapPlayers.clear();
	sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
	for (sf::Uint32 i = 0; i < ui32Data; ++i)
	{
		CPlayerState player;
		if (!player.Deserialize(rPacket))
			return false;
		m_mapPlayers[player.GetPlayerId()] = player;
	}
	m_mapMovingObjects.clear();
	ui32Data = 0; rPacket >> ui32Data;
	for (sf::Uint32 i = 0; i < ui32Data; ++i)
	{
		CMovingObj obj;
		if (!obj.Deserialize(rPacket))
			return false;
		m_mapMovingObjects[obj.GetObjectId()] = obj;
	}
	return true;
}