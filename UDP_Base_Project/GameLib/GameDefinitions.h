#pragma once
#include <windows.h>
#include <map>
#include <list>
#include <SFML/Graphics.hpp>

#define PLAYERS_COUNT_MIN			3
#define PLAYERS_COUNT_MAX			6
#define PLAYERS_NUMBER_OF_LIVES		1
#define PLAYERS_WIDTH				0.05f
#define PLAYERS_HEIGHT				0.05f
#define PLAYERS_VELOCITY			0.3f 
#define SHOT_WIDTH					0.01f
#define SHOT_HEIGHT					0.01f
#define SHOT_VELOCITY				0.5f 

enum eResult
{
	eResult_Success,
	eResult_Fail,

	eResult_InvalidParameter,
	eResult_AlreadyExists,
	eResult_InappropriateGameState,
	eResult_ExcessAmount,
	eResult_NameAlreadyExists,
	eResult_NotFound,
	eResult_GameNameAlreadyExists,
	eResult_CharacterIsBusy,
	eResult_PositionIsBusy,
	eResult_InvalidPlayerId,
	eResult_InappropriatePlayerState,
	eResult_InvalidPlayerTrack,
	eResult_PlayerHasNoMoves,
	eResult_PlayerNotInGame,
	eResult_NotReady,
	eResult_NotImplemented,
};

enum ePlayerShotDir
{
	ePlayerShotDir_None,
	ePlayerShotDir_Left,
	ePlayerShotDir_Right,
	ePlayerShotDir_Up,
	ePlayerShotDir_Down,
};
