#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>


enum eMovingObjType
{
	eMovingObjType_None,
	eMovingObjType_Player,
	eMovingObjType_Shot,
};

class CMovingObj
{
public:
	CMovingObj()
		: m_type(eMovingObjType_None)
		, m_nId(0)
	{}
	CMovingObj(eMovingObjType type, sf::Uint32 nId)
		: m_type(type)
		, m_nId(nId)
	{}
	CMovingObj(eMovingObjType type, sf::Uint32 nId, const sf::Vector2f& size, const sf::Vector2f& pos, float velocity)
		: m_type(type)
		, m_nId(nId)
		, m_size(size)
		, m_pos(pos)
		, m_velocity(velocity)
	{}
	virtual ~CMovingObj() {}

	eMovingObjType GetObjectType() const { return m_type; }

	sf::Uint32 GetObjectId() const { return m_nId; }

	void SetSize(const sf::Vector2f& size) { m_size = size; }
	const sf::Vector2f& GetSize() const { return m_size; }

	void SetPos(const sf::Vector2f& pos) { m_pos = pos; }
	const sf::Vector2f& GetPos() const { return m_pos; }

	void SetVelocity(float velocity) { m_velocity = velocity; }
	float GetVelocity() const { return m_velocity; }

	bool Serialize(sf::Packet& rPacket) const
	{
		rPacket << (sf::Uint8)m_type;
		rPacket << (sf::Uint32)m_nId;
		rPacket << (float)m_size.x;
		rPacket << (float)m_size.y;
		rPacket << (float)m_pos.x;
		rPacket << (float)m_pos.y;
		rPacket << (float)m_velocity;
		return true;
	}

	bool Deserialize(sf::Packet& rPacket)
	{
		sf::Uint8 ui8Data = 0; rPacket >> ui8Data;
		m_type = (eMovingObjType)ui8Data;
		sf::Uint32 ui32Data = 0; rPacket >> ui32Data;
		m_nId = ui32Data;
		float x = 0, y = 0; rPacket >> x >> y;
		m_size = sf::Vector2f(x, y);
		x = 0, y = 0; rPacket >> x >> y;
		m_pos = sf::Vector2f(x, y);
		rPacket >> m_velocity;
		return true;
	}

private:
	eMovingObjType m_type;
	sf::Uint32 m_nId;
	sf::Vector2f m_size;
	sf::Vector2f m_pos;
	float m_velocity;
};

