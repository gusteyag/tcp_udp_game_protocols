#pragma once
#include <Windows.h>
#include <list>
#include <memory>
#include <string>
#include <functional>
#include <SFML\Network.hpp>
#include "GameDefinitions.h"
#include "../GameServer/Protocol.h"
#include "UdpOutConnection.h"

class CGameInfo;
class CGameState;
class MotionUnit;

class CUdpClient
{
public:
	CUdpClient(const std::wstring& rsServerAddress, USHORT nServerPort);
	virtual ~CUdpClient();

	void Clear();

	virtual bool Connect(
		const std::function<void(void)>& rOnConnectedCB,
		const std::function<void(const std::wstring& rsError)>& rOnDisconnectedCB,
		const std::function<void(const CGameState& rGameState)>& rOnGameStateChangedCB
	);
	virtual void Disconnect();

	virtual bool Process();

	void Request_Play(
		const std::function<void(eResult eRes, sf::Uint32 nPlayerId, const CGameInfo& rGameInfo)>& rOnPlayAnswerCB,
		const std::wstring& rsPlayerName,
		sf::Uint16 uiPlayersCount
	);
	void Request_Quit(const std::function<void(eResult eRes)>& rCallback);

	void Request_PlayerShot(ePlayerShotDir eDirection);

	void Request_PlayerMotion(const MotionUnit& motion);

private:
	bool ProcessProtocol(sf::Packet& rPacket);

private:
	std::wstring m_sServerAddress;
	std::string m_sASCIIServerAddress;
	sf::IpAddress m_ServerIpAddress;
	USHORT m_nServerPort;

	CUdpOutConnection m_Connection;

	std::function<void(eResult eRes, sf::Uint32 nPlayerId, const CGameInfo& rGameInfo)> m_OnPlayAnswerCB;
	std::function<void(eResult eRes)> m_OnQuitAnswerCB;
	std::function<void(const CGameState& rGameState)> m_OnGameStateChangedCB;

	bool m_bOnConnectedCBCalled;
	std::function<void(void)> m_OnConnectedCB;
	std::function<void(const std::wstring& rsError)> m_OnDisconnectedCB;
	std::wstring m_sDisconnectionStatus;


};
