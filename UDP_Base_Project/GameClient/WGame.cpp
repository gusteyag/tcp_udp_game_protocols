#include "WGame.h"
#include "WGameLocation.h"
#include "WButton.h"
#include "WMessageBox.h"
#include "MotionManager.h"
#include "WWinnerDlg.h"
#include "GameInfo.h"
#include <MovingObj.h>


CWGame::CWGame(CScene& rScene)
	: CWContainer(rScene)
	, m_pGameLocation(new CWGameLocation(rScene))
	, m_pButtonQuit(new CWButton(rScene))
	, m_pButtonPrediction(new CWButton(rScene))
	, m_pButtonRecancelation(new CWButton(rScene))
	, m_pButtonInterpolation(new CWButton(rScene))
	, m_pMessageBox(new CWMessageBox(rScene))
	, m_pWinnerDlg(new CWWinnerDlg(rScene))
	, m_nThisPlayerObjectId(0)
	, m_bMovesLeft(false)
	, m_bMovesRight(false)
	, m_bMovesUp(false)
	, m_bMovesDown(false)
{
	AddChild(m_pGameLocation);
	AddChild(m_pButtonQuit);
	AddChild(m_pButtonPrediction);
	AddChild(m_pButtonRecancelation);
	AddChild(m_pButtonInterpolation);
	AddChild(m_pMessageBox);
	AddChild(m_pWinnerDlg);

	m_pMessageBox->SetVisible(false);

	SetBorderWidth(0);
	SetHeaderHeight(0);
	SetPadding(5);
	SetFillColor(sf::Color(128, 128, 128));

	m_pGameLocation->SetPadding(5);

	m_pButtonQuit->SetText(L"Quit");
	m_pButtonPrediction->SetText(L"Prediction");
	m_pButtonRecancelation->SetText(L"Recancelation");
	m_pButtonInterpolation->SetText(L"Interpolation");

	m_pButtonQuit->SetOnClickedHandler([this]() {
		if (m_OnQuitHandler)
			m_OnQuitHandler();
	});

	m_pButtonPrediction->SetStyleCheckBox(true);
	m_pButtonPrediction->SetOnCheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetPrediction(true);
	});
	m_pButtonPrediction->SetOnUncheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetPrediction(false);
		if (m_pMotionManager.get())
			m_pMotionManager->SetRecancelation(false);
		m_pButtonRecancelation->SetChecked(false);
	});

	m_pButtonRecancelation->SetStyleCheckBox(true);
	m_pButtonRecancelation->SetOnCheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetRecancelation(true);
		if (m_pMotionManager.get())
			m_pMotionManager->SetPrediction(true);
		m_pButtonPrediction->SetChecked(true);
	});
	m_pButtonRecancelation->SetOnUncheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetRecancelation(false);
	});

	m_pButtonInterpolation->SetStyleCheckBox(true);
	m_pButtonInterpolation->SetOnCheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetInterpolation(true);
	});
	m_pButtonInterpolation->SetOnUncheckedHandler([this]() {
		if (m_pMotionManager.get())
			m_pMotionManager->SetInterpolation(false);
	});
}

CWGame::~CWGame()
{
}

void CWGame::UpdateGameInfo(sf::Uint32 nPlayerId, const CGameInfo& rGameInfo)
{
	m_GameInfo = rGameInfo;
	m_nThisPlayerObjectId = 0;
	sf::Vector2f v2fThisPlayerPos;
	m_lstMovingObjs.clear();
	for (auto& player : m_GameInfo.GetPlayers())
	{
		m_lstMovingObjs.push_back(player.second);
		if (nPlayerId == player.second.GetPlayerId())
		{
			m_nThisPlayerObjectId = m_lstMovingObjs.back().GetObjectId();
			v2fThisPlayerPos = m_lstMovingObjs.back().GetPos();
		}
	}

	if (!m_pMotionManager.get() && m_nThisPlayerObjectId)
		m_pMotionManager.reset(new CMotionManager(m_nThisPlayerObjectId, v2fThisPlayerPos));
	if (!m_pMotionManager.get())
		return;

	m_pButtonPrediction->SetChecked(m_pMotionManager->GetPrediction());
	m_pButtonRecancelation->SetChecked(m_pMotionManager->GetRecancelation());
	m_pButtonInterpolation->SetChecked(m_pMotionManager->GetInterpolation());

	m_pGameLocation->SetMovingObjList(m_nThisPlayerObjectId, &m_lstMovingObjs);
}

void CWGame::UpdateGameState(const CGameState& rGameState)
{
	m_GameState = rGameState;

	std::map<sf::Uint32, sf::Vector2f> mapPositions;
	for (auto& mobj : m_GameState.GetMovingObjects())
	{
		if (mobj.second.GetObjectId() == m_nThisPlayerObjectId)
		{
			for (auto& player : m_GameState.GetPlayers())
			{
				if (player.second.GetObjectId() == m_nThisPlayerObjectId)
				{
					if (m_pMotionManager.get())
						m_pMotionManager->ConfirmPlayerMotion(player.second.GetLastMotionId(), mobj.second.GetPos());

					if (player.second.GetLeaves())
						m_pWinnerDlg->SetVisible(false);
					else
						m_pWinnerDlg->SetVisible(true);
					break;
				}
			}
		}
		else
		{
			mapPositions[mobj.second.GetObjectId()] = mobj.second.GetPos();
		}
	}

	if (m_pMotionManager.get())
		m_pMotionManager->UpdateObjectPositions(mapPositions);

	// Synchronize objects count
	std::list<CMovingObj>::iterator itMObj = m_lstMovingObjs.begin();
	while (itMObj != m_lstMovingObjs.end())
	{
		if (m_GameState.GetMovingObjects().count(itMObj->GetObjectId()))
			++itMObj;
		else
			itMObj = m_lstMovingObjs.erase(itMObj);
	}
	for (auto& mobj : m_GameState.GetMovingObjects())
	{
		if (!std::count_if(m_lstMovingObjs.begin(), m_lstMovingObjs.end(), [mobj](auto& item) { return (item.GetObjectId() == mobj.second.GetObjectId()); }))
			m_lstMovingObjs.push_back(mobj.second);
	}
}

MotionUnit CWGame::GetPlayerMotion()
{
	if (!m_nThisPlayerObjectId || !m_pMotionManager.get())
		return MotionUnit();
	return m_pMotionManager->GetPlayerMotion();
}

void CWGame::Process()
{
	if (!m_nThisPlayerObjectId || !m_pMotionManager.get())
		return;

	for (auto& mobj : m_lstMovingObjs)
	{
		if (mobj.GetObjectId() == m_nThisPlayerObjectId)
		{
			float velocity = mobj.GetVelocity();
			float diffLeft = 0;
			float diffRight = 0;
			float diffUp = 0;
			float diffDown = 0;
			if (GetScene().GetRenderWindow()->hasFocus() && sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				if (!m_bMovesLeft)
				{
					m_bMovesLeft = true;
					m_clkMotionLeft.restart();
				}
				else
				{
					diffLeft = m_clkMotionLeft.getElapsedTime().asMilliseconds() * velocity / 1000.f;
					m_clkMotionLeft.restart();
				}
			}
			else
			{
				m_bMovesLeft = false;
			}
			if (GetScene().GetRenderWindow()->hasFocus() && sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				if (!m_bMovesRight)
				{
					m_bMovesRight = true;
					m_clkMotionRight.restart();
				}
				else
				{
					diffRight = m_clkMotionRight.getElapsedTime().asMilliseconds() * velocity / 1000.f;
					m_clkMotionRight.restart();
				}

			}
			else
			{
				m_bMovesRight = false;
			}
			if (GetScene().GetRenderWindow()->hasFocus() && sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				if (!m_bMovesUp)
				{
					m_bMovesUp = true;
					m_clkMotionUp.restart();
				}
				else
				{
					diffUp = m_clkMotionUp.getElapsedTime().asMilliseconds() * velocity / 1000.f;
					m_clkMotionUp.restart();
				}
			}
			else
			{
				m_bMovesUp = false;
			}
			if (GetScene().GetRenderWindow()->hasFocus() && sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				if (!m_bMovesDown)
				{
					m_bMovesDown = true;
					m_clkMotionDown.restart();
				}
				else
				{
					diffDown = m_clkMotionDown.getElapsedTime().asMilliseconds() * velocity / 1000.f;
					m_clkMotionDown.restart();
				}
			}
			else
			{
				m_bMovesDown = false;
			}

			sf::Vector2f pos = m_pMotionManager->CalculatePlayerPosition(sf::Vector2f((diffRight - diffLeft), (diffDown - diffUp)));
			mobj.SetPos(pos);
		}
		else
		{
			sf::Vector2f pos = m_pMotionManager->GetObjectPosition(mobj.GetObjectId());
			if (pos.x || pos.y)
				mobj.SetPos(pos);
		}
	}
}

void CWGame::NormalizeSize()
{
	SetSize(sf::Vector2f(800, 600));

	m_pMessageBox->SetFont(GetFont());
	m_pWinnerDlg->SetFont(GetFont());
	//m_pWinnerDlg->SetVisible(true);

	m_pButtonQuit->SetFont(GetFont());
	m_pButtonQuit->SetSize(sf::Vector2f(100, 30));

	m_pButtonPrediction->SetFont(GetFont());
	m_pButtonPrediction->SetSize(sf::Vector2f(100, 30));

	m_pButtonRecancelation->SetFont(GetFont());
	m_pButtonRecancelation->SetSize(sf::Vector2f(100, 30));

	m_pButtonInterpolation->SetFont(GetFont());
	m_pButtonInterpolation->SetSize(sf::Vector2f(100, 30));

	m_pGameLocation->SetFont(GetFont());
	m_pGameLocation->SetSize(sf::Vector2f(
		GetSize().x - m_pButtonQuit->GetSize().x - GetPadding() * 3,
		GetSize().y - GetPadding() * 2
	));

	m_pWinnerDlg->SetSize(sf::Vector2f(m_pGameLocation->GetSize().x - 100, m_pGameLocation->GetSize().y - 100));
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pWinnerDlg), m_pGameLocation->GetAbsoluteWorkingRect());

	m_pGameLocation->SetPosition(sf::Vector2f(0, 0));
	m_pButtonQuit->SetPosition(sf::Vector2f(m_pGameLocation->GetSize().x + GetPadding(), 0));

	m_pButtonPrediction->SetPosition(sf::Vector2f(
		m_pGameLocation->GetSize().x + GetPadding(),
		m_pGameLocation->GetSize().y - m_pButtonPrediction->GetSize().y * 3 - GetPadding() * 2
	));
	m_pButtonRecancelation->SetPosition(sf::Vector2f(
		m_pGameLocation->GetSize().x + GetPadding(),
		m_pGameLocation->GetSize().y - m_pButtonPrediction->GetSize().y * 2 - GetPadding()
	));
	m_pButtonInterpolation->SetPosition(sf::Vector2f(
		m_pGameLocation->GetSize().x + GetPadding(),
		m_pGameLocation->GetSize().y - m_pButtonPrediction->GetSize().y
	));
}

void CWGame::Draw()
{
	NormalizeSize();

	__super::Draw();
}

void CWGame::OnKeyPressed(
	sf::Keyboard::Key code,	//< Code of the key that has been pressed
	bool          alt,		//< Is the Alt key pressed?
	bool          control,	//< Is the Control key pressed?
	bool          shift,	//< Is the Shift key pressed?
	bool          system	//< Is the System key pressed?
)
{
	if (m_pWinnerDlg->IsVisible())
		return;

	if (code == sf::Keyboard::Key::Left)
	{
		if (m_OnPlayerShotHandler)
			m_OnPlayerShotHandler(ePlayerShotDir_Left);
	}
	if (code == sf::Keyboard::Key::Right)
	{
		if (m_OnPlayerShotHandler)
			m_OnPlayerShotHandler(ePlayerShotDir_Right);
	}
	if (code == sf::Keyboard::Key::Up)
	{
		if (m_OnPlayerShotHandler)
			m_OnPlayerShotHandler(ePlayerShotDir_Up);
	}
	if (code == sf::Keyboard::Key::Down)
	{
		if (m_OnPlayerShotHandler)
			m_OnPlayerShotHandler(ePlayerShotDir_Down);
	}
}
