#pragma once
#include <Windows.h>
#include <memory>
#include <string>
#include <list>
#include <functional>
#include <SFML\Network.hpp>
#include "../GameServer/UdpConnectionProtocol.h"


class CUdpOutConnection
{
public:
	CUdpOutConnection(const std::wstring& rsServerAddress, USHORT nServerPort);
	virtual ~CUdpOutConnection();

	const sf::IpAddress& GetIpAddress() const { return m_ServerIpAddress; }
	const sf::Uint16 GetPort() const { return m_nServerPort; }

	bool IsConnected() const { return (m_eState == eStates_Connected); }
	bool IsDisconnected() const { return (m_eState == eStates_Disconnected); }

	sf::UdpSocket::Status GetDisconnectionStatus() const { return m_eDisconnectionStatus; }
	const std::wstring& GetDisconnectionStatusString() const { return m_sDisconnectionStatus; }

	void Clear();

	sf::UdpSocket::Status Connect();
	void Disconnect();

	sf::UdpSocket::Status Send(sf::Packet& rPacket);
	sf::UdpSocket::Status SendCritical(sf::Packet& rPacket);

	sf::UdpSocket::Status Receive(sf::Packet& rPacket);

	// Returns true if something happened.
	bool Process();

private:
	enum eStates
	{
		eStates_Disconnected,

		eStates_WaitingConnectionResponse,
		eStates_WaitingConnectionDoneResponse,
		eStates_Connected,

		eStates_WaitingDisconnectDoneResponse,
		eStates_Disconnecting,
	};

	struct CriticalPacket
	{
		sf::Uint32 id;
		sf::Packet packet;

		sf::Clock clock;
		static const sf::Int32 repeatInterval_ms = 200;

		CriticalPacket()
			: id(0)
		{}
		CriticalPacket(sf::Uint32 id, const sf::Packet& rPacket)
			: id(id)
			, packet(rPacket)
		{}
	};

	struct CriticalPacketInfo
	{
		sf::Uint32 id;

		sf::Clock clock;
		static const sf::Int32 deletingInterval_s = 30;

		CriticalPacketInfo()
			: id(0)
		{}
		CriticalPacketInfo(sf::Uint32 id)
			: id(id)
		{}
	};

	void DisconnectionClear();

private:
	std::wstring m_sServerAddress;
	std::string m_sASCIIServerAddress;
	sf::IpAddress m_ServerIpAddress;
	USHORT m_nServerPort;

	sf::UdpSocket m_Socket;

	sf::Uint64 m_uiSalt;
	sf::Uint64 m_uiConnectionId;
	eStates m_eState;
	sf::Uint32 m_uiPacketIdCounter;

	sf::Clock m_clkDisconnect;
	sf::Clock m_clkLastPacketReceiving;
	sf::Clock m_clkLastStateChanging;
	static const sf::Int32 repeatInterval_ms = 50;
	static const sf::Int32 lostInterval_s = 30;
	static const sf::Int32 disconnectingInterval_ms = 500;

	std::list<std::unique_ptr<sf::Packet> > m_lstOut;
	std::list<std::unique_ptr<sf::Packet> > m_lstInNested;

	std::map<sf::Uint32, CriticalPacket> m_mapOutCritical;
	std::map<sf::Uint32, CriticalPacketInfo> m_mapInfoCritical;

	sf::UdpSocket::Status m_eDisconnectionStatus;
	std::wstring m_sDisconnectionStatus;
};
