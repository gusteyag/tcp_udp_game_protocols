#pragma once
#include "WContainer.h"
#include <functional>
#include "GameDefinitions.h"


class CWText;
class CWButton;

class CWWinnerDlg :
	public CWContainer
{
public:
	CWWinnerDlg(CScene& rScene);
	virtual ~CWWinnerDlg();

	void Show(
		const std::function<void(void)>& rOnQuitHandler,
		bool bNoWinner = false,
		std::wstring sWinner = std::wstring()
	)
	{
		m_OnQuitHandler = rOnQuitHandler;
		m_bNoWinner = bNoWinner;
		m_sWinner = sWinner;
		SetVisible(true);
	}
	
	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pTextLarge;
	CWText* m_pTextSmall;
	CWButton* m_pButtonQuit;
	std::function<void(void)> m_OnQuitHandler;

	bool m_bNoWinner;
	std::wstring m_sWinner;
};

