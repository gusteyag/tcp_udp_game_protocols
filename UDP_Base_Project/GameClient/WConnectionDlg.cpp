#include "WConnectionDlg.h"
#include "WText.h"
#include "WButton.h"
#include "WEditBox.h"


CWConnectionDlg::CWConnectionDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pEditServer(new CWEditBox(rScene))
	, m_pEditServerPort(new CWEditBox(rScene))
	, m_pTextServer(new CWText(rScene))
	, m_pTextServerPort(new CWText(rScene))
	, m_pButtonConnect(new CWButton(rScene))
{
	AddChild(m_pTextServer);
	AddChild(m_pEditServer);
	AddChild(m_pTextServerPort);
	AddChild(m_pEditServerPort);
	AddChild(m_pButtonConnect);

	SetText(L"Connection Parameters");
	m_pTextServer->SetText(L"Server Address: ");
	m_pEditServer->SetText(L"127.0.0.1");
	m_pTextServerPort->SetText(L"Server Port: ");
	m_pEditServerPort->SetText(L"44444");
	m_pButtonConnect->SetText(L"Connect");
	m_pButtonConnect->SetOnClickedHandler([this]() { CallOnConnectHandler(); });
}

CWConnectionDlg::~CWConnectionDlg()
{
}

void CWConnectionDlg::CallOnConnectHandler()
{
	try
	{
		if (m_OnConnectHandler)
		{
			std::wstring sServerAddress = m_pEditServer->GetText();
			sf::Uint16 uiServerPort = (sf::Uint16)std::stoul(m_pEditServerPort->GetText());
			m_OnConnectHandler(sServerAddress, uiServerPort);
		}
	}
	catch (std::exception&)
	{
		// TODO: Error message
	}
}

void CWConnectionDlg::NormalizeSize()
{
	float fButtonsHeight = 30;
	float fLeftSpacing = 20;
	float fTextWidth = 150;
	float fEditWidth = 300;
	float fEditPortWidth = 100;
	float fTopSpacing = 40;
	float fHeight = 20;
	float fVSpacing = 20;

	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fLeftSpacing * 2 + fTextWidth + fEditWidth,
		GetWorkingPosition().y + fTopSpacing + (fHeight + fVSpacing) * 2 - fVSpacing + fTopSpacing + fButtonsHeight + GetPadding() + GetInBorderWidth()
	));

	m_pTextServer->SetFont(GetFont());
	m_pTextServer->SetFontSize(GetFontSize());
	m_pTextServer->SetTextHAlignment(eHAlignment_Right);
	m_pTextServer->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextServer->SetPosition(sf::Vector2f(fLeftSpacing, fTopSpacing));

	m_pEditServer->SetFont(GetFont());
	m_pEditServer->SetFontSize(GetFontSize());
	m_pEditServer->SetSize(sf::Vector2f(fEditWidth, fHeight));
	m_pEditServer->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fTopSpacing));

	m_pTextServerPort->SetFont(GetFont());
	m_pTextServerPort->SetFontSize(GetFontSize());
	m_pTextServerPort->SetTextHAlignment(eHAlignment_Right);
	m_pTextServerPort->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextServerPort->SetPosition(sf::Vector2f(fLeftSpacing, fTopSpacing + fHeight + fVSpacing));

	m_pEditServerPort->SetFont(GetFont());
	m_pEditServerPort->SetFontSize(GetFontSize());
	m_pEditServerPort->SetSize(sf::Vector2f(fEditPortWidth, fHeight));
	m_pEditServerPort->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fTopSpacing + fHeight + fVSpacing));

	m_pButtonConnect->SetFont(GetFont());
	m_pButtonConnect->SetFontSize(GetFontSize());
	m_pButtonConnect->SetSize(sf::Vector2f(GetWorkingSize().x / 3, fButtonsHeight));
	m_pButtonConnect->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - m_pButtonConnect->GetSize().x / 2,
		fTopSpacing + (fHeight + fVSpacing) * 2 - fVSpacing + fTopSpacing
	));
}

void CWConnectionDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
