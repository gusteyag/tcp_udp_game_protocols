#include "GameClient.h"
#include "UdpClient.h"
#include "WPlayDlg.h"
#include "Scene.h"
#include "Widget.h"
#include "WButton.h"
#include "WScrollBar.h"
#include "WEditBox.h"
#include "WTextBox.h"
#include "WText.h"
#include "WContainer.h"
#include "WChat.h"
#include "WListBox.h"
#include "WConnectionDlg.h"
#include "WMessageBox.h"
#include "WConnectionDlg.h"
#include "WGame.h"
#include "MotionManager.h"


CGameClient::CGameClient()
	: m_pConnectionDlg(new CWConnectionDlg(m_Scene))
	, m_pPlayDlg(new CWPlayDlg(m_Scene))
	, m_pMessageBox(new CWMessageBox(m_Scene))
	, m_pGameView(new CWGame(m_Scene))
{
	m_Scene.GetMainWidget()->AddChild(m_pGameView);
	m_pGameView->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pConnectionDlg);
	m_pConnectionDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pPlayDlg);
	m_pPlayDlg->SetVisible(false);
	m_Scene.GetMainWidget()->AddChild(m_pMessageBox);
	m_pMessageBox->SetVisible(false);
}

CGameClient::~CGameClient()
{
}

void CGameClient::Run()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Ventanita");
	window.setVerticalSyncEnabled(false);
	window.setKeyRepeatEnabled(false);

	srand((UINT)time(NULL));
	m_Font.loadFromFile("courbd.ttf");

	m_pMessageBox->SetFont(m_Font);

	m_Scene.SetRenderWindow(&window);

	ShowConnectionDlg();

	while (window.isOpen())
	{
		if (m_pUdpClient.get())
		{
			log_outdbg(L"[ONE TICK]");

			while (m_pUdpClient->Process());

			m_pGameView->Process();

			MotionUnit motion = m_pGameView->GetPlayerMotion();
			if(motion.id && (motion.diff.x || motion.diff.y))
				m_pUdpClient->Request_PlayerMotion(motion);
		}

		sf::Event Event;
		while (window.pollEvent(Event))
		{
			if ((Event.type == sf::Event::Closed) || ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape)))
			{
				window.close();
				return;
			}

			m_Scene.OnEvent(Event);
		}

		m_Scene.Draw();

		//Sleep(1);
	}
}

void CGameClient::ShowConnectionDlg()
{
	m_pMessageBox->SetVisible(false);
	m_pPlayDlg->SetVisible(false);
	m_pGameView->SetVisible(false);

	m_pConnectionDlg->SetFont(m_Font);
	m_pConnectionDlg->SetOnConnectHandler([this](const std::wstring& rsServerAddress, sf::Uint16 uiServerPort) {
		m_pConnectionDlg->SetVisible(false);
		m_pMessageBox->Show(eMessageType_Info, L"Waiting for Connection...", [this]() {
			if (m_pUdpClient.get())
			{
				m_pUdpClient->Disconnect();
				m_pUdpClient.reset(NULL);
			}
			ShowConnectionDlg();
		});
		m_pMessageBox->SetButtonText(L"Cancel");

		m_pUdpClient.reset(new CUdpClient(rsServerAddress, uiServerPort));
		if (!m_pUdpClient->Connect(
			[this](void) {
			m_pMessageBox->SetVisible(false);
			ShowPlayDlg();
		},
			[this](const std::wstring& rsError) {
			OnDisconnected(rsError);
		},
			[this](const CGameState& rGameState) {
			m_pGameView->UpdateGameState(rGameState);
		}))
		{
			OnDisconnected(L"Failed to connect to server. Check parameters.");
		}
	});
	m_pConnectionDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pConnectionDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pConnectionDlg->SetVisible(true);
}

void CGameClient::OnDisconnected(const std::wstring& rsError)
{
	m_pMessageBox->SetVisible(false);
	m_pPlayDlg->SetVisible(false);
	m_pGameView->SetVisible(false);

	wchar_t szBuff[256] = {};
	_snwprintf_s(szBuff, _TRUNCATE, L"Connection error:\n\"%s\"\n\nTry connecting again.", rsError.c_str());
	m_pMessageBox->Show(eMessageType_Error, szBuff, [this]() {
		m_pUdpClient->Disconnect();
		m_pUdpClient.reset(NULL);
		ShowConnectionDlg();
	});
}

void CGameClient::ShowPlayDlg()
{
	m_pMessageBox->SetVisible(false);

	m_pPlayDlg->SetFont(m_Font);
	m_pPlayDlg->SetOnPlayHandler([this](const std::wstring& rsPlayerName, sf::Uint16 uiPlayersCount) {
		m_pPlayDlg->SetVisible(false);
		m_pMessageBox->Show(eMessageType_Info, L"Waiting for Matchmaking...", [this]() {
			if (m_pUdpClient.get())
			{
				m_pUdpClient->Request_Quit([this](eResult eRes) {
					//
				});
			}
			m_pPlayDlg->SetVisible(true);
		});
		m_pMessageBox->SetButtonText(L"Cancel");
		if (m_pUdpClient.get())
		{
			m_pUdpClient->Request_Play([this](eResult eRes, sf::Uint32 nPlayerId, const CGameInfo& rGameInfo) {
				if (eRes == eResult_Success)
				{
					m_pGameView->UpdateGameInfo(nPlayerId, rGameInfo);
					ShowGameView();
				}
				else
				{
					m_pMessageBox->Show(eMessageType_Info, L"Failed to create the game on the server.", [this]() {
						m_pPlayDlg->SetVisible(true);
					});
				}
			}, rsPlayerName, uiPlayersCount);
		}
	});
	m_pPlayDlg->SetOnDisconnectHandler([this]() {
		m_pPlayDlg->SetVisible(false);
		if (m_pUdpClient.get())
		{
			m_pUdpClient->Disconnect();
			m_pUdpClient.reset(NULL);
		}
		ShowConnectionDlg();
	});
	m_pPlayDlg->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pPlayDlg),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pPlayDlg->SetVisible(true);
}

void CGameClient::ShowGameView()
{
	m_pMessageBox->SetVisible(false);

	m_pGameView->SetFont(m_Font);
	m_pGameView->SetOnQuitHandler([this]() {
		m_pGameView->SetVisible(false);
		if (m_pUdpClient.get())
		{
			m_pUdpClient->Request_Quit([this](eResult eRes) {
				//
			});
		}
		m_pPlayDlg->SetVisible(true);
	});
	m_pGameView->SetOnPlayerShotHandler([this](ePlayerShotDir eDirection) {
		if (m_pUdpClient.get())
			m_pUdpClient->Request_PlayerShot(eDirection);
	});
	m_pGameView->NormalizeSize();
	CScene::AlignWidget(eHAlignment_Center, eVAlignment_Center, (*m_pGameView),
		sf::FloatRect(0, 0, m_Scene.GetRenderWindow()->getDefaultView().getSize().x, m_Scene.GetRenderWindow()->getDefaultView().getSize().y));
	m_pGameView->SetVisible(true);
}
