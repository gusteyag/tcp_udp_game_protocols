#include "WPlayDlg.h"
#include "WText.h"
#include "WButton.h"
#include "WEditBox.h"


CWPlayDlg::CWPlayDlg(CScene& rScene)
	: CWContainer(rScene)
	, m_pTextDescription(new CWText(rScene))
	, m_pEditPlayerName(new CWEditBox(rScene))
	, m_pEditDesiredPlayersCount(new CWEditBox(rScene))
	, m_pTextPlayerName(new CWText(rScene))
	, m_pTextDesiredPlayersCount(new CWText(rScene))
	, m_pButtonPlay(new CWButton(rScene))
	, m_pButtonDisconnect(new CWButton(rScene))
{
	AddChild(m_pTextDescription);
	AddChild(m_pTextPlayerName);
	AddChild(m_pEditPlayerName);
	AddChild(m_pTextDesiredPlayersCount);
	AddChild(m_pEditDesiredPlayersCount);
	AddChild(m_pButtonPlay);
	AddChild(m_pButtonDisconnect);

	SetText(L"Select Parameters");
	m_pTextDescription->SetText(L"Please enter your name and desired players count\nto attach to the game.");
	m_pTextPlayerName->SetText(L"Player Name: ");
	m_pEditPlayerName->SetText(L"");
	m_pTextDesiredPlayersCount->SetText(L"Desired Players Count: ");
	m_pEditDesiredPlayersCount->SetText(L"2");
	m_pButtonPlay->SetText(L"Play");
	m_pButtonPlay->SetOnClickedHandler([this]() { CallOnPlayHandler(); });
	m_pButtonDisconnect->SetText(L"Disconnect");
	m_pButtonDisconnect->SetOnClickedHandler([this]() { if (m_OnDisconnectHandler) m_OnDisconnectHandler(); });
}

CWPlayDlg::~CWPlayDlg()
{
}

void CWPlayDlg::CallOnPlayHandler()
{
	try
	{
		if (m_OnPlayHandler)
		{
			std::wstring sPlayerName = m_pEditPlayerName->GetText();
			sf::Uint16 uiPlayersCount = (sf::Uint16)std::stoul(m_pEditDesiredPlayersCount->GetText());
			m_OnPlayHandler(sPlayerName, uiPlayersCount);
		}
	}
	catch (std::exception&)
	{
		// TODO: Error message
	}
}

void CWPlayDlg::NormalizeSize()
{
	float fDescHeight = 50;
	float fButtonsHeight = 30;
	float fButtonsHSpacing = 30;
	float fLeftSpacing = 20;
	float fTextWidth = 200;
	float fEditWidth = 300;
	float fEditNumberWidth = 100;
	float fTopSpacing = 25;
	float fHeight = 20;
	float fVSpacing = 20;

	if (m_pEditPlayerName->GetText().empty() || m_pEditDesiredPlayersCount->GetText().empty())
	{
		m_pButtonPlay->SetEnabled(false);
	}
	else
	{
		m_pButtonPlay->SetEnabled(true);
	}

	SetPadding(fVSpacing);
	SetSize(sf::Vector2f(
		GetWorkingPosition().x * 2 + fLeftSpacing * 2 + fTextWidth + fEditWidth,
		GetWorkingPosition().y + fDescHeight + fVSpacing + (fHeight + fVSpacing) * 2 - fVSpacing + fTopSpacing + fButtonsHeight + GetPadding() + GetInBorderWidth()
	));

	m_pTextDescription->SetFont(GetFont());
	//m_pTextDescription->SetPadding(20);
	m_pTextDescription->SetWordsWrap(true);
	m_pTextDescription->SetFontSize(GetFontSize() + 3);
	m_pTextDescription->SetTextHAlignment(eHAlignment_Center);
	m_pTextDescription->SetTextVAlignment(eVAlignment_Center);
	m_pTextDescription->SetSize(sf::Vector2f(GetWorkingSize().x, fDescHeight));
	m_pTextDescription->SetPosition(sf::Vector2f(0, 0));

	m_pTextPlayerName->SetFont(GetFont());
	m_pTextPlayerName->SetFontSize(GetFontSize());
	m_pTextPlayerName->SetTextHAlignment(eHAlignment_Right);
	m_pTextPlayerName->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextPlayerName->SetPosition(sf::Vector2f(fLeftSpacing, fDescHeight + fVSpacing));

	m_pEditPlayerName->SetFont(GetFont());
	m_pEditPlayerName->SetFontSize(GetFontSize());
	m_pEditPlayerName->SetSize(sf::Vector2f(fEditWidth, fHeight));
	m_pEditPlayerName->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fDescHeight + fVSpacing));

	m_pTextDesiredPlayersCount->SetFont(GetFont());
	m_pTextDesiredPlayersCount->SetFontSize(GetFontSize());
	m_pTextDesiredPlayersCount->SetTextHAlignment(eHAlignment_Right);
	m_pTextDesiredPlayersCount->SetSize(sf::Vector2f(fTextWidth, fHeight));
	m_pTextDesiredPlayersCount->SetPosition(sf::Vector2f(fLeftSpacing, fDescHeight + fVSpacing + fHeight + fVSpacing));

	m_pEditDesiredPlayersCount->SetFont(GetFont());
	m_pEditDesiredPlayersCount->SetFontSize(GetFontSize());
	m_pEditDesiredPlayersCount->SetSize(sf::Vector2f(fEditNumberWidth, fHeight));
	m_pEditDesiredPlayersCount->SetPosition(sf::Vector2f(fLeftSpacing + fTextWidth, fDescHeight + fVSpacing + fHeight + fVSpacing));

	m_pButtonPlay->SetFont(GetFont());
	m_pButtonPlay->SetFontSize(GetFontSize());
	m_pButtonPlay->SetSize(sf::Vector2f(GetWorkingSize().x / 3, fButtonsHeight));
	m_pButtonPlay->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - m_pButtonPlay->GetSize().x / 2 - m_pButtonPlay->GetSize().x / 2 - fButtonsHSpacing / 2,
		fDescHeight + fVSpacing + (fHeight + fVSpacing) * 2 - fVSpacing + fTopSpacing
	));
	m_pButtonDisconnect->SetFont(GetFont());
	m_pButtonDisconnect->SetFontSize(GetFontSize());
	m_pButtonDisconnect->SetSize(sf::Vector2f(GetWorkingSize().x / 3, fButtonsHeight));
	m_pButtonDisconnect->SetPosition(sf::Vector2f(
		GetWorkingSize().x / 2 - m_pButtonPlay->GetSize().x / 2 + m_pButtonPlay->GetSize().x / 2 + fButtonsHSpacing / 2,
		fDescHeight + fVSpacing + (fHeight + fVSpacing) * 2 - fVSpacing + fTopSpacing
	));
}

void CWPlayDlg::Draw()
{
	NormalizeSize();
	__super::Draw();
}
