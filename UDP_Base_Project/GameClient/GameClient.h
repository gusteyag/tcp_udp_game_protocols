#pragma once
#include <Windows.h>
#include <memory>
#include "Scene.h"
#include <GameState.h>


class CUdpClient;
class CWMessageBox;
class CWConnectionDlg;
class CWPlayDlg;
class CWGame;

class CGameClient
{
public:
	CGameClient();
	virtual ~CGameClient();

	void Run();

	void ShowConnectionDlg();
	void ShowPlayDlg();
	void ShowGameView();

	void OnDisconnected(const std::wstring& rsError);

private:
	std::unique_ptr<CUdpClient> m_pUdpClient;
	CScene m_Scene;
	sf::Font m_Font;
	CWMessageBox* m_pMessageBox;
	CWConnectionDlg* m_pConnectionDlg;
	CWPlayDlg* m_pPlayDlg;
	CWGame* m_pGameView;
};
