#pragma once
#include <MotionUnit.h>
#include <map>
#include <list>


#ifdef _DEBUG

#define log_outdbg(s, ...) \
{\
wchar_t szBuff[256] = {};\
_snwprintf_s(szBuff, _TRUNCATE, s, __VA_ARGS__);\
::OutputDebugStringW(szBuff);\
}

#else

#define log_outdbg(s, ...)

#endif


class CMotionManager
{
public:
	CMotionManager(sf::Uint32 nThisPlayerObjectId, const sf::Vector2f& v2fThisPlayerStartPos)
		: m_bPrediction(false)
		, m_bRecancelation(false)
		, m_bInterpolation(false)
	{
		m_nScreenMotion.pos = v2fThisPlayerStartPos;
		m_nScreenMotion.diff = sf::Vector2f();
		m_nScreenMotion.obj_id = nThisPlayerObjectId;
		m_nScreenMotion.id = 0;

		m_nServerMotion.pos = v2fThisPlayerStartPos;
		m_nServerMotion.diff = sf::Vector2f();
		m_nServerMotion.obj_id = nThisPlayerObjectId;
		m_nServerMotion.id = 0;
	}
	virtual ~CMotionManager() {}

	void SetPrediction(bool bPrediction) { m_bPrediction = bPrediction; }
	bool GetPrediction() const { return m_bPrediction; }

	void SetRecancelation(bool bRecancelation) { m_bRecancelation = bRecancelation; }
	bool GetRecancelation() const { return m_bRecancelation; }

	void SetInterpolation(bool bInterpolation) { m_bInterpolation = bInterpolation; }
	bool GetInterpolation() const { return m_bInterpolation; }

	void UpdateObjectPositions(const std::map<sf::Uint32, sf::Vector2f>& rmapPositions)
	{
		sf::Time elapsed = m_clk.getElapsedTime();
		if (m_LastUpdateTimestamp == sf::Time::Zero)
		{
			m_Lag = sf::Time::Zero;
		}		
		else
		{
			sf::Time lag = elapsed - m_LastUpdateTimestamp;
			if ((m_Lag == sf::Time::Zero) || (m_Lag < lag))
				m_Lag = lag;
			if (m_Lag.asMilliseconds() > max_lag_ms)
				m_Lag = sf::milliseconds(max_lag_ms);
		}
		m_LastUpdateTimestamp = elapsed;

		for (auto& obj : rmapPositions)
		{
			if (obj.first == m_nScreenMotion.obj_id)
				continue;

			if (!m_bInterpolation)
				m_mapObjects[obj.first].clear();

			if (m_bInterpolation && (m_mapObjects[obj.first].empty() || (m_mapObjects.at(obj.first).back().pos != obj.second)))
			{
				log_outdbg(L"[INTERPOLATION ====>>>] New Position added. obj_id = %u, timestamp = %i, lag = %i, list_size = %u\n",
					obj.first, elapsed.asMilliseconds(), m_Lag.asMilliseconds(), (DWORD)(m_mapObjects[obj.first].size() + 1));
				log_outdbg(L"[INTERPOLATION ====>>>] New Position added. obj_id = %u, pos_x = %f, pos_y = %f\n", obj.first, obj.second.x, obj.second.y);
			}

			m_mapObjects[obj.first].push_back(Interpolation(elapsed, obj.second));
		}
	}

	sf::Vector2f GetObjectPosition(sf::Uint32 obj_id)
	{
		sf::Vector2f pos;
		if (obj_id == m_nScreenMotion.obj_id)
			return pos;
		if (!m_mapObjects.count(obj_id))
			return pos;
		if (m_mapObjects[obj_id].empty())
			return pos;

		if(!m_bInterpolation)
			return m_mapObjects[obj_id].back().pos;

		sf::Time obj_time = m_clk.getElapsedTime() - m_Lag;

		if (obj_time >= m_mapObjects[obj_id].back().timestamp)
		{
			Interpolation inter = m_mapObjects[obj_id].back();
			m_mapObjects[obj_id].clear();
			m_mapObjects[obj_id].push_back(inter);
			return inter.pos;
		}
		else if (obj_time < m_mapObjects[obj_id].front().timestamp)
		{
			return m_mapObjects[obj_id].front().pos;
		}

		Interpolation interPrev;
		for (auto& inter : m_mapObjects[obj_id])
		{
			if (interPrev.timestamp != sf::Time::Zero)
			{
				if ((obj_time >= interPrev.timestamp) && (obj_time < inter.timestamp))
				{
					sf::Time diff_time = inter.timestamp - interPrev.timestamp;
					sf::Time obj_diff_time = obj_time - interPrev.timestamp;
					pos = sf::Vector2f(
						interPrev.pos.x + (float)((double)(inter.pos.x - interPrev.pos.x) * (double)obj_diff_time.asMicroseconds() / (double)diff_time.asMicroseconds()),
						interPrev.pos.y + (float)((double)(inter.pos.y - interPrev.pos.y) * (double)obj_diff_time.asMicroseconds() / (double)diff_time.asMicroseconds())
					);
					m_mapObjects[obj_id].remove_if([interPrev](auto& inter) { return (inter.timestamp < interPrev.timestamp); });

					if ((inter.pos.x != interPrev.pos.x) || (inter.pos.y != interPrev.pos.y))
					{
						log_outdbg(L"[INTERPOLATION ====>>>] obj_id = %u, obj_time = %i, lag = %i, list_size = %u\n",
							obj_id, obj_time.asMilliseconds(), m_Lag.asMilliseconds(), (DWORD)m_mapObjects[obj_id].size());
						log_outdbg(L"[INTERPOLATION ====>>>] obj_id = %u, prev_time = %i, pos_time = %i, curr_time = %i\n",
							obj_id, interPrev.timestamp.asMilliseconds(), obj_time.asMilliseconds(), inter.timestamp.asMilliseconds());
						log_outdbg(L"[INTERPOLATION ====>>>] obj_id = %u, prev_x = %f, pos_x = %f, curr_x = %f\n", obj_id, interPrev.pos.x, pos.x, inter.pos.x);
						log_outdbg(L"[INTERPOLATION ====>>>] obj_id = %u, prev_y = %f, pos_y = %f, curr_y = %f\n", obj_id, interPrev.pos.y, pos.y, inter.pos.y);
					}
					break;
				}
			}

			interPrev = inter;
		}

		return pos;
	}

	sf::Vector2f CalculatePlayerPosition(const sf::Vector2f& diff)
	{
		if (diff.x || diff.y)
		{
			m_nScreenMotion.pos += diff;
			m_nScreenMotion.diff += diff;

			log_outdbg(L"[MOTION ====>>>] diff.x = %f, diff.y = %f, pos.x = %f, pos.y = %f\n",
				diff.x, diff.y, (m_bPrediction ? m_nScreenMotion.pos : m_nServerMotion.pos).x, (m_bPrediction ? m_nScreenMotion.pos : m_nServerMotion.pos).y);
		}

		// Always return position
		if (m_bPrediction)
			return m_nScreenMotion.pos;
		return m_nServerMotion.pos;
	}

	MotionUnit GetPlayerMotion()
	{
		if (!m_nScreenMotion.diff.x && !m_nScreenMotion.diff.y)
			return MotionUnit();

		++m_nScreenMotion.id;

		log_outdbg(L"[MOTION REQ ====>>>] id = %u, diff.x = %f, diff.y = %f, pos.x = %f, pos.y = %f\n",
			m_nScreenMotion.id, m_nScreenMotion.diff.x, m_nScreenMotion.diff.y, m_nScreenMotion.pos.x, m_nScreenMotion.pos.y);

		MotionUnit motion = m_nScreenMotion;
		m_lstMotions.push_back(motion);
		m_nScreenMotion.diff = sf::Vector2f();
		return motion;
	}

	void ConfirmPlayerMotion(sf::Uint32 motion_id, const sf::Vector2f& pos)
	{
		if (!motion_id || (m_nServerMotion.id && (motion_id <= m_nServerMotion.id)))
		{
			//log_outdbg(L"[MOTION ACK ====>>>] Skipped id = %u, awaiting = %u\n", motion_id, (DWORD)m_lstMotions.size());
			return;
		}

		m_nServerMotion.pos = pos;
		m_nServerMotion.id = motion_id;
		m_lstMotions.remove_if([motion_id](auto& motion) { return (motion.id <= motion_id); });
		m_nScreenMotion.pos = pos; // Correct current position		
		if (m_bRecancelation)
		{
			for (auto& motion : m_lstMotions)
			{
				m_nScreenMotion.pos += motion.diff;
			}
		}

		log_outdbg(L"[MOTION ACK ====>>>] id = %u, pos.x = %f, pos.y = %f, awaiting = %u\n", motion_id, pos.x, pos.y, (DWORD)m_lstMotions.size());
	}

private:
	struct Interpolation
	{
		sf::Time timestamp;
		sf::Vector2f pos;

		Interpolation()
		{}
		Interpolation(const sf::Time& timestamp, const sf::Vector2f& pos)
			: timestamp(timestamp)
			, pos(pos)
		{}
	};

private:
	bool m_bPrediction;
	bool m_bRecancelation;
	bool m_bInterpolation;

	MotionUnit m_nScreenMotion;
	MotionUnit m_nServerMotion;

	sf::Clock m_clk;
	sf::Time m_LastUpdateTimestamp;
	sf::Time m_Lag;
	static const int max_lag_ms = 500;
	std::list<MotionUnit> m_lstMotions;
	std::map<sf::Uint32, std::list<Interpolation> > m_mapObjects;
};
