#pragma once
#include "WContainer.h"
#include "GameDefinitions.h"
#include <map>
#include <list>
#include <functional>
#include <list>
#include <MovingObj.h>


class CWGameLocation :
	public CWContainer
{
public:
	CWGameLocation(CScene& rScene);
	virtual ~CWGameLocation();

	void SetMovingObjList(sf::Uint32 nThisPlayerObjectId, std::list<CMovingObj>* plstMovingObjs) { m_nThisPlayerObjectId = nThisPlayerObjectId; m_plstMovingObjs = plstMovingObjs; }

	virtual void NormalizeSize();
	virtual void Draw();

private:
	sf::Uint32 m_nThisPlayerObjectId;
	std::list<CMovingObj>* m_plstMovingObjs;
};

