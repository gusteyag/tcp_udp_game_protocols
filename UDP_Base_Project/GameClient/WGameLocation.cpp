#include "WGameLocation.h"


CWGameLocation::CWGameLocation(CScene& rScene)
	: CWContainer(rScene)
	, m_nThisPlayerObjectId(0)
	, m_plstMovingObjs(NULL)
{
	SetBorderWidth(-1);
	SetHeaderHeight(0);
	SetFillColor(sf::Color::Black);
}

CWGameLocation::~CWGameLocation()
{
}

void CWGameLocation::NormalizeSize()
{
}

void CWGameLocation::Draw()
{
	if (!GetRenderTarget())
		return;

	NormalizeSize();

	__super::Draw();

	GetScene().PushClipping(GetAbsoluteWorkingRect());

	if (m_plstMovingObjs)
	{
		for (auto& mobj : (*m_plstMovingObjs))
		{
			if (mobj.GetObjectType() != eMovingObjType_Shot)
				continue;
			sf::RectangleShape shape(sf::Vector2f(mobj.GetSize().x * GetWorkingSize().x, mobj.GetSize().y * GetWorkingSize().y));
			shape.setPosition(sf::Vector2f(GetAbsoluteWorkingPosition().x + mobj.GetPos().x * GetWorkingSize().x, GetAbsoluteWorkingPosition().y + mobj.GetPos().y * GetWorkingSize().y));
			shape.setFillColor(sf::Color::Red);
			GetRenderTarget()->draw(shape);
		}

		for (auto& mobj : (*m_plstMovingObjs))
		{
			if (mobj.GetObjectType() == eMovingObjType_Shot)
				continue;
			sf::RectangleShape shape(sf::Vector2f(mobj.GetSize().x * GetWorkingSize().x, mobj.GetSize().y * GetWorkingSize().y));
			shape.setPosition(sf::Vector2f(GetAbsoluteWorkingPosition().x + mobj.GetPos().x * GetWorkingSize().x, GetAbsoluteWorkingPosition().y + mobj.GetPos().y * GetWorkingSize().y));
			if (mobj.GetObjectId() == m_nThisPlayerObjectId)
				shape.setFillColor(sf::Color::Green);
			else
				shape.setFillColor(sf::Color::White);
			GetRenderTarget()->draw(shape);
		}
	}

	GetScene().PopClipping();
}
