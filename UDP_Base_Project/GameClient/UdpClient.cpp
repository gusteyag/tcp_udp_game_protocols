#include "UdpClient.h"
#include <GameState.h>
#include <MotionUnit.h>
#include <GameInfo.h>


CUdpClient::CUdpClient(const std::wstring& rsServerAddress, USHORT nServerPort)
	: m_sServerAddress(rsServerAddress)
	, m_nServerPort(nServerPort)
	, m_Connection(rsServerAddress, nServerPort)
	, m_bOnConnectedCBCalled(false)
{
	char szBuff[256] = {};
	_snprintf_s(szBuff, _TRUNCATE, "%S", m_sServerAddress.c_str());
	m_sASCIIServerAddress = szBuff;
	m_ServerIpAddress = m_sASCIIServerAddress;
}

CUdpClient::~CUdpClient()
{
	Disconnect();
}

void CUdpClient::Clear()
{
	m_Connection.Clear();
	m_sDisconnectionStatus.clear();
	m_bOnConnectedCBCalled = false;
	m_OnConnectedCB = nullptr;
	m_OnDisconnectedCB = nullptr;

	m_OnPlayAnswerCB = nullptr;
	m_OnQuitAnswerCB = nullptr;

	m_OnGameStateChangedCB = nullptr;
}

bool CUdpClient::Connect(
	const std::function<void(void)>& rOnConnectedCB,
	const std::function<void(const std::wstring& rsError)>& rOnDisconnectedCB,
	const std::function<void(const CGameState& rGameState)>& rOnGameStateChangedCB
)
{
	sf::UdpSocket::Status status = m_Connection.Connect();
	if (status != sf::UdpSocket::Status::Done)
	{
		Clear();
		return false;
	}

	m_bOnConnectedCBCalled = false;
	m_OnConnectedCB = rOnConnectedCB;
	m_OnDisconnectedCB = rOnDisconnectedCB;
	m_sDisconnectionStatus.clear();

	m_OnGameStateChangedCB = rOnGameStateChangedCB;

	return true;
}

void CUdpClient::Disconnect()
{
	m_bOnConnectedCBCalled = false;
	m_OnConnectedCB = nullptr;
	m_OnDisconnectedCB = std::nullptr_t();

	m_OnPlayAnswerCB = nullptr;
	m_OnQuitAnswerCB = nullptr;

	m_Connection.Disconnect();
	while (!m_Connection.IsDisconnected())
	{
		while (m_Connection.Process());
		Sleep(1);
	}
	m_sDisconnectionStatus.clear();
}

bool CUdpClient::Process()
{
	if (m_Connection.IsDisconnected())
		return false;

	bool bSomethingHappened = false;

	while (m_Connection.Process())
		bSomethingHappened = true;

	if (m_Connection.IsDisconnected())
	{
		if (m_OnDisconnectedCB)
			m_OnDisconnectedCB(m_sDisconnectionStatus.empty() ? m_Connection.GetDisconnectionStatusString() : m_sDisconnectionStatus);
		bSomethingHappened = true;
	}

	if (m_Connection.IsConnected())
	{
		if (!m_bOnConnectedCBCalled)
		{
			m_bOnConnectedCBCalled = true;
			if (m_OnConnectedCB)
				m_OnConnectedCB();
		}

		sf::Packet Packet;
		m_Connection.Receive(Packet);
		if (Packet.getData())
		{
			bSomethingHappened = true;
			if (!ProcessProtocol(Packet))
				m_Connection.Disconnect();
		}
	}

	return bSomethingHappened;
}

bool CUdpClient::ProcessProtocol(sf::Packet& rPacket)
{
	sf::Uint8 nPacketType = 0;
	rPacket >> nPacketType;
	switch (nPacketType)
	{
	case ePacketType_Play:
	{
		sf::Uint8 ui8Data = 0; rPacket >> ui8Data;
		if (ui8Data == eResult_Success)
		{
			if (m_OnPlayAnswerCB)
			{
				sf::Uint32 ui32PlayerId = 0; rPacket >> ui32PlayerId;
				CGameInfo GameInfo;
				if (GameInfo.Deserialize(rPacket))
				{
					m_OnPlayAnswerCB(eResult_Success, ui32PlayerId, GameInfo);
					m_OnPlayAnswerCB = nullptr;
				}
				else
				{
					m_OnPlayAnswerCB(eResult_Fail, ui32PlayerId, CGameInfo());
					m_OnPlayAnswerCB = nullptr;
				}
			}
		}
		else
		{
			if (m_OnPlayAnswerCB)
			{
				m_OnPlayAnswerCB((eResult)ui8Data, 0, CGameInfo());
				m_OnPlayAnswerCB = nullptr;
			}
		}
	}
	break;

	case ePacketType_Quit:
	{
		sf::Uint8 ui8Data = 0; rPacket >> ui8Data;
		if (m_OnPlayAnswerCB)
		{
			m_OnQuitAnswerCB((eResult)ui8Data);
			m_OnQuitAnswerCB = nullptr;
		}
	}
	break;

	case ePacketType_UpdateGameState:
	{
		CGameState state;
		if (state.Deserialize(rPacket))
		{
			if (m_OnGameStateChangedCB)
				m_OnGameStateChangedCB(state);
		}
	}
	break;

	default:
	{
		m_sDisconnectionStatus = L"Unknown packet type.";
		Disconnect();
		return false;
	}
	break;
	}

	return true;
}

void CUdpClient::Request_Play(
	const std::function<void(eResult eRes, sf::Uint32 nPlayerId, const CGameInfo& rGameInfo)>& rAnswerCB,
	const std::wstring& rsPlayerName,
	sf::Uint16 uiPlayersCount
)
{
	sf::Packet Packet;
	Packet << (sf::Uint8)ePacketType_Play;
	Packet << rsPlayerName;
	Packet << (sf::Uint8)uiPlayersCount;
	sf::UdpSocket::Status status = m_Connection.SendCritical(Packet);
	if (status == sf::UdpSocket::Status::Done)
	{
		m_OnPlayAnswerCB = rAnswerCB;
	}
	else
	{
		rAnswerCB(eResult_NotReady, 0, CGameInfo());
	}
}


void CUdpClient::Request_Quit(const std::function<void(eResult eRes)>& rCallback)
{
	sf::Packet Packet;
	Packet << (sf::Uint8)ePacketType_Quit;
	sf::UdpSocket::Status status = m_Connection.SendCritical(Packet);
	if (status == sf::UdpSocket::Status::Done)
	{
		m_OnQuitAnswerCB = rCallback;
	}
	else
	{
		rCallback(eResult_NotReady);
	}
}

void CUdpClient::Request_PlayerShot(ePlayerShotDir eDirection)
{
	sf::Packet Packet;
	Packet << (sf::Uint8)ePacketType_PlayerShot;
	Packet << (sf::Uint8)eDirection;
	m_Connection.SendCritical(Packet);
}

void CUdpClient::Request_PlayerMotion(const MotionUnit& motion)
{
	sf::Packet Packet;
	Packet << (sf::Uint8)ePacketType_PlayerMotion;
	if (motion.Serialize(Packet))
		m_Connection.Send(Packet);
}
