#pragma once
#include <functional>
#include "WContainer.h"

class CWText;
class CWButton;
class CWEditBox;

class CWConnectionDlg :
	public CWContainer
{
public:
	CWConnectionDlg(CScene& rScene);
	virtual ~CWConnectionDlg();

	void SetOnConnectHandler(const std::function<void(const std::wstring& rsServerAddress, sf::Uint16 uiServerPort)>& rHandler) { m_OnConnectHandler = rHandler; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWEditBox* m_pEditServer;
	CWEditBox* m_pEditServerPort;
	CWText* m_pTextServer;
	CWText* m_pTextServerPort;
	CWButton* m_pButtonConnect;
	std::function<void(const std::wstring& rsServerAddress, sf::Uint16 uiServerPort)> m_OnConnectHandler;

private:
	void CallOnConnectHandler();
};
