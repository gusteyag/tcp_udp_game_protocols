#pragma once
#include "WContainer.h"
#include <functional>
#include <GameInfo.h>
#include <GameState.h>
#include <MotionUnit.h>


class CWGameLocation;
class CWButton;
class CWListBox;
class CMotionManager;
class CWMessageBox;
class CWWinnerDlg;

class CWGame :
	public CWContainer
{
public:
	CWGame(CScene& rScene);
	virtual ~CWGame();

	void SetOnQuitHandler(const std::function<void(void)>& rHandler) { m_OnQuitHandler = rHandler; }
	void SetOnPlayerShotHandler(const std::function<void(ePlayerShotDir eDirection)>& rHandler) { m_OnPlayerShotHandler = rHandler; }

	void UpdateGameInfo(sf::Uint32 nPlayerId, const CGameInfo& rGameInfo);
	void UpdateGameState(const CGameState& rGameState);

	void Process();
	MotionUnit GetPlayerMotion();

	virtual void NormalizeSize();

	virtual void Draw();

	virtual void OnKeyPressed(
		sf::Keyboard::Key code,	//< Code of the key that has been pressed
		bool          alt,		//< Is the Alt key pressed?
		bool          control,	//< Is the Control key pressed?
		bool          shift,	//< Is the Shift key pressed?
		bool          system	//< Is the System key pressed?
	);

private:
	std::unique_ptr<CMotionManager> m_pMotionManager;
	CWButton* m_pButtonQuit;
	CWButton* m_pButtonPrediction;
	CWButton* m_pButtonRecancelation;
	CWButton* m_pButtonInterpolation;
	CWGameLocation* m_pGameLocation;
	CWMessageBox* m_pMessageBox;
	CWWinnerDlg* m_pWinnerDlg;
	std::function<void(void)> m_OnQuitHandler;
	CGameInfo m_GameInfo;
	CGameState m_GameState;
	sf::Uint32 m_nThisPlayerObjectId;
	std::list<CMovingObj> m_lstMovingObjs;
	std::function<void(ePlayerShotDir eDirection)> m_OnPlayerShotHandler;
	bool m_bMovesLeft;
	bool m_bMovesRight;
	bool m_bMovesUp;
	bool m_bMovesDown;
	sf::Clock m_clkMotionLeft;
	sf::Clock m_clkMotionRight;
	sf::Clock m_clkMotionUp;
	sf::Clock m_clkMotionDown;
};
