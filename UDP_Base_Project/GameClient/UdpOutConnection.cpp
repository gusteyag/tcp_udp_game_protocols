#include "UdpOutConnection.h"


CUdpOutConnection::CUdpOutConnection(const std::wstring& rsServerAddress, USHORT nServerPort)
	: m_sServerAddress(rsServerAddress)
	, m_nServerPort(nServerPort)
	, m_uiSalt(0)
	, m_uiConnectionId(0)
	, m_eState(eStates_Disconnected)
	, m_uiPacketIdCounter(0)
	, m_eDisconnectionStatus(sf::UdpSocket::Status::Done)
{
	char szBuff[256] = {};
	_snprintf_s(szBuff, _TRUNCATE, "%S", m_sServerAddress.c_str());
	m_sASCIIServerAddress = szBuff;
	m_ServerIpAddress = m_sASCIIServerAddress;

	m_Socket.setBlocking(false);
}

CUdpOutConnection::~CUdpOutConnection()
{
}

void CUdpOutConnection::Clear()
{
	m_Socket.unbind();
	m_uiSalt = 0;
	m_uiConnectionId = 0;;
	m_eState = eStates_Disconnected;
	m_uiPacketIdCounter = 0;

	m_clkDisconnect.restart();
	m_clkLastPacketReceiving.restart();
	m_clkLastStateChanging.restart();

	m_lstOut.clear();
	m_lstInNested.clear();

	m_mapOutCritical.clear();
	m_mapInfoCritical.clear();

	m_eDisconnectionStatus = sf::UdpSocket::Status::Done;
	m_sDisconnectionStatus.clear();
}

void CUdpOutConnection::DisconnectionClear()
{
	m_Socket.unbind();
	m_uiSalt = 0;
	m_uiConnectionId = 0;;
	m_eState = eStates_Disconnected;

	m_clkDisconnect.restart();
	m_clkLastPacketReceiving.restart();
	m_clkLastStateChanging.restart();

	m_lstOut.clear();

	m_mapOutCritical.clear();
	m_mapInfoCritical.clear();
}

sf::UdpSocket::Status  CUdpOutConnection::Connect()
{
	if (m_eState != eStates_Disconnected)
		return sf::UdpSocket::Status::NotReady;

	Clear();

	sf::UdpSocket::Status status = m_Socket.bind(sf::Socket::AnyPort);
	if (status != sf::UdpSocket::Status::Done)
		return status;

	sf::Uint16 ui16Rand1 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand2 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand3 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand4 = (sf::Uint16)rand();
	m_uiSalt = ((sf::Uint64)ui16Rand4 << 48) | ((sf::Uint64)ui16Rand3 << 32) | ((sf::Uint64)ui16Rand2 << 16) | (sf::Uint64)ui16Rand1;

	std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
	(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionRequest;
	(*pPacket) << PROTOCOL_SIGNATURE_UDP_CONNECTION;
	(*pPacket) << CURRENT_PROTOCOL_VERSION_UDP_CONNECTION;
	(*pPacket) << m_uiSalt;
	m_lstOut.push_back(std::move(pPacket));

	m_eState = eStates_WaitingConnectionResponse;
	m_clkLastStateChanging.restart();

	return status;
}

void CUdpOutConnection::Disconnect()
{
	if ((m_eState == eStates_WaitingDisconnectDoneResponse) ||
		(m_eState == eStates_Disconnecting) ||
		(m_eState == eStates_Disconnected))
	{
		return;
	}

	m_lstOut.clear();
	m_lstInNested.clear();

	if (m_eState == eStates_WaitingConnectionResponse)
	{
		DisconnectionClear();
		m_eDisconnectionStatus = sf::UdpSocket::Status::Done;
		m_sDisconnectionStatus.clear();
	}
	else
	{
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		(*pPacket) << (sf::Uint8)eUdpConnPacketType_Disconnect;
		(*pPacket) << m_uiConnectionId;
		m_lstOut.push_back(std::move(pPacket));

		m_eState = eStates_WaitingDisconnectDoneResponse;
		m_clkLastStateChanging.restart();

		m_clkDisconnect.restart();
	}
}

sf::UdpSocket::Status CUdpOutConnection::Send(sf::Packet& rPacket)
{
	if (m_eState != eStates_Connected)
	{
		if (m_eState == eStates_Disconnected)
			return sf::UdpSocket::Status::Disconnected;
		return sf::UdpSocket::Status::NotReady;
	}

	if (rPacket.getDataSize() > sf::UdpSocket::MaxDatagramSize)
		return sf::UdpSocket::Status::Error;

	std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
	(*pPacket) << (sf::Uint8)eUdpConnPacketType_NonCriticalPacket;
	(*pPacket) << m_uiConnectionId;
	pPacket->append(rPacket.getData(), rPacket.getDataSize());
	rPacket.clear();

	m_lstOut.push_back(std::move(pPacket));

	return sf::UdpSocket::Status::Done;
}

sf::UdpSocket::Status CUdpOutConnection::SendCritical(sf::Packet& rPacket)
{
	if (m_eState != eStates_Connected)
	{
		if (m_eState == eStates_Disconnected)
			return sf::UdpSocket::Status::Disconnected;
		return sf::UdpSocket::Status::NotReady;
	}

	if ((rPacket.getDataSize() + 64) > sf::UdpSocket::MaxDatagramSize)
		return sf::UdpSocket::Status::Error;

	sf::Uint32 uiPacketId = ++m_uiPacketIdCounter;

	std::unique_ptr<sf::Packet> pCriticalPacket(new sf::Packet());
	(*pCriticalPacket) << (sf::Uint8)eUdpConnPacketType_CriticalPacket;
	(*pCriticalPacket) << m_uiConnectionId;
	(*pCriticalPacket) << uiPacketId;
	pCriticalPacket->append(rPacket.getData(), rPacket.getDataSize());
	rPacket.clear();

	if (m_mapOutCritical.empty()) // To avoid LostConnectionError
		m_clkLastPacketReceiving.restart();

	m_mapOutCritical[uiPacketId] = CriticalPacket(uiPacketId, (*pCriticalPacket));
	m_lstOut.push_back(std::move(pCriticalPacket));

	return sf::UdpSocket::Status::Done;
}

sf::UdpSocket::Status CUdpOutConnection::Receive(sf::Packet& rPacket)
{
	if (m_lstInNested.empty())
		return sf::UdpSocket::Status::NotReady;
	
	rPacket = std::move((*m_lstInNested.front()));
	m_lstInNested.pop_front();

	return sf::UdpSocket::Status::Done;
}

bool CUdpOutConnection::Process()
{
	if (m_eState == eStates_Disconnected)
		return false;

	bool bSomethingHappened = false;


	std::unique_ptr<sf::Packet> pInPacket(new sf::Packet());
	sf::IpAddress address;
	sf::Uint16 nPort = 0;
	sf::UdpSocket::Status status = m_Socket.receive((*pInPacket), address, nPort);
	if (status == sf::UdpSocket::Status::Error)
	{
		Clear();
		m_eDisconnectionStatus = status;
		m_sDisconnectionStatus = L"Failed to receive a packet.";
		return false;
	}
	if (status == sf::UdpSocket::Status::Done)
	{
		bSomethingHappened = true;
		if ((address != m_ServerIpAddress) || (nPort != m_nServerPort))
		{
			// Ignore this packet
			pInPacket->clear();
		}
	}


	if (pInPacket->getData())
	{
		m_clkLastPacketReceiving.restart();

		sf::Uint8 uiType = 0; (*pInPacket) >> uiType;

		switch (uiType)
		{
		case eUdpConnPacketType_ConnectionResponse:
		{
			if (m_eState != eStates_WaitingConnectionResponse)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiSalt || (ui64Data != m_uiSalt))
			{
				// Ignore this packet
				break;
			}
			ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data) // If not a server CURRENT_PROTOCOL_VERSION_UDP_CONNECTION
			{
				Clear();
				m_eDisconnectionStatus = sf::UdpSocket::Status::Disconnected;
				m_sDisconnectionStatus = L"Server rejected the connection due an invalid protocol version.";
				return true;
			}
			m_uiConnectionId = m_uiSalt & ui64Data;

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionOk;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));

			m_eState = eStates_WaitingConnectionDoneResponse;
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_ConnectionDone:
		{
			if (m_eState != eStates_WaitingConnectionDoneResponse)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}

			m_eState = eStates_Connected;
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_Disconnect:
		{
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_DisconnectDone;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));

			m_eState = eStates_Disconnecting;
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;

			m_clkDisconnect.restart();
		}
		break;

		case eUdpConnPacketType_DisconnectDone:
		{
			if (m_eState != eStates_WaitingDisconnectDoneResponse)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			//m_eState = eStates_Disconnected;
			DisconnectionClear();
			m_eDisconnectionStatus = sf::UdpSocket::Status::Done;
			m_sDisconnectionStatus.clear();
			//bSomethingHappened = true;
			return false;
		}
		break;

		case eUdpConnPacketType_NonCriticalPacket:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			m_lstInNested.push_back(std::move(pInPacket));
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_CriticalPacket:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			sf::Uint32 uiPacketId = 0; (*pInPacket) >> uiPacketId;
			if (!uiPacketId)
			{
				// Ignore this packet
				break;
			}

			if (!m_mapInfoCritical.count(uiPacketId))
			{
				m_mapInfoCritical[uiPacketId] = CriticalPacketInfo(uiPacketId);
				m_lstInNested.push_back(std::move(pInPacket));
			}
			m_mapInfoCritical.at(uiPacketId).clock.restart();

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_CriticalPacketACK;
			(*pPacket) << m_uiConnectionId;
			(*pPacket) << uiPacketId;
			m_lstOut.push_back(std::move(pPacket));
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_CriticalPacketACK:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pInPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			sf::Uint32 uiPacketId = 0; (*pInPacket) >> uiPacketId;
			if (!uiPacketId)
			{
				// Ignore this packet
				break;
			}
			// Confirmed
			m_mapOutCritical.erase(uiPacketId);
		}
		break;
		}
	}


	for (auto& out : m_mapOutCritical)
	{
		if (out.second.clock.getElapsedTime().asMilliseconds() < CriticalPacket::repeatInterval_ms)
			continue;
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet(out.second.packet));
		m_lstOut.push_back(std::move(pPacket));
		out.second.clock.restart();
	}
	std::map<sf::Uint32, CriticalPacketInfo>::iterator itInfo = m_mapInfoCritical.begin();
	while (itInfo != m_mapInfoCritical.end())
	{
		if (itInfo->second.clock.getElapsedTime().asSeconds() < CriticalPacketInfo::deletingInterval_s)
		{
			++itInfo;
			continue;
		}
		itInfo = m_mapInfoCritical.erase(itInfo);
	}


	if (m_clkLastPacketReceiving.getElapsedTime().asSeconds() > lostInterval_s)
	{
		if ((m_eState != eStates_Connected) || !m_mapOutCritical.empty())
		{
			DisconnectionClear();
			m_eDisconnectionStatus = sf::UdpSocket::Status::Error;
			m_sDisconnectionStatus = L"Connection lost.";
			return false;
		}
	}

	if (m_clkDisconnect.getElapsedTime().asMilliseconds() > disconnectingInterval_ms)
	{
		if (m_eState == eStates_WaitingDisconnectDoneResponse)
		{
			DisconnectionClear();
			m_eDisconnectionStatus = sf::UdpSocket::Status::Error;
			m_sDisconnectionStatus = L"Disconnection timeout.";
			return false;
		}
		else if (m_eState == eStates_Disconnecting)
		{
			DisconnectionClear();
			m_eDisconnectionStatus = sf::UdpSocket::Status::Disconnected;
			m_sDisconnectionStatus = L"Server closed the connection.";
			return false;
		}
	}
	
	if (m_clkLastStateChanging.getElapsedTime().asMilliseconds() > repeatInterval_ms)
	{
		if (m_eState == eStates_WaitingConnectionResponse)
		{
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionRequest;
			(*pPacket) << PROTOCOL_SIGNATURE_UDP_CONNECTION;
			(*pPacket) << CURRENT_PROTOCOL_VERSION_UDP_CONNECTION;
			(*pPacket) << m_uiSalt;
			m_lstOut.push_back(std::move(pPacket));
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
		else if (m_eState == eStates_WaitingConnectionDoneResponse)
		{
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionOk;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
		else if (m_eState == eStates_WaitingDisconnectDoneResponse)
		{
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_Disconnect;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
	}


	if (!m_lstOut.empty())
	{
		std::unique_ptr<sf::Packet> pPacket = std::move(m_lstOut.front());
		m_lstOut.pop_front();

		if (pPacket->getDataSize() > sf::UdpSocket::MaxDatagramSize)
		{
			Disconnect();
			m_eDisconnectionStatus = sf::UdpSocket::Status::Error;
			m_sDisconnectionStatus = L"Packet size more than MaxDatagramSize.";
		}
		else
		{
			sf::UdpSocket::Status status = m_Socket.send((*pPacket), m_ServerIpAddress, m_nServerPort);
			if (status == sf::UdpSocket::Status::Error)
			{
				Clear();
				m_eDisconnectionStatus = status;
				m_sDisconnectionStatus = L"Failed to send a packet.";
				return false;
			}
		}
		bSomethingHappened = true;
	}


	return bSomethingHappened;
}
