#pragma once
#include <functional>
#include "WContainer.h"

class CWText;
class CWButton;
class CWEditBox;

class CWPlayDlg :
	public CWContainer
{
public:
	CWPlayDlg(CScene& rScene);
	virtual ~CWPlayDlg();

	void SetOnPlayHandler(const std::function<void(const std::wstring& rsPlayerName, sf::Uint16 uiPlayersCount)>& rHandler) { m_OnPlayHandler = rHandler; }
	void SetOnDisconnectHandler(const std::function<void(void)>& rHandler) { m_OnDisconnectHandler = rHandler; }

	virtual void NormalizeSize();

	virtual void Draw();

private:
	CWText* m_pTextDescription;
	CWEditBox* m_pEditPlayerName;
	CWEditBox* m_pEditDesiredPlayersCount;
	CWText* m_pTextPlayerName;
	CWText* m_pTextDesiredPlayersCount;
	CWButton* m_pButtonPlay;
	CWButton* m_pButtonDisconnect;
	std::function<void(const std::wstring& rsServerAddress, sf::Uint16 uiServerPort)> m_OnPlayHandler;
	std::function<void(void)> m_OnDisconnectHandler;

private:
	void CallOnPlayHandler();
};
