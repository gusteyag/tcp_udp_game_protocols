#pragma once
#include <Windows.h>


enum ePacketType
{
	ePacketType_Play = 1,       // Critical packet
	// Sent by client:
	// 1 byte	- sf::Uint8	    - ePacketType
	// ???      - wstring       - PlayerName
	// 1 bytes  - sf::Uint8     - DesiredPlayersCount
	// Answer from server:
	// 1 byte	- sf::Uint8	    - ePacketType
	// 1 byte	- sf::Uint8	    - eResult
	//   if eResult_Success:
	// 4 bytes  - sf::Uint32    - PlayerId
	// ???      ---------       - GameInfo

	ePacketType_Quit,           // Critical packet
	// Sent by client:
	// 1 byte	- sf::Uint8	    - ePacketType
	// Answer from server:
	// 1 byte	- sf::Uint8	    - ePacketType
	// 1 byte	- sf::Uint8	    - eResult

	ePacketType_PlayerShot, // Critical packet
	// Sent by server:
	// 1 byte	- sf::Uint8	    - ePacketType
	// 1 byte	- sf::Uint8	    - ePlayerShotDir

	ePacketType_UpdateGameState, // Critical packet
	// Sent by server:
	// 1 byte	- sf::Uint8	    - ePacketType
	// ???      ---------       - GameState

	ePacketType_PlayerMotion, // Noncritical packet
	// Sent by client:
	// 1 byte	- sf::Uint8	    - ePacketType
	// ???      ---------       - MotionUnit
};
