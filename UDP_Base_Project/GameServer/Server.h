#pragma once
#include <windows.h>
#include <memory>
#include <list>
#include <condition_variable>
#include <SFML\Network.hpp>
#include "UdpInConnection.h"


class CGame;

class CServer
{
public:
	CServer(const std::string& rsAddress, unsigned short nPort);
	virtual ~CServer();

	void Run();
	void Stop();

	void SetPacketLossProbability(float fProbability) { m_fPacketLossProbability = fProbability; }

private:
	bool m_bStop;
	std::string m_sAddress;
	unsigned short m_nPort;
	sf::UdpSocket m_Socket;
	std::condition_variable m_cvWaitStopping;
	std::mutex m_mtxWaitStopping;
	static sf::Uint32 m_nClientIdCounter;

	float m_fPacketLossProbability;


	struct Client
	{
		sf::Uint32 nId;
		std::wstring sName;
		sf::Uint32 nDesiredPlayersCount;
		sf::Uint32 nGameId;
		sf::Uint32 nPlayerId;
		CUdpInConnection m_Connection;

		Client(sf::Uint32 nId, const sf::IpAddress& rClientAddress, USHORT nClientPort)
			: nId(nId)
			, m_Connection(rClientAddress, nClientPort)
			, nDesiredPlayersCount(0)
			, nGameId(0)
			, nPlayerId(0)
		{}
	};

	std::map<sf::Uint32, std::unique_ptr<Client> > m_mapClients;

	static sf::Uint32 m_nGameIdCounter;
	std::map<sf::Uint32, std::unique_ptr<CGame> > m_mapGames;

private:
	bool ProcessProtocol(Client& rClient);
	void RemoveDisconnectedClientFromGame(sf::Uint32 nGameId, sf::Uint32 nPlayerId);

	static const sf::Uint32 matchmakingDistanceMin = 2;
	static const sf::Uint32 matchmakingDistanceMax = L'z' - L'a';
	static const sf::Uint32 matchmakingTimeout_ms = 5000;
	sf::Clock m_clkMatchMaking;
	void MatchMaking();
	bool CheckMatchMakingDistance(const std::wstring& rsName1, const std::wstring& rsName2, sf::Uint32 nDistance);
	static const sf::Uint32 m_nServerProcessinTimeout_ms = 300;
	sf::Clock m_clkServerProcessing;
};
