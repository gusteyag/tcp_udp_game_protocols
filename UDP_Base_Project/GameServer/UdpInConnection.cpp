#include "UdpInConnection.h"
#include "Logger.h"


CUdpInConnection::CUdpInConnection(const sf::IpAddress& rClientAddress, USHORT nClientPort)
	: m_ClientIpAddress(rClientAddress)
	, m_nClientPort(nClientPort)
	, m_uiSalt(0)
	, m_uiConnectionId(0)
	, m_eState(eStates_WaitingConnectionRequest)
	, m_uiPacketIdCounter(0)
{
	sf::Uint16 ui16Rand1 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand2 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand3 = (sf::Uint16)rand();
	sf::Uint16 ui16Rand4 = (sf::Uint16)rand();
	m_uiSalt = ((sf::Uint64)ui16Rand4 << 48) | ((sf::Uint64)ui16Rand3 << 32) | ((sf::Uint64)ui16Rand2 << 16) | (sf::Uint64)ui16Rand1;
}

CUdpInConnection::~CUdpInConnection()
{
}

void CUdpInConnection::Clear()
{
	m_uiSalt = 0;
	m_uiConnectionId = 0;
	m_eState = eStates_Disconnected;
	m_uiPacketIdCounter = 0;

	m_clkDisconnect.restart();
	m_clkLastPacketReceiving.restart();
	m_clkLastStateChanging.restart();

	m_lstIn.clear();
	m_lstOut.clear();
	m_lstInNested.clear();

	m_mapOutCritical.clear();
	m_mapInfoCritical.clear();
}

void CUdpInConnection::DisconnectionClear()
{
	m_eState = eStates_Disconnected;

	m_clkDisconnect.restart();
	m_clkLastPacketReceiving.restart();
	m_clkLastStateChanging.restart();

	m_lstOut.clear();

	m_mapOutCritical.clear();
	m_mapInfoCritical.clear();
}

void CUdpInConnection::Disconnect()
{
	if ((m_eState == eStates_WaitingDisconnectDoneResponse) ||
		(m_eState == eStates_Disconnecting) ||
		(m_eState == eStates_Disconnected))
	{
		return;
	}

	m_lstOut.clear();
	m_lstIn.clear();
	m_lstInNested.clear();

	std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
	(*pPacket) << (sf::Uint8)eUdpConnPacketType_Disconnect;
	(*pPacket) << m_uiConnectionId;
	m_lstOut.push_back(std::move(pPacket));

	m_eState = eStates_WaitingDisconnectDoneResponse;
	m_clkLastStateChanging.restart();

	m_clkDisconnect.restart();
}

void CUdpInConnection::PushInPacket(std::unique_ptr<sf::Packet>& pPacket)
{
	if (m_eState == eStates_Disconnected)
		return;

	m_lstIn.push_back(std::move(pPacket));
}

std::unique_ptr<sf::Packet> CUdpInConnection::PopOutPacket()
{
	std::unique_ptr<sf::Packet> pPacket;
	if (!m_lstOut.empty())
	{
		pPacket = std::move(m_lstOut.front());
		m_lstOut.pop_front();
	}
	return pPacket;
}

bool CUdpInConnection::Send(sf::Packet& rPacket)
{
	if (m_eState != eStates_Connected)
		return false;

	if (rPacket.getDataSize() > sf::UdpSocket::MaxDatagramSize)
		return false;

	std::unique_ptr<sf::Packet> pNewPacket(new sf::Packet());
	(*pNewPacket) << (sf::Uint8)eUdpConnPacketType_NonCriticalPacket;
	(*pNewPacket) << m_uiConnectionId;
	pNewPacket->append(rPacket.getData(), rPacket.getDataSize());
	rPacket.clear();

	m_lstOut.push_back(std::move(pNewPacket));

	return true;
}

bool CUdpInConnection::SendCritical(sf::Packet& rPacket)
{
	if (m_eState != eStates_Connected)
		return false;

	if ((rPacket.getDataSize() + 64) > sf::UdpSocket::MaxDatagramSize)
		return false;

	sf::Uint32 uiPacketId = ++m_uiPacketIdCounter;

	std::unique_ptr<sf::Packet> pCriticalPacket(new sf::Packet());
	(*pCriticalPacket) << (sf::Uint8)eUdpConnPacketType_CriticalPacket;
	(*pCriticalPacket) << m_uiConnectionId;
	(*pCriticalPacket) << uiPacketId;
	pCriticalPacket->append(rPacket.getData(), rPacket.getDataSize());
	rPacket.clear();

	if (m_mapOutCritical.empty()) // To avoid LostConnectionError
		m_clkLastPacketReceiving.restart();

	m_mapOutCritical[uiPacketId] = CriticalPacket(uiPacketId, (*pCriticalPacket));
	m_lstOut.push_back(std::move(pCriticalPacket));

	return true;
}

std::unique_ptr<sf::Packet> CUdpInConnection::Receive()
{
	std::unique_ptr<sf::Packet> pPacket;
	if (!m_lstInNested.empty())
	{
		pPacket = std::move(m_lstInNested.front());
		m_lstInNested.pop_front();
	}
	return pPacket;
}

bool CUdpInConnection::Process()
{
	if (m_eState == eStates_Disconnected)
		return false;

	bool bSomethingHappened = false;

	std::unique_ptr<sf::Packet> pPacket;
	if (!m_lstIn.empty())
	{
		pPacket = std::move(m_lstIn.front());
		m_lstIn.pop_front();
		m_clkLastPacketReceiving.restart();
	}
	
	if (pPacket.get() && pPacket->getData())
	{
		sf::Uint8 uiType = 0; (*pPacket) >> uiType;

		switch (uiType)
		{
		case eUdpConnPacketType_ConnectionRequest:
		{
			bool bInvalidProtocolVersion = false;
			if ((m_eState != eStates_WaitingConnectionRequest) && (m_eState != eStates_WaitingConnectionOk))
			{
				// Ignore this packet
				break;
			}
			sf::Uint32 ui32Data = 0; (*pPacket) >> ui32Data;
			if (ui32Data != PROTOCOL_SIGNATURE_UDP_CONNECTION)
			{
				// Ignore this packet
				break;
			}
			(*pPacket) >> ui32Data;
			if (ui32Data != CURRENT_PROTOCOL_VERSION_UDP_CONNECTION)
			{
				bInvalidProtocolVersion = true;
			}
			sf::Uint64 ui64ClientSalt = 0; (*pPacket) >> ui64ClientSalt;
			if (!ui64ClientSalt)
			{
				// Ignore this packet
				break;
			}
			if (!bInvalidProtocolVersion)
				m_uiConnectionId = m_uiSalt & ui64ClientSalt;

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionResponse;
			(*pPacket) << ui64ClientSalt;
			(*pPacket) << (sf::Uint64)(bInvalidProtocolVersion ? 0 : m_uiSalt);
			m_lstOut.push_back(std::move(pPacket));

			if (bInvalidProtocolVersion)
			{
				//m_eState = eStates_Disconnected;
				Clear();
			}
			else
			{
				m_eState = eStates_WaitingConnectionOk;
				m_clkLastStateChanging.restart();
			}
			bSomethingHappened = true;

			log_debug("Connection request. Id=%I64u", m_uiConnectionId);
		}
		break;

		case eUdpConnPacketType_ConnectionOk:
		{
			if ((m_eState != eStates_WaitingConnectionOk) && (m_eState != eStates_Connected))
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_ConnectionDone;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));

			m_eState = eStates_Connected;
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;

			log_info("Connected. Id=%I64u", m_uiConnectionId);
		}
		break;

		case eUdpConnPacketType_Disconnect:
		{
			if (m_eState == eStates_WaitingConnectionRequest)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_DisconnectDone;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));

			m_eState = eStates_Disconnecting;
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;

			m_clkDisconnect.restart();

			log_debug("Disconnect request. Id=%I64u", m_uiConnectionId);
		}
		break;

		case eUdpConnPacketType_DisconnectDone:
		{
			if (m_eState != eStates_WaitingDisconnectDoneResponse)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			//m_eState = eStates_Disconnected;
			DisconnectionClear();
			log_info("Disconnected. Id=%I64u", m_uiConnectionId);
			bSomethingHappened = true;
			return false;
		}
		break;

		case eUdpConnPacketType_NonCriticalPacket:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			m_lstInNested.push_back(std::move(pPacket));
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_CriticalPacket:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			sf::Uint32 uiPacketId = 0; (*pPacket) >> uiPacketId;
			if (!uiPacketId)
			{
				// Ignore this packet
				break;
			}

			if (!m_mapInfoCritical.count(uiPacketId))
			{
				m_mapInfoCritical[uiPacketId] = CriticalPacketInfo(uiPacketId);
				m_lstInNested.push_back(std::move(pPacket));
			}
			m_mapInfoCritical.at(uiPacketId).clock.restart();

			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_CriticalPacketACK;
			(*pPacket) << m_uiConnectionId;
			(*pPacket) << uiPacketId;
			m_lstOut.push_back(std::move(pPacket));
			bSomethingHappened = true;
		}
		break;

		case eUdpConnPacketType_CriticalPacketACK:
		{
			if (m_eState != eStates_Connected)
			{
				// Ignore this packet
				break;
			}
			sf::Uint64 ui64Data = 0; (*pPacket) >> ui64Data;
			if (!ui64Data || !m_uiConnectionId || (ui64Data != m_uiConnectionId))
			{
				// Ignore this packet
				break;
			}
			sf::Uint32 uiPacketId = 0; (*pPacket) >> uiPacketId;
			if (!uiPacketId)
			{
				// Ignore this packet
				break;
			}
			// Confirmed
			m_mapOutCritical.erase(uiPacketId);
		}
		break;
		}
	}


	for (auto& out : m_mapOutCritical)
	{
		if (out.second.clock.getElapsedTime().asMilliseconds() < CriticalPacket::repeatInterval_ms)
			continue;
		std::unique_ptr<sf::Packet> pPacket(new sf::Packet(out.second.packet));
		m_lstOut.push_back(std::move(pPacket));
		out.second.clock.restart();
	}
	std::map<sf::Uint32, CriticalPacketInfo>::iterator itInfo = m_mapInfoCritical.begin();
	while (itInfo != m_mapInfoCritical.end())
	{
		if (itInfo->second.clock.getElapsedTime().asSeconds() < CriticalPacketInfo::deletingInterval_s)
		{
			++itInfo;
			continue;
		}
		itInfo = m_mapInfoCritical.erase(itInfo);
	}


	if (m_clkLastPacketReceiving.getElapsedTime().asSeconds() > lostInterval_s)
	{
		if ((m_eState != eStates_Connected) || !m_mapOutCritical.empty())
		{
			DisconnectionClear();
			log_error("Connection lost. Id=%I64u", m_uiConnectionId);
			return false;
		}
	}

	if (m_clkDisconnect.getElapsedTime().asMilliseconds() > disconnectingInterval_ms)
	{
		if (m_eState == eStates_WaitingDisconnectDoneResponse)
		{
			DisconnectionClear();
			log_warning("Disconnection timeout. Id=%I64u", m_uiConnectionId);
			return false;
		}
		else if (m_eState == eStates_Disconnecting)
		{
			DisconnectionClear();
			log_info("Disconnected. Id=%I64u", m_uiConnectionId);
			return false;
		}
	}
	
	if (m_clkLastStateChanging.getElapsedTime().asMilliseconds() > repeatInterval_ms)
	{
		if (m_eState == eStates_WaitingDisconnectDoneResponse)
		{
			std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
			(*pPacket) << (sf::Uint8)eUdpConnPacketType_Disconnect;
			(*pPacket) << m_uiConnectionId;
			m_lstOut.push_back(std::move(pPacket));
			m_clkLastStateChanging.restart();
			bSomethingHappened = true;
		}
	}


	return bSomethingHappened;
}
