#pragma once
#include <Windows.h>
#include <list>
#include <memory>
#include <string>
#include <functional>
#include <SFML\Network.hpp>
#include "UdpConnectionProtocol.h"


class CUdpInConnection
{
public:
	CUdpInConnection(const sf::IpAddress& rClientAddress, USHORT nClientPort);
	virtual ~CUdpInConnection();

	const sf::IpAddress& GetIpAddress() const { return m_ClientIpAddress; }
	const sf::Uint16 GetPort() const { return m_nClientPort; }

	bool IsConnected() const { return (m_eState == eStates_Connected); }
	bool IsDisconnected() const { return (m_eState == eStates_Disconnected); }

	sf::Uint64 GetConnectionId() const { return m_uiConnectionId; }
	bool HasPacketsToReceive() const { return !m_lstInNested.empty(); }

	void Clear();

	void Disconnect();

	void PushInPacket(std::unique_ptr<sf::Packet>& pPacket);
	std::unique_ptr<sf::Packet> PopOutPacket();

	bool Send(sf::Packet& rPacket);
	bool SendCritical(sf::Packet& rPacket);
	std::unique_ptr<sf::Packet> Receive();

	// Returns true if something happened.
	bool Process();

private:
	enum eStates
	{
		eStates_Disconnected,

		eStates_WaitingConnectionRequest,
		eStates_WaitingConnectionOk,
		eStates_Connected,

		eStates_WaitingDisconnectDoneResponse,
		eStates_Disconnecting
	};

	struct CriticalPacket
	{
		sf::Uint32 id;
		sf::Packet packet;

		sf::Clock clock;
		static const sf::Int32 repeatInterval_ms = 200;

		CriticalPacket()
			: id(0)
		{}
		CriticalPacket(sf::Uint32 id, const sf::Packet& rPacket)
			: id(id)
			, packet(rPacket)
		{}
	};

	struct CriticalPacketInfo
	{
		sf::Uint32 id;

		sf::Clock clock;
		static const sf::Int32 deletingInterval_s = 30;

		CriticalPacketInfo()
			: id(0)
		{}
		CriticalPacketInfo(sf::Uint32 id)
			: id(id)
		{}
	};

	void DisconnectionClear();

private:
	sf::IpAddress m_ClientIpAddress;
	USHORT m_nClientPort;

	sf::Uint64 m_uiSalt;
	sf::Uint64 m_uiConnectionId;
	eStates m_eState;
	sf::Uint32 m_uiPacketIdCounter;

	sf::Clock m_clkDisconnect;
	sf::Clock m_clkLastPacketReceiving;
	sf::Clock m_clkLastStateChanging;
	static const sf::Int32 repeatInterval_ms = 50;
	static const sf::Int32 lostInterval_s = 30;
	static const sf::Int32 disconnectingInterval_ms = 500;

	std::list<std::unique_ptr<sf::Packet> > m_lstIn;
	std::list<std::unique_ptr<sf::Packet> > m_lstOut;
	std::list<std::unique_ptr<sf::Packet> > m_lstInNested;

	std::map<sf::Uint32, CriticalPacket> m_mapOutCritical;
	std::map<sf::Uint32, CriticalPacketInfo> m_mapInfoCritical;
};
