#pragma once


#define PROTOCOL_SIGNATURE_UDP_CONNECTION				((sf::Uint32)0xACDCACDC) // 1.0
#define CURRENT_PROTOCOL_VERSION_UDP_CONNECTION			((sf::Uint32)0x00010000) // 1.0

enum eUdpConnPacketType
{
	eUdpConnPacketType_ConnectionRequest = 1,
	// Sent by client:
	// 1 byte	- sf::Uint8	    - ePacketType
	// 4 bytes  - sf::Uint32    - Signature
	// 4 bytes  - sf::Uint32    - Version
	// 8 bytes  - sf::Uint64    - ClientSalt

	eUdpConnPacketType_ConnectionResponse,
	// Sent by server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes  - sf::Uint64    - ClientSalt
	// 8 bytes	- sf::Uint64	- ServerSalt or 0 if invalid Protocol Version

	eUdpConnPacketType_ConnectionOk,
	// Sent by client:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt

	eUdpConnPacketType_ConnectionDone,
	// Sent by server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt

	eUdpConnPacketType_Disconnect,
	// Sent by client or server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt

	eUdpConnPacketType_DisconnectDone,
	// Sent by client or server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt

	eUdpConnPacketType_NonCriticalPacket,
	// Sent by client or server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt
	// ???	    ------------	- nested packet

	eUdpConnPacketType_CriticalPacket,
	// Sent by client or server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt
	// 4 bytes	- sf::Uint32	- PacketId
	// ???	    ------------	- nested packet

	eUdpConnPacketType_CriticalPacketACK,
	// Sent by client or server:
	// 1 bytes	- sf::Uint8	    - ePacketType
	// 8 bytes	- sf::Uint64	- ServerSalt & ClientSalt
	// 4 bytes	- sf::Uint32	- PacketId
};
