#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <map>
#include "PlayerInfo.h"
#include "GameDefinitions.h"
#include "MotionUnit.h"
#include "Shot.h"


class CGame
{
public:
	CGame(sf::Uint32 nId)
		: m_nId(nId)
	{}
	virtual ~CGame() {}

	sf::Uint32 GetId() const { return m_nId; }

	const std::map<sf::Uint32, CPlayerInfo>& GetPlayers() const { return m_mapPlayers; }
	const std::map<sf::Uint32, CShot>& GetShots() const { return m_mapShots; }

	sf::Uint32 AddPlayer(const std::wstring& rsName);

	void RemovePlayer(sf::Uint32 nPlayerId)
	{
		if (!m_mapPlayers.count(nPlayerId))
			return;
		m_mapPlayers.erase(nPlayerId);
	}

	void PlayerShot(sf::Uint32 nPlayerId, ePlayerShotDir eDirection)
	{
		if (!m_mapPlayers.count(nPlayerId))
			return;
		CShot shot = CShot(++m_nObjectsIdCounter, nPlayerId, eDirection);
		shot.SetSize(sf::Vector2f(SHOT_WIDTH, SHOT_HEIGHT));
		shot.SetPos(sf::Vector2f(
			shot.GetPos().x + (m_mapPlayers.at(nPlayerId).GetPos().x + m_mapPlayers.at(nPlayerId).GetSize().x / 2) - (shot.GetPos().x + shot.GetSize().x / 2),
			shot.GetPos().y + (m_mapPlayers.at(nPlayerId).GetPos().y + m_mapPlayers.at(nPlayerId).GetSize().y / 2) - (shot.GetPos().y + shot.GetSize().y / 2)
			));
		shot.SetVelocity(SHOT_VELOCITY);
		m_mapShots[shot.GetObjectId()] = shot;
	}

	void PlayerMotion(sf::Uint32 nPlayerId, const MotionUnit& motion);

	void Process();

private:
	sf::FloatRect GetIntersect(const sf::FloatRect& rc1, const sf::FloatRect& rc2);

private:
	sf::Uint32 m_nId;
	std::map<sf::Uint32, CPlayerInfo> m_mapPlayers;
	std::map<sf::Uint32, CShot> m_mapShots;
	static sf::Uint32 m_nPlayersIdCounter;
	static sf::Uint32 m_nObjectsIdCounter;
};