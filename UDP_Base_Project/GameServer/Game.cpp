#include "Game.h"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

sf::Uint32 CGame::m_nPlayersIdCounter = 0;
sf::Uint32 CGame::m_nObjectsIdCounter = 0;

sf::Uint32 CGame::AddPlayer(const std::wstring& rsName)
{
	CPlayerInfo player = CPlayerInfo(++m_nObjectsIdCounter, ++m_nPlayersIdCounter, rsName);
	player.SetSize(sf::Vector2f(PLAYERS_WIDTH, PLAYERS_HEIGHT));
	bool bRepeat = true;
	while (bRepeat)
	{
		bRepeat = false;
		player.SetPos(sf::Vector2f((float)rand() / RAND_MAX, (float)rand() / RAND_MAX));
		if ((player.GetPos().x < 0 || (player.GetPos().x + player.GetSize().x) > 1.f) ||
			(player.GetPos().y < 0 || (player.GetPos().y + player.GetSize().y) > 1.f))
			bRepeat = true;
		for (auto& player1 : m_mapPlayers)
		{
			if (player1.second.GetPlayerId() == player.GetPlayerId())
				continue;
			if (sf::FloatRect(player1.second.GetPos().x, player1.second.GetPos().y, player1.second.GetSize().x, player1.second.GetSize().y).intersects(
				sf::FloatRect(player.GetPos().x, player.GetPos().y, player.GetSize().x, player.GetSize().y)
			))
			{
				bRepeat = true;
				break;
			}
		}
	}
	player.SetVelocity(PLAYERS_VELOCITY);
	m_mapPlayers[player.GetPlayerId()] = player;
	return player.GetPlayerId();
}

void CGame::PlayerMotion(sf::Uint32 nPlayerId, const MotionUnit& motion)
{
	if (!m_mapPlayers.count(nPlayerId))
		return;
	if (motion.id <= m_mapPlayers.at(nPlayerId).GetLastMotionId())
		return;

	auto& player = m_mapPlayers.at(nPlayerId);
	player.SetPos(player.GetPos() + motion.diff);
	player.SetLastMotionId(motion.id);

	sf::FloatRect intersect = GetIntersect(
		sf::FloatRect(player.GetPos().x, player.GetPos().y, player.GetSize().x, player.GetSize().y),
		sf::FloatRect(0, 0, 1, 1)
	);
	if (intersect.width < player.GetSize().x)
	{
		if (player.GetPos().x < 0)
			player.SetPos(sf::Vector2f(0, player.GetPos().y));
		if ((player.GetPos().x + player.GetSize().x) > 1)
			player.SetPos(sf::Vector2f((1 - player.GetSize().x), player.GetPos().y));
	}
	if (intersect.height < player.GetSize().y)
	{
		if (player.GetPos().y < 0)
			player.SetPos(sf::Vector2f(player.GetPos().x, 0));
		if ((player.GetPos().y + player.GetSize().y) > 1)
			player.SetPos(sf::Vector2f(player.GetPos().x, (1 - player.GetSize().y)));
	}

	for (auto& player2 : m_mapPlayers)
	{
		if (player2.second.GetPlayerId() == player.GetPlayerId())
			continue;

		sf::FloatRect rc1(player.GetPos().x, player.GetPos().y, player.GetSize().x, player.GetSize().y);
		sf::FloatRect rc2(player2.second.GetPos().x, player2.second.GetPos().y, player2.second.GetSize().x, player2.second.GetSize().y);
		if (rc1.intersects(rc2))
		{
			sf::FloatRect intersect = GetIntersect(rc1, rc2);
			if (intersect.width)
			{
				if (player.GetPos().x > player2.second.GetPos().x)
					player.SetPos(sf::Vector2f((player2.second.GetPos().x + player2.second.GetSize().x), player.GetPos().y));
				else
					player.SetPos(sf::Vector2f((player2.second.GetPos().x - player.GetSize().x), player.GetPos().y));
			}
			if (intersect.height)
			{
				if (player.GetPos().y > player2.second.GetPos().y)
					player.SetPos(sf::Vector2f(player.GetPos().x, (player2.second.GetPos().y + player2.second.GetSize().y)));
				else
					player.SetPos(sf::Vector2f(player.GetPos().x, (player2.second.GetPos().y - player.GetSize().y)));
			}
		}
	}
}

void CGame::Process()
{
	std::list<sf::Uint32> lstReachedShots;
	for (auto& shot : m_mapShots)
	{
		shot.second.CalculatePosition();

		if (!sf::FloatRect(0, 0, 1, 1).intersects(
			sf::FloatRect(shot.second.GetPos().x, shot.second.GetPos().y, shot.second.GetSize().x, shot.second.GetSize().y)
		))
		{
			lstReachedShots.push_back(shot.second.GetObjectId());
		}

		for (auto& player : m_mapPlayers)
		{
			if (player.second.GetPlayerId() == shot.second.GetPlayerId())
				continue;
			if (sf::FloatRect(player.second.GetPos().x, player.second.GetPos().y, player.second.GetSize().x, player.second.GetSize().y).intersects(
				sf::FloatRect(shot.second.GetPos().x, shot.second.GetPos().y, shot.second.GetSize().x, shot.second.GetSize().y)
			))
			{
				player.second.DecrementLives();
				lstReachedShots.push_back(shot.second.GetObjectId());
			}
		}
	}
	for (auto& rsh : lstReachedShots)
		m_mapShots.erase(rsh);
}

sf::FloatRect CGame::GetIntersect(const sf::FloatRect& rc1, const sf::FloatRect& rc2)
{
	float left = rc1.left;
	float top = rc1.top;
	float right = rc1.left + rc1.width;
	float bottom = rc1.top + rc1.height;
	if (left < rc2.left)
		left = rc2.left;
	if (left > rc2.left + rc2.width)
		left = rc2.left + rc2.width;
	if (right < rc2.left)
		right = rc2.left;
	if (right > rc2.left + rc2.width)
		right = rc2.left + rc2.width;
	if (top < rc2.top)
		top = rc2.top;
	if (top > rc2.top + rc2.height)
		top = rc2.top + rc2.height;
	if (bottom < rc2.top)
		bottom = rc2.top;
	if (bottom > rc2.top + rc2.height)
		bottom = rc2.top + rc2.height;
	return sf::FloatRect(left, top, right - left, bottom - top);
}
