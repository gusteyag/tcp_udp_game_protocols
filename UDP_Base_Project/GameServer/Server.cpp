#include "Server.h"
#include "Logger.h"
#include "Protocol.h"
#include "GameDefinitions.h"
#include "Game.h"
#include <GameInfo.h>
#include <MotionUnit.h>
#include <GameState.h>


sf::Uint32 CServer::m_nClientIdCounter = 0;
sf::Uint32 CServer::m_nGameIdCounter = 0;

CServer::CServer(const std::string& rsAddress, unsigned short nPort)
	: m_bStop(false)
	, m_sAddress(rsAddress)
	, m_nPort(nPort)
	, m_fPacketLossProbability(0.0f)
{
	m_Socket.setBlocking(false);

	if (m_Socket.bind(m_nPort, m_sAddress) != sf::UdpSocket::Done)
	{
		throw std::exception("Failed to call m_Socket.bind().");
	}
}

CServer::~CServer()
{
	Stop();
}

void CServer::Run()
{
	log_info("Server running. Address=\"%s\", Port=%u", m_sAddress.c_str(), (DWORD)m_nPort);

	srand((UINT)time(NULL));

	while (true)
	{
		if (m_clkMatchMaking.getElapsedTime().asMilliseconds() > matchmakingTimeout_ms)
		{
			m_clkMatchMaking.restart();
			MatchMaking();
		}

		bool bSomethingHappened = false;

		if (m_bStop)
		{
			for (auto& client : m_mapClients)
				client.second->m_Connection.Disconnect();
			if (m_mapClients.empty())
				break;
		}

		std::unique_ptr<sf::Packet> pPacket(new sf::Packet());
		sf::IpAddress address;
		sf::Uint16 port = 0;
		sf::UdpSocket::Status status = m_Socket.receive((*pPacket), address, port);
		if (status == sf::UdpSocket::Status::Error)
		{
			log_error("Failed to receive a packet.");
		}
		if (status == sf::UdpSocket::Done)
		{
			if ((int)(rand() / (RAND_MAX * (1.0f - m_fPacketLossProbability))))
			{
				log_debug("Input packet loss.");
			}
			else
			{
				bSomethingHappened = true;

				bool bFound = false;
				for (auto& client : m_mapClients)
				{
					if ((client.second->m_Connection.GetIpAddress() == address) &&
						(client.second->m_Connection.GetPort() == port))
					{
						bFound = true;
						client.second->m_Connection.PushInPacket(pPacket);
						break;
					}
				}
				if (!bFound && !m_bStop)
				{
					sf::Uint8 uiType = 0; (*pPacket) >> uiType;
					sf::Uint32 uiSignature = 0; (*pPacket) >> uiSignature;
					sf::Uint32 uiVersion = 0; (*pPacket) >> uiVersion;
					sf::Uint64 uiSalt = 0; (*pPacket) >> uiSalt;
					if ((uiType == eUdpConnPacketType_ConnectionRequest) &&
						(uiSignature == PROTOCOL_SIGNATURE_UDP_CONNECTION) &&
						uiSalt)
					{
						std::unique_ptr<Client> pNewClient(new Client(++m_nClientIdCounter, address, port));
						std::unique_ptr<sf::Packet> pPacketTemp(new sf::Packet());
						(*pPacketTemp) << uiType << uiSignature << uiVersion << uiSalt;
						pNewClient->m_Connection.PushInPacket(pPacketTemp);
						m_mapClients[pNewClient->nId] = std::move(pNewClient);
					}
				}
			}
		}

		if (m_clkServerProcessing.getElapsedTime().asMilliseconds() > m_nServerProcessinTimeout_ms)
		{
			m_clkServerProcessing.restart();
			bool __bSomethingHappened = false;
			do
			{
				__bSomethingHappened = false;
				std::map<sf::Uint32, std::unique_ptr<Client> >::iterator itClient = m_mapClients.begin();
				while (itClient != m_mapClients.end())
				{
					while (itClient->second->m_Connection.Process())
						bSomethingHappened = true;

					if (itClient->second->m_Connection.IsConnected() && itClient->second->m_Connection.HasPacketsToReceive())
					{
						bSomethingHappened = true;
						__bSomethingHappened = true;
						if (!ProcessProtocol(*itClient->second))
						{
							sf::Uint32 nGameId = itClient->second->nGameId;
							sf::Uint32 nPlayerId = itClient->second->nPlayerId;
							itClient->second->m_Connection.Disconnect();
							RemoveDisconnectedClientFromGame(nGameId, nPlayerId);
						}
					}

					++itClient;
				}

			} while (__bSomethingHappened);

			for (auto& game : m_mapGames)
			{
				game.second->Process();

				for (auto& client : m_mapClients)
				{
					if (client.second->nGameId != game.second->GetId())
						continue;
					sf::Packet packet;
					packet << (sf::Uint8)ePacketType_UpdateGameState;
					if (CGameState(*game.second).Serialize(packet))
					{
						//client.second->m_Connection.SendCritical(packet);
						client.second->m_Connection.Send(packet);
					}
					else
					{
						log_error("Failed to serialize GameState.");
					}
				}
			}
		}

		std::map<sf::Uint32, std::unique_ptr<Client> >::iterator itClient = m_mapClients.begin();
		while (itClient != m_mapClients.end())
		{
			while (itClient->second->m_Connection.Process())
				bSomethingHappened = true;

			if (itClient->second->m_Connection.IsDisconnected())
			{
				sf::Uint32 nGameId = itClient->second->nGameId;
				sf::Uint32 nPlayerId = itClient->second->nPlayerId;
				itClient = m_mapClients.erase(itClient);
				RemoveDisconnectedClientFromGame(nGameId, nPlayerId);
				continue;
			}

			//if (itClient->second->m_Connection.IsConnected() && itClient->second->m_Connection.HasPacketsToReceive())
			//{
			//	bSomethingHappened = true;
			//	if (!ProcessProtocol(*itClient->second))
			//	{
			//		itClient->second->m_Connection.Disconnect();
			//		//RemoveDisconnectedClientFromGame(dwGameId, dwPlayerId);
			//	}
			//}

			std::unique_ptr<sf::Packet> pPacket = itClient->second->m_Connection.PopOutPacket();
			if (pPacket && pPacket->getData())
			{
				if (pPacket->getDataSize() > sf::UdpSocket::MaxDatagramSize)
				{
					log_error("Packet size more than MaxDatagramSize.");
				}
				else
				{
					if ((int)(rand() / (RAND_MAX * (1.0f - m_fPacketLossProbability))))
					{
						log_debug("Output packet loss.");
					}
					else
					{
						sf::UdpSocket::Status status = m_Socket.send((*pPacket), itClient->second->m_Connection.GetIpAddress(), itClient->second->m_Connection.GetPort());
						if (status == sf::UdpSocket::Status::Error)
						{
							log_error("Failed to send a packet for client %u.", itClient->second->nId);
						}
					}
				}
			}

			++itClient;
		}

		if (!bSomethingHappened)
			Sleep(1); // To avoid CPU load
	}

	m_cvWaitStopping.notify_all();
	log_info("Server stopped.");
}

void CServer::Stop()
{
	std::unique_lock<std::mutex> lock(m_mtxWaitStopping);

	if (!m_bStop)
	{
		m_bStop = true;
		m_cvWaitStopping.wait(lock);
	}

	m_Socket.unbind();
	m_mapClients.clear();
}

bool CServer::ProcessProtocol(Client& rClient)
{
	std::unique_ptr<sf::Packet> pPacket = rClient.m_Connection.Receive();
	if (!pPacket.get() || !pPacket->getData())
		return true;

	
	sf::Uint8 nPacketType = 0; (*pPacket) >> nPacketType;
	switch (nPacketType)
	{
	case ePacketType_Play:
	{
		std::wstring sPlayerName; (*pPacket) >> sPlayerName;
		sf::Uint8 nDesiredPlayersCount = 0; (*pPacket) >> nDesiredPlayersCount;

		if (rClient.nGameId != 0)
		{
			eResult eRes = eResult_AlreadyExists;
			sf::Packet Packet;
			Packet << (sf::Uint8)ePacketType_Play;
			Packet << (sf::Uint8)eRes;
			if (eRes == eResult_Success)
			{
				Packet << (sf::Uint32)rClient.nPlayerId;
			}
		}
		else
		{
			rClient.sName = sPlayerName;
			rClient.nDesiredPlayersCount = nDesiredPlayersCount;
		}

		log_debug("ePacketType_Play processed. Id=%I64u", rClient.m_Connection.GetConnectionId());
	}
	break;

	case ePacketType_Quit:
	{
		eResult eRes = eResult_Fail;

		if (!rClient.nGameId)
		{
			eRes = eResult_PlayerNotInGame;
		}
		else
		{
			if (!m_mapGames.count(rClient.nGameId))
			{
				eRes = eResult_NotFound;
			}
			else
			{
				if (!m_mapGames.at(rClient.nGameId)->GetPlayers().count(rClient.nPlayerId))
				{
					eRes = eResult_NotFound;
				}
				else
				{
					m_mapGames.at(rClient.nGameId)->RemovePlayer(rClient.nPlayerId);
					if (!m_mapGames.at(rClient.nGameId)->GetPlayers().size())
						m_mapGames.erase(rClient.nGameId);

					for (auto& client : m_mapClients)
					{
						if (!m_mapGames.count(rClient.nGameId) || !m_mapGames.at(rClient.nGameId)->GetPlayers().count(client.second->nPlayerId))
							continue;
						if (client.second->nPlayerId == rClient.nPlayerId)
							continue;

						//sf::Packet Packet;
						//Packet << (sf::Uint8)ePacketType_PlayerLeftGame;
						//Packet << (sf::Uint32)rClient.nPlayerId;
						//client.second->m_Connection.SendCritical(Packet);
					}
					eRes = eResult_Success;
				}
			}
		}

		rClient.sName.clear();
		rClient.nDesiredPlayersCount = 0;
		rClient.nGameId = 0;
		rClient.nPlayerId = 0;

		sf::Packet Packet;
		Packet << (sf::Uint8)ePacketType_Quit;
		Packet << (sf::Uint8)eRes;
		rClient.m_Connection.SendCritical(Packet);

		log_debug("ePacketType_Quit processed. Id=%I64u", rClient.m_Connection.GetConnectionId());
	}
	break;

	case ePacketType_PlayerShot:
	{
		if (!m_mapGames.count(rClient.nGameId) || !m_mapGames.at(rClient.nGameId)->GetPlayers().count(rClient.nPlayerId))
			break;
		sf::Uint8 ui8Data = 0; (*pPacket) >> ui8Data;
		m_mapGames.at(rClient.nGameId)->PlayerShot(rClient.nPlayerId, (ePlayerShotDir)ui8Data);

		log_debug("ePacketType_PlayerShot processed. Id=%I64u", rClient.m_Connection.GetConnectionId());
	}
	break;

	case ePacketType_PlayerMotion:
	{
		if (!m_mapGames.count(rClient.nGameId) || !m_mapGames.at(rClient.nGameId)->GetPlayers().count(rClient.nPlayerId))
			break;
		MotionUnit motion;
		if (motion.Deserialize(*pPacket))
		{
			m_mapGames.at(rClient.nGameId)->PlayerMotion(rClient.nPlayerId, motion);
		}
		else
		{
			log_error("Failed to deserialize MotionUnit object. Id=%I64u", rClient.m_Connection.GetConnectionId());
		}
	}
	break;

	default:
	{
		log_error("Unknown packet type");
		return false;
	}
	break;
	}

	return true;
}

void CServer::MatchMaking()
{
	std::list<std::set<sf::Uint32> > lstResults;
	sf::Uint32 nMatchMakingDistance = matchmakingDistanceMin;
	for (; nMatchMakingDistance < matchmakingDistanceMax; ++nMatchMakingDistance)
	{
		std::map<sf::Uint32, std::set<sf::Uint32> > mapMatches;
		std::map<sf::Uint32, sf::Uint32> mapPlayersCount;
		for (auto& client1 : m_mapClients)
		{
			bool bFound = false;
			for (auto& res : lstResults)
			{
				if (res.count(client1.second->nId))
				{
					bFound = true;
					break;
				}
			}
			if (bFound || client1.second->nGameId || client1.second->sName.empty() || !client1.second->nDesiredPlayersCount)
				continue;
			mapPlayersCount[client1.second->nId] = client1.second->nDesiredPlayersCount;
			for (auto& client2 : m_mapClients)
			{
				if (client2.second->nId == client1.second->nId)
					continue;
				if (client2.second->nGameId || client2.second->sName.empty() || !client2.second->nDesiredPlayersCount)
					continue;
				if (client2.second->nDesiredPlayersCount != client1.second->nDesiredPlayersCount)
					continue;
				if (CheckMatchMakingDistance(client1.second->sName, client2.second->sName, nMatchMakingDistance))
					mapMatches[client1.second->nId].insert(client2.second->nId);
			}
		}

		if (mapMatches.empty())
			continue;

		while (true)
		{
			std::set<sf::Uint32> setRes;
			for (auto& player : mapMatches)
			{
				bool bMatch = true;
				for (auto& idRes : setRes)
				{
					if (!(mapMatches.count(player.first) && mapMatches.at(player.first).count(idRes)) ||
						!(mapMatches.count(idRes) && mapMatches.at(idRes).count(player.first)))
					{
						bMatch = false;
						break;
					}
				}
				if (!bMatch)
					continue;
				setRes.insert(player.first);
				if (setRes.size() && (setRes.size() == mapPlayersCount.at(player.first)))
					break;
			}
			for (auto& player : setRes)
			{
				mapMatches.erase(player);
				for (auto& player2 : mapMatches)
					player2.second.erase(player);
			}
			if (setRes.empty())
				break;
			lstResults.push_back(setRes);
		}

		if (mapMatches.empty())
			break;
	}

	for (auto& res : lstResults)
	{
		std::unique_ptr<CGame> pGame(new CGame(++m_nGameIdCounter));
		for (auto id : res)
			m_mapClients.at(id)->nPlayerId = pGame->AddPlayer(m_mapClients.at(id)->sName);
		sf::Packet GameInfoPacket;
		if (!CGameInfo(*pGame).Serialize(GameInfoPacket))
		{
			log_error("Failed to serialize GameInfo.");
			continue;
		}
		sf::Uint32 nGameId = pGame->GetId();
		m_mapGames[pGame->GetId()] = std::move(pGame);
		for (auto id : res)
		{
			m_mapClients.at(id)->nGameId = nGameId;

			eResult eRes = eResult_Success;
			sf::Packet Packet;
			Packet << (sf::Uint8)ePacketType_Play;
			Packet << (sf::Uint8)eRes;
			if (eRes == eResult_Success)
			{
				Packet << (sf::Uint32)m_mapClients.at(id)->nPlayerId;
				Packet.append(GameInfoPacket.getData(), GameInfoPacket.getDataSize());
			}
			m_mapClients.at(id)->m_Connection.SendCritical(Packet);
		}
	}
}

bool CServer::CheckMatchMakingDistance(const std::wstring& rsName1, const std::wstring& rsName2, sf::Uint32 nDistances)
{
	if (rsName1.empty() || rsName2.empty())
		return false;
	wchar_t ch1 = towlower(rsName1[0]);
	wchar_t ch2 = towlower(rsName2[0]);
	if (ch1 > ch2)
	{
		wchar_t temp = ch1;
		ch1 = ch2;
		ch2 = temp;
	}
	sf::Uint32 distance1 = ch2 - ch1;
	sf::Uint32 distance2 = (ch1 - L'a') + (L'z' + 1 - ch2);
	if (min(distance1, distance2) < nDistances)
		return true;
	return false;
}

void CServer::RemoveDisconnectedClientFromGame(sf::Uint32 nGameId, sf::Uint32 nPlayerId)
{
	if (!m_mapGames.count(nGameId))
		return;
	if (!m_mapGames.at(nGameId)->GetPlayers().count(nPlayerId))
		return;
	
	//sf::Packet GameStatePacket;
	//if (!CGameState(*m_mapGames.at(nGameId)).Serialize(GameStatePacket))
	//{
	//	log_error("Failed to serialize GameState.");
	//}

	m_mapGames.at(nGameId)->RemovePlayer(nPlayerId);
	if (!m_mapGames.at(nGameId)->GetPlayers().size())
		m_mapGames.erase(nGameId);

	//if (!GameStatePacket.getData())
	//	return;

	//for (auto& client : m_mapClients)
	//{
	//	if (!m_mapGames.at(nGameId)->GetPlayers().count(client.second->nPlayerId))
	//		continue;
	//	if (client.second->nPlayerId == nPlayerId)
	//		continue;

	//	sf::Packet Packet;
	//	Packet << (sf::Uint8)ePacketType_UpdateGameState;
	//	Packet.append(GameStatePacket.getData(), GameStatePacket.getDataSize());
	//	client.second->m_Connection.SendCritical(Packet);
	//}
}
